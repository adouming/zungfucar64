//
//  dump.cpp
//  ZungFuCar
//
//  Created by adouming on 15/6/10.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#include <stdio.h>
#include <string.h>
extern "C"{
    size_t fwrite$UNIX2003( const void *a, size_t b, size_t c, FILE *d )
    {
        return fwrite(a, b, c, d);
    }
    char* strerror$UNIX2003( int errnum )
    {
        return strerror(errnum);
    }
}