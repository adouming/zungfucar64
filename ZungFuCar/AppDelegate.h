//
//  AppDelegate.h
//  ZungFuCar
//
//  Created by lxm on 13-9-25.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WXApi.h"
#import "BMapKit.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, WXApiDelegate>
{
    BMKMapManager* _mapManager;
    NSTimer *_timer;
}

@property (strong, nonatomic) UIWindow *window;

@end
