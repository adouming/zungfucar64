//  KAJSONMapping.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.



////////////////////////// . Imports.
#import <Foundation/Foundation.h>
#import "AFNetworking.h"

////////////////////////// . Forward declaration.
@class KAServiceBase;
@class AFJSONRequestOperation;


typedef void(^KAOnServiceSuccessBlock)(KAServiceBase *, NSURLRequest *, NSHTTPURLResponse *, id);
typedef void(^KAOnServiceFailBlock)(KAServiceBase *, NSURLRequest *, NSHTTPURLResponse *, NSError *, id);


////////////////////////// . Protocol.
@protocol KAServiceBaseDelegate <NSObject>

/**
 * Callback when service finish.
 */
- (void)serviceDidSuccess:(KAServiceBase *)service;
- (void)serviceDidFail:(KAServiceBase *)service error:(NSError *)error;

//- (void)serviceTimeUp;

@end


////////////////////////// . Interface.
/**
 * KAServiceBase use for JSON API from network.
 * Provice for subclass, delegate, block callback way to use. 
 */
@interface KAServiceBase : NSObject {
@protected
	NSURLRequest											*_request;
	AFJSONRequestOperation									*_operation;
	
	KAOnServiceSuccessBlock									_onServiceSuccessBlock;
	KAOnServiceFailBlock									_onServiceFailBlock;
	
    __weak	id <KAServiceBaseDelegate>						_delegate;
}

@property(nonatomic, weak)id <KAServiceBaseDelegate> delegate;

@property(nonatomic, strong)NSURLRequest *request;
@property(nonatomic, strong)AFJSONRequestOperation *operation;
@property(nonatomic, readonly)BOOL isFinished;

/**
 * Block callback.
 */
@property(nonatomic, copy)KAOnServiceSuccessBlock onServiceSuccessBlock;
@property(nonatomic, copy)KAOnServiceFailBlock onServiceFailBlock;

/**
 * Initialize.
 */
+ (id)service;
+ (id)serviceWithRequest:(NSURLRequest *)request;

- (id)initWithRequest:(NSURLRequest *)request;

/** 
 * Start & cancel the service.
 */
- (void)start;
- (void)cancel;

/**
 * Event callback when success & fail.
 * methods to be override in subclasse.
 */
- (void)onSuccessWithRequest:(NSURLRequest *)request response:(NSHTTPURLResponse *)response JSON:(id)JSON;
- (void)onFailWithRequest:(NSURLRequest *)request response:(NSHTTPURLResponse *)response error:(NSError *)error JSON:(id)JSON;

@end
