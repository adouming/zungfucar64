
//
//  NSDictionary+SHUtil.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.

#import <Foundation/Foundation.h>

@interface NSDictionary (SHUtil)

- (id)objectForKey:(id)aKey forClass:(Class)class;

- (id)stringForKey:(id)aKey;
- (id)numberForKey:(id)aKey;
- (NSArray *)arrayForKey:(id)aKey;

@end
