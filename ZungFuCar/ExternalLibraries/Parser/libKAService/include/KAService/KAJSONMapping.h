//  KAJSONMapping.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.

#import <Foundation/Foundation.h>

@protocol KAJSONMapping <NSObject>

+ (NSDictionary *)map;






@end
