//  KAItemBase.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.

#import <Foundation/Foundation.h>

/**
 * KAItemBase is the base model class for view's model.
 * It provide the base method
 */
@interface KAItemBase : NSObject 

/**
 * user for strong reference.
 */ 
@property(nonatomic, strong)id strongRef;	

/**
 * user for weak reference.
 */
@property(nonatomic, unsafe_unretained)id weakRef;			

/**
 * user tag for mark something.
 */
@property(nonatomic)int tag;							


@end
                                                                                          