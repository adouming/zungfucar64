
//  KADataBase.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "NSDictionary+SHUtil.h"

@protocol KAJSONParser <NSObject>
@optional
+ (id)dataForTest;
+ (id)dataWithJSON:(id)JSON;
- (id)initWithJSON:(id)JSON;

@end

@interface KADataBase : NSObject <KAJSONParser>


@end
