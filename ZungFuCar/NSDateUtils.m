//
//  NSDateUtils.m
//  TarBar
//
//  Created by 刘 勇斌 on 13-4-15.
//  Copyright (c) 2013年 刘 勇斌. All rights reserved.
//

#import "NSDateUtils.h"
#import "NSStringUtil.h"

@implementation NSDateUtils
//日期转换字符串
+(NSString *)stringFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
}

+(NSString *)stringOnlyDateFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
}

//日期转换时分字符串
+(NSString *)stringFromTime:(NSDate *)date{
    //这样获取的hour都是24小时制的, 不管手机实际日期是否是用24小时显示.
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
	NSInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit |
	NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
	
	NSDateComponents *comps  = [calendar components:unitFlags fromDate:date];

	int hour = [comps hour];
	int min = [comps minute];
    NSString *timeString;
    if (hour<10 && min<10) {
        timeString = [NSString stringWithFormat:@"0%d:0%d", hour, min];
    }else if (hour<10){
        timeString = [NSString stringWithFormat:@"0%d:%d", hour, min];
    }else if (min<10){
        timeString = [NSString stringWithFormat:@"%d:0%d", hour, min];
    }else{
        timeString = [NSString stringWithFormat:@"%d:%d", hour, min];
    }
    return timeString;
}

//字符串转换时分
+(NSString *)timeFromString:(NSString *)time{
    NSArray *array = [time componentsSeparatedByString:@":"];
    if ([array count]<2) {
        return @"";
    }
	NSString *hour = [array objectAtIndex:0];
	NSString *min = [array objectAtIndex:1];
    NSString *timeString = [NSString stringWithFormat:@"%@:%@", hour, min];
    return timeString;
}

//字符串转换日期
+(NSDate *)dateFromString:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy-MM-dd"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
}

+(BOOL)date1LaterDate2:(NSDate *)date1 date2:(NSDate *)date2{
    if ([date1 timeIntervalSinceDate:date2]<0)
        return NO;
    return YES;
}

//获取当前年月yyyy-MM-dd格式
+(NSString *)getNowYMD{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd "];
    NSString *destDateString = [dateFormatter stringFromDate:[NSDate date]];
    return destDateString;
}

//获取当前年月yyyy-MM-dd格式
+(NSString *)getNowYMDHMS{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *destDateString = [dateFormatter stringFromDate:[NSDate date]];
    return destDateString;
}

//获取今天是周几// 星期几（注意，周日是“1”，周一是“2”。。。。）
+(NSInteger)getNowWeekDay{
    //这样获取的hour都是24小时制的, 不管手机实际日期是否是用24小时显示.
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit |
	NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
	NSDateComponents *comps  = [calendar components:unitFlags fromDate:[NSDate date]];
    return [comps weekday];
}

+(NSString *)getAge:(NSString *)dateString{
    if ([NSStringUtil isBlankString:dateString]) {
        return @"";
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy-MM-dd"];
    NSDate *birthDate= [dateFormatter dateFromString:dateString];
    NSTimeInterval dateDiff = [birthDate timeIntervalSinceNow];
    int age = abs(trunc(dateDiff/(60*60*24))/365);
    return [NSString stringWithFormat:@"%d", age];
}

//获取周几的英文
+(NSString *)getWeekDay:(NSInteger)i{
    if (i>6 || i<0) {
        return @"";
    }
    NSArray *weekDayArray = [NSArray arrayWithObjects:@"Sunday", @"Monday",@"Tuesday",@"Wednesday",@"Thursday", @"Friday",@"Saturday", nil];
    return [weekDayArray objectAtIndex:i];
}

//获取月份英文
+(NSString *)getMonthEnglish:(NSInteger)i{
    if (i>11 || i<0) {
        return @"";
    }
    NSArray *weekDayArray = [NSArray arrayWithObjects:@"January", @"Febuary",@"March",@"Apirl",@"May", @"June",@"July",@"August",@"September",@"October",@"November",@"December", nil];
    return [weekDayArray objectAtIndex:i];}

@end





