//
//  AppDelegate.m
//  ZungFuCar
//
//  Created by lxm on 13-9-25.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "AppDelegate.h"
#import "DBManager.h"

//extern int newsCount;
//extern int privilegeCount;
int newsCount;
int privilegeCount;

@implementation AppDelegate

- (void)dealloc
{
//    [_window release];
//    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"IntroIsWatched"]!=YES)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IntroIsWatched"];
    }
    
    newsCount = [[[NSUserDefaults standardUserDefaults] valueForKey:@"newsCount"] integerValue];
    privilegeCount = [[[NSUserDefaults standardUserDefaults] valueForKey:@"privilegeCount"] integerValue];
    
    _timer =  [NSTimer scheduledTimerWithTimeInterval:1200.0 target:self selector:@selector(timeToDo) userInfo:nil repeats:YES];
    [self timeToDo];

    
#ifdef IOSVERSON
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
#endif
    [WXApi registerApp:@"wx74bc8fc866d10700"];
    BOOL ret = [_mapManager start:@"WohHRvMX0377A8ZvwNIMRcYk"  generalDelegate:nil];
    if (!ret) {
        NSLog(@"_mapManager start failed!");
    }
    return YES;
}

- (void)timeToDo
{
    //[self getLeftButtonData];
    //[self getRightButtonData];
}

#pragma mark - 获取市场优惠数据
- (void)getLeftButtonData{
    
    AFHTTPRequestOperationManager *LeftManger = [AFHTTPRequestOperationManager manager];
    LeftManger.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [LeftManger.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"Http-X-U"];
    [LeftManger.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"Http-X-P"];
    
    [LeftManger GET:[NSString stringWithFormat:@"%@%@/view/fn/getDataByviewid/viewid/8", ZF_BASE_URL, ZF_BASE_PATH] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"App.net Global Stream: %@", responseObject);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ([[responseObject valueForKey:@"Code"] integerValue]==200) {
            int n = 0;
            NSMutableArray *dataSource = [[responseObject valueForKey:@"Data"] valueForKey:@"List"];
            NSMutableArray *historyData = [[DBManager sharedManager] getNewsListData];
            for (int i=0; i<dataSource.count; i++) {
                NSString *newID = [[dataSource objectAtIndex:i] objectForKey:@"ID"];
                for (int j=0; j<historyData.count; j++) {
                    NSString *historyID = [[historyData objectAtIndex:j] objectForKey:@"ID"];
                    if ([historyID isEqualToString:newID]) {
                        n++;
                        break;
                    }
                }
                if (n == 0) {
                    newsCount++;
                }
            }
            [[DBManager sharedManager] storeNewsListData:dataSource Sucess:nil fail:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"NewSplitController error :%@", error);
    }];
    
}

#pragma mark - 获取车型优惠数据
- (void)getRightButtonData{
    
    AFHTTPRequestOperationManager *RightManger = [AFHTTPRequestOperationManager manager];
    RightManger.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [RightManger.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"Http-X-U"];
    [RightManger.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"Http-X-P"];
    
    [RightManger GET:[NSString stringWithFormat:@"%@%@/view/fn/getDataByviewid/viewid/7", ZF_BASE_URL, ZF_BASE_PATH] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"App.net Global Stream: %@", responseObject);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ([[responseObject valueForKey:@"Code"] integerValue]==200) {
            int n = 0;
            NSMutableArray *dataSource = [[responseObject valueForKey:@"Data"] valueForKey:@"List"];
            NSMutableArray *historyData = [[DBManager sharedManager] getModelPrivilegeListData];
            for (int i=0; i<dataSource.count; i++) {
                NSString *newID = [[dataSource objectAtIndex:i] objectForKey:@"ID"];
                for (int j=0; j<historyData.count; j++) {
                    NSString *historyID = [[historyData objectAtIndex:j] objectForKey:@"ID"];
                    if ([historyID isEqualToString:newID]) {
                        n++;
                        break;
                    }
                }
                if (n == 0) {
                    privilegeCount++;
                }
            }
            [[DBManager sharedManager] storeModelPrivilegeListData:dataSource Sucess:nil fail:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"NewSplitController error :%@", error);
    }];
    
}

							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //关闭定时器
    [_timer setFireDate:[NSDate distantFuture]];
    //保存
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:newsCount] forKey:@"newsCount"];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:privilegeCount] forKey:@"privilegeCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //开启定时器
    [_timer setFireDate:[NSDate distantPast]];
    newsCount = [[[NSUserDefaults standardUserDefaults] valueForKey:@"newsCount"] integerValue];
    privilegeCount = [[[NSUserDefaults standardUserDefaults] valueForKey:@"privilegeCount"] integerValue];

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //保存
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:newsCount] forKey:@"newsCount"];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:privilegeCount] forKey:@"privilegeCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

@end
