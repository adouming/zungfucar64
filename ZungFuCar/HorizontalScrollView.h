//
//  ViewController.h
//  HMScrollViewDemo
//
//  Created by Wang Eric on 12-5-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TapDetectingImageView.h"
@protocol HorizontalScrollViewDelegate
-(void)pushToDetail:(int )index;
@end

@interface HorizontalScrollView:UIView<UIScrollViewDelegate, TapDetectingImageViewFotParentViewDelegate> {
    UIScrollView *scrollView;
    UIPageControl *pageControl;
	NSMutableArray *imageViews;
    UILabel *spotNameLabel;
    UILabel *scoreLabel;
    UIImageView *backImageView;
    UIImageView *goImageView;
    int currentPage;
    BOOL pageControlUsed;
    NSMutableArray *titles;
    NSMutableArray *imageUrls;
    NSMutableArray *subTitles;
    
    int mTtype;
}

@property (nonatomic, retain) NSMutableArray *imageViews;
@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIPageControl *pageControl;
@property (nonatomic, retain) UILabel *spotNameLabel;
@property (nonatomic, retain) UILabel *scoreLabel;
@property (nonatomic, retain) UIImageView *backImageView;
@property (nonatomic, retain) UIImageView *goImageView;
@property int currentPage;
@property BOOL pageControlUsed;
@property (nonatomic, retain) NSMutableArray *titles;
@property (nonatomic, retain) NSMutableArray *subtitles;
@property (nonatomic) id<HorizontalScrollViewDelegate> delegate;

- (void)clearAllView;
- (void)initScrollView:(NSArray *)dataSource type:(int)type;
- (void)loadScrollViewWithPage:(int)page;
- (void)scrollViewDidScroll:(UIScrollView *)sender;
- (void)createAllEmptyPagesForScrollView: (int)pages type:(int)type;

@end
