//
//  NSString+FilterHtml.h
//  ZungFuCar
//
//  Created by Alex Peng on 11/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FilterHtml)
- (NSString *)flattenHTML:(NSString *)html;
- (BOOL) validatePng;
@end
