//
//  NSObject+ReadSubMenu.h
//  ZungFuCar
//
//  Created by Ed Lee on 11/29/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ReadSubMenu)

// 根据一级菜单点击的index解析并返回二级菜单题目
-(NSMutableArray*)subMenuTitlesOfIndex: (NSInteger)index;

// 根据一级菜单点击的index解析并返回二级菜单动作
-(NSMutableArray*)subMenuActionsOfIndex: (NSInteger)index;

// 根据一级菜单点击的index及二级菜单点击index 解析并返回三级菜单题目
-(NSMutableArray*)thirdLevelMenuOfTopIndex: (NSInteger)topIndex andSubIndex:(NSInteger)subIndex;

// 读取二级菜单信息
-(NSArray*)readMenuArray;

@end
