//
//  UIView+ShowLoading.h
//  
//
//  Created by Alex Peng on 11/07/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ShowLoading)
- (void) showLoadingText;
- (void) showLoadingText:(NSString*)text;
- (void) showLoadingText:(NSString*)text onView:(UIView *)view;
- (void) showShortDurationMessage:(NSString*)text;
- (void) showShortDurationMessage:(NSString*)text withBlock:(void(^)())block;
- (void) hideLoadingText;
- (void) hideLoadingTextOnView:(UIView*)view animated:(BOOL)animated;
@end
