//
//  NSString+MD5.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/19/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)
-(NSString *) MD5Encry: (NSString *) encryString;
@end
