//
//  MenuAbstractViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/4/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "MenuAbstractViewController.h"

@interface MenuAbstractViewController ()

@end

@implementation MenuAbstractViewController

@synthesize statusView = _statusView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
    if ([self.customNavigaitionBar.delegate respondsToSelector:@selector(navigationBarWillAppear)]) {
        [self.customNavigaitionBar.delegate navigationBarWillAppear];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"viewDidAppear");
    [super viewDidAppear:animated];
    if ([self.customNavigaitionBar.delegate respondsToSelector:@selector(navigationBarDidAppear)]) {
        [self.customNavigaitionBar.delegate navigationBarDidAppear];
    }
}
- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    if(IOS7)
    {
        _customNavigaitionBar = [[ZFNavigationBarView alloc] initWithFrame:CGRectMake(0, 20, 320,    44)];
        UIView *statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 20)];
        statusView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:statusView];
    }
    else
        _customNavigaitionBar = [[ZFNavigationBarView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];

    _customNavigaitionBar.delegate = self;
    _customNavigaitionBar.rightBtn.hidden = YES;
    _customNavigaitionBar.navigationController = self.navigationController;
    _customNavigaitionBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_customNavigaitionBar];
    [self.view bringSubviewToFront:_customNavigaitionBar];
    
    CGRect bgImgVFrame;
    
    if(IOS7)
        bgImgVFrame =  CGRectMake(self.view.bounds.origin.x, 20, self.view.bounds.size.width, self.view.bounds.size.height-20);
    else
        bgImgVFrame = CGRectMake(self.view.bounds.origin.x, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:bgImgVFrame];
	[self.backgroundImageView setImage:iPhone5 ? [UIImage imageNamed:@"index_bg_iphone5@2x"] : [UIImage imageNamed:@"index_bg.png"]];
    // always insert background at bottom
    [self.view insertSubview:self.backgroundImageView atIndex:0];
	// Do any additional setup after loading the view.
	// Do any additional setup after loading the view.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)navigationBarDidPressAtIndex:(NSInteger)index{
    switch (index) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}
-(void)navigationbarWillAppear{
    
}
-(void)navigationBarDidAppear{
    
}

@end
