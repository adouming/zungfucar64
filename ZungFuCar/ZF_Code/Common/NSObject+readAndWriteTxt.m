//
//  NSObject+readAndWriteTxt.m
//  ZungFuCar
//
//  Created by Ed Lee on 12/13/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NSObject+readAndWriteTxt.h"

@implementation NSObject (readAndWriteTxt)

-(NSString *)readStringFromFile: (NSString*)filename
{
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* filepath = [documentsPath stringByAppendingPathComponent:filename];//@"availableCamps.txt"];
    NSString* stringInFile;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
        stringInFile = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:nil];
    } else {
        stringInFile = @"";
    }
    //DLog(@"Output of %@: %@", filename, stringInFile);
    return stringInFile;
}

-(void)writeStringToFile: (NSString*)string toFile:(NSString*)fileName
{
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* filepath = [documentsPath stringByAppendingPathComponent:fileName];//@"availableCamps.txt"];
    //NSString* availableCampList = string;
    [string writeToFile:filepath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

@end
