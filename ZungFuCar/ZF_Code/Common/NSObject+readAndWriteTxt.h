//
//  NSObject+readAndWriteTxt.h
//  ZungFuCar
//
//  Created by Ed Lee on 12/13/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (readAndWriteTxt)

-(NSString *)readStringFromFile: (NSString*)filename;
-(void)writeStringToFile: (NSString*)string toFile:(NSString*)fileName;

@end
