//
//  NSObject+AddJavascript.m
//  ZungFuCar
//
//  Created by Ed Lee on 11/23/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NSObject+AddJavascript.h"

NSMutableArray           *mWPhotoAry;
MWPhotoBrowser *browser;
UINavigationController *nc;
@implementation NSObject (AddJavascript)


-(void)injectJavascript:(UIWebView*)webView {
    /* 加入Jquery, Jquery Mobile及自定义脚本 */
    NSString *path1 = [[NSBundle mainBundle] pathForResource:@"jqLib" ofType:@"js"];
    NSString *jq = [NSString stringWithContentsOfFile:path1 encoding:NSUTF8StringEncoding error:nil];
    NSString *path2 = [[NSBundle mainBundle] pathForResource:@"gallery" ofType:@"js"];
    NSString *gal = [NSString stringWithContentsOfFile:path2 encoding:NSUTF8StringEncoding error:nil];
    NSString *path3 = [[NSBundle mainBundle] pathForResource:@"jqmLib" ofType:@"js"];
    NSString *jqm = [NSString stringWithContentsOfFile:path3 encoding:NSUTF8StringEncoding error:nil];
    [webView stringByEvaluatingJavaScriptFromString:jq];
    [webView stringByEvaluatingJavaScriptFromString:gal];
    [webView stringByEvaluatingJavaScriptFromString:jqm];
}

-(UINavigationController*)getAlbumPhotosFromImgClick:(NSURLRequest *)request {
    static NSString *urlPrefix = @"popGallery://";
    NSString *url = [[request URL] absoluteString];
    if ([url hasPrefix:urlPrefix] || [url containsString:@".jpg"]|| [url containsString:@".png"]|| [url containsString:@".gif"]) {
        NSUInteger psize = 0;
        if([url hasPrefix:urlPrefix]) {
            psize = [urlPrefix length];
        }
        else {
            
            mWPhotoAry = [NSMutableArray array];
            MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:url]];
            [mWPhotoAry addObject:photo];
            
            
            browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
            browser.displayActionButton = NO;
            [browser setCurrentPhotoIndex: 0];
            nc = [[UINavigationController alloc] initWithRootViewController:browser];
            return nc;

        }
        NSString *paramsString = [url substringFromIndex: psize];
        NSArray *paramsArray = [paramsString componentsSeparatedByString:@"&"];
        int paramsAmount = [paramsArray count];
        
        NSString *value = nil;
        for (int i = 0; i < paramsAmount; i++) {
            NSArray *keyValuePair = [[paramsArray objectAtIndex:i] componentsSeparatedByString:@"="];
            NSString *key = [keyValuePair objectAtIndex:0];
            
            if ([keyValuePair count] > 1) {
                value = [keyValuePair objectAtIndex:1];
            }
            
            if (key && [key length] > 0) {
                if (value && [value length] > 0) {
                    if ([key isEqualToString:@"param"]) {
                        // Use the index...
                        NSLog(@"%@",value);
                        NSNumber* pic_index = 0;
                        mWPhotoAry = [NSMutableArray array];
                        NSArray *photoArray = [value componentsSeparatedByString:@","];
                        for (int i=0; i<photoArray.count; i++) {
                            NSString* url = [photoArray objectAtIndex:i];
                            if([url containsString:@"http"]) {
                            MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString: url]];
                            NSLog(@"add url to photo:%@",url);
                            [mWPhotoAry addObject:photo];
                            }
                            else {
                                if([url length]>0) { //is index

                                    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                                    if ([f numberFromString:url])
                                    {
                                        pic_index = [NSNumber numberWithInteger:[url integerValue]];
                                    }
                                    else
                                    {
                                        pic_index = [photoArray objectAtIndex:mWPhotoAry.count-1];
                                    }
                                }
                            }
                        }
                        
                        browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
                        browser.displayActionButton = NO;
                        [browser setCurrentPhotoIndex: [pic_index integerValue] ];
                        nc = [[UINavigationController alloc] initWithRootViewController:browser];
                        return nc;
                    }
                }
            }
        }
    }
    else {
        return nil;
    }
}

-(NSInteger)webViewImgIsClickedByRequest:(NSURLRequest*)request
{
    NSString *url = [[request URL] absoluteString];
    static NSString *urlPrefix = @"popGallery://";
    static NSString *httpPrefix = @"http://";
    static NSString *httpsPrefix = @"https://";
    if ([url hasPrefix:urlPrefix] || [url containsString:@".jpg"]|| [url containsString:@".png"]|| [url containsString:@".gif"]) {
//        NSArray *sv = [[UIApplication sharedApplication].keyWindow subviews];
//        [[sv objectAtIndex:1] setTag:666];
//        [[sv objectAtIndex:1] setHidden:YES];
        return 0; // Pop up gallery
    }
    else if ([url hasPrefix:httpPrefix] || [url hasPrefix:httpsPrefix]) {
        return 1; // Launch Safari
    }
    else {
        return 2;
    }
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return mWPhotoAry.count;
    NSLog(@"photo count: %d",mWPhotoAry.count);
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < mWPhotoAry.count)
        return [mWPhotoAry objectAtIndex:index];
    return nil;
}

- (void)photoBrowserDidClose:(MWPhotoBrowser *)photoBrowser {
    NSArray *sv = [[UIApplication sharedApplication].keyWindow subviews];
    for (UIView *v in sv) {
        if (v.tag == 666) {
            v.hidden = NO;
        }
    }
}

@end
