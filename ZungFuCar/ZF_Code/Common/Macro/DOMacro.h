/*
 *Created by lxm on 12-3-13.
 */


/** all comment Macro define in here ***/


#define iPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
#define iPhone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

//**** 调试输出 ****
#if DEBUG
    #define DOLOG(_format,args...)  printf("%s\n",[[NSString stringWithFormat:(_format),##args] UTF8String])
    #define LOG(xx, ...)  DOLOG(@"\n##%s(%d)##" xx, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
    #define DOLOGDATA(__DATA) LOG(@"%@",[[[NSString alloc]initWithData:__DATA encoding:NSUTF8StringEncoding]autorelease])
#else
    #define DOLOG(_format,args...)
    #define LOG(xx, ...)
    #define DOLOGDATA(__DATA)
#endif

//重写NSLog,Debug模式下打印日志和当前行数
#if DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr,"\nfunction:%s line:%d content:%s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(FORMAT, ...) nil
#endif

//DEBUG  模式下打印日志,当前行 并弹出一个警告
#ifdef DEBUG
#   define ULog(fmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }
#else
#   define ULog(...)
#endif

//NavBar高度
#define NavigationBar_HEIGHT 44
//获取屏幕 宽度、高度
#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)


//**** 安全释放 ****
#define DO_RELEASE_SAFELY(__POINTER) [__POINTER release];__POINTER = nil;

//**** 计算数组大小 ****
#define countof(x)      (sizeof(x)/sizeof((x)[0]))

// rgb颜色转换（16进制->10进制）
#define HEXRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


// 获取RGB颜色
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b) RGBA(r,g,b,1.0f)


