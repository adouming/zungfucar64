//
//  AlexMacro.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DOMacro.h"

#define SET_OBJECT_ORIGIN_Y(_object,_y) _object.frame = CGRectMake(_object.frame.origin.x, _y, _object.frame.size.width, _object.frame.size.height);



#define TITLE_FONT_SIZE     15
#define TITLE_BG_COLOR  0xFFF
#define Gray_Color_List 0x999
#define Content_Font_Size  14
#define ZF_GoldColor  0xba8047
#define Gray_Color_Content 0x444
#define Gray_Color_Btn 0x777

#define kPickerViewDuration 0.3

#define RoadRescueTableRowHight  59

#define iPhone5Step          80

#define NOTICENAME_LOGIN_SUCESS   @"loginSucess"

// Devices
#define IPAD                    (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IPHONE                  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IPHONE_5                (IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IPHONE_4                (IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)

// OS Version
#define IS_IOS_AT_LEAST(ver)    ([[[UIDevice currentDevice] systemVersion] compare:ver] != NSOrderedAscending)
#define IS_IOS7                 (__IPHONE_OS_VERSION_MAX_ALLOWED >= 70000 && IS_IOS_AT_LEAST(@"7.0"))


typedef enum {
    ReserveTypeTest=1,
    ReserveTypeService
} ReserveType;
typedef enum {
    ReserveTypeServiceTypeConst=1,
    ReserveTypeServiceTypeMend
}ReserveServiceType;