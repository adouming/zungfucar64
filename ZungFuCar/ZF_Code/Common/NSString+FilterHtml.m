//
//  NSString+FilterHtml.m
//  ZungFuCar
//
//  Created by Alex Peng on 11/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NSString+FilterHtml.h"

@implementation NSString (FilterHtml)
- (NSString *)flattenHTML:(NSString *)html {
    
    NSScanner *theScanner;
    NSString *text = nil;
    
    theScanner = [NSScanner scannerWithString:html];
    
    while ([theScanner isAtEnd] == NO) {
        // find start of tag
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        // find end of tag
        [theScanner scanUpToString:@">" intoString:&text] ;
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        html = [html stringByReplacingOccurrencesOfString:
                [ NSString stringWithFormat:@"%@>", text]
                                               withString:@""];
    } // while //
    
    return html;
}

/**
 *  验证url 是否是图片
 */
- (BOOL) validatePng
{
    if(self == nil) {
        return false;
    }
    NSString *patternStr = [NSString stringWithFormat:@"(.jpg|.gif|.png|.JPG|.GIF|.PNG)$"];
    NSRegularExpression *regularexpression = [[NSRegularExpression alloc]
                                              initWithPattern:patternStr
                                              options:NSRegularExpressionCaseInsensitive
                                              error:nil];
    NSUInteger numberofMatch = [regularexpression numberOfMatchesInString:self
                                                                  options:NSMatchingReportProgress
                                                                    range:NSMakeRange(0, self.length)];
    
    if(numberofMatch > 0)
    {
        return YES;
    }
    return NO;
}

@end
