//
//  MenuAbstractViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/4/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AbstractViewController.h"
#import "DOMacro.h"
#import "AlexMacro.h"
#import "ZFNavigationBarView.h"
@interface MenuAbstractViewController : AbstractViewController<ZFNavigationBarDelegate>

@property (nonatomic,strong) UIImageView *backgroundImageView;

@property (nonatomic,retain) ZFNavigationBarView *customNavigaitionBar;
@property (nonatomic, retain) UIView *statusView;
@end
