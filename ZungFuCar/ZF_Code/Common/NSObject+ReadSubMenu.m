//
//  NSObject+ReadSubMenu.m
//  ZungFuCar
//
//  Created by Ed Lee on 11/29/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NSObject+ReadSubMenu.h"

@implementation NSObject (ReadSubMenu)

// 根据一级菜单点击的index解析并返回二级菜单题目
-(NSMutableArray*)subMenuTitlesOfIndex: (NSInteger)index
{
    NSArray *submenusOfIndex = [[[self readMenuArray] objectAtIndex:index] objectForKey:@"menus"];
    NSMutableArray *menuTitleArray = [[NSMutableArray alloc]init];
    for(int i =0;i<[submenusOfIndex count];i++) {
        if (index==4 && i==[submenusOfIndex count]-1 && [[NSUserDefaults standardUserDefaults] boolForKey:@"isLogined"]) {
            [menuTitleArray insertObject:@"用户中心" atIndex:i];
        }else{
            [menuTitleArray insertObject:[[submenusOfIndex objectAtIndex:i] objectForKey:@"title"] atIndex:i];
        }
    }
    return menuTitleArray;
}

// 根据一级菜单点击的index解析并返回二级菜单动作
-(NSMutableArray*)subMenuActionsOfIndex: (NSInteger)index
{
    NSArray *submenusOfIndex = [[[self readMenuArray] objectAtIndex:index] objectForKey:@"menus"];
    NSMutableArray *menuActionArray = [[NSMutableArray alloc]init];
    for(int i =0;i<[submenusOfIndex count];i++) {
        [menuActionArray insertObject:[[submenusOfIndex objectAtIndex:i] objectForKey:@"action"] atIndex:i];
    }
    return menuActionArray;
}

// 根据一级菜单点击的index及二级菜单点击index 解析并返回三级菜单题目
-(NSMutableArray*)thirdLevelMenuOfTopIndex: (NSInteger)topIndex andSubIndex:(NSInteger)subIndex
{
    NSArray *submenusOfIndex = [[[self readMenuArray] objectAtIndex:topIndex] objectForKey:@"menus"];
    NSArray *thirdLevelMenu = [[submenusOfIndex objectAtIndex:subIndex] objectForKey:@"thirdlevel"];
    
    NSMutableArray *thirdLevelMenuTitleAry = [[NSMutableArray alloc]init];
    for(int i =0;i<[thirdLevelMenu count];i++) {
        [thirdLevelMenuTitleAry insertObject:[[thirdLevelMenu objectAtIndex:i] objectForKey:@"title"] atIndex:i];
        NSLog(@"third level menu no %d is: %@", i, [[thirdLevelMenu objectAtIndex:i] objectForKey:@"title"]);
    }
    return thirdLevelMenuTitleAry;
}

-(NSMutableArray*)fourthLevelMenuTitlesOfThirdIndex:(NSInteger)thirdIndex fromThirdLevelArray:(NSMutableArray*)thirdLevelAry {
    NSArray *fourthLevelMenu = [[thirdLevelAry objectAtIndex:thirdIndex] objectForKey:@"fourthlevel"];
    NSMutableArray *fourthLevelTitles = [[NSMutableArray alloc]init];
    for (int i=0; i<fourthLevelMenu.count; i++) {
        [fourthLevelTitles insertObject:[[fourthLevelMenu objectAtIndex:i] objectForKey:@"title"] atIndex:i];
    }
    return fourthLevelTitles;
}

// 读取菜单信息
-(NSArray*)readMenuArray
{
    NSString *bundlePathofPlist = [[NSBundle mainBundle]pathForResource:@"Submenus" ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:bundlePathofPlist];
    NSArray *dataFromPlist = [dict valueForKey:@"subMenu"];
    return dataFromPlist;
}

@end
