//
//  UILabel+autoResize.m
//  Starbucks
//
//  Created by Alex Peng on 8/30/13.
//  Copyright (c) 2013 FabriQate Inc. All rights reserved.
//

#import "UILabel+autoResize.h"
@implementation UILabel (autoResize)
@dynamic autoResize;
-(void)setAutoResize:(BOOL)autoResize{
    if (autoResize == YES) {
        [self setNumberOfLines:0];
        CGSize size = CGSizeMake(self.frame.size.width,MAXFLOAT);
        CGSize labelsize = [self.text sizeWithFont:self.font constrainedToSize:size lineBreakMode:UILineBreakModeWordWrap];
        if (labelsize.height>self.frame.size.height) {
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, labelsize.width, labelsize.height);
        }
        
    }
    
}
@end
