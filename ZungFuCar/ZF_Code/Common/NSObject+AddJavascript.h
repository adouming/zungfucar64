//
//  NSObject+AddJavascript.h
//  ZungFuCar
//
//  Created by Ed Lee on 11/23/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MWPhotoBrowser.h"

@interface NSObject (AddJavascript) <MWPhotoBrowserDelegate>

-(void)injectJavascript:(UIWebView*)webView;
-(NSInteger)webViewImgIsClickedByRequest:(NSURLRequest*)request;
//-(NSString*)getAlbumPhotosFromImgClick:(NSURLRequest *)request;
-(UINavigationController*)getAlbumPhotosFromImgClick:(NSURLRequest *)request;

@end
