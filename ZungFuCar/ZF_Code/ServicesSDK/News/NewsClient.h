//
//  NewsClient.h
//  ZungFuCar
//
//  Created by kc on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZFAPIClient.h"
#import "NewsData.h"

@interface NewsClient : ZFAPIClient

//news list
- (void)getDataWithToken:(NSString *)token
                withType:(NSInteger)aType
                 success:(void (^)(NSArray *data))success
                 failure:(void (^)(NSError *error))failure;

//news detail
- (void)getDataWithToken2:(NSString *)token
                  success:(void (^)(NewsDetailData *data))success
                  failure:(void (^)(NSError *error))failure;


@end



