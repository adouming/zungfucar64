//
//  NewsManager.h
//  ZungFuCar
//
//  Created by kc on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZFAPIManagerBase.h"
#import "NewsClient.h"

@interface NewsManager : ZFAPIManagerBase

+ (NewsManager *)sharedManager;


-(NSMutableArray*)getDBNewsListDataWithType:(NSInteger)aType;




//列表请求
- (void)getData:(NSInteger)aType
        success:(void (^)(NSArray *data))success
        failure:(void (^)(NSError *error))failure;
//- (void)getDataSuccess:(void (^)(NSArray *data))success
//               failure:(void (^)(NSError *error))failure;




//详情----没有用到
- (void)getDataSuccess2:(void (^)(NewsDetailData *data))success
               failure:(void (^)(NSError *error))failure;

@end











