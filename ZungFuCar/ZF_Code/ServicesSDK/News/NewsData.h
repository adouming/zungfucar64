//
//  NewsData.h
//  ZungFuCar
//
//  Created by kc on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KADataBase.h"
#import "ZFResponseDataBase.h"
#import "UIImageView+AFNetworking.h"


@interface NewsData : KADataBase

@end




///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

@interface NewsListResponseData : ZFResponseDataBase

@property(nonatomic, strong)NSMutableArray *dataArray;

+ (id)dataWithJSON:(id)JSON;
+ (NSMutableArray *)dataForTesting;

@end


/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
//@class NewsDetailData;
//@interface NewsDetailResponseData : ZFResponseDataBase
//
//@property(nonatomic, strong)NewsDetailData *detailData;
//
//+ (id)dataWithJSON:(id)JSON;
//+ (NewsDetailData *)dataForTesting;
//
//@end



/////////////////////////////////////////////////////////
/////////////////////// list
/////////////////////////////////////////////////////////
@interface NewsListData : KADataBase

@property (nonatomic,strong) NSString *listID;
@property (nonatomic,strong) NSString *titleStr;
@property (nonatomic,strong) NSString *shortDes;
@property (nonatomic,strong) NSString *iconStr;
@property (nonatomic,strong) NSString *summaryStr;
@property (nonatomic,strong) NSString *imgUrl;
@property (nonatomic,strong) NSString *timeStr;
@property (nonatomic,strong) NSString *UpdatedTime;

+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end



/////////////////////////////////////////////////////////
/////////////////////// detail
/////////////////////////////////////////////////////////
@interface NewsDetailData : KADataBase

@property(nonatomic,strong) NSMutableArray *images;

@property (nonatomic,strong) NSString *titleStr;
@property (nonatomic,strong) NSString *timeStr;

@property (nonatomic,strong) NSString *vedioUrl;
@property (nonatomic,strong) NSString *shareMsg;

@property (nonatomic,strong) NSString *summaryStr;

+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end












