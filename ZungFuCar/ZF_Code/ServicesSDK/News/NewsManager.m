//
//  NewsManager.m
//  ZungFuCar
//
//  Created by kc on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "NewsManager.h"
#import "DBManager.h"

@implementation NewsManager


static NewsManager *shared_newsManager = nil;

+ (NewsManager *)sharedManager {
    static dispatch_once_t pred;
	dispatch_once(&pred, ^{
        shared_newsManager = [[self alloc] init];
    });
	return shared_newsManager;
}



/*
 从数据库读取数据
 */
-(NSMutableArray*)getDBNewsListDataWithType:(NSInteger)aType{
    
    return [[DBManager sharedManager] getNewsListDataWithType:aType];
}



/*
 网络请求列表数据，并加入到数据库中
 */
- (void)getData:(NSInteger)aType
        success:(void (^)(NSArray *data))success
        failure:(void (^)(NSError *error))failure{
    
    //网络请求
    [[NewsClient sharedClient] getDataWithToken:@""
                                       withType:aType
                                        success:^(NSArray *clientData) {
        
        //加入数据库
        [[DBManager sharedManager] storeNewsListData:clientData
                                            withType:aType
                                              Sucess:^(NSMutableArray *storeData) {
            
            //成功返回数据库
            success(storeData);
            
        }fail:^(NSError *error) {
            
            //保存数据库失败，返回网络请求数据
            success(clientData);
        }];
        
        
    } failure:^(NSError *error) {
        
        
        //网络请求失败
        failure(error);
    }];
}







- (void)getDataSuccess2:(void (^)(NewsDetailData *data))success
               failure:(void (^)(NSError *error))failure{
    [[NewsClient sharedClient] getDataWithToken2:@"" success:^(NewsDetailData *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
    
    //  [[ZFStewardClient sharedClient] postMethodExamplesuccess:^(NSInteger code) {
    //      success(nil);
    //  } failure:^(NSError *error) {
    //      failure(error);
    //  }];
}


@end

