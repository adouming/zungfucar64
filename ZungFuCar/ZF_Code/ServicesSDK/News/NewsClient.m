//
//  NewsClient.m
//  ZungFuCar
//
//  Created by kc on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "NewsClient.h"
#import "NewsData.h"
#import "DBManager.h"

//最新资讯news
#define ZF_PATH_NEWS_LIST_              @"/view/fn/getDataByviewid"



@implementation NewsClient

+ (id)sharedClient {
	static dispatch_once_t pred;
    static NewsClient *shared_news_instance = nil;
	
    dispatch_once(&pred, ^{
		shared_news_instance = [[self alloc] init];
        shared_news_instance.client = [self sharedHTTPClient];
    });
    
	return shared_news_instance;
}


- (void)getDataWithToken:(NSString *)token
                withType:(NSInteger)aType
                 success:(void (^)(NSArray *data))success
                 failure:(void (^)(NSError *error))failure{
    
    
    NSString *viewType = @"8";
    if (aType==0) {
        viewType = @"8";
    }else{
        viewType = @"7";
    }
    
    
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_NEWS_LIST_];
    
    
    
    //获取当前时间
    NSString *aUpateTime = [[DBManager sharedManager] getNewsListTimeDataWithType:aType];
    NSMutableDictionary *params;
    
    
    //NSLog(@"-------%@",aUpateTime);
    NSMutableArray *aPreArray = [[DBManager sharedManager] getNewsListDataWithType:aType];
    /*
     第一次请求不要传date
     
     方法：没有数据时候，就不传date
     */
    if (aPreArray.count==0) {
        
        NSLog(@"第一次请求，或者是没有数据 请求");
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                  viewType,@"viewid",
                  nil];
    }else{
        
        NSLog(@"再次 请求-上次请求时间：%@",aUpateTime);
        
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                  viewType,@"viewid",
                  aUpateTime,@"date",
                  nil];
        
    }
    
    
    
    
    
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NewsListResponseData *responseData =[NewsListResponseData dataWithJSON:responseObject];
        
        // 3.1 Parser json dictionary to data object
        
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode intValue];
        /*
         #define API_Sucess_Code                     200
         #define API_IDType_Error_Code               400
         #define API_NotAuthorized_Error_Code        401
         #define API_PayMent_Required_Error_Code     402
         #define API_Forbidden_Error_Code            403
         #define API_RequestURL_NotFound_Error_Code  404
         #define API_Server_Error_Code               500
         #define API_Not_Implemented_Error_Code      501
         
         Code	Message
         200	OK / Success
         400	ID or  data must be numeric
         401	Not authorized
         402	Payment Required
         403	Forbidden
         404	The request URL was not found
         500	The server encountered an error when processing your request
         501	The requested function is not implemented
         */
        switch (returnCode) {
            case API_Sucess_Code: {
                success(responseData.dataArray);
                break;
            }
                
            case API_IDType_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            case API_NotAuthorized_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_PayMent_Required_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Forbidden_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_RequestURL_NotFound_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Server_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Not_Implemented_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            default: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}



- (void)getDataWithToken2:(NSString *)token
                  success:(void (^)(NewsDetailData *data))success
                  failure:(void (^)(NSError *error))failure{
    if (USE_TEST_DATA_MODE) {
        NewsDetailData *responseData =[NewsDetailData dataForTesting];
        success(responseData);
        return;
    }
    
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_NEWS_LIST];
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"6",@"viewid",
                                   @"2012-12-02 23:59:59",@"date",
                                   nil];
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NewsListResponseData *responseData =[NewsListResponseData dataWithJSON:responseObject];
        
        // 3.1 Parser json dictionary to data object
        
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode intValue];
        /*
         #define API_Sucess_Code                     200
         #define API_IDType_Error_Code               400
         #define API_NotAuthorized_Error_Code        401
         #define API_PayMent_Required_Error_Code     402
         #define API_Forbidden_Error_Code            403
         #define API_RequestURL_NotFound_Error_Code  404
         #define API_Server_Error_Code               500
         #define API_Not_Implemented_Error_Code      501
         
         Code	Message
         200	OK / Success
         400	ID or  data must be numeric
         401	Not authorized
         402	Payment Required
         403	Forbidden
         404	The request URL was not found
         500	The server encountered an error when processing your request
         501	The requested function is not implemented
         */
        switch (returnCode) {
            case API_Sucess_Code: {
                success(responseData.dataArray);
                break;
            }
                
            case API_IDType_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            case API_NotAuthorized_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_PayMent_Required_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Forbidden_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_RequestURL_NotFound_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Server_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Not_Implemented_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            default: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}


@end

