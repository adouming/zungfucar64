//
//  NewsData.m
//  ZungFuCar
//
//  Created by kc on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "NewsData.h"

@implementation NewsData

@end



///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

#pragma mark - NewsListResponseData

@implementation NewsListResponseData

+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON];
}
+ (NSMutableArray *)dataForTesting {
    
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i<5; i++) {
        [data addObject:[NewsListData dataForTesting]];
    }
    return data;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        id response = [JSON objectForKey:@"Data"];
        NSLog(@"res=%@",response);
        if ([response isKindOfClass:[NSDictionary class]]) {
            NSArray *data = [response arrayForKey:@"List"];
            NSMutableArray *temData = [NSMutableArray array];
            
            for (id json in data) {
                NewsListData *objectData = [NewsListData dataWithJSON:json];
                [temData addObject:objectData];
            }
            
            self.dataArray = temData;
        }
        
    }
    
    return self;
}
@end



///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
//
//#pragma mark - NewsListResponseData
//
//@implementation NewsDetailResponseData
//
//+ (id)dataWithJSON:(id)JSON {
//    return [[self alloc] initWithJSON:JSON];
//}
//+ (NewsDetailData *)dataForTesting {
//    
//    NSMutableArray *data = [NSMutableArray array];
//    for (int i = 0; i<5; i++) {
//        [data addObject:[NewsListData dataForTesting]];
//    }
//    return data;
//}
//- (id)initWithJSON:(id)JSON {
//    if (self = [super initWithJSON:JSON]) {
//        id response = [JSON objectForKey:@"Data"];
//        
//        NSArray *data = [response arrayForKey:@"List"];
//        self.detailData = [NewsListData dataWithJSON:JSON];
////        NSMutableArray *temData = [NSMutableArray array];
////        
////        for (id json in data) {
////            NewsListData *objectData = [NewsListData dataWithJSON:json];
////            [temData addObject:objectData];
////        }
////        
////        self.dic = temData;
//    }
//    
//    return self;
//}
//@end




///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
@implementation NewsListData

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
        self.titleStr = @"Name";
        self.shortDes = @"Content";
        self.imgUrl = @"testImageUrl";
        
    }
    
    return self;
}
-(NSString *)dateToStr:(NSString *)_strDate{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[_strDate doubleValue]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}
-(NSString *)dateToStr2:(NSString *)_strDate{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[_strDate doubleValue]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}
- (id)initWithJSON:(id)JSON {
    //NSLog(@"json=%@",JSON);
    if (self = [super initWithJSON:JSON]) {
        self.listID = [JSON stringForKey:@"ID"];
        self.summaryStr = [JSON stringForKey:@"Descp"];
        self.titleStr = [JSON stringForKey:@"Name"];
        self.shortDes = [JSON stringForKey:@"ShortDesc"];
        self.imgUrl = [JSON stringForKey:@"PromoImageURL"];
        self.timeStr = [JSON stringForKey:@"UpdatedTime"];
        self.UpdatedTime = [JSON stringForKey:@"UpdatedTime"];
    }
    
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:_listID forKey:@"ID"];
        [coder encodeObject:_summaryStr forKey:@"Descp"];
        [coder encodeObject:_titleStr forKey:@"Name"];
        [coder encodeObject:_shortDes forKey:@"ShortDesc"];
        [coder encodeObject:_imgUrl forKey:@"PromoImageURL"];
        [coder encodeObject:_timeStr forKey:@"Time"];
        [coder encodeObject:_UpdatedTime forKey:@"UpdatedTime"];
        
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _listID = [coder decodeObjectForKey:@"ID"];
        _summaryStr = [coder decodeObjectForKey:@"Descp"];
        _titleStr = [coder decodeObjectForKey:@"Name"];
        _shortDes = [coder decodeObjectForKey:@"ShortDesc"];
        _imgUrl = [coder decodeObjectForKey:@"PromoImageURL"];
        _timeStr = [coder decodeObjectForKey:@"Time"];
        _UpdatedTime = [coder decodeObjectForKey:@"UpdatedTime"];
        
    }
    
    return self;
}


@end



///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
@implementation NewsDetailData

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
        NSMutableArray *aImages = [[NSMutableArray alloc] init];
        [aImages addObject:@"banner11.png"];
        [aImages addObject:@"banner12.png"];
        self.images = aImages;
        
        self.titleStr = @"2013梅塞德系";
        self.timeStr = @"2013-07-05";
        
        self.vedioUrl = @"http://baidu.mytv365.com/kan/AW8R?fr=v.baidu.com/";
        self.shareMsg = @"share-2013梅塞德系-share";
        
        self.summaryStr = @"2013奔驰-仁孚邀请赛.";
        
        
    }
    
    return self;
}
-(NSString *)dateToStr:(NSString *)_strDate{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[_strDate doubleValue]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        self.summaryStr = [JSON stringForKey:@"Content"];
        self.titleStr = [JSON stringForKey:@"Name"];
        //self.imgUrl = [JSON stringForKey:@"thumbnail"];
    }
    
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:_summaryStr forKey:@"Content"];
        [coder encodeObject:_titleStr forKey:@"Name"];
        //[coder encodeObject:_imgUrl forKey:@"imageUrl"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _titleStr = [coder decodeObjectForKey:@"Name"];
        _summaryStr = [coder decodeObjectForKey:@"Content"];
        //_imgUrl = [coder decodeObjectForKey:@"imageUrl"];
        
    }
    
    return self;
}


@end




