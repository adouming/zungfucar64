//
//  ZFSeriviceAPIManager.m
//  ZungFuCar
//
//  Created by lxm on 13-10-27.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "ZFSeriviceAPIManager.h"
#import "AboutZFAPIClient.h"

@implementation ZFSeriviceAPIManager
static ZFSeriviceAPIManager *shared_manager = nil;

+ (ZFSeriviceAPIManager *)sharedManager {
    static dispatch_once_t pred;
	dispatch_once(&pred, ^{
        shared_manager = [[self alloc] init];
    });
	return shared_manager;
}

- (void)requestProductDetailDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure
{
    
    NSString *strLocalHtml = [NSString stringWithFormat:@"%@%@/api/Productdetail/fn/getDataByproductid/productid/12/2013-12-03 23:59:59", ZF_BASE_URL, ZF_BASE_PATH];
    strLocalHtml= [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

- (void)requestCarparametersDataSuccess:(void (^)(NSArray *data))success
                                failure:(void (^)(NSError *error))failure
{
    
    NSString *strLocalHtml = [NSString stringWithFormat:@"%@%@/api/Calparameters/fn/getDataByproductdetailid/productdetailid/1/type/loan/lastmodified/2013-12-03 23:59:59", ZF_BASE_URL, ZF_BASE_PATH];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

- (void)requestImageDataSuccess:(void (^)(id data))success
                                failure:(void (^)(NSError *error))failure
{
    
    NSString *strLocalHtml = [NSString stringWithFormat:@"%@%@ProductRoot/0/cntImage/914D801F-94C2-7656-D643-8DA9FE5D0572.JPG", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedImageClient] requestImageDataWithPath:strLocalHtml
                                                     success:^(id data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

@end
