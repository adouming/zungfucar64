//
//  ZFSeriviceAPIManager.h
//  ZungFuCar
//
//  Created by lxm on 13-10-27.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "AboutZFAPIManager.h"

@interface ZFSeriviceAPIManager : AboutZFAPIManager

+ (ZFSeriviceAPIManager *)sharedManager;
/**
 *	获取精品
 */
- (void)requestProductDetailDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure;

/**
 *	获取配件
 */
- (void)requestCarparametersDataSuccess:(void (^)(NSArray *data))success
                                failure:(void (^)(NSError *error))failure;

- (void)requestImageDataSuccess:(void (^)(id data))success
                        failure:(void (^)(NSError *error))failure;

@end
