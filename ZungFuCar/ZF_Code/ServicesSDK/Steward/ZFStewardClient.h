//
//  ZFStewardClient.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import "ZFAPIClient.h"
#import "ZFStewardData.h"
#import "AlexMacro.h"

@interface ZFStewardClient : ZFAPIClient {

}


+ (id)sharedClient;

- (void)getStoreClientWithPage:(NSString *)page andPageSize:(NSString *)pageSize
                       Success:(void (^)(ZFNetServerStoreMutableArray *data))success
                       failure:(void (^)(NSError *error))failure;

- (void)getCarTypeClientWithType:(NSString *)type
                         Success:(void (^)(ZFNetServerCarTypeDictionary *data))success
                         failure:(void (^)(NSError *error))failure;

- (void)getCarModelWithID:(NSUInteger )idStr
                  Success:(void (^)(ZFNetServerCarTypeDictionary *data))success
                  failure:(void (^)(NSError *error))failure;

- (void)getIndustryClientSuccess:(void (^)(ZFNetServerIndustryData *data))success
                         failure:(void (^)(NSError *error))failure;

- (void)getJobClientSuccess:(void (^)(ZFReserveDataDictionary *data))success
                    failure:(void (^)(NSError *error))failure;

- (void)getPersonInfoClientWithUserID:(NSString *)userID
                              Success:(void (^)(ZFUserInfoData *data))success
                              failure:(void (^)(NSError *error))failure;

- (void)postBookingClientWithInfo:(NSDictionary *)paraDic Success:(void (^)(ZFResponseDataBase *response))success
                          failure:(void (^)(NSError *error))failure;

-(void)getReserveInfoClientSucess:(void(^)(ZFReserveDataDictionary *data))success
                          failure:(void(^)(NSError *error))failure;

-(void)getExhibitionClientSucess:
                    (void(^)(ZFReserveCityExhibitionMutableArray *data))success
                   failure:(void(^)(NSError *error))failure
                             cid:(int)cityID type:(ReserveType)type;

-(void)getCityClientSucess:(void(^)(ZFReserveCityExhibitionMutableArray *data))success
                             failure:(void(^)(NSError *error))failure;
-(void)getSchemeWithScheme:(NSString *)scheme
                     carID:(NSString *)ID
                   Success:(void (^)(NSMutableArray *data))success
                   failure:(void (^)(NSError *error))failure;

@end
