//
//  ZFStewardData.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KADataBase.h"
#import "ZFResponseDataBase.h"

@interface ZFStewardData : KADataBase

@end

typedef NSMutableArray ZFNetServerStoreMutableArray;
@interface ZFNetServerStoreResponseData : ZFResponseDataBase

@property(nonatomic, strong)ZFNetServerStoreMutableArray *storeDataArray;
@property(nonatomic, strong)NSString *totlePage;

+ (id)dataWithJSON:(id)JSON;
+ (ZFNetServerStoreMutableArray *)dataForTesting;

@end


@interface ZFNetServerStoreData : KADataBase


@property (nonatomic,assign) NSInteger storeID;
@property (nonatomic,retain) NSString *latitude;
@property (nonatomic,retain) NSString *longtitude;
@property (nonatomic,retain) NSString *location;
@property (nonatomic,assign) NSInteger displayOrder;
@property (nonatomic,retain) NSString *name;
@property (nonatomic,retain) NSString *shortDesc;
@property (nonatomic,retain) NSString *descp;
@property (nonatomic,retain) NSString *address;
@property (nonatomic,retain) NSString *phone;
@property (nonatomic,retain) NSString *sImageUrl;
@property (nonatomic,retain) NSString *updatedTime;
@property (nonatomic,retain) NSString *hasService;
@property (nonatomic,retain) NSString *hasTestDrive;
@property (nonatomic,retain) NSString *fax;
@property (nonatomic,retain) NSString *contactEmail;
@property (nonatomic,retain) NSString *sroom;
@property (nonatomic,retain) NSString *mservice;
@property (nonatomic,retain) NSString *emergencyPhone;
@property (nonatomic,retain) NSString *serverPhone;
@property (nonatomic,retain) NSString *bImageUrl;
@property (nonatomic,retain) NSString *url;
@property (nonatomic,retain) NSString *serviceURL;

+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;


@end

typedef enum {
    ZFCarTypeBenz=1,
    ZFCarTypeAMG,
    ZFCarTypeSmart
}ZFCarType;

typedef NSMutableDictionary ZFNetServerCarTypeDictionary;
@interface ZFNetServerCarTypeResponseData : ZFResponseDataBase

@property(nonatomic, strong)ZFNetServerCarTypeDictionary *carTypeDataDic;
@property(nonatomic, strong)NSString *totlePage;

+ (id)dataWithJSON:(id)JSON withCarType:(ZFCarType)carType;
+ (ZFNetServerStoreMutableArray *)dataForTesting;

@end

typedef NSMutableDictionary ZFNetServerCarModelDictionary;
@interface ZFNetServerCarModelResponseData : ZFResponseDataBase

@property(nonatomic, strong)ZFNetServerCarModelDictionary *carModelDataDic;
@property(nonatomic, strong)NSString *totlePage;

+ (id)dataWithJSON:(id)JSON withCarID:(NSUInteger)idStr;
+ (ZFNetServerStoreMutableArray *)dataForTesting;

@end

typedef NSDictionary ZFReserveDataDictionary;
@interface ZFNetServerReserveResponseData : ZFResponseDataBase

@property(nonatomic, strong)ZFReserveDataDictionary *reserveDataDic;
@property(nonatomic, strong)NSString *totlePage;

+ (id)dataWithJSON:(id)JSON;
+ (ZFReserveDataDictionary *)dataForTesting;

@end
typedef NSMutableArray ZFReserveCityExhibitionMutableArray;
@interface ZFNetServerCityExhibitionResponseData : ZFResponseDataBase

@property(nonatomic, strong)ZFReserveCityExhibitionMutableArray *cityExhibitionDataArray;
@property(nonatomic, strong)NSString *totlePage;

+ (id)dataWithJSON:(id)JSON;
+ (ZFReserveDataDictionary *)dataForTesting;

@end
typedef NSMutableArray CarProductMutableArray;
@interface ZFNetServerCarTypeData : KADataBase

@property (nonatomic,assign) NSInteger carID;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,retain) CarProductMutableArray *productArray;
//@property (nonatomic,assign) NSInteger displayOrder;
//@property (nonatomic,strong) NSString *shortDesc;
//@property (nonatomic,strong) NSString *descp;
//@property (nonatomic,strong) NSString *banner;
//@property (nonatomic,strong) NSString *updatedTime;

+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end


@interface ZFNetServerCarProductData : KADataBase

@property (nonatomic,assign) NSInteger carModelID;
@property (nonatomic,strong) NSString *name;


+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end

@interface ZFNetServerCarModelData : KADataBase

@property (nonatomic,assign) NSInteger carModelID;
@property (nonatomic,assign) NSInteger displayOrder;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *promoImageURL;
@property (nonatomic,strong) NSString *updatedTime;

+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end


@interface ZFNetServerIndustryData : KADataBase


@property (nonatomic,strong) NSString *displayName;
@property (nonatomic,strong) NSString *value;
@property (nonatomic,retain) NSString *updateTime;


+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end

@interface ZFNetServerJobData : KADataBase


@property (nonatomic,strong) NSString *displayName;
@property (nonatomic,strong) NSString *value;
@property (nonatomic,retain) NSString *updateTime;

+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end


@interface ZFUserInfoData : KADataBase
@property (nonatomic,assign) NSInteger userID;
@property (nonatomic,assign) NSInteger storeID;
@property (nonatomic,assign) NSInteger job;
@property (nonatomic,assign) NSInteger otherJob;
@property (nonatomic,strong) NSString *nickName;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *homePhone;
@property (nonatomic,strong) NSString *updateTime;
@property (nonatomic,strong) NSString *firstName;
@property (nonatomic,strong) NSString *lastName;
@property (nonatomic,strong) NSString *gender;

+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end

@interface ZFNetServerServiceTimeData : KADataBase


@property (nonatomic,strong) NSString *updateTime;
@property (nonatomic,strong) NSString *value;


+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end

@interface ZFNetServerTestTimeData : KADataBase


@property (nonatomic,strong) NSString *updateTime;
@property (nonatomic,strong) NSString *value;


+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end
typedef NSMutableArray ExhibitionMutableArray;
@interface ZFNetServerCityExhibitionData : KADataBase

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *cityid;
@property (nonatomic,strong) NSString *updateTime;
@property (nonatomic,retain) ExhibitionMutableArray *exhibitionArray;

+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end

@interface ZFNetServerExhibitionData : KADataBase

@property (nonatomic,assign) NSInteger exhibitionID;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *location;
+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;
@end

typedef NSMutableArray ZFUserMutableArray;
@interface ZFUserResponseData : ZFResponseDataBase

@property(nonatomic, strong)ZFUserInfoData *userDataArray;
@property(nonatomic, strong)NSString *totlePage;

+ (id)dataWithJSON:(id)JSON;
+ (ZFUserInfoData *)dataForTesting;

@end

@interface ZFSchemeData : KADataBase

@property int LoanPeriod;
@property float LoanRate;
@property float LoanInterestRate;
@property int LeasingPeriod;
@property int LeasingDeposit;
@property float LeasingDepositRate;
@property float LeasingInterestRate;
+ (id)dataWithJSON:(id)JSON;
@end

