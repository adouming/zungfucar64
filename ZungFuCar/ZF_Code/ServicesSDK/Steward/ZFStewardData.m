//
//  ZFStewardData.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import "ZFStewardData.h"

@implementation ZFStewardData



@end


#pragma mark - ZFNetServer

@implementation ZFNetServerStoreResponseData

+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON];
}
+ (ZFNetServerStoreMutableArray *)dataForTesting {
    
    ZFNetServerStoreMutableArray *data = [ZFNetServerStoreMutableArray array];
    for (int i = 0; i<5; i++) {
        [data addObject:[ZFNetServerStoreData dataForTesting]];
    }
    return data;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        self.totlePage = [self.data objectForKey:@"TotalPages"];
        NSArray *data = [self.data arrayForKey:@"List"];
        NSMutableArray *temData = [NSMutableArray array];
        
        for (id json in data) {
            ZFNetServerStoreData *objectData = [ZFNetServerStoreData dataWithJSON:json];
            [temData addObject:objectData];
        }
        
        self.storeDataArray = temData;
    }
    
    return self;
}
@end

@implementation ZFNetServerStoreData
static float i =0.0;
+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        self.storeID = i++;
        self.latitude = [NSString stringWithFormat:@"%f",24.138727+i/1000];
        self.longtitude = [NSString stringWithFormat:@"%f",120.713827+i/1000];
        self.location = [NSString stringWithFormat:@"testName%f",i];
        self.displayOrder = 1;
        self.name = [NSString stringWithFormat:@"testName%f",i];
        self.shortDesc = [NSString stringWithFormat:@"testShortDesc-testShortDesc---%f",i];
        self.descp = @"testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc-testDesc";
        self.address = @"AddressAddressAddressAddress";
        self.phone = @"13800138000";
    
        self.updatedTime = @"2013-10-02 01:01:01";
        if (i > 20) {
            i=0;
        }    
    }
    return self;
}
-(NSString *)dateToStr:(NSString *)_strDate{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[_strDate doubleValue]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
   
        NSLog(@"网络服务---》%@",JSON);
        self.storeID = [[JSON objectForKey:@"ID"] integerValue];
        self.location = [JSON stringForKey:@"Location"];
        self.displayOrder = [[JSON objectForKey:@"DisplayOrder"] integerValue];

        NSRange range = [[JSON stringForKey:@"Name"] rangeOfString: @"<br/>"];
        if (range.location == NSNotFound) {
            self.name = [JSON stringForKey:@"Name"];
        } else {
            self.name = [[JSON stringForKey:@"Name"] stringByReplacingCharactersInRange:range withString:@""];
        }
        
        self.shortDesc = [JSON stringForKey:@"ShortDesc"];
        self.descp = [JSON stringForKey:@"Descp"];
        
        range = [[JSON stringForKey:@"Address"] rangeOfString: @"<br/>"];
        if (range.location == NSNotFound) {
            self.address = [JSON stringForKey:@"Address"];
        } else {
            self.address = [[JSON stringForKey:@"Address"] stringByReplacingCharactersInRange:range withString:@""];
        }
        
        self.phone = [JSON stringForKey:@"Phone"];
        self.sImageUrl = [JSON stringForKey:@"SImageUrl"];
        self.bImageUrl = [JSON stringForKey:@"BImageUrl"];
        self.mservice = [JSON stringForKey:@"MService"];
        self.emergencyPhone = [JSON stringForKey:@"Emergency"];
        self.updatedTime = [JSON stringForKey:@"UpdatedTime"];
        self.serverPhone = [JSON stringForKey:@"EmPhone"];
        self.sroom = [JSON stringForKey:@"SRoom"];
        self.fax = [JSON stringForKey:@"Fax"];
        self.contactEmail = [JSON stringForKey:@"ContactEmail"];
        self.latitude = [JSON stringForKey:@"Latitude"];
        self.longtitude = [JSON stringForKey:@"Longitude"];
        self.hasService = [JSON stringForKey:@"HasService"];
        self.hasTestDrive = [JSON stringForKey:@"HasTestDrive"];
        self.url = [JSON stringForKey:@"URL"];
        self.serviceURL = [JSON stringForKey:@"ServiceURL"];
        
    }
    
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:[NSString stringWithFormat:@"%d",_storeID] forKey:@"storeID"];
        [coder encodeObject:[NSString stringWithFormat:@"%@",_location] forKey:@"Location"];
        [coder encodeObject:[NSString stringWithFormat:@"%d",_displayOrder] forKey:@"DisplayOrder"];
        [coder encodeObject:_name forKey:@"Name"];
        [coder encodeObject:_shortDesc forKey:@"ShortDesc"];
        [coder encodeObject:_descp forKey:@"Descp"];
        [coder encodeObject:_address forKey:@"Address"];
        [coder encodeObject:_phone forKey:@"Phone"];
        [coder encodeObject:_bImageUrl forKey:@"Banner"];
        [coder encodeObject:_sImageUrl forKey:@"Icon"];
        [coder encodeObject:_mservice forKey:@"Service"];
        [coder encodeObject:_url forKey:@"URL"];
        [coder encodeObject:_updatedTime forKey:@"UpdatedTime"];
        [coder encodeObject:_latitude forKey:@"latitude"];
        [coder encodeObject:_longtitude forKey:@"longtitude"];
        
        [coder encodeObject:_hasService forKey:@"hasService"];
        [coder encodeObject:_hasTestDrive forKey:@"hasTestDrive"];
        [coder encodeObject:_contactEmail forKey:@"contactEmail"];
        [coder encodeObject:_fax forKey:@"fax"];
        [coder encodeObject:_sroom forKey:@"sroom"];
        [coder encodeObject:_emergencyPhone forKey:@"emergencyPhone"];
        [coder encodeObject:_serverPhone forKey:@"serverPhone"];
        [coder encodeObject:_serviceURL forKey:@"ServiceURL"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _storeID = [[coder decodeObjectForKey:@"storeID"] integerValue];
        _location = [coder decodeObjectForKey:@"Location"];
        _displayOrder = [[coder decodeObjectForKey:@"DisplayOrder"] integerValue];
        _name = [coder decodeObjectForKey:@"Name"];
        _shortDesc = [coder decodeObjectForKey:@"ShortDesc"];
        _descp = [coder decodeObjectForKey:@"Descp"];
        _address = [coder decodeObjectForKey:@"Address"];
        _phone = [coder decodeObjectForKey:@"Phone"];
        _bImageUrl = [coder decodeObjectForKey:@"Banner"];
        _sImageUrl = [coder decodeObjectForKey:@"Icon"];
        _mservice = [coder decodeObjectForKey:@"Service"];
        _url = [coder decodeObjectForKey:@"URL"];
        _updatedTime = [coder decodeObjectForKey:@"UpdatedTime"];
        _latitude = [coder decodeObjectForKey:@"latitude"];
        _longtitude = [coder decodeObjectForKey:@"longtitude"];
        _serverPhone = [coder decodeObjectForKey:@"serverPhone"];
        _hasService = [coder decodeObjectForKey:@"hasService"];
        _hasTestDrive = [coder decodeObjectForKey:@"hasTestDrive"];
        _contactEmail = [coder decodeObjectForKey:@"contactEmail"];
        _fax = [coder decodeObjectForKey:@"fax"];
        _sroom = [coder decodeObjectForKey:@"sroom"];
        _emergencyPhone = [coder decodeObjectForKey:@"emergencyPhone"];
        _serviceURL = [coder decodeObjectForKey:@"ServiceURL"];
    }
    
    return self;
}


@end


@implementation ZFNetServerReserveResponseData

+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON ];
}
+ (ZFReserveDataDictionary *)dataForTesting{
    
    ZFReserveDataDictionary *dataDic = [ZFReserveDataDictionary dictionary];


    return dataDic;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        self.totlePage = [self.data objectForKey:@"TotalPages"];
        
        NSDictionary *data = (NSDictionary *)[self.data objectForKey:@"List"];
        NSArray *serviceTimeData = [data arrayForKey:@"ServiceTime"];
        NSArray *testTimeData = [data arrayForKey:@"TestTime"];
        NSArray *jobData = [data arrayForKey:@"Job"];
        NSArray *industryData = [data arrayForKey:@"Industry"];
        
        NSMutableDictionary *dataDic = [NSMutableDictionary dictionary];
        if (serviceTimeData) {
            NSMutableArray *temData = [NSMutableArray array];
            for (id json in serviceTimeData) {
                ZFNetServerServiceTimeData *objectData = [ZFNetServerServiceTimeData dataWithJSON:json];
                [temData addObject:objectData];
            }
            [dataDic setObject:temData forKey:@"ServiceTime"];
        }
        if (testTimeData) {
            NSMutableArray *temData = [NSMutableArray array];
            for (id json in testTimeData) {
                ZFNetServerTestTimeData *objectData = [ZFNetServerTestTimeData dataWithJSON:json];
                [temData addObject:objectData];
            }
            [dataDic setObject:temData forKey:@"TestTime"];
        }
        if (jobData) {
            NSMutableArray *temData = [NSMutableArray array];
            for (id json in jobData) {
                ZFNetServerJobData *objectData = [ZFNetServerJobData dataWithJSON:json];
                [temData addObject:objectData];
            }
            [dataDic setObject:temData forKey:@"Job"];
        }
        if (industryData) {
            NSMutableArray *temData = [NSMutableArray array];
            for (id json in industryData) {
                ZFNetServerIndustryData *objectData = [ZFNetServerIndustryData dataWithJSON:json];
                [temData addObject:objectData];
            }
            [dataDic setObject:temData forKey:@"Industry"];
        }

        self.reserveDataDic = dataDic;
    }
    
    return self;
}

@end

@implementation ZFUserResponseData


+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON];
}
+ (ZFUserInfoData *)dataForTesting {
    
    ZFUserInfoData *data = [ZFUserInfoData dataForTesting];//[ZFUserMutableArray array];
//    for (int i = 0; i<5; i++) {
//        [data addObject:[ZFUserInfoData dataForTesting]];
//    }
    return data;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        self.totlePage = [self.data objectForKey:@"TotalPages"];
        NSDictionary *list = [self.data objectForKey:@"List"];
//        NSMutableArray *temData = [NSMutableArray array];
//        for (id json in list) {
            ZFUserInfoData *objectData = [ZFUserInfoData dataWithJSON:list];
//            [temData addObject:objectData];
//        }
        self.userDataArray = objectData;
    }
    return self;
}

@end

@implementation ZFNetServerCityExhibitionResponseData
+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON ];
}
+ (ZFReserveDataDictionary *)dataForTesting{
    
    ZFReserveDataDictionary *dataDic = [ZFReserveDataDictionary dictionary];
    
    
    return dataDic;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        self.cityExhibitionDataArray = self.data;
        self.totlePage = [NSString stringWithFormat:@"%d", [self.cityExhibitionDataArray count] ] ;
        
         
    }
    return self;
}

@end

@implementation ZFNetServerExhibitionData

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
    }
    return self;
}

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        
        self.exhibitionID = [[JSON objectForKey:@"ID"] integerValue];
        self.location = [JSON stringForKey:@"Location"];
        NSRange range = [[JSON stringForKey:@"Name"] rangeOfString: @"<br/>"];
        if (range.location == NSNotFound) {
            self.name = [JSON stringForKey:@"Name"];
        } else {
            self.name = [[JSON stringForKey:@"Name"] stringByReplacingCharactersInRange:range withString:@""];
        }
        
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:[NSString stringWithFormat:@"%d",_exhibitionID] forKey:@"exhibitionID"];
        [coder encodeObject:_name forKey:@"Name"];
        [coder encodeObject:_location forKey:@"Location"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _exhibitionID = [[coder decodeObjectForKey:@"exhibitionID"] integerValue];
        _name = [coder decodeObjectForKey:@"Name"];
        _location = [coder decodeObjectForKey:@"Location"];
    }
    
    return self;
}


@end

@implementation ZFNetServerCarTypeResponseData


+ (id)dataWithJSON:(id)JSON withCarType:(ZFCarType)carType{
    return [[self alloc] initWithJSON:JSON withCarType:(ZFCarType)carType];
}
+ (ZFNetServerCarTypeDictionary *)dataForTesting {
    
    ZFNetServerCarTypeDictionary *data = [ZFNetServerCarTypeDictionary dictionary];
//    for (int i = 0; i<5; i++) {
//        [data addObject:[ZFNetServerCarTypeData dataForTesting]];
//    }
    return data;
}
- (id)initWithJSON:(id)JSON withCarType:(ZFCarType)carType {
    if (self = [super initWithJSON:JSON]) {
        self.totlePage = [NSString stringWithFormat:@"%d", [self.data count]];
        //NSArray *data = [self.data];
        NSMutableArray *temData = [NSMutableArray array];
        for (id dict in self.data) {
            ZFNetServerCarTypeData *objectData = dict;// [ZFNetServerCarTypeData dataWithJSON:json];
            [temData addObject:objectData];
        }

        self.carTypeDataDic = [NSDictionary dictionaryWithObject:temData forKey:[NSString stringWithFormat:@"%d",carType]];
 
    }
    
    return self;
}


@end
@implementation ZFNetServerCarTypeData : KADataBase
+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
  
    }
    return self;
}

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        
        self.carID = [[JSON objectForKey:@"ID"] integerValue];
        self.name = [JSON stringForKey:@"Name"];
        NSArray *data = [JSON objectForKey:@"Product"];
        NSMutableArray *temData = [NSMutableArray array];
        for (id json in data) {
            ZFNetServerCarProductData *objectData = [ZFNetServerCarProductData dataWithJSON:json];
            [temData addObject:objectData];
        }
        self.productArray = temData;
//        self.displayOrder = [[JSON objectForKey:@"DisplayOrder"] integerValue];
//        self.shortDesc = [JSON stringForKey:@"ShortDesc"];
//        self.descp = [JSON stringForKey:@"Descp"];
//        self.banner = [JSON stringForKey:@"Banner"];
//        self.updatedTime = [JSON objectForKey:@"UpdatedTime"];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:[NSString stringWithFormat:@"%d",_carID] forKey:@"carID"];
        [coder encodeObject:_productArray forKey:@"product"];
        [coder encodeObject:_name forKey:@"Name"];
//        [coder encodeObject:[NSString stringWithFormat:@"%d",_displayOrder] forKey:@"DisplayOrder"];
//        [coder encodeObject:_shortDesc forKey:@"ShortDesc"];
//        [coder encodeObject:_descp forKey:@"Descp"];
//        [coder encodeObject:_banner forKey:@"Banner"];
//        [coder encodeObject:_updatedTime forKey:@"UpdatedTime"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
  
        _carID = [[coder decodeObjectForKey:@"carID"] integerValue];
        _productArray = [coder decodeObjectForKey:@"product"];
        _name = [coder decodeObjectForKey:@"Name"];
//        _displayOrder = [[coder decodeObjectForKey:@"DisplayOrder"] integerValue];
//        _shortDesc = [coder decodeObjectForKey:@"ShortDesc"];
//        _descp = [coder decodeObjectForKey:@"Descp"];
//        _banner = [coder decodeObjectForKey:@"Banner"];
//        _updatedTime = [coder decodeObjectForKey:@"UpdatedTime"];
    }
    
    return self;
}
@end

@implementation ZFNetServerCarProductData

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
    }
    return self;
}

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        
        self.carModelID = [[JSON objectForKey:@"ID"] integerValue];
        self.name = [JSON stringForKey:@"Name"];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:[NSString stringWithFormat:@"%d",_carModelID] forKey:@"carModelID"];
        [coder encodeObject:_name forKey:@"Name"];
        
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _carModelID = [[coder decodeObjectForKey:@"carModelID"] integerValue];
        _name = [coder decodeObjectForKey:@"Name"];
        
    }
    
    return self;
}


@end

@implementation ZFNetServerIndustryData : KADataBase

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
    }
    return self;
}

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        
        self.displayName = [JSON stringForKey:@"DisplayText"];
        self.value = [JSON stringForKey:@"Value"];
        self.updateTime = [JSON stringForKey:@"UpdatedTime"];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:_displayName forKey:@"DisplayText"];
        [coder encodeObject:_value forKey:@"Value"];
        [coder encodeObject:_updateTime forKey:@"UpdatedTime"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _displayName = [coder decodeObjectForKey:@"DisplayText"];
        _value = [coder decodeObjectForKey:@"Value"];
        _updateTime = [coder decodeObjectForKey:@"UpdatedTime"];
    }
    
    return self;
}


@end

@implementation ZFNetServerJobData : KADataBase

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
    }
    return self;
}

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        
        self.displayName = [JSON stringForKey:@"DisplayText"];
        self.value = [JSON stringForKey:@"Value"];
        self.updateTime = [JSON stringForKey:@"UpdatedTime"];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:_displayName forKey:@"DisplayText"];
        [coder encodeObject:_value forKey:@"Value"];
        [coder encodeObject:_updateTime forKey:@"UpdatedTime"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _displayName = [coder decodeObjectForKey:@"DisplayText"];
        _value = [coder decodeObjectForKey:@"Value"];
        _updateTime = [coder decodeObjectForKey:@"UpdatedTime"];
    }
    
    return self;
}


@end


@implementation ZFUserInfoData : KADataBase

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
    }
    return self;
}

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        /**判断各个字段返回值是否为数字，防止转为数字出错**/
        NSString *regex = @"^[0-9]*$";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        
        if ([predicate evaluateWithObject:[JSON objectForKey:@"ID"]] == YES) {
            self.userID = [[JSON objectForKey:@"ID"] integerValue];
        } else {
            self.userID = 0;
        }
        if ([predicate evaluateWithObject:[JSON objectForKey:@"StoreID"]] == YES) {
            self.storeID = [[JSON objectForKey:@"StoreID"] integerValue];
        } else {
            self.storeID = 0;
        }
        if ([predicate evaluateWithObject:[JSON objectForKey:@"Job"]] == YES) {
            self.job = [[JSON objectForKey:@"Job"] integerValue];
        } else {
            self.job = 0;
        }
        if ([predicate evaluateWithObject:[JSON objectForKey:@"OtherJob"]] == YES) {
            self.otherJob = [[JSON objectForKey:@"OtherJob"] integerValue];
        } else {
            self.otherJob = 0;
        }
        /*******************************************/
        self.nickName = [JSON objectForKey:@"NickName"];
        self.email = [JSON objectForKey:@"Email"];
        self.homePhone = [JSON objectForKey:@"HomePhone"];
        self.firstName = [JSON objectForKey:@"FirstName"];
        self.lastName = [JSON objectForKey:@"LastName"];
        self.gender = [JSON objectForKey:@"Gender"];
        self.updateTime = [JSON objectForKey:@"UpdatedTime"];
        
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:[NSString stringWithFormat:@"%d",_userID] forKey:@"userID"];
        [coder encodeObject:[NSString stringWithFormat:@"%d",_storeID] forKey:@"storeID"];
        [coder encodeObject:[NSString stringWithFormat:@"%d",_job] forKey:@"job"];
        [coder encodeObject:[NSString stringWithFormat:@"%d",_otherJob] forKey:@"industry"];
        [coder encodeObject:_nickName forKey:@"nickName"];
        [coder encodeObject:_email forKey:@"email"];
        [coder encodeObject:_homePhone forKey:@"cellphone"];
        
        [coder encodeObject:_firstName forKey:@"firstName"];
        [coder encodeObject:_lastName forKey:@"lastName"];
        [coder encodeObject:_gender forKey:@"gender"];
        [coder encodeObject:_updateTime forKey:@"updateTime"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _userID = [[coder decodeObjectForKey:@"userID"] integerValue];
        _storeID = [[coder decodeObjectForKey:@"storeID"] integerValue];
        _job = [[coder decodeObjectForKey:@"job"] integerValue];
        _otherJob = [[coder decodeObjectForKey:@"industry"] integerValue];
        _nickName = [coder decodeObjectForKey:@"nickName"];
        _email = [coder decodeObjectForKey:@"email"];
        _homePhone = [coder decodeObjectForKey:@"cellphone"];
        _firstName = [coder decodeObjectForKey:@"firstName"];
        _lastName = [coder decodeObjectForKey:@"lastName"];
        _gender = [coder decodeObjectForKey:@"gender"];
        _updateTime = [coder decodeObjectForKey:@"updateTime"];
        
    }
    
    return self;
}
@end


@implementation ZFNetServerCityExhibitionData : KADataBase

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
    }
    return self;
}

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        
        self.name = [JSON stringForKey:@"DisplayText"];
        self.cityid = [JSON stringForKey:@"Value"];
        self.updateTime = [JSON stringForKey:@"UpdatedTime"];
        NSArray *data = [JSON objectForKey:@"Store"];
        NSMutableArray *temData = [NSMutableArray array];
        for (id json in data) {
            ZFNetServerExhibitionData *objectData = [ZFNetServerExhibitionData dataWithJSON:json];
            [temData addObject:objectData];
        }
        self.exhibitionArray = temData;
       
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:_name forKey:@"DisplayText"];
        [coder encodeObject:_cityid forKey:@"Value"];
        [coder encodeObject:_updateTime forKey:@"updateTime"];
        [coder encodeObject:_exhibitionArray forKey:@"exhibition"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _name = [coder decodeObjectForKey:@"DisplayText"];
        _cityid = [coder decodeObjectForKey:@"Value"];
        _updateTime = [coder decodeObjectForKey:@"updateTime"];
        _exhibitionArray = [coder decodeObjectForKey:@"exhibition"];
    }
    
    return self;
}


@end

@implementation ZFNetServerServiceTimeData : KADataBase

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
    }
    return self;
}

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        
        self.updateTime = [JSON stringForKey:@"UpdatedTime"];
        self.value = [JSON stringForKey:@"Value"];
        
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:_updateTime forKey:@"UpdatedTime"];
        [coder encodeObject:_value forKey:@"Value"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _updateTime = [coder decodeObjectForKey:@"UpdatedTime"];
        _value = [coder decodeObjectForKey:@"Value"];
    }
    
    return self;
}


@end

@implementation ZFNetServerTestTimeData : KADataBase

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
    }
    return self;
}

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        
        self.updateTime = [JSON stringForKey:@"UpdatedTime"];
        self.value = [JSON stringForKey:@"Value"];
        
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:_updateTime forKey:@"UpdatedTime"];
        [coder encodeObject:_value forKey:@"Value"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _updateTime = [coder decodeObjectForKey:@"UpdatedTime"];
        _value = [coder decodeObjectForKey:@"Value"];
    }
    
    return self;
}
@end




@implementation ZFNetServerCarModelResponseData


+ (id)dataWithJSON:(id)JSON withCarID:(NSUInteger)idStr{
    return [[self alloc] initWithJSON:JSON withCarID:idStr];
}
+ (ZFNetServerCarModelDictionary *)dataForTesting {
    
    ZFNetServerCarModelDictionary *data = [ZFNetServerCarModelDictionary dictionary];
    return data;
}
- (id)initWithJSON:(id)JSON withCarID:(NSUInteger)carID {
    if (self = [super initWithJSON:JSON]) {
//        self.totlePage = [self.data objectForKey:@"TotalPages"];
//        NSArray *data = [self.data arrayForKey:@"List"];
//        NSMutableArray *temData = [NSMutableArray array];
//        for (id json in data) {
//            ZFNetServerCarTypeData *objectData = [ZFNetServerCarTypeData dataWithJSON:json];
//            [temData addObject:objectData];
//        }
        
//        self.carModelDataDic = [NSDictionary dictionaryWithObject:temData forKey:[NSString stringWithFormat:@"%d",carID]];
  
        
    }
    
    return self;
}


@end


@implementation ZFNetServerCarModelData : KADataBase


+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
    }
    return self;
}

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        
        self.carModelID = [[JSON objectForKey:@"ID"] integerValue];
        self.displayOrder = [[JSON objectForKey:@"DisplayOrder"] integerValue];
        self.name = [JSON stringForKey:@"Name"];
        self.price = [JSON stringForKey:@"Price"];
        self.promoImageURL = [JSON stringForKey:@"PromoImageURL"];
        self.updatedTime = [JSON objectForKey:@"UpdatedTime"];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:[NSString stringWithFormat:@"%d",_carModelID] forKey:@"carModelID"];
        [coder encodeObject:[NSString stringWithFormat:@"%d",_displayOrder] forKey:@"DisplayOrder"];
        [coder encodeObject:_name forKey:@"Name"];
        [coder encodeObject:_price forKey:@"Price"];
        [coder encodeObject:_promoImageURL forKey:@"PromoImageURL"];
        [coder encodeObject:_updatedTime forKey:@"UpdatedTime"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        
        _carModelID = [[coder decodeObjectForKey:@"carModelID"] integerValue];
        _displayOrder = [[coder decodeObjectForKey:@"DisplayOrder"] integerValue];
        _name = [coder decodeObjectForKey:@"Name"];
        _price = [coder decodeObjectForKey:@"Price"];
        _promoImageURL = [coder decodeObjectForKey:@"PromoImageURL"];
        _updatedTime = [coder decodeObjectForKey:@"UpdatedTime"];
    }
    
    return self;
}
@end


@implementation ZFSchemeData : KADataBase

+ (id)dataWithJSON:(id)JSON
{
    return [[self alloc] initWithJSON:JSON];
}

- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        
        self.LoanPeriod = [JSON objectForKey:@"LoanPeriod"] ? [[JSON objectForKey:@"LoanPeriod"] integerValue] : 0;
        self.LoanRate = [JSON objectForKey:@"LoanRate"] ? [[JSON objectForKey:@"LoanRate"] floatValue] : 0;
        self.LoanInterestRate = [JSON stringForKey:@"LoanInterestRate"] ? [[JSON stringForKey:@"LoanInterestRate"] floatValue] : 0;
        self.LeasingPeriod = [JSON stringForKey:@"LeasingPeriod"] ? [[JSON stringForKey:@"LeasingPeriod"] integerValue] : 0;
        self.LeasingDeposit = [JSON stringForKey:@"LeasingDeposit"] ? [[JSON stringForKey:@"LeasingDeposit"] integerValue] : 0;
        self.LeasingDepositRate = [JSON objectForKey:@"LeasingDepositRate"] ? [[JSON objectForKey:@"LeasingDepositRate"] floatValue] : 0;
        self.LeasingInterestRate = [JSON objectForKey:@"LeasingInterestRate"] ? [[JSON objectForKey:@"LeasingInterestRate"] floatValue] : 0;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:[NSString stringWithFormat:@"%d", self.LoanPeriod] forKey:@"LoanPeriod"];
        [coder encodeObject:[NSString stringWithFormat:@"%.4f", self.LoanRate] forKey:@"LoanRate"];
        [coder encodeObject:[NSString stringWithFormat:@"%.4f", self.LoanInterestRate] forKey:@"LoanInterestRate"];
        [coder encodeObject:[NSString stringWithFormat:@"%d", self.LeasingPeriod] forKey:@"LeasingPeriod"];
        [coder encodeObject:[NSString stringWithFormat:@"%d", self.LeasingDeposit] forKey:@"LeasingDeposit"];
        [coder encodeObject:[NSString stringWithFormat:@"%.4f", self.LeasingDepositRate] forKey:@"LeasingDepositRate"];
        [coder encodeObject:[NSString stringWithFormat:@"%.4f", self.LeasingInterestRate] forKey:@"LeasingInterestRate"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        
        self.LoanPeriod = [[coder decodeObjectForKey:@"LoanPeriod"] integerValue];
        self.LoanRate = [[coder decodeObjectForKey:@"LoanRate"] floatValue];
        self.LoanInterestRate = [[coder decodeObjectForKey:@"LoanInterestRate"] floatValue];
        self.LeasingPeriod = [[coder decodeObjectForKey:@"LeasingPeriod"] integerValue];
        self.LeasingDeposit = [[coder decodeObjectForKey:@"LeasingDeposit"] integerValue];
        self.LeasingDepositRate = [[coder decodeObjectForKey:@"LeasingDepositRate"] floatValue];
        self.LeasingInterestRate = [[coder decodeObjectForKey:@"LeasingInterestRate"] floatValue];
    }
    
    return self;
}

@end