//
//  ZFStewardManager.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import "ZFStewardManager.h"
#import "AlexMacro.h"

@implementation ZFStewardManager

static ZFStewardManager *shared_manager = nil;

+ (ZFStewardManager *)sharedManager {
    static dispatch_once_t pred;
	dispatch_once(&pred, ^{
        shared_manager = [[self alloc] init];
    });
	return shared_manager;
}


- (void)getStoreWithPage:(NSString *)page andPageSize:(NSString *)pageSize
                 Success:(void (^)(ZFNetServerStoreMutableArray *data))success
                 failure:(void (^)(NSError *error))failure{
    [[ZFStewardClient sharedClient] getStoreClientWithPage:page andPageSize:pageSize Success:^(ZFNetServerStoreMutableArray *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
    
}
- (void)getCarTypeWithType:(NSString *)type
                   Success:(void (^)(ZFNetServerCarTypeDictionary *data))success
                   failure:(void (^)(NSError *error))failure{
    [[ZFStewardClient sharedClient] getCarTypeClientWithType:type Success:^(ZFNetServerCarTypeDictionary *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)getCarModelWithID:(NSUInteger )idStr
                  Success:(void (^)(ZFNetServerCarTypeDictionary *data))success
                  failure:(void (^)(NSError *error))failure{
    [[ZFStewardClient sharedClient] getCarModelWithID:idStr Success:^(ZFNetServerCarTypeDictionary *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)getIndustrySuccess:(void (^)(ZFNetServerIndustryData *data))success
                   failure:(void (^)(NSError *error))failure{
    [[ZFStewardClient sharedClient] getIndustryClientSuccess:^(ZFNetServerIndustryData *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)getJobSuccess:(void (^)(ZFReserveDataDictionary *data))success
              failure:(void (^)(NSError *error))failure{
    [[ZFStewardClient sharedClient] getJobClientSuccess:^(ZFReserveDataDictionary *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
-(void)getReserveInfoSucess:(void(^)(ZFReserveDataDictionary *data))success
                    failure:(void(^)(NSError *error))failure{
    [[ZFStewardClient sharedClient] getReserveInfoClientSucess:^(ZFReserveDataDictionary *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
- (void)getPersonInfoWithUserID:(NSString *)userID
                        Success:(void (^)(ZFUserInfoData *data))success
                        failure:(void (^)(NSError *error))failure{
    [[ZFStewardClient sharedClient] getPersonInfoClientWithUserID:userID Success:^(ZFUserInfoData *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
-(void)postBookingWithInfo:(NSDictionary *)paraDic Success:(void (^)(ZFResponseDataBase *response))success
                   failure:(void (^)(NSError *error))failure{
    [[ZFStewardClient sharedClient] postBookingClientWithInfo:paraDic Success:^(ZFResponseDataBase *response) {
        success(response);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
-(void)getExhibitionSucess:(void(^)(ZFReserveCityExhibitionMutableArray *data))success
                   failure:(void(^)(NSError *error))failure  cid:(int)cityID type:(ReserveType)type{
    [[ZFStewardClient sharedClient] getExhibitionClientSucess:^(ZFReserveCityExhibitionMutableArray *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }  cid: cityID type:type];
}
-(void)getCitySucess:(void(^)(ZFReserveCityExhibitionMutableArray *data))success
                       failure:(void(^)(NSError *error))failure{
    [[ZFStewardClient sharedClient] getCityClientSucess:^(ZFReserveCityExhibitionMutableArray *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
-(void)getSchemeWithCase:(NSString *)scheme
                andCarID:(NSString *)ID
                 Success:(void (^)(NSMutableArray *data))success
                  failue:(void (^)(NSError *error))failure
{
    [[ZFStewardClient sharedClient] getSchemeWithScheme:scheme carID:ID Success:^(NSMutableArray *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
    
}
@end
