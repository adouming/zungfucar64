//
//  ZFStewardClient.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import "ZFStewardClient.h"
#import "AlexMacro.h"
@implementation ZFStewardClient


+ (id)sharedClient {
	static dispatch_once_t pred;
    static ZFStewardClient *shared_instance = nil;
	
    dispatch_once(&pred, ^{
		shared_instance = [[self alloc] init];
        shared_instance.client = [self sharedHTTPClient];
    });
    
	return shared_instance;
}

- (void)getStoreClientWithPage:(NSString *)page andPageSize:(NSString *)pageSize
                       Success:(void (^)(ZFNetServerStoreMutableArray *data))success
                       failure:(void (^)(NSError *error))failure{
    if (USE_TEST_DATA_MODE) {
        ZFNetServerStoreMutableArray *responseData =[ZFNetServerStoreResponseData dataForTesting];
        success(responseData);
        return;
    }
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_NETWORK_STORE];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   ZF_API_PAGE,@"page",
                                   ZF_API_PAGESIZE,@"pageSize",
                                   nil];
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // 3.1 Parser json dictionary to data object
        ZFNetServerStoreResponseData *responseData =[ZFNetServerStoreResponseData dataWithJSON:responseObject];
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode integerValue];
        if (returnCode == API_Sucess_Code) {
            success(responseData.storeDataArray);
        }else{
            [[ZFStewardClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
    
}

-(void)getExhibitionClientSucess:(void(^)(ZFReserveCityExhibitionMutableArray *data))success
                   failure:(void(^)(NSError *error))failure
                             cid:(int)cityID
                            type:(ReserveType)type{
    if (USE_TEST_DATA_MODE) {
        //        ZFReserveCityExhibitionMutableArray *responseData =[ZFNetServerCityExhibitionResponseData dataForTesting];
        //        success(responseData);
        return;
    }
    NSString* path0;
    
    if(type == ReserveTypeTest)  {//试驾
        path0 = [NSString stringWithFormat:@"%@%d", ZF_PATH_EXHIBITION, cityID]; //试驾
    }
    else if(type == ReserveTypeService) {//维修
        path0 = [NSString stringWithFormat:@"%@%d", ZF_PATH_SERVICE, cityID];
    }
    
    NSString *path = [ZFAPIClient getFullPathFromPath:path0];
    
    [self.client GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", operation);
        // 3.1 Parser json dictionary to data object
        ZFNetServerCityExhibitionResponseData *responseData =[ZFNetServerCityExhibitionResponseData dataWithJSON:responseObject];
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode integerValue];
        if (returnCode == API_Sucess_Code) {
            success(responseData.cityExhibitionDataArray);
        }else{
            [[ZFStewardClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
        
    }];
    
}

-(void)getCityClientSucess:(void(^)(ZFReserveCityExhibitionMutableArray *data))success
                             failure:(void(^)(NSError *error))failure{
    if (USE_TEST_DATA_MODE) {
        //        ZFReserveCityExhibitionMutableArray *responseData =[ZFNetServerCityExhibitionResponseData dataForTesting];
        //        success(responseData);
        return;
    }
    
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_CITY];
    
    [self.client GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", operation);
        // 3.1 Parser json dictionary to data object
        ZFNetServerCityExhibitionResponseData *responseData =[ZFNetServerCityExhibitionResponseData dataWithJSON:responseObject];
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode integerValue];
        if (returnCode == API_Sucess_Code) {
            success(responseData.cityExhibitionDataArray);
        }else{
            [[ZFStewardClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
        
    }];
    
}
-(void)getReserveInfoClientSucess:(void(^)(ZFReserveDataDictionary *data))success
                          failure:(void(^)(NSError *error))failure{
    if (USE_TEST_DATA_MODE) {
        ZFReserveDataDictionary *responseData =[ZFNetServerReserveResponseData dataForTesting];
        success(responseData);
        return;
    }
    
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_GET_BOOKINFO];
    
    [self.client GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        // 3.1 Parser json dictionary to data object
        ZFNetServerReserveResponseData *responseData =[ZFNetServerReserveResponseData dataWithJSON:responseObject];
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode integerValue];
        if (returnCode == API_Sucess_Code) {
            success(responseData.reserveDataDic);
        }else{
            [[ZFStewardClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}
- (void)getCarTypeClientWithType:(NSString *)type
                         Success:(void (^)(ZFNetServerCarTypeDictionary *data))success
                         failure:(void (^)(NSError *error))failure{
    ZFCarType cartype = (ZFCarType)[type intValue];
    if (USE_TEST_DATA_MODE) {
        //        ZFNetServerCarTypeDictionary *responseData =[ZFNetServerCarTypeResponseData dataForTesting];
        //        success(responseData);
        return;
    }
    //    NSString *currentTime = [ZFAPIClient currentDateTime];
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_CAR_TYPEMODEL];
    
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  //                                  currentTime,@"date",
                                  type,@"category",
                                  //                                  ZF_API_PAGESIZE,@"pageSize",
                                  nil];
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // 3.1 Parser json dictionary to data object
        ZFNetServerCarTypeResponseData *responseData =[ZFNetServerCarTypeResponseData dataWithJSON:responseObject withCarType:cartype ];
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode integerValue];
        if (returnCode == API_Sucess_Code) {
            //NSLog(@"%@",responseData.carTypeDataDic);
            success(responseData.carTypeDataDic);
        }else{
            [[ZFStewardClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

/*获取车型*/
- (void)getCarModelWithID:(NSUInteger )idStr
                  Success:(void (^)(ZFNetServerCarTypeDictionary *data))success
                  failure:(void (^)(NSError *error))failure{
    
    // ZFCarType cartype = (ZFCarType)[type intValue];
    
    //    NSString *currentTime = [ZFAPIClient currentDateTime];
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_CARMODEL_TYPE];
    NSString* url = [NSString stringWithFormat:@"%@%d",path, idStr];
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%d",idStr],@"productid",
                                  nil];
    [self.client GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // 3.1 Parser json dictionary to data object
        ZFNetServerCarModelResponseData *responseData =[ZFNetServerCarModelResponseData dataWithJSON:responseObject withCarID:idStr ];
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode integerValue];
        if (returnCode == API_Sucess_Code) {
            NSLog(@"%@",responseData.data);
            success(responseData.data);
        }else{
            [[ZFStewardClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}


- (void)getPersonInfoClientWithUserID:(NSString *)userID
                              Success:(void (^)(ZFUserInfoData *data))success
                              failure:(void (^)(NSError *error))failure{
    if (USE_TEST_DATA_MODE) {
        ZFUserInfoData *responseData =[ZFUserResponseData dataForTesting];
        success(responseData);
        return;
    }
    
    NSString *pathWithUserID = [NSString stringWithFormat:@"%@%@", ZF_PATH_GET_PERSONALINFO, userID];
    NSString *path = [ZFAPIClient getFullPathFromPath:pathWithUserID];
    
    //    NSDictionary *params =[NSDictionary dictionaryWithObjectsAndKeys:
    //                           userID,@"id",
    //                           nil];

    
    [self.client GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"PersonInfo--》%@", responseObject);
        // 3.1 Parser json dictionary to data object
        ZFUserResponseData *responseData =[ZFUserResponseData dataWithJSON:responseObject];
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode integerValue];
        if (returnCode == API_Sucess_Code) {
            success(responseData.userDataArray);
        }else{
            [[ZFStewardClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"get user info error %@", error);
        failure(error);
        
    }];
}


-(void)postBookingClientWithInfo:(NSDictionary *)paraDic Success:(void (^)(ZFResponseDataBase *response))success
                         failure:(void (^)(NSError *error))failure{
    
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_SAVE_BOOKING_WX];
    // 3. load from server.
    
    [self.client POST:path parameters:paraDic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"post book %@", responseObject);
        // 3.1 Parser json dictionary to data object
        ZFResponseDataBase *responseData =[ZFResponseDataBase dataWithJSON:responseObject];
        
        // 3.2 Check return code and return.
        
        int returnCode = [responseData.returnCode integerValue];
        if (returnCode == API_Sucess_Code) {
            success(responseData);
        }else{
            [[ZFStewardClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error with %@", error);
        failure(error);
    }];
    
}

-(void)getSchemeWithScheme:(NSString *)scheme
                     carID:(NSString *)ID
                   Success:(void (^)(NSMutableArray *data))success
                   failure:(void (^)(NSError *error))failure
{
    NSString *SchemeURL;
    SchemeURL = [NSString stringWithFormat:@"%@%@/api/Calparameters/fn/getDataByproductdetailid/productdetailid/%@/type/%@", ZF_BASE_URL, ZF_BASE_PATH, ID, scheme];
    
    [self.client GET:SchemeURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"App.net Global Stream: %@", responseObject);
        if ([[responseObject valueForKey:@"Code"] integerValue]==200) {
            if ([[responseObject valueForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                NSMutableArray *dataSource = [[responseObject valueForKey:@"Data"] valueForKey:@"List"];
                success(dataSource);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Calculator error :%@", error);
        failure(error);
    }];
}

@end
