//
//  ZFStewardManager.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import "ZFAPIManagerBase.h"
#import "ZFStewardClient.h"
@interface ZFStewardManager : ZFAPIManagerBase
+ (ZFStewardManager *)sharedManager;

- (void)getDataSuccess:(void (^)(NSArray *data))success
               failure:(void (^)(NSError *error))failure;



- (void)getStoreWithPage:(NSString *)page andPageSize:(NSString *)pageSize
                 Success:(void (^)(ZFNetServerStoreMutableArray *data))success
                 failure:(void (^)(NSError *error))failure;

- (void)getCarTypeWithType:(NSString *)type
                         Success:(void (^)(ZFNetServerCarTypeDictionary *data))success
                         failure:(void (^)(NSError *error))failure;
- (void)getCarModelWithID:(NSUInteger )idStr
                  Success:(void (^)(ZFNetServerCarTypeDictionary *data))success
                  failure:(void (^)(NSError *error))failure;
- (void)getIndustrySuccess:(void (^)(ZFNetServerIndustryData *data))success
                         failure:(void (^)(NSError *error))failure;

- (void)getJobSuccess:(void (^)(ZFReserveDataDictionary *data))success
                    failure:(void (^)(NSError *error))failure;

- (void)getPersonInfoWithUserID:(NSString *)userID
                              Success:(void (^)(ZFUserInfoData *data))success
                              failure:(void (^)(NSError *error))failure;

-(void)postBookingWithInfo:(NSDictionary *)paraDic Success:(void (^)(ZFResponseDataBase *response))success
                   failure:(void (^)(NSError *error))failure;

-(void)getReserveInfoSucess:(void(^)(ZFReserveDataDictionary *data))success
                          failure:(void(^)(NSError *error))failure;

-(void)getExhibitionSucess:(void(^)(ZFReserveCityExhibitionMutableArray *data))success
                   failure:(void(^)(NSError *error))failure
                       cid:(int)cityID type:(ReserveType)type;
    
-(void)getCitySucess:(void(^)(ZFReserveCityExhibitionMutableArray *data))success
                             failure:(void(^)(NSError *error))failure;
-(void)getSchemeWithCase:(NSString *)scheme
                andCarID:(NSString *)ID
                 Success:(void (^)(NSMutableArray *data))success
                  failue:(void (^)(NSError *error))failure;

@end
