//
//  ZFClubData.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/8/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "KADataBase.h"
#import "ZFResponseDataBase.h"
@interface ZFClubData : KADataBase

@end

typedef NSMutableArray ZFNoticeMutableArray;
@interface ZFMemberNoticeResponseData : ZFResponseDataBase

@property(nonatomic, strong)ZFNoticeMutableArray *noticeDataArray;
@property(nonatomic, strong)NSString *totlePage;

+ (id)dataWithJSON:(id)JSON;
+ (ZFNoticeMutableArray *)dataForTesting;

@end


@interface ZFMemberNoticeeData : KADataBase

@property (nonatomic,assign) NSInteger noticeID;
@property (nonatomic,strong) NSString *subject;
@property (nonatomic,strong) NSString *body;
@property (nonatomic,strong) NSString *sendBy;
@property (nonatomic,strong) NSString *createdTime;
@property (nonatomic,assign) NSInteger isread;


+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;


@end

typedef NSMutableArray ZFBookMutableArray;
@interface ZFMemberBookResponseData : ZFResponseDataBase

@property(nonatomic, strong)ZFBookMutableArray *bookDataArray;
@property(nonatomic, strong)NSString *totlePage;

+ (id)dataWithJSON:(id)JSON;
+ (ZFBookMutableArray *)dataForTesting;

@end

@interface ZFMemberBookData : KADataBase

@property (nonatomic,assign) NSInteger bookID;
@property (nonatomic,assign) NSInteger storeID;
@property (nonatomic,assign) NSInteger productdetailID;
@property (nonatomic,assign) NSInteger productID;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *storeName;
@property (nonatomic,strong) NSString *productName;
@property (nonatomic,strong) NSString *productdetailName;
@property (nonatomic,strong) NSString *location;
@property (nonatomic,strong) NSString *createdTime;
@property (nonatomic,strong) NSString *updatedTime;
@property (nonatomic,strong) NSString *scheduleTime;


+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;


@end