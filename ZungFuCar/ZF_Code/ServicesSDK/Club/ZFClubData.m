//
//  ZFClubData.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/8/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ZFClubData.h"

@implementation ZFClubData

@end

@implementation ZFMemberNoticeResponseData

+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON];
}
+ (ZFNoticeMutableArray *)dataForTesting {
    
    ZFNoticeMutableArray *data = [ZFNoticeMutableArray array];
    for (int i = 0; i<5; i++) {
        [data addObject:[ZFMemberNoticeeData dataForTesting]];
    }
    return data;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        self.totlePage = [self.data objectForKey:@"TotalPages"];
        NSArray *data = [self.data arrayForKey:@"List"];
        NSMutableArray *temData = [NSMutableArray array];
        
        for (id json in data) {
            ZFMemberNoticeeData *objectData = [ZFMemberNoticeeData dataWithJSON:json];
            [temData addObject:objectData];
        }
        
        self.noticeDataArray = temData;
    }
    
    return self;
}


@end

@implementation ZFMemberNoticeeData
static float i =0.0;
+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        self.noticeID = i++;
        self.body = @"this is a test testBody";
        self.sendBy = @"this is a test jack";
        self.subject = @"this is a test subject!";
        self.createdTime = @"2013-10-01 01:01:01";
       
        if (i > 20) {
            i=0;
        }
    }
    return self;
}

- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        self.noticeID = [JSON integerForKey:@"ID"];
        self.subject = [JSON stringForKey:@"Subject"];
        self.body = [JSON stringForKey:@"Body"];
        self.sendBy = [JSON stringForKey:@"SendBy"];
        self.createdTime = [JSON objectForKey:@"CreatedTime"];
        self.isread = [JSON integerForKey:@"isread"];
    }
    
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:[NSString stringWithFormat:@"%d",_noticeID] forKey:@"noticeID"];
        [coder encodeObject:[NSString stringWithFormat:@"%d",_isread] forKey:@"isread"];
        [coder encodeObject:_body forKey:@"body"];
        [coder encodeObject:_subject forKey:@"subject"];
        [coder encodeObject:_sendBy forKey:@"sendby"];
        [coder encodeObject:_createdTime forKey:@"createTime"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _noticeID = [[coder decodeObjectForKey:@"noticeID"] integerValue];
        _isread = [[coder decodeObjectForKey:@"isread"] integerValue];
        _body = [coder decodeObjectForKey:@"body"];
        _sendBy = [coder decodeObjectForKey:@"subject"];
        _subject = [coder decodeObjectForKey:@"sendby"];
        _createdTime = [coder decodeObjectForKey:@"createdTime"];
    }
    
    return self;
}


@end

@implementation ZFMemberBookResponseData

+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON];
}
+ (ZFBookMutableArray *)dataForTesting {
    
    ZFBookMutableArray *data = [ZFBookMutableArray array];
    for (int i = 0; i<5; i++) {
        [data addObject:[ZFMemberBookData dataForTesting]];
    }
    return data;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        self.totlePage = [self.data objectForKey:@"TotalPages"];
        NSArray *data = [self.data arrayForKey:@"List"];
        NSMutableArray *temData = [NSMutableArray array];
        
        for (id json in data) {
            ZFMemberBookData *objectData = [ZFMemberBookData dataWithJSON:json];
            [temData addObject:objectData];
        }
        
        self.bookDataArray = temData;
    }
    
    return self;
}

@end

@implementation ZFMemberBookData


+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        self.bookID = i++;
        self.storeID = i++;
        self.productID = i++;
        self.productdetailID = i++;
        self.type = @"1";
        self.storeName = @"test storeName";
        self.productName = @"ProductName";
        self.productdetailName = @"ProductdetailName";
        self.location = @"Location";
        self.createdTime = @"2013-10-01 01:01:00";
        self.updatedTime = @"2013-10-02 01:01:00";
        self.scheduleTime = @"2013-10-03 01:01:00";

        if (i > 20) {
            i=0;
        }
    }
    return self;
}

- (id)initWithJSON:(id)JSON {

    if (self = [super initWithJSON:JSON]) {
        self.bookID = [JSON integerForKey:@"ID"];
        self.storeID = [JSON integerForKey:@"StoreID"];
        self.productID = [JSON integerForKey:@"ProductD"];
        self.productdetailID = [JSON integerForKey:@"ProductdetailID"];
        self.type = [JSON objectForKey:@"Type"];
        self.storeName = [JSON stringForKey:@"StoreName"];
        self.productName = [JSON objectForKey:@"ProductName"];
        self.productdetailName = [JSON objectForKey:@"ProductdetailName"];
        self.location = [JSON stringForKey:@"Location"];
        self.createdTime = [JSON objectForKey:@"CreatedTime"];
        self.updatedTime = [JSON objectForKey:@"UpdatedTime"];
        self.scheduleTime = [JSON objectForKey:@"ScheduleTime"];
        
    }
    
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:[NSString stringWithFormat:@"%d",_bookID] forKey:@"bookID"];
        [coder encodeObject:[NSString stringWithFormat:@"%d",_storeID] forKey:@"storeID"];
        [coder encodeObject:[NSString stringWithFormat:@"%d",_productID] forKey:@"prodectID"];
        [coder encodeObject:[NSString stringWithFormat:@"%d",_productdetailID] forKey:@"productdetailID"];
        [coder encodeObject:_type forKey:@"type"];
        [coder encodeObject:_storeName forKey:@"storeName"];
        [coder encodeObject:_productName forKey:@"productName"];
        [coder encodeObject:_productdetailName forKey:@"productdetailName"];
        [coder encodeObject:_location forKey:@"location"];
        [coder encodeObject:_updatedTime forKey:@"updatedTime"];
        [coder encodeObject:_scheduleTime forKey:@"scheduleTime"];
        [coder encodeObject:_createdTime forKey:@"createdTime"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _bookID = [[coder decodeObjectForKey:@"bookID"] integerValue];
        _storeID = [[coder decodeObjectForKey:@"storeID"] integerValue];
        _productID = [[coder decodeObjectForKey:@"prodectID"] integerValue];
        _productdetailID = [[coder decodeObjectForKey:@"productdetailID"] integerValue];
        _type = [coder decodeObjectForKey:@"type"];
        _storeName = [coder decodeObjectForKey:@"storeName"];
        _productName = [coder decodeObjectForKey:@"productName"];
        _productdetailName = [coder decodeObjectForKey:@"productdetailName"];
        _location = [coder decodeObjectForKey:@"location"];
        _updatedTime = [coder decodeObjectForKey:@"updatedTime"];
        _scheduleTime = [coder decodeObjectForKey:@"scheduleTime"];
        _createdTime = [coder decodeObjectForKey:@"createdTime"];
    }
    
    return self;
}

@end