//
//  ZFClubManager.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/8/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ZFClubManager.h"
static ZFClubManager *shared_manager = nil;
@implementation ZFClubManager
+ (ZFClubManager *)sharedManager {
    static dispatch_once_t pred;
	dispatch_once(&pred, ^{
        shared_manager = [[self alloc] init];
    });
	return shared_manager;
}

-(void)postForgetPasswordWithEmail:(NSString *)email
                 Success:(void (^)(ZFResponseDataBase *response))success
                 failure:(void (^)(NSError *error))failure{
    [[ZFClubClient sharedClient] postForgetPasswordClientWithEmail:email Success:^(ZFResponseDataBase *response) {
        success(response);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

-(void)postLoginWithUser:(NSString *)user andPassword:(NSString *)password
                 Success:(void (^)(NSDictionary *response))success
                 failure:(void (^)(NSError *error))failure{
    [[ZFClubClient sharedClient] postLoginClientWithUser:user andPassword:password Success:^(NSDictionary *response) {
        success(response);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
- (void)postRegisterWithInfo:(NSDictionary *)paraDic
                     Success:(void (^)(NSDictionary *response))success
                     failure:(void (^)(NSError *error))failure{
    [[ZFClubClient sharedClient] postRegisterClientWithInfo:paraDic Success:^(NSDictionary *response) {
        success(response);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
- (void)getMemberNoticeWithMemberID:(NSString *)memberID
                             Sucess:(void(^)(ZFNoticeMutableArray *data))success
                            failure:(void(^)(NSError *error))failure{
    [[ZFClubClient sharedClient] getMemberClientNoticeWithMemberID:memberID Sucess:^(ZFNoticeMutableArray *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)getMemberBookDataWithMemberID:(NSString *)memberID
                               Sucess:(void(^)(ZFBookMutableArray *data))success
                              failure:(void(^)(NSError *error))failure
{
    [[ZFClubClient sharedClient] getMemberClientBookWithMemberID:memberID Sucess:^(ZFBookMutableArray *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

@end
