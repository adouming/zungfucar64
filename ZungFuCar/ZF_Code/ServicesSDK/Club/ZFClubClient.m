//
//  ZFClubClient.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/8/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ZFClubClient.h"
//#import "DBManager.h"

@implementation ZFClubClient


+ (id)sharedClient {
	static dispatch_once_t pred;
    static ZFClubClient *shared_instance = nil;
	
    dispatch_once(&pred, ^{
		shared_instance = [[self alloc] init];
        shared_instance.client = [self sharedHTTPClient];
    });
    
	return shared_instance;
}


-(void)postForgetPasswordClientWithEmail:(NSString *)email
                                 Success:(void (^)(ZFResponseDataBase *response))success
                                 failure:(void (^)(NSError *error))failure{
    // 1. get full path.
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_FORGETPASSWORD];
    // 2. create params.
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            email,@"Email",
                            nil];
    // 3. load from server.
    
    [self.client POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // 3.1 Parser json dictionary to data object
        ZFResponseDataBase *responseData =[ZFResponseDataBase dataWithJSON:responseObject];
        
        // 3.2 Check return code and return.
        
        int returnCode = [responseData.returnCode integerValue];
        if (returnCode == API_Sucess_Code) {
            success(responseData);
        }else{
            [[ZFClubClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}

-(void)postLoginClientWithUser:(NSString *)user andPassword:(NSString *)password
                       Success:(void (^)(NSDictionary *response))success
                       failure:(void (^)(NSError *error))failure{
    // 1. get full path.
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_LOGIN];
    // 2. create params.
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            user,@"Email",
                            password ,@"Password",
                            nil];
    
    // 3. load from server.
    [self.client POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // 3.1 Parser json dictionary to data object
        
        NSDictionary *responseDataDic = [[NSDictionary alloc] initWithDictionary:responseObject];
        
        // 3.2 Check return code and return.
        
        int returnCode = [[responseDataDic valueForKey:@"Code"] intValue];
        if (returnCode == API_Sucess_Code) {
            success(responseDataDic);
        }else{
            [[ZFClubClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        failure(error);
    }];
    
}


- (void)postRegisterClientWithInfo:(NSDictionary *)paraDic
                           Success:(void (^)(NSDictionary *response))success
                           failure:(void (^)(NSError *error))failure{
    // 1. get full path.
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_REGISTER];
    // 2. create params.
    NSDictionary *params = paraDic;
    
    // 3. load from server.
    [self.client POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // 3.1 Parser json dictionary to data object
        
        NSDictionary *responseDataDic = [[NSDictionary alloc] initWithDictionary:responseObject];
        
        // 3.2 Check return code and return.
        
        int returnCode = [[responseDataDic objectForKey:@"Code"] integerValue];
        if (returnCode == API_Sucess_Code) {
            success(responseDataDic);
        }else{
            [[ZFClubClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error %@", error);
        failure(error);
        
    }];
    
}

- (void)getMemberClientNoticeWithMemberID:(NSString *)memberID
                                   Sucess:(void(^)(ZFNoticeMutableArray *data))success
                                  failure:(void(^)(NSError *error))failure{
    if (USE_TEST_DATA_MODE) {
        ZFNoticeMutableArray *responseData =[ZFMemberNoticeResponseData dataForTesting];
        success(responseData);
        return;
    }
    //    NSString *currentTime = [ZFAPIClient currentDateTime];
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_NOTICE];
    
    NSDictionary *params =[NSDictionary dictionaryWithObjectsAndKeys:
                           memberID, @"mId",
                           nil];
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"operation %@", operation);
        NSLog(@"responseObject %@", responseObject);
        
        // 3.1 Parser json dictionary to data object
        
        NSDictionary *responseDataDic = [[NSDictionary alloc] initWithDictionary:responseObject];
        
        // 3.2 Check return code and return.
        int returnCode = [[responseDataDic objectForKey:@"Code"] integerValue];
        if (returnCode == API_Sucess_Code) {
            if ([[responseDataDic objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                success([[responseDataDic objectForKey:@"Data"] objectForKey:@"List"]);
            }
        }else{
            [[ZFClubClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
        failure(error);
    }];
    
}

- (void)getMemberClientBookWithMemberID:(NSString *)memberID
                                 Sucess:(void(^)(ZFBookMutableArray *data))success
                                failure:(void(^)(NSError *error))failure{
    if (USE_TEST_DATA_MODE) {
        ZFBookMutableArray *responseData =[ZFMemberBookResponseData dataForTesting];
        success(responseData);
        return;
    }
    NSString *currentTime = [ZFAPIClient currentDateTime];
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_BOOKED];
    
    NSDictionary *params =[NSDictionary dictionaryWithObjectsAndKeys:
                           memberID, @"mId",
                           currentTime,@"date",
                           nil];
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // 3.1 Parser json dictionary to data object
        
        NSDictionary *responseDataDic = [[NSDictionary alloc] initWithDictionary:responseObject];
        
        // 3.2 Check return code and return.
        int returnCode = [[responseDataDic objectForKey:@"Code"] integerValue];
        if (returnCode == API_Sucess_Code) {
            if ([[responseDataDic objectForKey:@"Data"] isKindOfClass:[NSDictionary class]]) {
                success([[responseDataDic objectForKey:@"Data"] objectForKey:@"List"]);
            } else {
                success(nil);
            }
        }else{
            [[ZFClubClient sharedClient] responseErrorWithReturnCode:returnCode failure:^(NSError *error) {
                failure(error);
            }];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

@end
