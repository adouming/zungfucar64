//
//  ZFClubManager.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/8/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ZFAPIManagerBase.h"
#import "ZFClubClient.h"
@interface ZFClubManager : ZFAPIManagerBase
+ (ZFClubManager *)sharedManager;

-(void)postForgetPasswordWithEmail:(NSString *)email
                           Success:(void (^)(ZFResponseDataBase *response))success
                           failure:(void (^)(NSError *error))failure;

-(void)postLoginWithUser:(NSString *)user andPassword:(NSString *)password
                 Success:(void (^)(NSDictionary *response))success
                 failure:(void (^)(NSError *error))failure;

- (void)postRegisterWithInfo:(NSDictionary *)paraDic
                     Success:(void (^)(NSDictionary *response))success
                     failure:(void (^)(NSError *error))failure;

- (void)getMemberNoticeWithMemberID:(NSString *)memberID
                             Sucess:(void(^)(ZFNoticeMutableArray *data))success
                            failure:(void(^)(NSError *error))failure;

- (void)getMemberBookDataWithMemberID:(NSString *)memberID
                               Sucess:(void(^)(ZFBookMutableArray *data))success
                              failure:(void(^)(NSError *error))failure;
@end
