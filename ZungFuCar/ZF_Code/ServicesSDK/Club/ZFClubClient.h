//
//  ZFClubClient.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/8/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ZFAPIClient.h"
#import "ZFClubData.h"

@interface ZFClubClient : ZFAPIClient

-(void)postForgetPasswordClientWithEmail:(NSString *)email
                                 Success:(void (^)(ZFResponseDataBase *response))success
                                 failure:(void (^)(NSError *error))failure;

- (void)postLoginClientWithUser:(NSString *)user andPassword:(NSString *)password
                        Success:(void (^)(NSDictionary *response))success
                        failure:(void (^)(NSError *error))failure;

- (void)postRegisterClientWithInfo:(NSDictionary *)paraDic
                           Success:(void (^)(NSDictionary *response))success
                           failure:(void (^)(NSError *error))failure;

- (void)getMemberClientNoticeWithMemberID:(NSString *)memberID
                                   Sucess:(void(^)(ZFNoticeMutableArray *data))success
                                  failure:(void(^)(NSError *error))failure;

- (void)getMemberClientBookWithMemberID:(NSString *)memberID
                                 Sucess:(void(^)(ZFBookMutableArray *data))success
                                failure:(void(^)(NSError *error))failure;

@end
