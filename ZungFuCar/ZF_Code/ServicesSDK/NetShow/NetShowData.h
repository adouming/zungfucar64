//
//  NetShowData.h
//  ZungFuCar
//
//  Created by kc on 13-10-26.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KADataBase.h"
#import "ZFResponseDataBase.h"

@interface NetShowData : KADataBase

@end




///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

@interface NetShowClassListResponseData : ZFResponseDataBase

@property(nonatomic, strong)NSMutableArray *dataArray;

+ (id)dataWithJSON:(id)JSON;
+ (NSMutableArray *)dataForTesting;

@end



/////////////////////////////////////////////////////////
/////////////////////// class list
/////////////////////////////////////////////////////////
@interface NetShowClassListData : KADataBase

/*
 {
 BannerURL = "https://54.250.184.46/zf-web-ios/ProductRoot/0/cntImage/7C774A01-07BC-95F1-CCF2-393AC8416F38.jpg";
 CarType =                 (
 {
 Details =                         (
 {
 ID = 71;
 Name = "SL 350";
 Price = 1498000;
 PromoImageURL = "";
 }
 );
 DisplayText = "\U655e\U7bf7\U8dd1\U8f66";
 ID = 198;
 }
 );
 Descp = "\n\tproductcontent-47\n";
 ID = 47;
 Name = SL;
 },
 
 */

@property (nonatomic,strong) NSString *NameStr;
@property (nonatomic,strong) NSString *IDStr;
@property (nonatomic,strong) NSString *DescpStr;
@property (nonatomic,strong) NSString *BannerURLUrl;
@property (nonatomic,strong) NSString *UpdatedTime;
@property (nonatomic,strong) NSArray *CarTypeArray;
//
+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end



/////////////////////////////////////////////////////////
/////////////////////// detail
/////////////////////////////////////////////////////////
@interface NetShowClassDetailResponseData : ZFResponseDataBase

@property(nonatomic, strong)NSMutableArray *dataArray;

+ (id)dataWithJSON:(id)JSON;
+ (NSMutableArray *)dataForTesting;

@end


@interface NetShowDetailData : KADataBase

@property(nonatomic,strong) NSMutableArray *activitys;//保留，是否相关活动一起请求

@property (nonatomic,strong) NSString *imageUrl;
@property (nonatomic,strong) NSString *titleStr;
@property (nonatomic,strong) NSString *summaryStr;
@property (nonatomic,strong) NSString *shareMsg;


//子菜单
@property (nonatomic,strong) NSString *geXingStr;
@property (nonatomic,strong) NSString *shuShiStr;
@property (nonatomic,strong) NSString *zhengTiStr;
@property (nonatomic,strong) NSString *xingNengStr;




+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end



///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

@interface NetShowDetailListResponseData : ZFResponseDataBase

@property(nonatomic, strong)NSMutableArray *dataArray;

+ (id)dataWithJSON:(id)JSON;
+ (NSMutableArray *)dataForTesting;

@end


/////////////////////////////////////////////////////////
/////////////////////// class list
/////////////////////////////////////////////////////////
@interface NetShowDetailListData : KADataBase

@property (nonatomic,strong) NSString *titleStr;
@property (nonatomic,strong) NSString *iconStr;
@property (nonatomic,strong) NSString *summaryStr;
@property (nonatomic,strong) NSString *imgUrl;
+ (id)dataForTesting;
- (id)initForTesting;
+ (id)dataWithJSON:(id)JSON;

@end










