//
//  NetShowClient.m
//  ZungFuCar
//
//  Created by kc on 13-10-26.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "NetShowClient.h"
#import "NetShowData.h"
#import "DBManager.h"



//class list
#define ZF_PATH_NETSHOW_CLASS_LIST_              @"/api/Product/fn/getproductByCategory"
//详情
#define ZF_PATH_NETSHOW_CLASS_DETAIL_              @"/api/Productdetail/fn/getDataByproductid"

//相关活动
#define ZF_PATH_NETSHOW_CLASS_DETAIL_RELATE_              @"/api/Eventscope/fn/getDataByproductid"


@implementation NetShowClient

+ (id)sharedClient {
	static dispatch_once_t pred;
    static NetShowClient *shared_netshow_instance = nil;
	
    dispatch_once(&pred, ^{
		shared_netshow_instance = [[self alloc] init];
        shared_netshow_instance.client = [self sharedHTTPClient];
    });
    
	return shared_netshow_instance;
}


- (void)getDataWithToken:(NSString *)token
                withType:(NSInteger)aType
                 success:(void (^)(NSArray *data))success
                 failure:(void (^)(NSError *error))failure{
    
    
    NSString *categoryType = @"1";
    if (aType==0) {
        categoryType = @"1";
    }else if (aType==0) {
        categoryType = @"2";
    }else{
        categoryType = @"2";
    }
    
    
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_NETSHOW_CLASS_LIST_];
    
    
    
    //获取当前时间
    NSString *aUpateTime = [[DBManager sharedManager] getNetShowClassListTimeDataWithType:aType];
    NSMutableDictionary *params;
    
    
    //NSLog(@"-------%@",aUpateTime);
    NSMutableArray *aPreArray = [[DBManager sharedManager] getNetShowClassListDataWithType:aType];
    /*
     第一次请求不要传date
     
     方法：没有数据时候，就不传date
     */
    if (aPreArray.count==0) {
        
        NSLog(@"第一次请求，或者是没有数据 请求");
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                  categoryType,@"category",
                  nil];
    }else{
        
        NSLog(@"再次 请求-上次请求时间：%@",aUpateTime);
        
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                  categoryType,@"category",
                  aUpateTime,@"lastmodified",
                  nil];
        
    }
    
    
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NetShowClassListResponseData *responseData =[NetShowClassListResponseData dataWithJSON:responseObject];
        
        // 3.1 Parser json dictionary to data object
        
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode intValue];
        /*
         #define API_Sucess_Code                     200
         #define API_IDType_Error_Code               400
         #define API_NotAuthorized_Error_Code        401
         #define API_PayMent_Required_Error_Code     402
         #define API_Forbidden_Error_Code            403
         #define API_RequestURL_NotFound_Error_Code  404
         #define API_Server_Error_Code               500
         #define API_Not_Implemented_Error_Code      501
         
         Code	Message
         200	OK / Success
         400	ID or  data must be numeric
         401	Not authorized
         402	Payment Required
         403	Forbidden
         404	The request URL was not found
         500	The server encountered an error when processing your request
         501	The requested function is not implemented
         */
        switch (returnCode) {
            case API_Sucess_Code: {
                success(responseData.dataArray);
                break;
            }
                
            case API_IDType_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            case API_NotAuthorized_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_PayMent_Required_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Forbidden_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_RequestURL_NotFound_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Server_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Not_Implemented_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            default: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}



- (void)getDataWithToken:(NSString *)token
                  withID:(NSString*)aID
                 success:(void (^)(NSArray *data))success
                 failure:(void (^)(NSError *error))failure{
    
    
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_NETSHOW_CLASS_DETAIL_];
    NSMutableDictionary *params;
    
    //帅选之前有没有的ID
    
    
    //获取
    params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
              aID,@"productid",
              nil];
    
    //    //获取当前时间
    //    NSString *aUpateTime = [[DBManager sharedManager] getNetShowClassListTimeDataWithType:aType];
    //    NSMutableDictionary *params;
    //
    //
    //    //NSLog(@"-------%@",aUpateTime);
    //    NSMutableArray *aPreArray = [[DBManager sharedManager] getNetShowClassListDataWithType:aType];
    //    /*
    //     第一次请求不要传date
    //
    //     方法：没有数据时候，就不传date
    //     */
    //    if (aPreArray.count==0) {
    //
    //        NSLog(@"第一次请求，或者是没有数据 请求");
    //        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
    //                  categoryType,@"category",
    //                  nil];
    //    }else{
    //
    //        NSLog(@"再次 请求-上次请求时间：%@",aUpateTime);
    //
    //        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
    //                  categoryType,@"category",
    //                  aUpateTime,@"lastmodified",
    //                  nil];
    //
    //    }
    
    
    
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NetShowClassDetailResponseData *responseData =[NetShowClassDetailResponseData dataWithJSON:responseObject];
        
        // 3.1 Parser json dictionary to data object
        
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode intValue];
        /*
         #define API_Sucess_Code                     200
         #define API_IDType_Error_Code               400
         #define API_NotAuthorized_Error_Code        401
         #define API_PayMent_Required_Error_Code     402
         #define API_Forbidden_Error_Code            403
         #define API_RequestURL_NotFound_Error_Code  404
         #define API_Server_Error_Code               500
         #define API_Not_Implemented_Error_Code      501
         
         Code	Message
         200	OK / Success
         400	ID or  data must be numeric
         401	Not authorized
         402	Payment Required
         403	Forbidden
         404	The request URL was not found
         500	The server encountered an error when processing your request
         501	The requested function is not implemented
         */
        switch (returnCode) {
            case API_Sucess_Code: {
                success(responseData.dataArray);
                break;
            }
                
            case API_IDType_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            case API_NotAuthorized_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_PayMent_Required_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Forbidden_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_RequestURL_NotFound_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Server_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Not_Implemented_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            default: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}


//相关活动
- (void)getRelateActivityDataWithToken:(NSString *)token
                                withID:(NSString*)aID
                               success:(void (^)(NSArray *data))success
                               failure:(void (^)(NSError *error))failure{
    
    //    NSString *categoryType = @"1";
    //    if (aType==0) {
    //        categoryType = @"1";
    //    }else if (aType==0) {
    //        categoryType = @"2";
    //    }else{
    //        categoryType = @"2";
    //    }
    //
    
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_NETSHOW_CLASS_DETAIL_RELATE_];
    NSMutableDictionary *params;
    
    //帅选之前有没有的ID
    
    
    //获取
    params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
              aID,@"productid",
              nil];
    
    //    //获取当前时间
    //    NSString *aUpateTime = [[DBManager sharedManager] getNetShowClassListTimeDataWithType:aType];
    //    NSMutableDictionary *params;
    //
    //
    //    //NSLog(@"-------%@",aUpateTime);
    //    NSMutableArray *aPreArray = [[DBManager sharedManager] getNetShowClassListDataWithType:aType];
    //    /*
    //     第一次请求不要传date
    //
    //     方法：没有数据时候，就不传date
    //     */
    //    if (aPreArray.count==0) {
    //
    //        NSLog(@"第一次请求，或者是没有数据 请求");
    //        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
    //                  categoryType,@"category",
    //                  nil];
    //    }else{
    //
    //        NSLog(@"再次 请求-上次请求时间：%@",aUpateTime);
    //
    //        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
    //                  categoryType,@"category",
    //                  aUpateTime,@"lastmodified",
    //                  nil];
    //
    //    }
    
    
    
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NewsListResponseData *responseData =[NewsListResponseData dataWithJSON:responseObject];
        
        
        // 3.1 Parser json dictionary to data object
        
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode intValue];
        /*
         #define API_Sucess_Code                     200
         #define API_IDType_Error_Code               400
         #define API_NotAuthorized_Error_Code        401
         #define API_PayMent_Required_Error_Code     402
         #define API_Forbidden_Error_Code            403
         #define API_RequestURL_NotFound_Error_Code  404
         #define API_Server_Error_Code               500
         #define API_Not_Implemented_Error_Code      501
         
         Code	Message
         200	OK / Success
         400	ID or  data must be numeric
         401	Not authorized
         402	Payment Required
         403	Forbidden
         404	The request URL was not found
         500	The server encountered an error when processing your request
         501	The requested function is not implemented
         */
        switch (returnCode) {
            case API_Sucess_Code: {
                success(responseData.dataArray);
                break;
            }
                
            case API_IDType_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            case API_NotAuthorized_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_PayMent_Required_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Forbidden_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_RequestURL_NotFound_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Server_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Not_Implemented_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            default: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}




- (void)getDataWithToken3:(NSString *)token
                  success:(void (^)(NSArray *data))success
                  failure:(void (^)(NSError *error))failure{
    if (USE_TEST_DATA_MODE) {
        NSMutableArray *responseData =[NetShowDetailListResponseData dataForTesting];
        success(responseData);
        return;
    }
    
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_NEWS_LIST];
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"6",@"viewid",
                                   @"2012-12-02 23:59:59",@"date",
                                   nil];
    
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NetShowDetailListResponseData *responseData =[NetShowDetailListResponseData dataWithJSON:responseObject];
        
        // 3.1 Parser json dictionary to data object
        
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode intValue];
        /*
         #define API_Sucess_Code                     200
         #define API_IDType_Error_Code               400
         #define API_NotAuthorized_Error_Code        401
         #define API_PayMent_Required_Error_Code     402
         #define API_Forbidden_Error_Code            403
         #define API_RequestURL_NotFound_Error_Code  404
         #define API_Server_Error_Code               500
         #define API_Not_Implemented_Error_Code      501
         
         Code	Message
         200	OK / Success
         400	ID or  data must be numeric
         401	Not authorized
         402	Payment Required
         403	Forbidden
         404	The request URL was not found
         500	The server encountered an error when processing your request
         501	The requested function is not implemented
         */
        switch (returnCode) {
            case API_Sucess_Code: {
                success(responseData.dataArray);
                break;
            }
                
            case API_IDType_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            case API_NotAuthorized_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_PayMent_Required_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Forbidden_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_RequestURL_NotFound_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Server_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Not_Implemented_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            default: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}

@end



