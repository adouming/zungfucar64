//
//  NetShowManager.h
//  ZungFuCar
//
//  Created by kc on 13-10-26.
//  Copyright (c) 2013年 Alex. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "ZFAPIManagerBase.h"
#import "NetShowClient.h"

@interface NetShowManager : ZFAPIManagerBase

+ (NetShowManager *)sharedManager;


//车型列表
//class list

-(NSMutableArray*)getDBNetshowClassListDataWithType:(NSInteger)aType;
- (void)getData:(NSInteger)aType
        Success:(void (^)(NSArray *data))success
        failure:(void (^)(NSError *error))failure;


//车系详情
// detail
-(NetShowDetailData*)getDBNetshowClassListDataWithID:(NSString*)aID;
- (void)getDataWithID:(NSString*)aID
              Success:(void (^)(NetShowDetailData *data))success
              failure:(void (^)(NSError *error))failure;


//相关活动
-(NetShowDetailData*)getDBNetshowClassRelateActivityListDataWithID:(NSString*)aID;
- (void)getDataRelateActivityWithID:(NSString*)aID
              Success:(void (^)(NSMutableArray *data))success
              failure:(void (^)(NSError *error))failure;


// detail list
- (void)getDataSuccess3:(void (^)(NSArray *data))success
                failure:(void (^)(NSError *error))failure;

@end


















