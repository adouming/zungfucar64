//
//  NetShowClient.h
//  ZungFuCar
//
//  Created by kc on 13-10-26.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZFAPIClient.h"
#import "NetShowData.h"

@interface NetShowClient : ZFAPIClient


- (void)getDataWithToken:(NSString *)token
                withType:(NSInteger)aType
                 success:(void (^)(NSArray *data))success
                 failure:(void (^)(NSError *error))failure;

//news detail
- (void)getDataWithToken:(NSString *)token
                  withID:(NSString*)aID
                 success:(void (^)(NSArray *data))success
                 failure:(void (^)(NSError *error))failure;

// detail list
- (void)getDataWithToken3:(NSString *)token
                  success:(void (^)(NSArray *data))success
                  failure:(void (^)(NSError *error))failure;

//相关活动-列表
- (void)getRelateActivityDataWithToken:(NSString *)token
                                withID:(NSString*)aID
                               success:(void (^)(NSArray *data))success
                               failure:(void (^)(NSError *error))failure;

@end

