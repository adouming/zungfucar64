//
//  NetShowData.m
//  ZungFuCar
//
//  Created by kc on 13-10-26.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "NetShowData.h"

@implementation NetShowData

@end


///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

#pragma mark - NewsListResponseData

@implementation NetShowClassListResponseData

+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON];
}
+ (NSMutableArray *)dataForTesting {
    
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i<5; i++) {
        [data addObject:[NetShowClassListData dataForTesting]];
    }
    return data;
}
- (id)initWithJSON:(id)JSON {
    
    NSLog(@"json=%@",JSON);
    if (self = [super initWithJSON:JSON]) {
        id response = [JSON objectForKey:@"Data"];
        
        NSArray *data = [response arrayForKey:@"List"];
        NSMutableArray *temData = [NSMutableArray array];
        
        for (id json in data) {
            NetShowClassListData *objectData = [NetShowClassListData dataWithJSON:json];
            [temData addObject:objectData];
        }
        
        self.dataArray = temData;
    }
    
    return self;
}
@end





///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
@implementation NetShowClassListData

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
        
//        self.titleStr = @"B Class";
//        self.summaryStr = @"动感生活定制，创新的设计，独树易帜的动感风格 因你所需尽情享受完全舒展空间";
//        self.imgUrl = @"testImageUrl";
        
    }
    
    return self;
}
-(NSString *)dateToStr:(NSString *)_strDate{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[_strDate doubleValue]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}
- (id)initWithJSON:(id)JSON {
    /*
     {
     BannerURL = "https://54.250.184.46/zf-web-ios/ProductRoot/0/cntImage/7C774A01-07BC-95F1-CCF2-393AC8416F38.jpg";
     CarType =                 (
     {
     Details =                         (
     {
     ID = 71;
     Name = "SL 350";
     Price = 1498000;
     PromoImageURL = "";
     }
     );
     DisplayText = "\U655e\U7bf7\U8dd1\U8f66";
     ID = 198;
     }
     );
     Descp = "\n\tproductcontent-47\n";
     ID = 47;
     Name = SL;
     },

     */
    if (self = [super initWithJSON:JSON]) {
        self.NameStr = [JSON stringForKey:@"Name"];
        self.IDStr = [JSON stringForKey:@"ID"];
        self.DescpStr = [JSON stringForKey:@"Descp"];
        self.BannerURLUrl = [JSON stringForKey:@"BannerURL"];
        self.UpdatedTime = [JSON stringForKey:@"UpdatedTime"];
        self.CarTypeArray = [JSON objectForKey:@"CarType"];
    }
    
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:_NameStr forKey:@"Name"];
        [coder encodeObject:_IDStr forKey:@"ID"];
        [coder encodeObject:_DescpStr forKey:@"Descp"];
        [coder encodeObject:_BannerURLUrl forKey:@"BannerURL"];
        [coder encodeObject:_UpdatedTime forKey:@"UpdatedTime"];
        [coder encodeObject:_CarTypeArray forKey:@"CarType"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _NameStr = [coder decodeObjectForKey:@"Name"];
        _IDStr = [coder decodeObjectForKey:@"ID"];
        _DescpStr = [coder decodeObjectForKey:@"Descp"];
        _BannerURLUrl = [coder decodeObjectForKey:@"BannerURL"];
        _UpdatedTime = [coder decodeObjectForKey:@"UpdatedTime"];
        _CarTypeArray = [coder decodeObjectForKey:@"CarType"];
        
        
    }
    
    return self;
}


@end



///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

#pragma mark - NewsListResponseData

@implementation NetShowClassDetailResponseData

+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON];
}
+ (NSMutableArray *)dataForTesting {
    
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i<5; i++) {
        [data addObject:[NetShowClassListData dataForTesting]];
    }
    return data;
}
- (id)initWithJSON:(id)JSON {
    
    NSLog(@"detail json=%@",JSON);
    if (self = [super initWithJSON:JSON]) {
        id response = [JSON objectForKey:@"Data"];
        
        NSArray *data = [response arrayForKey:@"List"];
        NSMutableArray *temData = [NSMutableArray array];
        
        for (id json in data) {
            NetShowDetailData *objectData = [NetShowDetailData dataWithJSON:json];
            [temData addObject:objectData];
        }
        
        self.dataArray = temData;
    }
    
    return self;
}
@end


@implementation NetShowDetailData

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
        
        self.imageUrl = @"banner25";
        self.titleStr = @"B Class";
        self.summaryStr =@"动感生活定制，创新的设计，因你所需尽情享受完全的舒张空间.";
        
        self.shareMsg = @"share----share";
        
        self.geXingStr = @"个性设计展示内容";
        self.shuShiStr = @"舒适便捷展示内容";
        self.zhengTiStr = @"整体安全展示内容";
        self.xingNengStr = @"优越性能展示内容";
        
    }
    
    return self;
}
-(NSString *)dateToStr:(NSString *)_strDate{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[_strDate doubleValue]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        self.summaryStr = [JSON stringForKey:@"Content"];
        self.titleStr = [JSON stringForKey:@"Name"];
        //self.imgUrl = [JSON stringForKey:@"thumbnail"];
    }
    
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:_summaryStr forKey:@"Content"];
        [coder encodeObject:_titleStr forKey:@"Name"];
        //[coder encodeObject:_imgUrl forKey:@"imageUrl"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _titleStr = [coder decodeObjectForKey:@"Name"];
        _summaryStr = [coder decodeObjectForKey:@"Content"];
        //_imgUrl = [coder decodeObjectForKey:@"imageUrl"];
        
    }
    
    return self;
}





@end







///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

#pragma mark - NewsListResponseData

@implementation NetShowDetailListResponseData

+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON];
}
+ (NSMutableArray *)dataForTesting {
    
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i<5; i++) {
        [data addObject:[NetShowDetailListData dataForTesting]];
    }
    return data;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        id response = [JSON objectForKey:@"Data"];
        
        NSArray *data = [response arrayForKey:@"List"];
        NSMutableArray *temData = [NSMutableArray array];
        
        for (id json in data) {
            NetShowDetailListData *objectData = [NetShowDetailListData dataWithJSON:json];
            [temData addObject:objectData];
        }
        
        self.dataArray = temData;
    }
    
    return self;
}
@end


///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
@implementation NetShowDetailListData

+ (id)dataWithJSON:(id)JSON{
    return [[self alloc] initWithJSON:JSON];
}

+ (id)dataForTesting {
    return [[self alloc] initForTesting];
}

- (id)initForTesting {
    if (self = [super init]) {
        
        self.titleStr = @"Name";
        self.summaryStr = @"Content";
        self.imgUrl = @"testImageUrl";
        
    }
    
    return self;
}
-(NSString *)dateToStr:(NSString *)_strDate{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[_strDate doubleValue]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        self.summaryStr = [JSON stringForKey:@"Content"];
        self.titleStr = [JSON stringForKey:@"Name"];
        self.imgUrl = [JSON stringForKey:@"thumbnail"];
    }
    
    return self;
}
- (void)encodeWithCoder:(NSCoder *)coder {
    if (coder) {
        [coder encodeObject:_summaryStr forKey:@"Content"];
        [coder encodeObject:_titleStr forKey:@"Name"];
        [coder encodeObject:_imgUrl forKey:@"imageUrl"];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self && coder) {
        _titleStr = [coder decodeObjectForKey:@"Name"];
        _summaryStr = [coder decodeObjectForKey:@"Content"];
        _imgUrl = [coder decodeObjectForKey:@"imageUrl"];
        
    }
    
    return self;
}


@end















