//
//  NetShowManager.m
//  ZungFuCar
//
//  Created by kc on 13-10-26.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "NetShowManager.h"
#import "DBManager.h"

@implementation NetShowManager

static NetShowManager *shared_netShowManager = nil;

+ (NetShowManager *)sharedManager {
    static dispatch_once_t pred;
	dispatch_once(&pred, ^{
        shared_netShowManager = [[self alloc] init];
    });
	return shared_netShowManager;
}

-(NSMutableArray*)getDBNetshowClassListDataWithType:(NSInteger)aType{
    return [[DBManager sharedManager] getNetShowClassListDataWithType:aType];
}

/*
 
 网络请求列表数据，并加入到数据库中
 车型列表
 
 */
- (void)getData:(NSInteger)aType
        Success:(void (^)(NSArray *data))success
        failure:(void (^)(NSError *error))failure{
    
    //网络请求
    [[NetShowClient sharedClient]
     getDataWithToken:@""
     withType:aType
     success:^(NSArray *clientData) {
         //加入数据库
         [[DBManager sharedManager] storeNetShowClassListData:clientData
                                                     withType:aType
                                                       Sucess:^(NSMutableArray *storeData) {
                                                           //成功返回数据库
                                                           success(storeData);
                                                       }fail:^(NSError *error) {
                                                           //保存数据库失败，返回网络请求数据
                                                           success(clientData);
                                                       }];
     } failure:^(NSError *error) {
         //网络请求失败
         failure(error);
     }];
}



//详情
-(NetShowDetailData*)getDBNetshowClassListDataWithID:(NSString*)aID{
    
    //从数据库读出存储数据
    NSMutableArray *aStoreArray = [[DBManager sharedManager] getNetShowClassDetailData];
    
    //帅选符合id的数据
    
    
    return nil;
}
- (void)getDataWithID:(NSString*)aID
              Success:(void (^)(NetShowDetailData *data))success
              failure:(void (^)(NSError *error))failure{
    //网络请求
    [[NetShowClient sharedClient]
     getDataWithToken:@""
     withID:aID
     success:^(NSArray *clientData) {
         //加入数据库
         [[DBManager sharedManager] storeNetShowClassDetailData:clientData
                                                       Sucess:^(NSMutableArray *storeData) {
                                                           //成功返回数据库
                                                           success(storeData);
                                                       }fail:^(NSError *error) {
                                                           //保存数据库失败，返回网络请求数据
                                                           success(clientData);
                                                       }];
     } failure:^(NSError *error) {
         //网络请求失败
         failure(error);
     }];
}


//相关活动
-(NetShowDetailData*)getDBNetshowClassRelateActivityListDataWithID:(NSString*)aID{
    return nil;
}
- (void)getDataRelateActivityWithID:(NSString*)aID
                            Success:(void (^)(NSMutableArray *data))success
                            failure:(void (^)(NSError *error))failure{
    
    //网络请求
    [[NetShowClient sharedClient]
     getRelateActivityDataWithToken:@""
     withID:aID
     success:^(NSArray *clientData) {
         success(clientData);
         
//         //加入数据库
//         [[DBManager sharedManager] storeNetShowClassDetailData:clientData
//                                                         Sucess:^(NSMutableArray *storeData) {
//                                                             //成功返回数据库
//                                                             success(storeData);
//                                                         }fail:^(NSError *error) {
//                                                             //保存数据库失败，返回网络请求数据
//                                                             success(clientData);
//                                                         }];
     } failure:^(NSError *error) {
         //网络请求失败
         failure(error);
     }];
    
    
}



- (void)getDataSuccess3:(void (^)(NSArray *data))success
               failure:(void (^)(NSError *error))failure{
    [[NetShowClient sharedClient] getDataWithToken3:@"" success:^(NSArray *data) {
        success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
    
    //  [[ZFStewardClient sharedClient] postMethodExamplesuccess:^(NSInteger code) {
    //      success(nil);
    //  } failure:^(NSError *error) {
    //      failure(error);
    //  }];
}


@end





