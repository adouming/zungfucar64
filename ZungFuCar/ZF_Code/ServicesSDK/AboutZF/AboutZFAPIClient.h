//
//  AboutZFAPIClient.h
//  ZungFuCar
//
//  Created by lxm on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "ZFAPIClient.h"
#import "ZFStewardData.h"
typedef enum {
	Get = 0,
	Post,
	Put
} NetworkMethod;
@interface AboutZFAPIClient : ZFAPIClient

+ (id)sharedImageHTTPClient;

+ (id)sharedImageClient;

/**
 *	json请求方法
 *
 *	@param	aPath	urlPath
 *	@param	params	body参数
 *	@param	NetworkMethod	Get Post
 */
- (void)requestJsonDataWithPath:(NSString *)aPath
                     withParams:(NSMutableDictionary*)params
                 withMethodType:(int)NetworkMethod
                        success:(void (^)(NSArray *data))success
                        failure:(void (^)(NSError *error))failure;

- (void)requestImageDataWithPath:(NSString *)aPath
                         success:(void (^)(id data))success
                         failure:(void (^)(NSError *error))failure;
@end
