//
//  AboutZFAPIManager.m
//  ZungFuCar
//
//  Created by lxm on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "AboutZFAPIManager.h"
#import "AboutZFAPIClient.h"



@implementation AboutZFAPIManager

static AboutZFAPIManager *shared_manager = nil;

+ (AboutZFAPIManager *)sharedManager {
    static dispatch_once_t pred;
	dispatch_once(&pred, ^{
        shared_manager = [[self alloc] init];

    });
	return shared_manager;
}

// 公司简介:146, 发展历程:4, 人才培训何发展:148, 联系我们:182, 社会招聘:185, 校园招聘:175, 关于仁孚会:183, 会员手册:181
- (void)requestWebViewDataWithViewID:(NSString*)viewID
                             success:(void (^)(NSArray *data))success
                             failure:(void (^)(NSError *error))failure
{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:[NSString stringWithFormat:@"ViewID%@_UpdatedTime",viewID]];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@%@/view/fn/getDataByviewid/viewid/%@/lastmodified/%@", ZF_BASE_URL, ZF_BASE_PATH,viewID,lastTime]:[NSString stringWithFormat:@"%@%@/view/fn/getDataByviewid/viewid/%@", ZF_BASE_URL, ZF_BASE_PATH,viewID];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}



/*关于仁孚会*/
- (void)requestAboutClubDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID183_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@183/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 183];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}
/*配件*/
- (void)requestPeiJianDataSuccess:(void (^)(NSArray *data))success
                                failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID22_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@22/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 22];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}
/*售后服务*/
- (void)requestCustomServiceDataSuccess:(void (^)(NSArray *data))success
                             failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID21_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@21/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 21];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}
/*会员章程*/
- (void)requestMemberBookDataSuccess:(void (^)(NSArray *data))success
                            failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID43_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@43/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 43];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}
/*价格表*/
- (void)requestPriceTableDataSuccess:(void (^)(NSArray *data))success
                             failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID43_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@150/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 150];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*奖赏计划*/
- (void)requestMemberRewardPlanDataSuccess:(void (^)(NSArray *data))success
                             failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID44_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@44/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 44];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*会员优惠*/
- (void)requestMemberDiscountDataSuccess:(void (^)(NSArray *data))success
                                   failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID45_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@45/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 45];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*公司简介*/
- (void)requestCompanyProfileDataSuccess:(void (^)(NSArray *data))success
                   failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:ViewID146_UpdatedTime];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@2&lastmodified=%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 2];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                     withParams:nil
                                                 withMethodType:Get
                                                        success:^(NSArray *data) {
                                                            success(data);
                                                        } failure:^(NSError *error) {
                                                            failure(error);
                                                        }];
}

/*发展历程*/
- (void)requestDevelopmentHistoryDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID4_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@3&lastmodified=%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 3];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*企业文化*/
- (void)requestCompanyCultureDataSuccess:(void (^)(NSArray *data))success
                                     failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID4_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@4&lastmodified=%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 4];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}
/*社会责任*/

- (void)requestSocialResponsibilitySuccess:(void (^)(NSArray *data))success
                               failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID6_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@6&lastmodified=%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 6];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*员工关怀*/
- (void)requestStaffConcernDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID4_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@7&lastmodified=%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 7];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*人才培训与发展*/
- (void)requestTalentTrainingDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID148_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@148/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 148];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*臻至服务*/
- (void)requestRealSeriviceDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID177_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@177/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 177];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}


/*金融*/
- (void)requestFinanceDataSuccess:(void (^)(NSArray *data))success
                               failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID184_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@184/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 184];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*保险*/
- (void)requestInsuranceDataSuccess:(void (^)(NSArray *data))success
                          failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID136_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@18/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 18];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*大客户销售*/
- (void)requestBigCoustomerDataSuccess:(void (^)(NSArray *data))success
                            failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID16_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@19/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 19];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*精品*/
- (void)requestBoutiqueDataSuccess:(void (^)(NSArray *data))success
                            failure:(void (^)(NSError *error))failure
{
    
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    NSString *lastTime = [userDefaults objectForKey:@"ViewID21_UpdatedTime"];
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    
    NSString *strLocalHtml= [NSString stringWithFormat:@"%@%d", pathURL, 21];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*配件*/
- (void)requestPartsDataSuccess:(void (^)(NSArray *data))success
                           failure:(void (^)(NSError *error))failure
{
    
    //    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //    NSString *lastTime = [userDefaults objectForKey:@"ViewID21_UpdatedTime"];
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSString *strLocalHtml= [NSString stringWithFormat:@"%@%d", pathURL, 19];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*社会招聘*/
- (void)requestSocietyRecruitmentDataSuccess:(void (^)(NSArray *data))success
                             failure:(void (^)(NSError *error))failure
{
    
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID185_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@185/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 185];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*校园招聘*/
- (void)requestCampusRecruitmentDataSuccess:(void (^)(NSArray *data))success
                                     failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID175_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@175/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 175];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*二手车*/
- (void)requestSecondCarDataSuccess:(void (^)(NSArray *data))success
                                cid:(int) cid
                                 failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID131_UpdatedTime"];
    NSString *strLocalHtml = lastTime? [NSString stringWithFormat:@"%@%d/lastmodified/%@", cid, pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, cid];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}



/*加入我们*/
- (void)requestJoinUSDataSuccess:(void (^)(NSArray *data))success
                            failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID148_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@148/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 148];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*售后服务*/
- (void)requestAfterSeriveceDataSuccess:(void (^)(NSArray *data))success
                         failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID18_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@18/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL, 18];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}

/*contact us*/
- (void)requestContactUSDataSuccess:(void (^)(NSArray *data))success
                                failure:(void (^)(NSError *error))failure
{
    NSString *pathURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *lastTime = [userDefaults objectForKey:@"ViewID182_UpdatedTime"];
    NSString *strLocalHtml= lastTime?[NSString stringWithFormat:@"%@182/lastmodified/%@", pathURL,lastTime]:[NSString stringWithFormat:@"%@%d", pathURL,182];
    strLocalHtml = [strLocalHtml stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[AboutZFAPIClient sharedClient] requestJsonDataWithPath:strLocalHtml
                                                  withParams:nil
                                              withMethodType:Get
                                                     success:^(NSArray *data) {
                                                         success(data);
                                                     } failure:^(NSError *error) {
                                                         failure(error);
                                                     }];
}
@end
