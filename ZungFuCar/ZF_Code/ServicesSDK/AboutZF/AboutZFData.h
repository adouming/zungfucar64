//
//  AboutZFData.h
//  ZungFuCar
//
//  Created by lxm on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "KADataBase.h"
#import "KADataBase.h"
#import "ZFResponseDataBase.h"
@interface AboutZFData : KADataBase
@end;


@interface AboutZFResponseData : ZFResponseDataBase
@property(nonatomic, strong)NSMutableArray *dataArray;
@property(nonatomic, strong)NSMutableDictionary *dataRow;
//@property(nonatomic, assign)int dataReturnCode;
+ (id)dataWithJSON:(id)JSON;
+ (NSMutableArray *)dataForTesting;

@end


//@interface ZFNetServerData : KADataBase
//
//@property (nonatomic,strong) NSString *titleStr;
//@property (nonatomic,strong) NSString *iconStr;
//@property (nonatomic,strong) NSString *summaryStr;
//@property (nonatomic,strong) NSString *imgUrl;
//+ (id)dataForTesting;
//- (id)initForTesting;
//+ (id)dataWithJSON:(id)JSON;
//@end
