//
//  AboutZFAPIManager.h
//  ZungFuCar
//
//  Created by lxm on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AboutZFAPIManager : NSObject

- (void)requestWebViewDataWithViewID:(NSString*)viewID
                             success:(void (^)(NSArray *data))success
                             failure:(void (^)(NSError *error))failure;
/*关于仁孚会*/
- (void)requestAboutClubDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure;
/*配件*/
- (void)requestPeiJianDataSuccess:(void (^)(NSArray *data))success
                          failure:(void (^)(NSError *error))failure;

/*售后服务*/
- (void)requestCustomServiceDataSuccess:(void (^)(NSArray *data))success
                                failure:(void (^)(NSError *error))failure;

/*会员章程*/
- (void)requestMemberBookDataSuccess:(void (^)(NSArray *data))success
                            failure:(void (^)(NSError *error))failure;

/*价格表*/
- (void)requestPriceTableDataSuccess:(void (^)(NSArray *data))success
                             failure:(void (^)(NSError *error))failure;

/*奖赏计划简介*/
- (void)requestMemberRewardPlanDataSuccess:(void (^)(NSArray *data))success
                                   failure:(void (^)(NSError *error))failure;
/*会员优惠*/
- (void)requestMemberDiscountDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure;
/*公司简介*/
- (void)requestCompanyProfileDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure;

/*发展历程*/
- (void)requestDevelopmentHistoryDataSuccess:(void (^)(NSArray *data))success
                                     failure:(void (^)(NSError *error))failure;

/*企业文化*/
- (void)requestCompanyCultureDataSuccess:(void (^)(NSArray *data))success
                                     failure:(void (^)(NSError *error))failure;
/*社会责任*/
- (void)requestSocialResponsibilitySuccess:(void (^)(NSArray *data))success
                               failure:(void (^)(NSError *error))failure;
/*员工关怀*/
- (void)requestStaffConcernDataSuccess:(void (^)(NSArray *data))success
                               failure:(void (^)(NSError *error))failure;


/*人才培训与发展*/
- (void)requestTalentTrainingDataSuccess:(void (^)(NSArray *data))success
                                 failure:(void (^)(NSError *error))failure;

/*臻至服务*/
- (void)requestRealSeriviceDataSuccess:(void (^)(NSArray *data))success
                               failure:(void (^)(NSError *error))failure;

/*金融*/
- (void)requestFinanceDataSuccess:(void (^)(NSArray *data))success
                          failure:(void (^)(NSError *error))failure;

/*保险*/
- (void)requestInsuranceDataSuccess:(void (^)(NSArray *data))success
                            failure:(void (^)(NSError *error))failure;

/*大客户销售*/
- (void)requestBigCoustomerDataSuccess:(void (^)(NSArray *data))success
                               failure:(void (^)(NSError *error))failure;

/*精品*/
- (void)requestBoutiqueDataSuccess:(void (^)(NSArray *data))success
                             failure:(void (^)(NSError *error))failure;

/*配件*/
- (void)requestPartsDataSuccess:(void (^)(NSArray *data))success
                        failure:(void (^)(NSError *error))failure;

/*社会招聘*/
- (void)requestSocietyRecruitmentDataSuccess:(void (^)(NSArray *data))success
                                     failure:(void (^)(NSError *error))failure;

/*校园招聘*/
- (void)requestCampusRecruitmentDataSuccess:(void (^)(NSArray *data))success
                                    failure:(void (^)(NSError *error))failure;

/*星睿二手车*/
- (void)requestSecondCarDataSuccess:(void (^)(NSArray *data))success
                                cid:(int) cid
                            failure:(void (^)(NSError *error))failure;

/*加入我们*/
- (void)requestJoinUSDataSuccess:(void (^)(NSArray *data))success
                         failure:(void (^)(NSError *error))failure;

/*售后服务*/
- (void)requestAfterSeriveceDataSuccess:(void (^)(NSArray *data))success
                                failure:(void (^)(NSError *error))failure;

/*contact us*/
- (void)requestContactUSDataSuccess:(void (^)(NSArray *data))success
                            failure:(void (^)(NSError *error))failure;
@end
