//
//  AboutZFData.m
//  ZungFuCar
//
//  Created by lxm on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "AboutZFData.h"

@implementation AboutZFData

@end

@implementation AboutZFResponseData

+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON];
}
+ (NSMutableArray *)dataForTesting {
    
    NSMutableArray *data = [NSMutableArray array];
    for (int i = 0; i<5; i++) {
        //FIXME
        //[data addObject:[ZFNetServerData dataForTesting]];
    }
    return data;
}
- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
//        id response = [JSON objectForKey:@"Data"];
        
//        if ([response respondsToSelector:@selector(arrayForKey:@"List")]) {
//            
//        }
        
        NSArray *data = JSON;
//        NSMutableArray *temData = [NSMutableArray array];
//        
//        for (id json in data) {
//            ZFNetServerData *objectData = [ZFNetServerData dataWithJSON:json];
//            [temData addObject:objectData];
//        }
        self.returnCode = @200;
        self.dataArray =  [NSMutableArray arrayWithArray: @[data]];
    }
    
    return self;
}
@end