//
//  AboutZFAPIClient.m
//  ZungFuCar
//
//  Created by lxm on 13-10-23.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "AboutZFAPIClient.h"
#import "AboutZFData.h"
#import "ZFAPIUrl.h"
@implementation AboutZFAPIClient

+ (id)sharedImageHTTPClient {
	static dispatch_once_t pred;
    static AFHTTPRequestOperationManager *shared_instance = nil;
	
    dispatch_once(&pred, ^{
        shared_instance = [[self alloc] init];
        shared_instance = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:ZF_BASE_URL]];
        [shared_instance.requestSerializer setValue:@"image/jepg" forHTTPHeaderField:@"Accept"];
        shared_instance.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [shared_instance.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"HTTP-X-U"];
        [shared_instance.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"HTTP-X-P"];
    });
    
	return shared_instance;
}

+ (id)sharedImageClient {
	static dispatch_once_t pred;
    static ZFAPIClient *shared_instance = nil;
	
    dispatch_once(&pred, ^{
		shared_instance = [[self alloc] init];
        shared_instance.client = [self sharedImageHTTPClient];
    });
    
	return shared_instance;
}

/**
 *	json请求方法
 *
 *	@param	aPath	urlPath
 *	@param	params	body参数
 *	@param	NetworkMethod	Get Post
 */
- (void)requestJsonDataWithPath:(NSString *)aPath
                     withParams:(NSMutableDictionary*)params
                 withMethodType:(int)NetworkMethod
                        success:(void (^)(NSArray *data))success
                        failure:(void (^)(NSError *error))failure
{
    
    NSString *path = aPath;
    switch (NetworkMethod) {
        case Get:
        {
            [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                // 3.1 Parser json dictionary to data object
                AboutZFResponseData *responseData =[AboutZFResponseData dataWithJSON:responseObject];
                
                // 3.2 Check return code and return.
                int returnCode = [responseData.returnCode intValue];
                /*
                 #define API_Sucess_Code                     200
                 #define API_IDType_Error_Code               400
                 #define API_NotAuthorized_Error_Code        401
                 #define API_PayMent_Required_Error_Code     402
                 #define API_Forbidden_Error_Code            403
                 #define API_RequestURL_NotFound_Error_Code  404
                 #define API_Server_Error_Code               500
                 #define API_Not_Implemented_Error_Code      501
                 
                 Code	Message
                 200	OK / Success
                 400	ID or  data must be numeric
                 401	Not authorized
                 402	Payment Required
                 403	Forbidden
                 404	The request URL was not found
                 500	The server encountered an error when processing your request
                 501	The requested function is not implemented
                 */
                switch (returnCode) {
                    case API_Sucess_Code: {
                        success(responseData.dataArray);
                        break;
                    }
                        
                    case API_IDType_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                        
                    case API_NotAuthorized_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                    case API_PayMent_Required_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                    case API_Forbidden_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                    case API_RequestURL_NotFound_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                    case API_Server_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                    case API_Not_Implemented_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                        
                    default: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"string==%@",operation.responseString);
                
                
                NSLog(@"--json----%@",operation.response);
                failure(error);
                
            }];
            
        }
            break;
        case Post:
        {
            [self.client POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                AboutZFResponseData *responseData =[AboutZFResponseData dataWithJSON:responseObject];
                
                // 3.2 Check return code and return.
                
                int returnCode = [responseData.returnCode intValue];
                
                switch (returnCode) {
                    case API_Sucess_Code: {
                        success(responseData.dataArray);
                        break;
                    }
                        
                    case API_IDType_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                        
                    case API_NotAuthorized_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                    case API_PayMent_Required_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                    case API_Forbidden_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                    case API_RequestURL_NotFound_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                    case API_Server_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                    case API_Not_Implemented_Error_Code: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                        
                    default: {
                        NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
                        failure(err);
                        break;
                    }
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                failure(error);
            }];
        }
            break;
        case Put:
        {
            [self.client PUT:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                //FIXME
                success(responseObject);
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                failure(error);
            }];
        }
            break;
        default:
            break;
    }
    
}



- (void)requestImageDataWithPath:(NSString *)aPath
                         success:(void (^)(id data))success
                         failure:(void (^)(NSError *error))failure
{
    [self.client GET:aPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        int returnCode = API_Sucess_Code;
        switch (returnCode) {
            case API_Sucess_Code: {
                success(responseObject);
                break;
            }
                
            case API_IDType_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            case API_NotAuthorized_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_PayMent_Required_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Forbidden_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_RequestURL_NotFound_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Server_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Not_Implemented_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            default: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"string==%@",error.description);
        
        
        failure(error);
    }];
    
}
@end
