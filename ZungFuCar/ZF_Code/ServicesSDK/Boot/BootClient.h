//
//  BootClient.h
//  ZungFuCar
//
//  Created by kc on 13-11-9.
//  Copyright (c) 2013年 Alex. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ZFAPIClient.h"

@interface BootClient : ZFAPIClient


- (void)getDataBootImageSuccess:(void (^)(NSDictionary  *data))success
                        failure:(void (^)(NSError *error))failure;

@end

