//
//  BootData.m
//  ZungFuCar
//
//  Created by kc on 13-11-9.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "BootData.h"

@implementation BootData

+ (id)dataWithJSON:(id)JSON {
    return [[self alloc] initWithJSON:JSON];
}

- (id)initWithJSON:(id)JSON {
    
    NSLog(@"json=%@",JSON);
    if (self = [super initWithJSON:JSON]) {
        id response = [JSON objectForKey:@"Data"];
        
        NSArray *data = [response arrayForKey:@"List"];
        if (data.count>0) {
            if ([[data objectAtIndex:0] isKindOfClass:[NSDictionary class]]) {
                self.dataDic = [data objectAtIndex:0];
            }
            
        }
    }
    
    return self;
}


@end


