//
//  BootClient.m
//  ZungFuCar
//
//  Created by kc on 13-11-9.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "BootClient.h"
#import "DBManager.h"
#import "BootData.h"


//date
//启动背景
#define ZF_PATH_BOOT_              @"/api/option/fn/getoptionByCategory/category/loadingpage"
#define BOOT_IMAGE_PATH_TIME @"BOOT_IMAGE_PATH_TIME"

@implementation BootClient

+ (id)sharedClient {
	static dispatch_once_t pred;
    static BootClient *shared_boot_instance = nil;
	
    dispatch_once(&pred, ^{
		shared_boot_instance = [[self alloc] init];
        shared_boot_instance.client = [self sharedHTTPClient];
    });
    
	return shared_boot_instance;
}


- (void)getDataBootImageSuccess:(void (^)(NSDictionary  *data))success
                        failure:(void (^)(NSError *error))failure{
    
    NSString *path = [ZFAPIClient getFullPathFromPath:ZF_PATH_BOOT_];
    
    
    NSMutableDictionary *params=nil;
    NSString *aUpateTime = nil;
    if ([[NSUserDefaults standardUserDefaults] valueForKeyPath:BOOT_IMAGE_PATH_TIME]) {
        aUpateTime = [[NSUserDefaults standardUserDefaults] valueForKeyPath:BOOT_IMAGE_PATH_TIME];
    }
    
    if (aUpateTime) {
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:aUpateTime,@"date", nil];
    }
    
    
    
    
    
    [self.client GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        BootData *responseData =[BootData dataWithJSON:responseObject];
        
        // 3.1 Parser json dictionary to data object
        
        // 3.2 Check return code and return.
        int returnCode = [responseData.returnCode intValue];
        /*
         #define API_Sucess_Code                     200
         #define API_IDType_Error_Code               400
         #define API_NotAuthorized_Error_Code        401
         #define API_PayMent_Required_Error_Code     402
         #define API_Forbidden_Error_Code            403
         #define API_RequestURL_NotFound_Error_Code  404
         #define API_Server_Error_Code               500
         #define API_Not_Implemented_Error_Code      501
         
         Code	Message
         200	OK / Success
         400	ID or  data must be numeric
         401	Not authorized
         402	Payment Required
         403	Forbidden
         404	The request URL was not found
         500	The server encountered an error when processing your request
         501	The requested function is not implemented
         */
        switch (returnCode) {
            case API_Sucess_Code: {
                success(responseData.dataDic);
                break;
            }
                
            case API_IDType_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            case API_NotAuthorized_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_PayMent_Required_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Forbidden_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_RequestURL_NotFound_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Server_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
            case API_Not_Implemented_Error_Code: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
                
            default: {
                NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
                failure(err);
                break;
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}

@end




