//
//  BootManager.h
//  ZungFuCar
//
//  Created by kc on 13-11-9.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZFAPIManagerBase.h"
#import "BootClient.h"

@interface BootManager : ZFAPIManagerBase

+ (BootManager *)sharedManager;


//获取图片
-(UIImage*)getDBBootImage;
- (void)getDataBootImageSuccess:(void (^)(UIImage *data))success failure:(void (^)(NSError *error))failure;



@end


