//
//  BootData.h
//  ZungFuCar
//
//  Created by kc on 13-11-9.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZFResponseDataBase.h"


@interface BootData : ZFResponseDataBase

@property(nonatomic, strong)NSDictionary *dataDic;

+ (id)dataWithJSON:(id)JSON;
//+ (NSDictionary *)dataForTesting;

@end



