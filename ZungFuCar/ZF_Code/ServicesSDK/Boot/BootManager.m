//
//  BootManager.m
//  ZungFuCar
//
//  Created by kc on 13-11-9.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "BootManager.h"
#import "DBManager.h"
#import "ASIHTTPRequest.h"


#define BOOT_IMAGE_PATH @"BOOT_IMAGE_PATH"
#define BOOT_IMAGE_PATH_TIME @"BOOT_IMAGE_PATH_TIME"

@implementation BootManager

static BootManager *shared_bootManager = nil;

+ (BootManager *)sharedManager {
    static dispatch_once_t pred;
	dispatch_once(&pred, ^{
        shared_bootManager = [[self alloc] init];
    });
	return shared_bootManager;
}

//获取图片
-(UIImage*)getDBBootImage{
    NSData *aImageData = [[NSUserDefaults standardUserDefaults] valueForKeyPath:BOOT_IMAGE_PATH];
    UIImage *aImage = [[UIImage alloc] initWithData:aImageData];
    return aImage;
}
- (void)getDataBootImageSuccess:(void (^)(UIImage *data))success
                        failure:(void (^)(NSError *error))failure{
    
    [[BootClient sharedClient] getDataBootImageSuccess:^(NSDictionary *data) {
        
        if (!data) {
            failure(nil);
        }
        
        NSString *aURLStr = [data objectForKey:@"LoadimageURL"];
        //获取时间
        NSString *timeStr = [data objectForKey:@"UpdatedTime"];
        if (timeStr) {
            //最后更新时间
            [[NSUserDefaults standardUserDefaults] setValue:timeStr forKeyPath:BOOT_IMAGE_PATH_TIME];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
        //test
        //aURLStr = @"http://f.hiphotos.baidu.com/album/w%3D2048/sign=89a56dc418d8bc3ec60801cab6b3a71e/8694a4c27d1ed21bebd0f61bac6eddc451da3f37.jpg";
        
        //请求保存图片
        NSURL *aImageURL = [NSURL URLWithString:aURLStr];
        
        
        //
        //请求
        ASIHTTPRequest *aASI = [ASIHTTPRequest requestWithURL:aImageURL];
        [aASI setCompletionBlock:^{
            //获取数据
            NSData *aData = aASI.responseData;
            UIImage *aImage = [UIImage imageWithData:aData];
            if (aImage) {
                NSLog(@"success");
                
                //删除原来图片
                
                
                //保存到沙盒
                [self saveImage:aImage];
                
                success(nil);
            }
            else{
                NSLog(@"fail");
                
                //删除原来图
                [self saveImage:nil];
                failure(nil);
                
            }
            
            
            
        }];
        [aASI setFailedBlock:^{
            //删除原来图
            
            [self saveImage:nil];
            failure(nil);
            
        }];
        [aASI startAsynchronous];
        
        
        
        
        //
        NSLog(@"manger success");
        //success(data);
    } failure:^(NSError *error) {
        failure(error);
    }];
}



-(void)saveImage:(UIImage *)aImage{
    
    //删除原来图片
    [[NSUserDefaults standardUserDefaults] setValue:nil forKeyPath:BOOT_IMAGE_PATH];
    
    
    
    //保存新图片
    if (aImage) {
        
        //UIImage *aImageNew = [[UIImage alloc] initWithCGImage:aImage.CGImage];
        [[NSUserDefaults standardUserDefaults] setValue:UIImagePNGRepresentation(aImage) forKeyPath:BOOT_IMAGE_PATH];
        
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}



@end




