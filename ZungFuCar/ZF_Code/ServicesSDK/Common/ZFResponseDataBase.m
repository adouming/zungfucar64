//
//  ZFResponseDataBase.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import "ZFResponseDataBase.h"

@implementation ZFResponseDataBase

- (id)initWithJSON:(id)JSON {
    if (self = [super initWithJSON:JSON]) {
        id response = JSON;
        
        self.returnCode = @200;
        //self.message = [response stringForKey:@"Msg"];
        self.data = JSON;
    }
    
    return self;
}

@end
