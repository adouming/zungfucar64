//
//  ZFAPIUrl.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

/*所有api接口请求都放这里*/
 
#import <Foundation/Foundation.h>


#define USE_TEST_DATA_MODE 0

#define ZF_API_PAGESIZE       @"100"
#define ZF_API_PAGE           @"1"

#define UAT_Version                          1

#define Product_Version                      0

//API Return Code

#define API_Sucess_Code                     200
#define API_IDType_Error_Code               400 
#define API_NotAuthorized_Error_Code        401
#define API_PayMent_Required_Error_Code     402
#define API_Forbidden_Error_Code            403
#define API_RequestURL_NotFound_Error_Code  404
#define API_Server_Error_Code               500
#define API_Not_Implemented_Error_Code      501




/* if depart from difference Version ,define here */
/*UAT Version*/

#if UAT_Version

#define ZF_API_ERROR_DOMAIN                 @""


#define ZF_HTML_START                       @"<html>"
#define ZF_HTML_END                         @"</html>"
#define ZF_META                             @"<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'><meta name='viewport' content='initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no'>"
#define ZF_CSSRESET                         @"<style>" \
                                                "</style>"

//#define ZF_BASE_URL                         @"http://115.29.77.197/zfchina/"
//#define ZF_BASE_PATH                        @""
#define ZF_BASE_URL                         @"https://www.zfchina.com/"
#define ZF_BASE_PATH                        @"api/"

#define ZF_BASE_PATH_ROOT                   @"/"

#define ZF_BASE_IMAGE_PATH                  @"/"
#define ZF_SMART_PATH                       @"/smart/index.php"
#define ZF_BASE_SMART_IMAGE_PATH            @"/smart/"

#endif

/* Product Version */
#if Product_Version

#define ZF_API_ERROR_DOMAIN                     @""
#define ZF_BASE_URL                         @"http://serverurl"
#define ZF_BASE_PATH                        @"/api"


#endif

/* const api format define here*/

//Steward
#define ZF_PATH_BECOMEMEMBER                @"/api/Booking/fn/becomeMember"
#define ZF_PATH_GETPRODUCTBYCATEGORY        @"/api/Product/fn/getproductByCategory/category/"
#define ZF_PATH_GETDATABYPRODUCTID          @"/api/Eventscope/fn/getDataByproductid/productid/"
#define ZF_PATH_FORGETPASSWORD              @"/api/Member/fn/forgotPassword"
#define ZF_PATH_GET_BOOKINFO                @"/api/Booking/fn/getInfo"
#define ZF_PATH_SAVE_BOOKING_WX             @"post.php?act=wx" //维修
#define ZF_PATH_SAVE_BOOKING_SC             @"post.php?act=sc" //试驾

#define ZF_PATH_NETWORK_STORE               @"/api/Store"
#define ZF_PATH_REGISTER                    @"/api/Member/fn/register"
#define ZF_PATH_LOGIN                       @"/api/Member/fn/login"

#define ZF_PATH_GETCARLIST                  @"get.php?act=car" //车系
#define ZF_PATH_CAR_TYPE                    @"/api/product/fn/getproductByCategory"
#define ZF_PATH_CAR_TYPEMODEL               @"get.php?act=cx&cid=" //车型
#define ZF_PATH_CARMODEL_TYPE               @"get.php?act=cx&cid="

//#define ZF_PATH_CAR_MODEL                   @"/api/Productdetail/fn/getCarModel"
#define ZF_PATH_GET_INDUSTRY                @"/api/Dictionary/fn/getIndustry"
#define ZF_PATH_GET_JOBTYPE                 @"/api/Dictionary/fn/getJob"
//#define ZF_PATH_GET_PERSONALINFO            @"/api/Member/fn/getMemberById"
#define ZF_PATH_GET_PERSONALINFO            @"/api/Member/"

#define ZF_PATH_NOTICE                      @"/api/Emaillog/fn/getByMemberId"
#define ZF_PATH_BOOKED                      @"/api/Booking/fn/getByMemberId"
#define ZF_PATH_CITY                        @"get.php?act=wxcity" //城市
#define ZF_PATH_EXHIBITION                  @"get.php?act=sc&cityid=" //试驾店
#define ZF_PATH_SERVICE                     @"get.php?act=wx&cityid=" //维修店

//最新资讯news
#define ZF_PATH_NEWS_LIST                   @"/api/option/fn/getoptionByCategory"

#define ZF_PATH_VIEWID                      @"list.php?id="

//#define DEFAULTHttp_X_U                     @"app@credads.com"
//#define DEFAULTHttp_X_P                     @"33d95126c39d8cf9d0ca92189290cedd"
#define DEFAULTHttp_X_U                     @"dengqw@zfchina.com"
#define DEFAULTHttp_X_P                     @"16d7a4fca7442dda3ad93c9a726597e4"

#define HTTP_X_U                     @"dengqw@zfchina.com"
#define HTTP_X_P                     @"16d7a4fca7442dda3ad93c9a726597e4"
/* const api end */
