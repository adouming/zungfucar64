//
//  ZFResponseDataBase.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KADataBase.h"
@interface ZFResponseDataBase : KADataBase

@property(nonatomic, strong)NSNumber *returnCode;
@property(nonatomic, strong)NSString *message;
@property(nonatomic, strong)NSDictionary *data;

@end
