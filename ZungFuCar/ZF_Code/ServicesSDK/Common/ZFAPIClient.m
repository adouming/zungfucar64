//
//  ZFAPIClient.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import "ZFAPIClient.h"
//#import "AFHTTPClientLogger.h"
@implementation ZFAPIClient



+ (id)sharedHTTPClient {
	static dispatch_once_t pred;
    static AFHTTPRequestOperationManager *shared_instance = nil;
	
    dispatch_once(&pred, ^{
        shared_instance = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:ZF_BASE_URL]];
        [shared_instance.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        shared_instance.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [shared_instance.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"HTTP-X-U"];
        [shared_instance.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"HTTP-X-P"];
    });
    
	return shared_instance;
}

+ (id)sharedClient {
	static dispatch_once_t pred;
    static ZFAPIClient *shared_instance = nil;
	
    dispatch_once(&pred, ^{
		shared_instance = [[self alloc] init];
        shared_instance.client = [self sharedHTTPClient];
    });
    
	return shared_instance;
}

+ (NSString *)currentDateTime{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *strDateTime = [dateFormatter stringFromDate:date];
    return [strDateTime stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString *)getFullPathFromPath:(NSString *)path {
    return [[NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_BASE_PATH_ROOT] stringByAppendingString:path];
}
- (void)responseErrorWithReturnCode:(NSInteger)returnCode
                            failure:(void (^)(NSError *error))failure{
    switch (returnCode) {
        case API_IDType_Error_Code: {
            NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"ID or  data must be numeric" forKey:NSLocalizedDescriptionKey]];
            failure(err);
            break;
        }
            
        case API_NotAuthorized_Error_Code: {
            NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Not authorized" forKey:NSLocalizedDescriptionKey]];
            failure(err);
            break;
        }
        case API_PayMent_Required_Error_Code: {
            NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Payment Required" forKey:NSLocalizedDescriptionKey]];
            failure(err);
            break;
        }
        case API_Forbidden_Error_Code: {
            NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Forbidden" forKey:NSLocalizedDescriptionKey]];
            failure(err);
            break;
        }
        case API_RequestURL_NotFound_Error_Code: {
            NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The request URL was not found" forKey:NSLocalizedDescriptionKey]];
            failure(err);
            break;
        }
        case API_Server_Error_Code: {
            NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The server encountered an error when processing your request" forKey:NSLocalizedDescriptionKey]];
            failure(err);
            break;
        }
        case API_Not_Implemented_Error_Code: {
            NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"The requested function is not implemented" forKey:NSLocalizedDescriptionKey]];
            failure(err);
            break;
        }
            
        default: {
            NSError *err = [NSError errorWithDomain:ZF_API_ERROR_DOMAIN code:returnCode userInfo:[NSDictionary dictionaryWithObject:@"Unknow error" forKey:NSLocalizedDescriptionKey]];
            failure(err);
            break;
        }
    }
    
}
@end
