//
//  ZFAPIClient.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZFAPIUrl.h"
#import "NSString+MD5.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
@interface ZFAPIClient : NSObject


@property(nonatomic, strong)AFHTTPRequestOperationManager *client;


+ (id)sharedHTTPClient;
+ (id)sharedClient;
+ (NSString *)currentDateTime;
+ (NSString *)getFullPathFromPath:(NSString *)path;
- (void)responseErrorWithReturnCode:(NSInteger)returnCode
                            failure:(void (^)(NSError *error))failure;
@end
