//
//  SelectTableViewCell.m
//  AudiShow
//
//  Created by zhangyt on 14-3-18.
//  Copyright (c) 2014年 LanOu3G. All rights reserved.
//

#import "SelectTableViewCell.h"

@implementation SelectTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _carNameLable = [[UILabel alloc] initWithFrame:CGRectMake(2, 0.5f, 105 , 44)];
        [self.contentView addSubview:_carNameLable];
        [self setBackgroundColor:[UIColor clearColor]];
        
        [_carNameLable setTextAlignment:NSTextAlignmentRight];

        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 1)];
        [lineView setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:lineView];
      

        self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.frame] ;
        self.selectedBackgroundView.backgroundColor = [UIColor blueColor];
    }
    
    return self;
}






- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
