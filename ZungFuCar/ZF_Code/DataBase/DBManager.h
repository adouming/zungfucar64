//
//  DBManager.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NULevelDB/NULDBDB.h"
#import "ZFAPIUrl.h"
#import "ZFStewardManager.h"
#import "NewsManager.h"
#import "NetShowManager.h"
#import "ZFClubManager.h"


#define ZF_StoreData_Error_DOMAIN      @""
#define ZF_StoreData_Error_Code        -200

#define ZF_DB_NAME                          		@"ZungFuCarDB"
#define ZF_MESSAGES_KEY                             @"Message"

#define ZF_STORE_KEY                                @"ZF_Store"
#define ZF_NOTICE_KEY                               @"ZF_Notice"
#define ZF_BOOK_KEY                                 @"ZF_Book"
#define ZF_USER_INFO_KEY                            @"ZF_User_Info"
#define ZF_CITY_KEY                                 @"ZF_City_Info"

#define ZF_Exhibition_KEY                           @"ZF_Exhibition_Info"

#define ZF_JOB_INFO_KEY                             @"ZF_JOB_Info"
#define ZF_INDUSTRY_INFO_KEY                        @"ZF_Industry_Info"
#define ZF_SERVICE_TIME_KEY                         @"ZF_Service_Time"
#define ZF_TEST_TIME_KEY                            @"ZF_TEST_Time"

#define ZF_ALL_car_type_list                        @"ZF_ALL_car_type_list" //车系
#define ZF_ALL_car_model_list                       @"ZF_ALL_car_model_list"//车型
#define ZF_ALL_CAR_KEY                              @"ZF_All_Car" //车系
#define ZF_ALL_CAR_MODEL_KEY                        @"ZF_All_Car_model" //车型
#define ZF_BANZ_CARD_KEY                            @"ZF_Banz_Car"
#define ZF_Net_Show_BANZ_CARD_KEY                   @"ZF_Net_Show_Banz_Car"
#define ZF_AMG_CARD_KEY                             @"ZF_AMG_Car"
#define ZF_SMART_CARD_KEY                           @"ZF_Smart_Car"
#define ZF_CAR_MODEL_KEY                            @"ZF_Car_Model"

#define ZF_NEWS_LIST_KEY                            @"ZF_NEWS_LIST_KEY"
#define ZF_NEWS_LIST_TIME_KEY                       @"ZF_NEWS_LIST_TIME_KEY"
#define ZF_NEWS_LIST2_KEY                           @"ZF_NEWS_LIST2_KEY"
#define ZF_NEWS_LIST2_TIME_KEY                      @"ZF_NEWS_LIST2_TIME_KEY"
#define ZF_MODEL_PRIVILEGE_KEY                      @"ZF_MODEL_PRIVILEGE_KEY"
#define ZF_NETSHOW_CLASS_LIST_KEY                   @"ZF_NETSHOW_CLASS_LIST_KEY"
#define ZF_NETSHOW_CLASS_LIST_TIME_KEY              @"ZF_NETSHOW_CLASS_LIST_TIME_KEY"
#define ZF_NETSHOW_CLASS_LIST2_KEY                  @"ZF_NETSHOW_CLASS_LIST2_KEY"
#define ZF_NETSHOW_CLASS_LIST2_TIME_KEY             @"ZF_NETSHOW_CLASS_LIST2_TIME_KEY"
#define ZF_NETSHOW_CLASS_LIST3_KEY                  @"ZF_NETSHOW_CLASS_LIST3_KEY"
#define ZF_NETSHOW_CLASS_LIST3_TIME_KEY             @"ZF_NETSHOW_CLASS_LIST3_TIME_KEY"
#define ZF_NETSHOW_CLASS_DETAIL_KEY                 @"ZF_NETSHOW_CLASS_DETAIL_KEY"
#define ZF_RELATED_EVENTS_KEY                       @"ZF_RELATED_EVENTS_KEY"

@interface DBManager : NSObject

@property(nonatomic, strong)NULDBDB *db;

+ (DBManager *)sharedManager;
+ (void)destroyDatabase;
- (NSURL*)baseUrl;
- (NSString*)wrapperHtml:(NSString*) contentStr ;

//******* example ********
- (void)storeMessages:(NSMutableArray *)messages sucess:(void (^)(BOOL state))sucess fail:(void (^)(NSError *error))failure;
- (NSMutableArray *)getMessages;
///*********end*************

// storeData
- (void)storeStoreData:(ZFNetServerStoreMutableArray *)storeData
                    Sucess:(void(^)(ZFNetServerStoreMutableArray *storeData))sucess
                    fail:(void(^)(NSError *error))failure;
- (ZFNetServerStoreMutableArray *)getStoreData;

//userInfo
- (void)storeUserData:(ZFUserInfoData *)storeData
                Sucess:(void(^)(ZFUserInfoData *storeData))sucess
                  fail:(void(^)(NSError *error))failure;
- (ZFUserInfoData *)getUserData;
//noticeData
- (void)storeNoticeData:(ZFNoticeMutableArray *)storeData
                 Sucess:(void(^)(ZFNoticeMutableArray *storeData))sucess
                   fail:(void(^)(NSError *error))failure;
- (ZFNoticeMutableArray *)getNoticeData;

//bookData
- (void)storeBookData:(ZFBookMutableArray *)storeData
                 Sucess:(void(^)(ZFBookMutableArray *storeData))sucess
                   fail:(void(^)(NSError *error))failure;
- (ZFBookMutableArray *)getBookData;
//city
- (void)storeCityData:(NSMutableArray *)storeData
               Sucess:(void(^)(NSMutableArray *storeData))sucess
                 fail:(void(^)(NSError *error))failure;
- (void)storeExhibitionData:(NSMutableArray *)storeData
                        cid:(int)cityID
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure;

- (NSMutableArray *)getCityData;
- (NSMutableArray *)getExhibitionData:(int)cityID;
//ServiceTime
- (void)storeServiceTimeData:(NSMutableArray *)storeData
                      Sucess:(void(^)(NSMutableArray *storeData))sucess
                        fail:(void(^)(NSError *error))failure;
- (NSMutableArray*)getServiceData;
- (NSMutableArray *)getServiceTimeData;

//TestTime
- (void)storeTestTimeData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getTestTimeData;

//job
- (void)storeJobData:(NSMutableArray *)storeData
              Sucess:(void(^)(NSMutableArray *storeData))sucess
                fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getJobData;

//industry
- (void)storeIndustryData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getIndustryData;

//Banz Car
- (void)storeCarData:(NSMutableArray *)storeData
                  Sucess:(void(^)(NSMutableArray *storeData))sucess
                    fail:(void(^)(NSError *error))failure;
- (void)storeCarModelData:(NSMutableArray *)storeData
                      cid:(int)cid
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getCarModelByType: (int)cid ;
- (NSMutableArray *)getCallCarModelData;
- (NSMutableArray *)getCallCarTypeData;
- (NSMutableArray *)getBanzCarData;
//配件
- (void)storePeijianData:(NSMutableArray *)storeData
                  Sucess:(void(^)(NSMutableArray *storeData))sucess
                    fail:(void(^)(NSError *error))failure key:(NSString *)key;
- (NSMutableArray *)getPeijianData:(NSString *)key;
//精品
- (void)storeJingPingData:(NSMutableArray *)storeData
                  Sucess:(void(^)(NSMutableArray *storeData))sucess
                    fail:(void(^)(NSError *error))failure key:(NSString *)key;
- (NSMutableArray *)getJingPingData:(NSString *)key;
//车主专享
- (void)storeCarMasterShareData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure key:(NSString *)key;
- (NSMutableArray *)getCarMasterShareData:(NSString *)key;
//网上展厅
- (void)storeNetShowCarData:(NSMutableArray *)storeData
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure key:(NSString *)key;
- (NSMutableArray *)getNetShowCarData:(NSString *)key;
//AMG Car
- (void)storeAMGCarData:(NSMutableArray *)storeData
                 Sucess:(void(^)(NSMutableArray *storeData))sucess
                   fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getAMGCarData;
//Smart Car
- (void)storeSmartCarData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getSmartCarData;

//市场活动
- (void)storeNewsListData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getNewsListData;
//车型优惠
- (void)storeModelPrivilegeListData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getModelPrivilegeListData;

//相关活动
- (void)storeRelatedEventsListData:(NSMutableArray *)storeData
                            Sucess:(void(^)(NSMutableArray *storeData))sucess
                              fail:(void(^)(NSError *error))failure key:(NSString *)key1;
- (NSMutableArray *)getRelatedEventsListData:(NSString *)key1;

/********************************************************************************
 liming start
 *********************************************************************************/
//newsList
//市场活动
- (void)storeNewsListData:(NSMutableArray *)storeData withType:(NSInteger)aType Sucess:(void(^)(NSMutableArray *storeData))sucess fail:(void(^)(NSError *error))failure;
- (NSString *)getNewsListTimeDataWithType:(NSInteger)aType;
- (NSMutableArray *)getNewsListDataWithType:(NSInteger)aType;

//网上展厅
//车型列表
- (void)storeNetShowClassListData:(NSMutableArray *)storeData
                         withType:(NSInteger)aType
                           Sucess:(void(^)(NSMutableArray *storeData))sucess
                             fail:(void(^)(NSError *error))failure;
- (NSString *)getNetShowClassListTimeDataWithType:(NSInteger)aType;
- (NSMutableArray *)getNetShowClassListDataWithType:(NSInteger)aType;
//车型详情
/*
 全部详情放到array保存
 */
- (void)storeNetShowClassDetailData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                             fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getNetShowClassDetailData;

/********************************************************************************
 liming end
 *********************************************************************************/

/*关于仁孚会*/
- (void)storeAboutClubData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getAboutClubData;
/*配件*/
- (void)storePeiJianData:(NSMutableArray *)storeData
                        Sucess:(void(^)(NSMutableArray *storeData))sucess
                          fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getPeiJianData;
/*售后服务*/
- (void)storeCustomServiceData:(NSMutableArray *)storeData
                        Sucess:(void(^)(NSMutableArray *storeData))sucess
                          fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getCustomServiceData;
/*会员章程*/
- (void)storeMemberBookData:(NSMutableArray *)storeData
                    Sucess:(void(^)(NSMutableArray *storeData))sucess
                      fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getMemberBookData;

- (void)storePriceTableData:(NSMutableArray *)storeData
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure;
    
- (NSMutableArray *)getPriceTableData;

/*奖赏计划*/
- (void)storeRewardPlanData:(NSMutableArray *)storeData
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getRewardPlanData;
/*会员优惠*/
- (void)storeMemberDiscountData:(NSMutableArray *)storeData
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getMemberDiscountData;

 
/********************************************************************************
 xiaoming start
 *********************************************************************************/

#pragma mark - xiaoming 车型
- (void)storeCarModelData:(NSMutableArray *)storeData
                withIDStr:(NSString *)idStr
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getCarModelDataWithID:(NSString *)idStr;
/*通用存储方法*/
- (void)storeModelData:(NSMutableArray *)storeData
                keyStr:(NSString*)key
                Sucess:(void(^)(NSMutableArray *storeData))sucess
                  fail:(void(^)(NSError *error))failure;
/* 通用读取方法 */
- (NSMutableArray *)getModelData:(NSString*)key;
/*公司简介*/
- (void)storeCompanyProfileData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getCompanyProfileData;

/*发展历程*/
- (void)storeDevelopmentHistoryData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getDevelopmentHistoryData;

/*企业文化*/
- (void)storeCompanyCultureData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getCompanyCultureData;
/*社会责任*/

- (void)storeSocialResponsibilityData:(NSMutableArray *)storeData
                       Sucess:(void(^)(NSMutableArray *storeData))sucess
                         fail:(void(^)(NSError *error))failure;
- (NSMutableDictionary *)getSocialResponsibilityData;
/*员工关怀*/

- (void)storeStaffConcernData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure;
- (NSMutableDictionary *)getStaffConcernData;
/*人才培训与发展*/
- (void)storeTalentTrainingData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getTalentTrainingData;

/*臻至服务*/
- (void)storeRealSeriviceData:(NSMutableArray *)storeData
                       Sucess:(void(^)(NSMutableArray *storeData))sucess
                         fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getRealSeriviceData;

/*金融*/
- (void)storeFinanceData:(NSMutableArray *)storeData
                  Sucess:(void(^)(NSMutableArray *storeData))sucess
                    fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getFinanceData;

/*保险*/
- (void)storeInsuranceData:(NSMutableArray *)storeData
                    Sucess:(void(^)(NSMutableArray *storeData))sucess
                      fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getInsuranceData;

/*大客户销售*/
- (void)storeBigCustomerData:(NSMutableArray *)storeData
                      Sucess:(void(^)(NSMutableArray *storeData))sucess
                        fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getBigCustomerData;

/*精品*/
- (void)storeBoutiqueData:(NSMutableArray *)storeData
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getBoutiqueData;

/*配件*/
- (void)storePartsData:(NSMutableArray *)storeData
                Sucess:(void(^)(NSMutableArray *storeData))sucess
                  fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getPartsData;

/*社会招聘*/
- (void)storeSocietyRecruitmentData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getSocietyRecruitmentData;

/*校园招聘*/
- (void)storeCampusRecruitmentData:(NSMutableArray *)storeData
                            Sucess:(void(^)(NSMutableArray *storeData))sucess
                              fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getCampusRecruitmentData;


/*星睿二手车*/
- (void)storeSecondCarData:(NSMutableArray *)storeData
                       cid:(int) cid
                    Sucess:(void(^)(NSMutableArray *storeData))sucess
                      fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getSecondCarData:(int) cid;

/*加入我们*/
- (void)storeJoinUSData:(NSMutableArray *)storeData
                 Sucess:(void(^)(NSMutableArray *storeData))sucess
                   fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getJoinUSData;

/*售后服务*/
- (void)storeAfterSeriveceData:(NSMutableArray *)storeData
                        Sucess:(void(^)(NSMutableArray *storeData))sucess
                          fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getAfterSeriveceData;

/*联系我们*/
- (void)storeContactUSData:(NSMutableArray *)storeData
                    Sucess:(void(^)(NSMutableArray *storeData))sucess
                      fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getContactUSData;

#pragma mark - 计算器方案
//计算器方案
- (void)storeSchemeData:(NSMutableArray *)storeData
                    key:(NSString *)storeKey
                 scheme:(NSString *)scheme
                 Sucess:(void(^)(NSMutableArray *storeData))sucess
                   fail:(void(^)(NSError *error))failure;
- (NSMutableArray *)getSchemeData:(NSString *)storeKey scheme:(NSString *)scheme;

#pragma mark - 列表选择车型数据
- (void)storeCarListData:(NSMutableArray *)storeData
                  Sucess:(void(^)(NSMutableArray *storeData))sucess
                    fail:(void(^)(NSError *error))failure
                     key:(NSString *)key;
- (NSMutableArray *)getCarListData:(NSString *)key;

/********************************************************************************
 xiaoming end
 *********************************************************************************/

@end











