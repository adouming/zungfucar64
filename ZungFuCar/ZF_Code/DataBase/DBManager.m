//
//  DBManager.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/1/13.
//  Copyright (c) 2013 lxm. All rights reserved.
//

#import "DBManager.h"





@implementation DBManager
+ (DBManager *)sharedManager {
	static dispatch_once_t pred;
	static id shared_manager = nil;
	
	dispatch_once(&pred, ^{
        shared_manager = [[self alloc] init];
    });
	
	return shared_manager;
}

- (id)init {
	if (self = [super init]) {
		NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
		NSString *dbPath = [documentPath stringByAppendingPathComponent:ZF_DB_NAME];
		
		_db = [[NULDBDB alloc] initWithLocation:dbPath bufferSize:(1<<22)];
		NSLog(@"db path : %@", _db.location);
	}
	
    
    
	return self;
}

+ (void)destroyDatabase {
	
	NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
	NSString *dbPath = [documentPath stringByAppendingPathComponent:ZF_DB_NAME];
    [NULDBDB destroyDatabase:dbPath];
}
- (NSURL*)baseUrl {
    // 获取当前应用的根目录
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    return baseURL;
}

- (NSString*)wrapperHtml:(NSString*) contentStr {
    NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@",  @"http://www.zfchina.com", ZF_BASE_IMAGE_PATH];
    contentStr=[contentStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
    
    
    contentStr=[contentStr stringByReplacingOccurrencesOfString:@"http://www.zfchina.com/https://www.zfchina.com" withString:@"http://www.zfchina.com"];
    
    
    contentStr=[contentStr stringByReplacingOccurrencesOfString:@"http://www.zfchina.com/http://www.zfchina.com" withString:@"http://www.zfchina.com"];
    
    NSString *injectJquery = @"<script type=\"text/javascript\" src=\"jquery-1.11.3.min.js\"></script><script type=\"text/javascript\" src=\"app.js\"></script>";
    NSString *htmlStr =[NSString stringWithFormat:@"%@%@%@%@%@%@", ZF_HTML_START, ZF_META, injectJquery, ZF_CSSRESET, contentStr, ZF_HTML_END];
    NSLog(htmlStr);
    return htmlStr;
}
#pragma mark -
#pragma mark - Alex start
#pragma mark - example
- (void)storeMessages:(NSMutableArray *)messages sucess:(void (^)(BOOL state))sucess fail:(void (^)(NSError *error))failure{
    BOOL store = [self storeMessages:messages];
    if (store) {
        sucess(YES);
    }else {
      
    }
}

- (BOOL)storeMessages:(NSMutableArray *)messages   {
    NSString *key = [NSString stringWithFormat:@"%@", ZF_MESSAGES_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:messages];
    return [self.db storeData:data forKey:key error:nil];
}

- (NSMutableArray *)getMessages {
    NSString *key = [NSString stringWithFormat:@"%@", ZF_MESSAGES_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *messages = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
  
    return messages;
}

#pragma mark - store
- (void)storeStoreData:(ZFNetServerStoreMutableArray *)storeData
                Sucess:(void(^)(ZFNetServerStoreMutableArray *Storedata))sucess
                  fail:(void(^)(NSError *error))failure{
    
    //db store data style is cover,firt we need to filter the new data and store the filtered data with the history one
    
    // 1. filter
    NSArray *newTempDataArray = [NSArray arrayWithArray:storeData];
    ZFNetServerStoreMutableArray *historyStoreDatas = [[DBManager sharedManager] getStoreData];
   
    ZFNetServerStoreMutableArray *currentStoreDatas = historyStoreDatas;
 
    NSArray *historyTempDataArray = [NSArray arrayWithArray:historyStoreDatas];
    if (!historyStoreDatas.count) {
        currentStoreDatas = storeData;
    }else{
        for (int i=0; i<newTempDataArray.count; i++) {
            ZFNetServerStoreData *newStoreData = [newTempDataArray objectAtIndex:i];
            int exit = NO;
            for (int j=0; j<historyTempDataArray.count; j++) {
                ZFNetServerStoreData *historyStoreData = [historyTempDataArray objectAtIndex:j];
                if (historyStoreData.storeID == newStoreData.storeID) {
                    [currentStoreDatas replaceObjectAtIndex:j withObject:newStoreData];
                    exit = YES;
                    break;
                }
            }
            if (!exit) {
                [currentStoreDatas addObject:newStoreData];
            }
        }
    }
 
    //2. store

    NSString *key = [NSString stringWithFormat:@"%@", ZF_STORE_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:currentStoreDatas];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        sucess(currentStoreDatas);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeStoreData Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}

- (ZFNetServerStoreMutableArray *)getStoreData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_STORE_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    ZFNetServerStoreMutableArray *storeDatas = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return storeDatas;
}

#pragma mark - notice
- (void)storeNoticeData:(ZFNoticeMutableArray *)storeData
                 Sucess:(void(^)(ZFNoticeMutableArray *storeData))sucess
                   fail:(void(^)(NSError *error))failure{
    
    //db store data style is cover,firt we need to filter the new data and store the filtered data with the history one
    
    // 1. filter
    NSArray *newTempDataArray = [NSArray arrayWithArray:storeData];
    NSMutableArray *newDataArrayClass = [NSMutableArray array];
    for (int i=0; i<newTempDataArray.count; i++) {
        ////////////////////
        ZFMemberNoticeeData *newStoreData = [[ZFMemberNoticeeData alloc] init];
        NSDictionary *newTempDataDic = [newTempDataArray objectAtIndex:i];
        newStoreData.noticeID = [[newTempDataDic objectForKey:@"ID"] integerValue];
        newStoreData.subject = [newTempDataDic objectForKey:@"Subject"];
        newStoreData.sendBy = [newTempDataDic objectForKey:@"SendBy"];
        newStoreData.createdTime = [newTempDataDic objectForKey:@"CreatedTime"];
        newStoreData.body = [newTempDataDic objectForKey:@"Body"];
        id isRead = [newTempDataDic objectForKey:@"isread"];
        if ([isRead isKindOfClass:[NSString class]]) {
            newStoreData.isread = [isRead integerValue];
        }else{
            newStoreData.isread = 0;
        }
        /////////////////////////////////////
        [newDataArrayClass addObject:newStoreData];
    }
    
    
    
    ZFNoticeMutableArray *historyStoreDatas = [[DBManager sharedManager] getNoticeData];
    
    ZFNoticeMutableArray *currentStoreDatas = historyStoreDatas;
    
    NSArray *historyTempDataArray = [NSArray arrayWithArray:historyStoreDatas];
    if (!historyStoreDatas.count) {
        currentStoreDatas = newDataArrayClass;
    }else{
        for (int i=0; i<newTempDataArray.count; i++) {
            
            ZFMemberNoticeeData *newTmp = [newDataArrayClass objectAtIndex:i];
            
            int exit = NO;
            for (int j=0; j<historyTempDataArray.count; j++) {
                ZFMemberNoticeeData *historyStoreData = [historyTempDataArray objectAtIndex:j];
                if (historyStoreData.noticeID == newTmp.noticeID) {
                    [currentStoreDatas replaceObjectAtIndex:j withObject:newTmp];
                    exit = YES;
                    break;
                }
            }
            if (!exit) {
                [currentStoreDatas addObject:newTmp];
            }
        }
    }
    
    //2. store
    
    NSString *key = [NSString stringWithFormat:@"%@", ZF_NOTICE_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:currentStoreDatas];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        sucess(currentStoreDatas);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeNoticeData Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (ZFNoticeMutableArray *)getNoticeData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_NOTICE_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    ZFNoticeMutableArray *noticeDatas = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return noticeDatas;

}

#pragma mark - book
- (void)storeBookData:(ZFBookMutableArray *)storeData
               Sucess:(void(^)(ZFBookMutableArray *storeData))sucess
                 fail:(void(^)(NSError *error))failure{
    // 1. filter
    NSArray *newTempDataArray = [NSArray arrayWithArray:storeData];
    ZFBookMutableArray *historyStoreDatas = [[DBManager sharedManager] getBookData];
    
    ZFBookMutableArray *currentStoreDatas = historyStoreDatas;
    
    NSArray *historyTempDataArray = [NSArray arrayWithArray:historyStoreDatas];
    if (!historyStoreDatas.count) {
        currentStoreDatas = storeData;
    }else{
        for (int i=0; i<newTempDataArray.count; i++) {
            ZFMemberBookData *newStoreData = [newTempDataArray objectAtIndex:i];
            int exit = NO;
            for (int j=0; j<historyTempDataArray.count; j++) {
                ZFMemberBookData *historyStoreData = [historyTempDataArray objectAtIndex:j];
                if (historyStoreData.bookID == newStoreData.bookID) {
                    [currentStoreDatas replaceObjectAtIndex:j withObject:newStoreData];
                    exit = YES;
                    break;
                }
            }
            if (!exit) {
                [currentStoreDatas addObject:newStoreData];
            }
        }
    }
    
    //2. store
    
    NSString *key = [NSString stringWithFormat:@"%@", ZF_BOOK_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:currentStoreDatas];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        sucess(currentStoreDatas);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeBookData Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };

    
}
- (ZFBookMutableArray *)getBookData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_BOOK_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    ZFBookMutableArray *noticeDatas = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return noticeDatas;

}

#pragma mark - userInfo
- (void)storeUserData:(ZFUserInfoData *)storeData
               Sucess:(void(^)(ZFUserInfoData *storeData))sucess
                 fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_USER_INFO_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeUserData Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
    
}
- (ZFUserInfoData *)getUserData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_USER_INFO_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    ZFUserInfoData *userData = (ZFUserInfoData *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    return userData;
}
#pragma mark - city & exhibition
- (void)storeExhibitionData:(NSMutableArray *)storeData
                        cid:(int)cityID
                            Sucess:(void(^)(NSMutableArray *storeData))sucess
                              fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@%d", ZF_Exhibition_KEY,cityID];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        //        sucess(storeData);
    }else{
        //        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeCityAndExhibitionData Fail" forKey:NSLocalizedDescriptionKey]];
        //        failure(err);
    };
    
}
- (void)storeCityData:(NSMutableArray *)storeData
               Sucess:(void(^)(NSMutableArray *storeData))sucess
                 fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_CITY_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
//        sucess(storeData);
    }else{
//        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeCityAndExhibitionData Fail" forKey:NSLocalizedDescriptionKey]];
//        failure(err);
    };

}
- (NSMutableArray *)getExhibitionData:(int)cityID {
    NSString *key = [NSString stringWithFormat:@"%@%d", ZF_Exhibition_KEY, cityID];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *cityData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    ///////////////////////按字母排序/////////
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"_displayText" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sorter count:1];
    NSArray *sortedArray = [cityData sortedArrayUsingDescriptors:sortDescriptors];
    NSMutableArray *temp = [NSMutableArray arrayWithArray:sortedArray];
 
    return temp;

}
- (NSMutableArray *)getCityData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_CITY_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *cityData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    ///////////////////////按字母排序/////////    
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"_displayText" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sorter count:1];
    NSArray *sortedArray = [cityData sortedArrayUsingDescriptors:sortDescriptors];
    NSMutableArray *temp = [NSMutableArray arrayWithArray:sortedArray];
    
    //去除字母
//    if (temp.count) {
//        for (int i=0; i<temp.count; i++) {
//            ZFNetServerCityExhibitionData *city = [temp objectAtIndex:i];
//            NSMutableDictionary* dCity = city;
//            NSMutableString *str = [NSMutableString stringWithString: [dCity  objectForKey:@"name"]];
//            [str deleteCharactersInRange:NSMakeRange(0, 2)];
//            
//            [dCity setValue:str forKey:@"name"];
//        }
//    }
    //////////////////
    return temp;
}

#pragma mark - ServiceTime
- (void)storeServiceTimeData:(NSMutableArray *)storeData
                      Sucess:(void(^)(NSMutableArray *storeData))sucess
                        fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_SERVICE_TIME_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeServiceTimeData Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray*)getServiceData {
                //一年的秒数
    NSMutableArray* dataObj = [@[] mutableCopy];
    float oneYearTime = 60*60*24*365;
    float oneDayTime = 60*60*24;
    //获得当前时间
    NSDate *currentDate = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
    //NSDate *minDate = [currentDate dateByAddingTimeInterval:0];
    //NSDate *maxDate = [currentDate dateByAddingTimeInterval:100*oneYearTime];
    //最小可选时间
    for (int i = 1; i < 100; i++) {
        NSDate *itrDate =  [currentDate dateByAddingTimeInterval: i * oneDayTime];
        [dataObj addObject:itrDate];
    }
    
    
    
    return dataObj;
}
- (NSMutableArray *)getServiceTimeData{
//    NSString *key = [NSString stringWithFormat:@"%@", ZF_SERVICE_TIME_KEY];
//    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
//    NSMutableArray *serviceTimeData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
//    return serviceTimeData;
    NSMutableArray* data = [@[
                              @"上午8：30-9：30",
                              @"上午9：30-10：30",
                              @"上午10：30-11：30",
                              @"上午11：30-12：30",
                              @"下午1：30-2：30",
                              @"下午2：30-3：30",
                              @"下午3：30-4：30",
                              @"下午4：30-5：30",
                              @"下午5：30-6：30",
                              @"晚上6：30-7：30",
                              @"晚上7：30-8：00"] mutableCopy];
    
    return data;
}

#pragma mark - TestTime
- (void)storeTestTimeData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_TEST_TIME_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeTestTimeData Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
    
}

- (NSMutableArray *)getTestTimeData{
    /*
    NSString *key = [NSString stringWithFormat:@"%@", ZF_TEST_TIME_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *testTimeData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return testTimeData;
     */
    NSMutableArray* data = [@[
    @"上午8：30-9：30",
    @"上午9：30-10：30",
    @"上午10：30-11：30",
    @"上午11：30-12：30",
    @"下午1：30-2：30",
    @"下午2：30-3：30",
    @"下午3：30-4：30",
    @"下午4：30-5：30",
    @"下午5：30-6：30",
    @"晚上6：30-7：30",
    @"晚上7：30-8：00"] mutableCopy];
    return data;
}

#pragma mark - job
- (void)storeJobData:(NSMutableArray *)storeData
              Sucess:(void(^)(NSMutableArray *storeData))sucess
                fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_JOB_INFO_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@", @"存储工作列表成功");
    }else{
        NSLog(@"%@", @"存储工作列表失败");
    };

    
}
- (NSMutableArray *)getJobData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_JOB_INFO_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *jobData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];

    return jobData;
}

#pragma mark - industry
- (void)storeIndustryData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_INDUSTRY_INFO_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@", @"存储行业列表成功");
    }else{
        NSLog(@"%@", @"存储行业列表失败");
    };

    
}
- (NSMutableArray *)getIndustryData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_INDUSTRY_INFO_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *industryData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return industryData;
}


#pragma mark - 
#pragma mark - Alex end




#pragma mark -
#pragma mark - liming start
/********************************************************************************
 liming start
 *********************************************************************************/

//市场活动
- (void)storeNewsListData:(NSMutableArray *)storeData
                 withType:(NSInteger)aType
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure{
    
    
    NSString *aKey1 = ZF_NEWS_LIST_KEY;
    NSString *aKey2 = ZF_NEWS_LIST_TIME_KEY;
    if (aType==0) {
        aKey1 = ZF_NEWS_LIST_KEY;
         aKey2 = ZF_NEWS_LIST_TIME_KEY;
    }else{
        aKey1 = ZF_NEWS_LIST2_KEY;
         aKey2 = ZF_NEWS_LIST2_TIME_KEY;
    }
    
    //db store data style is cover,firt we need to filter the new data and store the filtered data with the history one
    
    // 1. filter
    NSArray *newTempDataArray = [NSArray arrayWithArray:storeData];
    NSMutableArray *historyStoreDatas = [[DBManager sharedManager] getNewsListDataWithType:aType];
    
    NSMutableArray *currentStoreDatas = historyStoreDatas;
    
    //删除原来保存的数据
    [currentStoreDatas removeAllObjects];
    
    //保存新数据
    
    for (int i=0; i<newTempDataArray.count; i++) {
        
        NewsListData *newStoreData = [newTempDataArray objectAtIndex:i];
        
        [currentStoreDatas addObject:newStoreData];
        if (i==newTempDataArray.count-1) {
            NSString *updatedTime = newStoreData.UpdatedTime;
            //
            if (![updatedTime isEqualToString:@""]) {
                //updatedTime = @"2012-12-02 23:59:59";
                //updatedTime
                NSData *timeData = [NSKeyedArchiver archivedDataWithRootObject:updatedTime];
                [self.db storeData:timeData forKey:aKey2 error:nil];
            } 
        }
    }

    // [currentStoreDatas removeAllObjects];
    //2. store
    
    
    NSString *key = [NSString stringWithFormat:@"%@", aKey1];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:currentStoreDatas];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        sucess(currentStoreDatas);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeStoreData Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
    
}

- (NSString *)getNewsListTimeDataWithType:(NSInteger)aType{
    NSString *aKey = ZF_NEWS_LIST_TIME_KEY;
    if (aType==0) {
        aKey = ZF_NEWS_LIST_TIME_KEY;
    }else{
        aKey = ZF_NEWS_LIST2_TIME_KEY;
    }
    
    NSString *key = [NSString stringWithFormat:@"%@", aKey];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSString *aString = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    //NSMutableArray *storeDatas = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return aString;
    
}

- (NSMutableArray *)getNewsListDataWithType:(NSInteger)aType{
    NSString *aKey = ZF_NEWS_LIST_KEY;
    if (aType==0) {
        aKey = ZF_NEWS_LIST_KEY;
    }else{
        aKey = ZF_NEWS_LIST2_KEY;
    }
    
    NSString *key = [NSString stringWithFormat:@"%@", aKey];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *storeDatas = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return storeDatas;
    
}


//网上展厅
- (void)storeNetShowClassListData:(NSMutableArray *)storeData
                         withType:(NSInteger)aType
                           Sucess:(void(^)(NSMutableArray *storeData))sucess
                             fail:(void(^)(NSError *error))failure{
    NSString *aKey1 = ZF_NETSHOW_CLASS_LIST_KEY;
    NSString *aKey2 = ZF_NETSHOW_CLASS_LIST_TIME_KEY;
    if (aType==0) {
        aKey1 = ZF_NETSHOW_CLASS_LIST_KEY;
        aKey2 = ZF_NETSHOW_CLASS_LIST_TIME_KEY;
    }else if (aType==1) {
        aKey1 = ZF_NETSHOW_CLASS_LIST2_KEY;
        aKey2 = ZF_NETSHOW_CLASS_LIST2_TIME_KEY;
    }else{
        aKey1 = ZF_NETSHOW_CLASS_LIST3_KEY;
        aKey2 = ZF_NETSHOW_CLASS_LIST3_TIME_KEY;
    }
    
    //db store data style is cover,firt we need to filter the new data and store the filtered data with the history one
    
    // 1. filter
    NSArray *newTempDataArray = [NSArray arrayWithArray:storeData];
    NSMutableArray *historyStoreDatas = [[DBManager sharedManager] getNetShowClassListDataWithType:aType];
    
    NSMutableArray *currentStoreDatas = historyStoreDatas;
    
    //删除原来保存的数据
    [currentStoreDatas removeAllObjects];
    
    //保存新数据
    for (int i=0; i<newTempDataArray.count; i++) {
        
        NetShowClassListData *newStoreData = [newTempDataArray objectAtIndex:i];
        
        [currentStoreDatas addObject:newStoreData];
        if (i==newTempDataArray.count-1) {
            NSString *updatedTime = newStoreData.UpdatedTime;
            //
            if (![updatedTime isEqualToString:@""]) {
                //updatedTime = @"2012-12-02 23:59:59";
                //updatedTime
                NSData *timeData = [NSKeyedArchiver archivedDataWithRootObject:updatedTime];
                [self.db storeData:timeData forKey:aKey2 error:nil];
            }
        }
    }
    
    
    
//    [currentStoreDatas removeAllObjects];
    //2. store
    
    
    NSString *key = [NSString stringWithFormat:@"%@", aKey1];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:currentStoreDatas];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        sucess(currentStoreDatas);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeStoreData Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };

}
- (NSString *)getNetShowClassListTimeDataWithType:(NSInteger)aType{
    NSString *aKey = ZF_NETSHOW_CLASS_LIST_TIME_KEY;
    if (aType==0) {
        aKey = ZF_NETSHOW_CLASS_LIST_TIME_KEY;
    }else if (aType==1) {
        aKey = ZF_NETSHOW_CLASS_LIST2_TIME_KEY;
    }else{
        aKey = ZF_NETSHOW_CLASS_LIST3_TIME_KEY;
    }
    
    NSString *key = [NSString stringWithFormat:@"%@", aKey];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSString *aString = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    //NSMutableArray *storeDatas = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return aString;
}
- (NSMutableArray *)getNetShowClassListDataWithType:(NSInteger)aType{
    NSString *aKey = ZF_NETSHOW_CLASS_LIST_KEY;
    if (aType==0) {
        aKey = ZF_NETSHOW_CLASS_LIST_KEY;
    }else if (aType==1) {
        aKey = ZF_NETSHOW_CLASS_LIST2_KEY;
    }else{
        aKey = ZF_NETSHOW_CLASS_LIST3_KEY;
    }
    
    NSString *key = [NSString stringWithFormat:@"%@", aKey];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *storeDatas = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return storeDatas;
}


- (void)storeNetShowClassDetailData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure{
    
    //db store data style is cover,firt we need to filter the new data and store the filtered data with the history one
    
    // 1. filter
    NSArray *newTempDataArray = [NSArray arrayWithArray:storeData];
    NSMutableArray *historyStoreDatas = [[DBManager sharedManager] getNetShowClassDetailData];
    
    NSMutableArray *currentStoreDatas = historyStoreDatas;
    
    
    for (int i=0; i<newTempDataArray.count; i++) {
        
       // NetShowClassListData *newStoreData = [newTempDataArray objectAtIndex:i];
        
        [currentStoreDatas addObject:[newTempDataArray objectAtIndex:i]];
//        if (i==newTempDataArray.count-1) {
//            NSString *updatedTime = newStoreData.UpdatedTime;
//            //
//            if (![updatedTime isEqualToString:@""]) {
//                //updatedTime = @"2012-12-02 23:59:59";
//                //updatedTime
//                NSData *timeData = [NSKeyedArchiver archivedDataWithRootObject:updatedTime];
//                [self.db storeData:timeData forKey:ZF_NETSHOW_CLASS_DETAIL_KEY error:nil];
//            }
//        }
    }
    
    //[currentStoreDatas removeAllObjects];
    //2. store
    
    
    NSString *key = [NSString stringWithFormat:@"%@", ZF_NETSHOW_CLASS_DETAIL_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:currentStoreDatas];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        sucess(currentStoreDatas);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"storeStoreData Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };

}
- (NSMutableArray *)getNetShowClassDetailData{

    NSString *key = [NSString stringWithFormat:@"%@", ZF_NETSHOW_CLASS_DETAIL_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *storeDatas = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return storeDatas;
    
    
    //
    return nil;
}


/********************************************************************************
 liming end
 *********************************************************************************/
#pragma mark -
#pragma mark - liming end



/********************************************************************************
 xiaoming start
 *********************************************************************************/
#pragma mark -
#pragma mark - xiaoming start
#pragma mark -  CarModel



- (void)storeCarModelData:(NSMutableArray *)storeData
                withIDStr:(NSString *)idStr
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@_%@", ZF_CAR_MODEL_KEY,idStr];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_CAR_MODEL_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getCarModelDataWithID:(NSString *)idStr{
    NSString *key = [NSString stringWithFormat:@"%@_%@", ZF_CAR_MODEL_KEY,idStr];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *carModelData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return carModelData;
}
/* 通用存储方法 */

- (void)storeModelData:(NSMutableArray *)storeData
                keyStr:(NSString*)key
                Sucess:(void(^)(NSMutableArray *storeData))sucess
                  fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    NSString* k1 = [NSString stringWithFormat:@"%@_KEY", key];
    NSString* k2 = [NSString stringWithFormat:@"%@_Data", key];
    NSString* failstr = [NSString stringWithFormat:@"%@ Fail", k2];
    BOOL isSucess = [self.db storeData:data forKey:k1 error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:failstr forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
/* 通用读取方法 */
- (NSMutableArray *)getModelData: (NSString*)key
{
    NSString* k1 = [NSString stringWithFormat:@"%@_KEY", key];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:k1 error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}



/*关于仁孚会*/
- (void)storeAboutClubData:(NSMutableArray *)storeData
                    Sucess:(void(^)(NSMutableArray *storeData))sucess
                      fail:(void(^)(NSError *error))failure{
    [self storeModelData:storeData keyStr:@"ZF_AboutClub" Sucess:sucess fail:failure];
}
- (NSMutableArray *)getAboutClubData{
    return [self getModelData: @"ZF_AboutClub"];
}
/*配件*/

- (void)storePeiJianData:(NSMutableArray *)storeData
                        Sucess:(void(^)(NSMutableArray *storeData))sucess
                          fail:(void(^)(NSError *error))failure{
    [self storeModelData:storeData keyStr:@"ZF_PeiJian" Sucess:sucess fail:failure];
}
- (NSMutableArray *)getPeiJianData{
    return [self getModelData: @"ZF_PeiJian"];
}

/*售后服务*/

- (void)storeCustomServiceData:(NSMutableArray *)storeData
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure{
    [self storeModelData:storeData keyStr:@"ZF_CustomService" Sucess:sucess fail:failure];
}
- (NSMutableArray *)getCustomServiceData{
    return [self getModelData: @"ZF_CustomService"];
}


/*会员章程*/

- (void)storeMemberBookData:(NSMutableArray *)storeData
                    Sucess:(void(^)(NSMutableArray *storeData))sucess
                      fail:(void(^)(NSError *error))failure{
    [self storeModelData:storeData keyStr:@"ZF_MemberBook" Sucess:sucess fail:failure];
}

- (NSMutableArray *)getMemberBookData{
    return [self getModelData: @"ZF_MemberBook"];
}

/*价格表*/

- (void)storePriceTableData:(NSMutableArray *)storeData
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure{
    [self storeModelData:storeData keyStr:@"ZF_PriceTable" Sucess:sucess fail:failure];
}

- (NSMutableArray *)getPriceTableData{
    return [self getModelData: @"ZF_PriceTable"];
}

/*奖赏计划*/
- (void)storeRewardPlanData:(NSMutableArray *)storeData
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure{
    [self storeModelData:storeData keyStr:@"ZF_RewardPlan" Sucess:sucess fail:failure];
}

- (NSMutableArray *)getRewardPlanData{
    return [self getModelData: @"ZF_RewardPlan"];
}

/*会员优惠*/
- (void)storeMemberDiscountData:(NSMutableArray *)storeData
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure{
    [self storeModelData:storeData keyStr:@"ZF_MemberDiscount" Sucess:sucess fail:failure];
}

- (NSMutableArray *)getMemberDiscountData{
    return [self getModelData: @"ZF_MemberDiscount"];
}
#pragma mark -  公司简介
- (void)storeCompanyProfileData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure{
    //NSString *key = [NSString stringWithFormat:@"%@", ZF_CompanyProfile_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_CompanyProfile_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_CompanyProfile_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getCompanyProfileData{
    //NSString *key = [NSString stringWithFormat:@"%@_%@", ZF_CAR_MODEL_KEY,idStr];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_CompanyProfile_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  发展历程
- (void)storeDevelopmentHistoryData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure{
    //NSString *key = [NSString stringWithFormat:@"%@", ZF_CompanyProfile_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_DevelopmentHistory_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_DevelopmentHistory_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getDevelopmentHistoryData{
    //NSString *key = [NSString stringWithFormat:@"%@_%@", ZF_CAR_MODEL_KEY,idStr];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_DevelopmentHistory_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}


#pragma mark -  企业文化
- (void)storeCompanyCultureData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure{
    //NSString *key = [NSString stringWithFormat:@"%@", ZF_CompanyProfile_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_CompanyCulture_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_CompanyCulture_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getCompanyCultureData{
    //NSString *key = [NSString stringWithFormat:@"%@_%@", ZF_CAR_MODEL_KEY,idStr];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_CompanyCulture_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}
#pragma mark -  社会责任
- (void)storeSocialResponsibilityData:(NSMutableArray *)storeData
                       Sucess:(void(^)(NSMutableArray *storeData))sucess
                         fail:(void(^)(NSError *error))failure{
    //NSString *key = [NSString stringWithFormat:@"%@", ZF_CompanyProfile_KEY];
    NSDictionary *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_StaffConcern_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_StaffConcern_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableDictionary *)getSocialResponsibilityData{
    //NSString *key = [NSString stringWithFormat:@"%@_%@", ZF_CAR_MODEL_KEY,idStr];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_StaffConcern_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}


#pragma mark -  员工关怀
- (void)storeStaffConcernData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure{
    //NSString *key = [NSString stringWithFormat:@"%@", ZF_CompanyProfile_KEY];
    NSDictionary *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_StaffConcern_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_StaffConcern_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableDictionary *)getStaffConcernData{
    //NSString *key = [NSString stringWithFormat:@"%@_%@", ZF_CAR_MODEL_KEY,idStr];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_StaffConcern_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}


#pragma mark -  /*人才培训与发展*/
- (void)storeTalentTrainingData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_TalentTraining_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_TalentTraining_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getTalentTrainingData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_TalentTraining_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*臻至服务*/
- (void)storeRealSeriviceData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_RealSerivice_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_RealSerivice_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getRealSeriviceData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_RealSerivice_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*金融*/
- (void)storeFinanceData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_Finance_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_Finance_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getFinanceData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_Finance_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*保险*/
- (void)storeInsuranceData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_Insurance_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_Insurance_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getInsuranceData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_Insurance_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*大客户销售*/
- (void)storeBigCustomerData:(NSMutableArray *)storeData
                    Sucess:(void(^)(NSMutableArray *storeData))sucess
                      fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_BigCustomer_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_BigCustomer_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getBigCustomerData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_BigCustomer_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*精品*/
- (void)storeBoutiqueData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_Boutique_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_Boutique_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getBoutiqueData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_Boutique_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*配件*/
- (void)storePartsData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_Parts_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_Parts_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getPartsData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_Parts_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*社会招聘*/
- (void)storeSocietyRecruitmentData:(NSMutableArray *)storeData
                         Sucess:(void(^)(NSMutableArray *storeData))sucess
                           fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_SocietyRecruitment_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_SocietyRecruitment_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getSocietyRecruitmentData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_SocietyRecruitment_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*校园招聘*/
- (void)storeCampusRecruitmentData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_CampusRecruitment_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_CampusRecruitment_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getCampusRecruitmentData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_CampusRecruitment_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}
#pragma mark -  /*二手车通用存储读取*/
- (void)storeSecondCarData:(NSMutableArray *)storeData
                            cid:(int) cid
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    NSString* storeKey = [NSString stringWithFormat:@"%@%d", @"ZF_SecondCar_KEY", cid];
    BOOL isSucess = [self.db storeData:data forKey:storeKey error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_SecondCar_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getSecondCarData:(int) cid {
    NSString* storeKey = [NSString stringWithFormat:@"%@%d", @"ZF_SecondCar_KEY", cid];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:storeKey error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*加入我们*/
- (void)storeJoinUSData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_JoinUS_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_JoinUS_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getJoinUSData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_JoinUS_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*售后服务*/
- (void)storeAfterSeriveceData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_AfterSerivece_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_AfterSerivece_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getAfterSeriveceData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_AfterSerivece_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark -  /*联系我们*/
- (void)storeContactUSData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:@"ZF_ContactUS_KEY" error:nil];
    if (isSucess) {
        sucess(storeData);
    }else{
        NSError *err = [NSError errorWithDomain:ZF_StoreData_Error_DOMAIN code:ZF_StoreData_Error_Code userInfo:[NSDictionary dictionaryWithObject:@"ZF_ContactUS_Data Fail" forKey:NSLocalizedDescriptionKey]];
        failure(err);
    };
}
- (NSMutableArray *)getContactUSData{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:@"ZF_ContactUS_KEY" error:nil];
    NSMutableArray *backData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return backData;
}

#pragma mark - Banz Car
- (void)storeCarData:(NSMutableArray *)storeData
              Sucess:(void(^)(NSMutableArray *storeData))sucess
                fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_ALL_CAR_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@:%@", @"存储车系成功 ", key);
    }else{
        NSLog(@"%@", @"存储车系失败");
    };
}
- (void)storeCarModelData:(NSMutableArray *)storeData
             cid:(int)cid
              Sucess:(void(^)(NSMutableArray *storeData))sucess
                fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@%d", ZF_ALL_CAR_MODEL_KEY, cid];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@:%@", @"存储车型成功 ", key);
    }else{
        NSLog(@"%@", @"存储车型失败");
    };
}
- (NSMutableArray *)getCarModelByType: (int)cid {
    NSString *key = [NSString stringWithFormat:@"%@%d", ZF_ALL_CAR_MODEL_KEY, cid];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *banzCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return banzCarData;
}
- (NSMutableArray *)getCallCarModelData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_ALL_car_model_list];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *banzCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return banzCarData;
}
- (NSMutableArray *)getCallCarTypeData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_ALL_car_type_list];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *banzCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return banzCarData;
}


- (NSMutableArray *)getBanzCarData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_BANZ_CARD_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *banzCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return banzCarData;
}
#pragma mark - 配件
- (void)storePeijianData:(NSMutableArray *)storeData
                     Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure key:(NSString *)key{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@%@", @"存储配件成功", key);
    }else{
        NSLog(@"%@%@", @"存储配件失败", key);
    };
}
- (NSMutableArray *)getPeijianData:(NSString *)key{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *banzCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return banzCarData;
}
#pragma mark - 精品
- (void)storeJingPingData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure key:(NSString *)key{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@%@", @"存储精品成功", key);
    }else{
        NSLog(@"%@%@", @"存储精品失败", key);
    };
}
- (NSMutableArray *)getJingPingData:(NSString *)key{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *banzCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return banzCarData;
}
#pragma mark - 车主专享
- (void)storeCarMasterShareData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure key:(NSString *)key{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@%@", @"存储车主专享成功", key);
    }else{
        NSLog(@"%@%@", @"存储车主专享失败", key);
    };
}
- (NSMutableArray *)getCarMasterShareData:(NSString *)key{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *banzCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return banzCarData;
}
#pragma mark - 网上展厅 Banz Car
- (void)storeNetShowCarData:(NSMutableArray *)storeData
                  Sucess:(void(^)(NSMutableArray *storeData))sucess
                       fail:(void(^)(NSError *error))failure key:(NSString *)key{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@%@", @"存储网上展厅车成功", key);
    }else{
        NSLog(@"%@%@", @"存储网上展厅车失败", key);
    };
}
- (NSMutableArray *)getNetShowCarData:(NSString *)key{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *banzCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return banzCarData;
}

#pragma mark - AMG Car
- (void)storeAMGCarData:(NSMutableArray *)storeData
                 Sucess:(void(^)(NSMutableArray *storeData))sucess
                   fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_AMG_CARD_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@", @"存储AMG系列车成功");
    }else{
        NSLog(@"%@", @"存储AMG系列车失败");
    };
}

- (NSMutableArray *)getAMGCarData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_AMG_CARD_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *AMGCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return AMGCarData;
}
#pragma mark - Smart Car
- (void)storeSmartCarData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_SMART_CARD_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@", @"存储smart车成功");
    }else{
        NSLog(@"%@", @"存储smart车失败");
    };
}
- (NSMutableArray *)getSmartCarData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_SMART_CARD_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *smartCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return smartCarData;
}
#pragma mark - 市场活动
- (void)storeNewsListData:(NSMutableArray *)storeData
                   Sucess:(void(^)(NSMutableArray *storeData))sucess
                     fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_NEWS_LIST_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@", @"存储市场活动成功");
    }else{
        NSLog(@"%@", @"存储市场活动失败");
    };
}
- (NSMutableArray *)getNewsListData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_NEWS_LIST_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *newsListData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return newsListData;
}

#pragma mark - 车型优惠
- (void)storeModelPrivilegeListData:(NSMutableArray *)storeData
                             Sucess:(void(^)(NSMutableArray *storeData))sucess
                               fail:(void(^)(NSError *error))failure{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_MODEL_PRIVILEGE_KEY];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@", @"存储车型优惠成功");
    }else{
        NSLog(@"%@", @"存储车型优惠失败");
    };
}
- (NSMutableArray *)getModelPrivilegeListData{
    NSString *key = [NSString stringWithFormat:@"%@", ZF_MODEL_PRIVILEGE_KEY];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *modelPrivilegeListData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return modelPrivilegeListData;
}

#pragma mark - 相关活动
//相关活动
- (void)storeRelatedEventsListData:(NSMutableArray *)storeData
                            Sucess:(void(^)(NSMutableArray *storeData))sucess
                              fail:(void(^)(NSError *error))failure key:(NSString *)key1{
    NSString *key = [NSString stringWithFormat:@"%@%@", ZF_RELATED_EVENTS_KEY, key1];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@", @"存储相关活动成功");
    }else{
        NSLog(@"%@", @"存储相关活动失败");
    };
}
- (NSMutableArray *)getRelatedEventsListData:(NSString *)key1{
    NSString *key = [NSString stringWithFormat:@"%@%@", ZF_RELATED_EVENTS_KEY, key1];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *modelPrivilegeListData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return modelPrivilegeListData;
}

#pragma mark - 计算器方案
//计算器方案
- (void)storeSchemeData:(NSMutableArray *)storeData
                    key:(NSString *)storeKey
                 scheme:(NSString *)scheme
                 Sucess:(void(^)(NSMutableArray *storeData))sucess
                   fail:(void(^)(NSError *error))failure
{
    NSString *key = [NSString stringWithFormat:@"ZF_Scheme_KEY_%@_%@", storeKey, scheme];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@", @"存储计算器方案成功");
    }else{
        NSLog(@"%@", @"存储计算器方案失败");
    };
}

- (NSMutableArray *)getSchemeData:(NSString *)storeKey
                           scheme:(NSString *)scheme
{
    NSString *key = [NSString stringWithFormat:@"ZF_Scheme_KEY_%@_%@", storeKey,scheme];
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *modelPrivilegeListData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return modelPrivilegeListData;
}

#pragma mark - 列表选择车系/车型数据
- (void)storeCarListData:(NSMutableArray *)storeData
                  Sucess:(void(^)(NSMutableArray *storeData))sucess
                    fail:(void(^)(NSError *error))failure
                     key:(NSString *)key{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:storeData];
    BOOL isSucess = [self.db storeData:data forKey:key error:nil];
    if (isSucess) {
        NSLog(@"%@%@", @"存储列表选择车系/车型数据成功", key);
    }else{
        NSLog(@"%@%@", @"存储列表选择车系/车型数据失败", key);
    };
}
- (NSMutableArray *)getCarListData:(NSString *)key{
    NSData *data = [[DBManager sharedManager].db storedDataForKey:key error:nil];
    NSMutableArray *banzCarData = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return banzCarData;
}
/********************************************************************************
 xiaoming end
 *********************************************************************************/
#pragma mark -
#pragma mark - xiaoming end
@end
