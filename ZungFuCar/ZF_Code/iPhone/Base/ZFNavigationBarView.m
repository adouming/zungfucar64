//
//  ZFNavigationBarView.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ZFNavigationBarView.h"

@implementation ZFNavigationBarView

-(void)dealloc{
    self.delegate = nil;
}
- (id)initWithFrame:(CGRect)frame
{
//    if(IOS7)
//        self = [super initWithFrame:CGRectMake(frame.origin.x, 20, frame.size.width, frame.size.height)];
//    else
        self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ZFNavigationBarView" owner:self options:nil];
        UIView *nibView = [nibArray objectAtIndex:0];
        [self addSubview:nibView];
        self.userInteractionEnabled = YES;
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    _itemType = NavigationItemTypeHome;
    _rightBtn.hidden = YES;
}
- (IBAction)itemPressAction:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    if ([self.delegate respondsToSelector:@selector(navigationBarDidPressAtIndex:)]) {
        [self.delegate navigationBarDidPressAtIndex:btn.tag];
    }
}

- (IBAction)rightAction:(id)sender {
    
    switch (_itemType) {
        case NavigationItemTypeSwitch:{
            self.rightBtn.selected = !self.rightBtn.selected;
            
            if (self.rightBtn.selected) {
                [_rightBtn setTitle:@"列表" forState:UIControlStateNormal];
                [_rightBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
                [_rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 35)];
                
                
                
            }else{
                [_rightBtn setTitle:@"地图" forState:UIControlStateNormal];
                [_rightBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -65, 0, 0)];
                [_rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 40, 0, 0)];
            }
        }
            break;

        case NavigationItemTypeHomeAndSwitch:{
            
//            [_leftBtn setImage:[UIImage imageNamed:@"homebtn.png"] forState:UIControlStateNormal];
//            self.rightBtn.hidden = NO;
            
            self.rightBtn.selected = !self.rightBtn.selected;
            
            if (self.rightBtn.selected) {
                [_rightBtn setTitle:@"列表" forState:UIControlStateNormal];
                [_rightBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
                [_rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 35)];
                
                
                
            }else{
                [_rightBtn setTitle:@"翻页" forState:UIControlStateNormal];
                [_rightBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -65, 0, 0)];
                [_rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 40, 0, 0)];
            }
            
  
        }
            break;
            
        default:
            self.rightBtn.hidden = YES;
            break;
    }

    
}


-(void)setItemType:(NavigationItemType)itemType{
    _itemType = itemType;
    
    switch (itemType) {
        case NavigationItemTypeHome:{
            [_leftBtn setImage:[UIImage imageNamed:@"homebtn.png"] forState:UIControlStateNormal];
        }
            break;
        case NavigationItemTypeBack:{
            [_leftBtn setImage:[UIImage imageNamed:@"backbtn.png"] forState:UIControlStateNormal];
        }
            break;
        case NavigationItemTypeSwitch:{
            [_leftBtn setImage:[UIImage imageNamed:@"backbtn.png"] forState:UIControlStateNormal];
            self.rightBtn.hidden = NO;
            [_rightBtn setTitle:@"地图" forState:UIControlStateNormal];
            [_rightBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -65, 0, 0)];
            [_rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 40, 0, 0)];
        }
            break;
        case NavigationItemTypeHomeAndSwitch:{
            
            
            [_leftBtn setImage:[UIImage imageNamed:@"homebtn.png"] forState:UIControlStateNormal];
            self.rightBtn.hidden = NO;
            
            
            [_rightBtn setTitle:@"翻页" forState:UIControlStateNormal];
            [_rightBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -65, 0, 0)];
            [_rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 40, 0, 0)];
            
            
        }
            break;
            
        default:
            self.rightBtn.hidden = YES;
            break;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


@end
