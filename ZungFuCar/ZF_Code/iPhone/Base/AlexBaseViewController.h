//
//  AlexBaseViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFBaseViewController.h"
#import "ZFStewardManager.h"
#import "UIImageView+AFNetworking.h"
#import  "UILabel+autoResize.h"
#import "DBManager.h"
#import "AlexMacro.h"
#import "DataDBModel.h"

@interface AlexBaseViewController : ZFBaseViewController{
    
}

@end
