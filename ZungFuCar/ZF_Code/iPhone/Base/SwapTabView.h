//
//  SwapTabView.h
//  ZungFuCar
//  滑动切换按钮
//  Created by kc on 13-10-6.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SwapTabViewDelegate;
@interface SwapTabView : UIView<UIScrollViewDelegate>{
    
    
    UIScrollView *_sv;
    
    NSMutableArray *_views;
    
    NSArray *_labels;
    NSArray *_nums;
    
    //int _curPage;
}

@property(assign) int curPage;
@property(nonatomic,assign)id<SwapTabViewDelegate> delegate;

-(void)loadLabels:(NSArray*)aLabels;
-(void)loadNums:(NSArray*)aNums;
-(void)reload;
-(void)selectIndex:(NSInteger)aIndex;

@end


@protocol SwapTabViewDelegate <NSObject>

-(void)switchTab:(SwapTabView*)aView;

@end



