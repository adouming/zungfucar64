//
//  ZFBaseViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DOMacro.h"
#import "ZFNavigationBarView.h"
#import "AbstractViewController.h"
@interface ZFBaseViewController : AbstractViewController<ZFNavigationBarDelegate>

@property (nonatomic,strong) ZFNavigationBarView *customNavigaitionBar;

@end
