//
//  AlexBaseViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"

@interface AlexBaseViewController ()

@end

@implementation AlexBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
   }
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
  }
- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
     
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)navigationBarDidPressAtIndex:(NSInteger)index{
    switch (index) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}
@end
