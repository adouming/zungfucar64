//
//  SwapTabView.m
//  ZungFuCar
//
//  Created by kc on 13-10-6.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "SwapTabView.h"

#define SWapViewY 10
#define SWapOffset 40

@implementation SwapTabView



-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self=[super initWithCoder:aDecoder]) {
        
        [self initView];
    }
    return self;
}


-(void)initView{
    
    _sv = [[UIScrollView alloc] initWithFrame:self.bounds];
    _sv.delegate = self;
    _sv.showsHorizontalScrollIndicator = NO;
    _sv.showsVerticalScrollIndicator = NO;
    //_sv.pagingEnabled = YES;
    
    _sv.backgroundColor = [UIColor clearColor];
    [self addSubview:_sv];
    
    self.backgroundColor = [UIColor clearColor];
    
    _curPage = 0;
}

-(void)loadLabels:(NSArray*)aLabels{
    _labels = aLabels;
    
}
-(void)loadNums:(NSArray*)aNums{
    _nums = aNums;
}


-(UIView*)oneView:(int)tag{
    
    NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SwapTabView" owner:self options:nil];
    UIView *nibView = [nibArray objectAtIndex:0];
    
    UILabel *titleLabel = (UILabel*)[nibView viewWithTag:1];
    [titleLabel setText:[_labels objectAtIndex:tag]];
    
    return nibView;
}

//refresh
-(void)reload{
    
    if (!_views) {
        //init views
        _views = [[NSMutableArray alloc] init];
        for (int i=0; i<_labels.count; i++) {
            [_views addObject:[self oneView:i]];
        }
        
        
        //add to scrollView
        for (int i=0; i<_views.count; i++) {
            
            UIView *aView = [_views objectAtIndex:i];
            
            // 0
            if (i==0) {
                [aView setFrame:CGRectMake(0, SWapViewY, aView.frame.size.width, aView.frame.size.height)];
            }else{
                
                [aView setFrame:CGRectMake(320*i-SWapOffset, SWapViewY, aView.frame.size.width, aView.frame.size.height)];
            }
         
            [_sv addSubview:aView];
        }
        
    }

    //[self resetFrame];
    
    
    [_sv setContentSize:CGSizeMake(320*_views.count, _sv.frame.size.height)];
    
}


//选择标题
-(void)selectIndex:(NSInteger)aIndex{
    
    [_sv setContentOffset:CGPointMake(320*aIndex, 0)];
    [self scrollFix:_sv];
}


-(void)resetFrame{
    for (int i=0; i<_views.count; i++) {
        
        UIView *aView = [_views objectAtIndex:i];
        
        // 0
        if (i==0) {
            [aView setFrame:CGRectMake(0, SWapViewY, aView.frame.size.width, aView.frame.size.height)];
        }else{
            
            [aView setFrame:CGRectMake(320*i-SWapOffset, SWapViewY, aView.frame.size.width, aView.frame.size.height)];
        }
    
    }

}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
}



-(void)scrollFix:(UIScrollView*)scrollView{
    CGFloat x = scrollView.contentOffset.x;
    
    CGFloat toX = 0;
    if ((((int)x)%(320-SWapOffset))>((320-SWapOffset)/2)) {
        
        _curPage = (int)(((int)x)/(320-SWapOffset)+1);
        
//        toX = (((int)x)/(320-SWapOffset)+1)*(320-SWapOffset);
//        NSLog(@"next==%f",toX);
        
    }else{
        
        _curPage = (int)(((int)x)/(320-SWapOffset));
        
//        toX = (((int)x)/(320-SWapOffset))*(320-SWapOffset);
//        NSLog(@"cur=%f",toX);
    }
    
    toX = _curPage*(320-SWapOffset);
    
    [scrollView scrollRectToVisible:CGRectMake(toX, 0, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [self scrollFix:scrollView];

}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [self scrollFix:scrollView];
}


- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    //NSLog(@"end-----%d--",_curPage);
    
    
    if ([_delegate respondsToSelector:@selector(switchTab:)]) {
        [_delegate performSelector:@selector(switchTab:) withObject:self];
    }
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    
    NSLog(@"touch begin");
}


@end



