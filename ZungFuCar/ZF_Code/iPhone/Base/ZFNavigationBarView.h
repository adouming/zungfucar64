//
//  ZFNavigationBarView.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    NavigationItemTypeHome,
    NavigationItemTypeBack,
    NavigationItemTypeSwitch,
    NavigationItemTypeHomeAndSwitch
}NavigationItemType;

@protocol ZFNavigationBarDelegate <NSObject>

-(void)navigationBarDidPressAtIndex:(NSInteger)index;
-(void)navigationBarWillAppear;
-(void)navigationBarDidAppear;

@end
@interface ZFNavigationBarView : UIView
@property (strong, nonatomic) IBOutlet UIButton *leftBtn;
@property (strong, nonatomic) IBOutlet UIButton *rightBtn;
@property (nonatomic,unsafe_unretained) id<ZFNavigationBarDelegate> delegate;
@property (nonatomic,strong) UINavigationController *navigationController;
@property (nonatomic,assign) NavigationItemType itemType;
- (IBAction)itemPressAction:(id)sender;
- (IBAction)rightAction:(id)sender;

@end
