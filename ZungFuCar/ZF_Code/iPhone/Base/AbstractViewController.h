//
//  AbstractViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AbstractViewController : UIViewController

@end
