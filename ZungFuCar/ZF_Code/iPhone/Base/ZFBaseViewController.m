//
//  ZFBaseViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ZFBaseViewController.h"

@interface ZFBaseViewController ()

@end

@implementation ZFBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
    if ([self.customNavigaitionBar.delegate respondsToSelector:@selector(navigationBarWillAppear)]) {
        [self.customNavigaitionBar.delegate navigationBarWillAppear];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([self.customNavigaitionBar.delegate respondsToSelector:@selector(navigationBarDidAppear)]) {
        [self.customNavigaitionBar.delegate navigationBarDidAppear];
    }
}
- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    if(IOS7)
    {
        //auto resize
        CGRect rect = CGRectMake(0, 0,  [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height);
        
        _customNavigaitionBar = [[ZFNavigationBarView alloc] initWithFrame:CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, 44)];
        UIView *statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 20)];
        statusView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:statusView];

    }
    else
        _customNavigaitionBar = [[ZFNavigationBarView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
    _customNavigaitionBar.delegate = self;
    _customNavigaitionBar.navigationController = self.navigationController;
    _customNavigaitionBar.rightBtn.hidden = YES;
    _customNavigaitionBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_customNavigaitionBar];
    [self.view bringSubviewToFront:_customNavigaitionBar];
    
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)navigationBarDidPressAtIndex:(NSInteger)index{
    switch (index) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}
-(void)navigationBarWillAppear{
    
}
-(void)navigationBarDidAppear{
    
}
@end
