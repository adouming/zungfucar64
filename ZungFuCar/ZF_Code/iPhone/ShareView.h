//
//  ShareView.h
//  ZungFuCar
//
//  Created by 刘 勇斌 on 13-12-7.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WBEngine.h"
#import "WXApi.h"

@interface ShareView : UIView<WBEngineDelegate, UIActionSheetDelegate>{
    UIImage *image;
    NSString *imageUrl;
    NSString *text;
    NSString *newsTitle;
    NSString *bannerName;
    WBEngine *_shareEngine;
    int selectRow;
}
-(id)initWithImage:(UIImage *)image1 imageUrl:(NSString *)imageUrl1 text:(NSString *)text1 title:(NSString *)title bannerName:(NSString *)bannerName1;
-(void)showInView:(UIView *)view;
@end
