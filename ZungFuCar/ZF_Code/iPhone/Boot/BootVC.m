//
//  BootVC.m
//  DemoProject
//
//  Created by apple on 13-3-22.
//  Copyright (c) 2013年 do1.com.cn. All rights reserved.
//

#import "BootVC.h"
#import "BootManager.h"


@interface BootVC ()

@end

@implementation BootVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
        // Custom initialization

    }
    return self;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleBlackOpaque;  
    [UIApplication sharedApplication].statusBarHidden = YES;
}

- (void)viewDidLoad
{
    
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    UIImage *image = nil;
    
    
//    UIImage *localImage =  [[BootManager sharedManager] getDBBootImage];
//    if (localImage) {
//        NSLog(@"localIMge");
//        image = localImage;
//    }else
    
    {
        if(iPad) {
            image = [UIImage imageNamed:@"Default-Landscape.png"];
        }
        else if(iPhone5)
        {
            image = [UIImage imageNamed:@"Default-568h.png"];
        }
        else
        {
            image = [UIImage imageNamed:@"Default.png"];
        }
        
    }
    
    imageView.image = [image stretchableImageWithLeftCapWidth:0  topCapHeight:0];
    [self.view addSubview:imageView];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:image]];

    
    
    //[self createModel];
   // [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(pushNextVC) userInfo:nil repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(pushNextVC) userInfo:nil repeats:NO];
}


-(void)createModel{
    
    [[BootManager sharedManager] getDataBootImageSuccess:^(UIImage *data) {
        NSLog(@"success");;
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(pushNextVC) userInfo:nil repeats:NO];
    } failure:^(NSError *error) {
        NSLog(@"error");

        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(pushNextVC) userInfo:nil repeats:NO];
    }];
    
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *	进入主页面
 */
-(void)pushNextVC
{
    //BootVC2MainVCId
    
    [self performSegueWithIdentifier:@"BootVC2MainVCId" sender:self];
    return;
    /*
    // 获取故事板
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    // 获取故事板中某个View
    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"MainVCID"];
    // 跳转
    [self presentViewController:next animated:NO completion:nil];
     */
}




@end
