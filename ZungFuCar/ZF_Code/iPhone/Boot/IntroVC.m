//
//  IntroVC.m
//  ZungFuCar
//
//  Created by Ed Lee on 12/19/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "IntroVC.h"

@interface IntroVC ()

@end

@implementation IntroVC

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    [self setupPage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupPage
{
    CGFloat viewH = iPhone5?568:480;
    _introScrollView.delegate = self;
    UIImage *page1Img = iPhone5?[UIImage imageNamed:@"guide_1@2x-568h.png"]:[UIImage imageNamed:@"guide_1.png"];
    UIImage *page2Img = iPhone5?[UIImage imageNamed:@"guide_2@2x-568h.png"]:[UIImage imageNamed:@"guide_2.png"];
    UIImage *page3Img = iPhone5?[UIImage imageNamed:@"guide_3@2x-568h.png"]:[UIImage imageNamed:@"guide_3.png"];
    UIImage *page4Img = iPhone5?[UIImage imageNamed:@"guide_4@2x-568h.png"]:[UIImage imageNamed:@"guide_4.png"];
    UIImage *page5Img = iPhone5?[UIImage imageNamed:@"guide_5@2x-568h.png"]:[UIImage imageNamed:@"guide_5.png"];
    NSArray *introImgs = [NSArray arrayWithObjects:page1Img,page2Img,page3Img,page4Img,page5Img, nil];
    CGFloat cx = 0;
    for (int i=0; i<introImgs.count; i++) {
        UIImageView *galImgV = [[UIImageView alloc] initWithImage:[introImgs objectAtIndex:i]];
        [_introScrollView addSubview:galImgV];
        CGRect rect = CGRectMake(0 + cx, 0, self.view.bounds.size.width, viewH);
        galImgV.frame = rect;
        cx += _introScrollView.frame.size.width;
    }
    _introPageControl.numberOfPages = introImgs.count;
    [_introScrollView setContentSize:CGSizeMake(cx, viewH)];
}

- (IBAction)changePage:(id)sender {
    CGRect frame = _introPageControl.frame;
    frame.origin.x = frame.size.width * _introPageControl.currentPage;
    frame.origin.y = 0;
    [_introScrollView scrollRectToVisible:frame animated:YES];
    pageControlIsChangingPage = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView
{
    if (_scrollView == _introScrollView) {
        if (pageControlIsChangingPage) {
            return;
        }
        CGFloat pageWidth = _scrollView.frame.size.width;
        int page = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        _introPageControl.currentPage = page;
        if (page == 4) {
            [_skipBtn setImage:[UIImage imageNamed:@"enter.png"] forState:UIControlStateNormal];
        } else {
            [_skipBtn setImage:[UIImage imageNamed:@"skip.png"] forState:UIControlStateNormal];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView
{
    if (_scrollView == _introScrollView) {
        pageControlIsChangingPage = NO;
    }
}
- (IBAction)skipIntro:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IntroIsWatched"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
