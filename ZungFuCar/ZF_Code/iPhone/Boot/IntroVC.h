//
//  IntroVC.h
//  ZungFuCar
//
//  Created by Ed Lee on 12/19/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroVC : UIViewController <UIScrollViewDelegate> {
    BOOL pageControlIsChangingPage;
}

@property (strong, nonatomic) IBOutlet UIScrollView *introScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *introPageControl;
- (IBAction)changePage:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *skipBtn;
- (IBAction)skipIntro:(id)sender;
@end
