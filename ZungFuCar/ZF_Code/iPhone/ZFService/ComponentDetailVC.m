//
//  ComponentDetailVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-16.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "ComponentDetailVC.h"
#import "DBManager.h"
#import "AboutZFAPIManager.h"
#import "NSObject+AddJavascript.h"

@interface ComponentDetailVC ()

@end

@implementation ComponentDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _titleLabel.text = [self.dic objectForKey:@"Name"];
    [self.backgroundImageView removeFromSuperview];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    _contentWebView.scrollView.scrollEnabled = NO;
    /**
     *	非二级菜单显示按钮
     */
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    _titleLabel.text = [self.dic objectForKey:@"Name"];
    NSString *contentStr = [self.dic objectForKey:@"Descp"];
    [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
    [self showLoadingText];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 * 给UIWebView 设置高度 自适应内容
 */

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
//    NSURL *a = inRequest.URL;
    if (inType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self injectJavascript:webView];
    //获取html body 的内容长度 计算高度
    NSString *height_str= [webView stringByEvaluatingJavaScriptFromString: @"document.body.offsetHeight"];
    int height = [height_str intValue];
    webView.frame = CGRectMake(webView.frame.origin.x,webView.frame.origin.y,webView.frame.size.width,height+50);
    //设置WebView的superView=_bgScrollView的contentSize
    [_bgScrollView setContentSize:CGSizeMake(_bgScrollView.frame.size.width, 63 + height+ 70)];
    [self hideLoadingText];
}

@end
