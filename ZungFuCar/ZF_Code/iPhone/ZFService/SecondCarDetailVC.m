//
//  SecondCarDetailVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-16.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "SecondCarDetailVC.h"
#import "AboutZFAPIManager.h"
#import "DBManager.h"
#import "NSObject+AddJavascript.h"
#import "NSString+FilterHtml.h"
@interface SecondCarDetailVC ()

@end

@implementation SecondCarDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.backgroundImageView removeFromSuperview];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _contentWebView.scrollView.scrollEnabled = NO;
    
    /**
     *	非二级菜单显示按钮
     */
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;

    
    _titleLabel.text = [self.dic objectForKey:@"Name"];
    [self creatModel:[self.dic objectForKey:@"Name"]];
    //[self requestData:[self.dic objectForKey:@"Name"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)creatModel:(NSString *)title
{
    
    NSString *contentStr = nil;
    NSString *bannerUrl = nil;
    NSArray *array = nil;
    if([title isEqualToString:@"臻至服务"])
    {
        array = [[DBManager sharedManager] getRealSeriviceData];
        if([array count] ==0 )
            return;
        contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    }
    else if([title isEqualToString:@"五星评估"])
    {
        //array = [[DBManager sharedManager] getFiveStarData];
        if([array count] ==0 )
            return;
        contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    }
    else if([title isEqualToString:@"一站式置换"])
    {
        //array = [[DBManager sharedManager] getOneChangeData];
        if([array count] ==0 )
            return;
    contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    }
    else if([title isEqualToString:@"星睿认证"])
    {
        //array = [[DBManager sharedManager] getXingRuiData];
        if([array count] ==0 )
            return;
        contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    }
    else if([title isEqualToString:@"尊品寄售及优质回购"])
    {
        //array = [[DBManager sharedManager] getBuyBackData];
        if([array count] ==0 )
            return;
        contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    }
    else if([title isEqualToString:@"问题答疑"])
    {
        //array = [[DBManager sharedManager] getAnswerQuestionData];
        if([array count] ==0 )
            return;
        contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    }
    
    [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
    bannerUrl = [[array objectAtIndex:0] objectForKey:@"BannerURL"];
//    if([self validatePng:bannerUrl]&&![bannerUrl isEqualToString:@""])
//        [_bannerView setImageWithURL:[NSURL URLWithString:[[array objectAtIndex:0] objectForKey:@"BannerURL"]]];
}

//- (void)requestData:(NSString *)title
//{
//    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
//    if([title isEqualToString:@"臻至服务"])
//    {
//        if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID177_UpdatedTime"]==nil)
//        {
//            [self showLoadingText];
//        }
//        [manager requestRealSeriviceDataSuccess:^(NSArray *data) {
//            BOOL isFirst = NO;
//            if([data count]==0)
//            {
//                return ;
//            }
//            else
//            {
//                /*写lastmodifyTime到沙盒*/
//                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//                if([userDefaults objectForKey:@"ViewID177_UpdatedTime"]==nil)
//                    isFirst = YES;
//                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID177_UpdatedTime"];
//                [userDefaults synchronize];
//                
//                NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"Content"];
//                NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
//                htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
//                [self hideLoadingText];
//                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"BannerURL"],@"BannerURL",
//                                     htmlStr,@"Content",
//                                     [[data objectAtIndex:0] objectForKey:@"ID"],@"ID",
//                                     [[data objectAtIndex:0] objectForKey:@"Name"],@"Name",
//                                     [[data objectAtIndex:0] objectForKey:@"UpdatedTime"],@"UpdatedTime",
//                                     nil];
//                
//                NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
//                [[DBManager sharedManager] storeRealSeriviceData:dataTemp Sucess:^(NSMutableArray *storeData) {
//                    if(isFirst==YES)
//                        [self creatModel:title];
//                } fail:^(NSError *error) {
//                    [self hideLoadingText];
//                }];
//            }
//            
//        }failure:^(NSError *error){
//            [self hideLoadingText];
//        }];
//        
//    }
//    else if([title isEqualToString:@"五星评估"])
//    {
//        if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID178_UpdatedTime"]==nil)
//        {
//            [self showLoadingText];
//        }
//
//        //[manager requestFiveStarDataSuccess:^(NSArray *data) {
//            BOOL isFirst = NO;
//            if([data count]==0)
//            {
//                return ;
//            }
//            else
//            {
//                /*写lastmodifyTime到沙盒*/
//                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//                if([userDefaults objectForKey:@"ViewID178_UpdatedTime"]==nil)
//                    isFirst = YES;
//                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID178_UpdatedTime"];
//                [userDefaults synchronize];
//                
//                NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"Content"];
//                NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
//                htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
//                [self hideLoadingText];
//                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"BannerURL"],@"BannerURL",
//                                     htmlStr,@"Content",
//                                     [[data objectAtIndex:0] objectForKey:@"ID"],@"ID",
//                                     [[data objectAtIndex:0] objectForKey:@"Name"],@"Name",
//                                     [[data objectAtIndex:0] objectForKey:@"UpdatedTime"],@"UpdatedTime",
//                                     nil];
//                
//                NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
//                //[[DBManager sharedManager] storeFiveStarData:dataTemp Sucess:^(NSMutableArray *storeData) {
//                    if(isFirst==YES)
//                        [self creatModel:title];
//                } fail:^(NSError *error) {
//                    [self hideLoadingText];
//                }];
//            }
//            
//        }failure:^(NSError *error){
//            [self hideLoadingText];
//        }];
//    }
//    else if([title isEqualToString:@"一站式置换"])
//    {
//        if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID127_UpdatedTime"]==nil)
//        {
//            [self showLoadingText];
//        }
//
//        [manager requestOneChangeDataSuccess:^(NSArray *data) {
//            BOOL isFirst = NO;
//            if([data count]==0)
//            {
//                return ;
//            }
//            else
//            {
//                /*写lastmodifyTime到沙盒*/
//                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//                if([userDefaults objectForKey:@"ViewID127_UpdatedTime"]==nil)
//                    isFirst = YES;
//                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID127_UpdatedTime"];
//                [userDefaults synchronize];
//                
//                NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"Content"];
//                NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
//                htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
//                [self hideLoadingText];
//                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"BannerURL"],@"BannerURL",
//                                     htmlStr,@"Content",
//                                     [[data objectAtIndex:0] objectForKey:@"ID"],@"ID",
//                                     [[data objectAtIndex:0] objectForKey:@"Name"],@"Name",
//                                     [[data objectAtIndex:0] objectForKey:@"UpdatedTime"],@"UpdatedTime",
//                                     nil];
//                
//                NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
//                [[DBManager sharedManager] storeOneChangeData:dataTemp Sucess:^(NSMutableArray *storeData) {
//                    if(isFirst==YES)
//                        [self creatModel:title];
//                } fail:^(NSError *error) {
//                    [self hideLoadingText];
//                }];
//            }
//            
//        }failure:^(NSError *error){
//            [self hideLoadingText];
//        }];
//    }
//    else if([title isEqualToString:@"星睿认证"])
//    {
//        if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID126_UpdatedTime"]==nil)
//        {
//            [self showLoadingText];
//        }
//
//        [manager requestXingRuiDataSuccess:^(NSArray *data) {
//            BOOL isFirst = NO;
//            if([data count]==0)
//            {
//                return ;
//            }
//            else
//            {
//                /*写lastmodifyTime到沙盒*/
//                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//                if([userDefaults objectForKey:@"ViewID126_UpdatedTime"]==nil)
//                    isFirst = YES;
//                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID126_UpdatedTime"];
//                [userDefaults synchronize];
//                
//                NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"Content"];
//                NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
//                htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
//                [self hideLoadingText];
//                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"BannerURL"],@"BannerURL",
//                                     htmlStr,@"Content",
//                                     [[data objectAtIndex:0] objectForKey:@"ID"],@"ID",
//                                     [[data objectAtIndex:0] objectForKey:@"Name"],@"Name",
//                                     [[data objectAtIndex:0] objectForKey:@"UpdatedTime"],@"UpdatedTime",
//                                     nil];
//                
//                NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
//                [[DBManager sharedManager] storeXingRuiData:dataTemp Sucess:^(NSMutableArray *storeData) {
//                    if(isFirst==YES)
//                        [self creatModel:title];
//                } fail:^(NSError *error) {
//                  [self hideLoadingText];
//                }];
//            }
//            
//        }failure:^(NSError *error){
//            [self hideLoadingText];
//        }];
//    }
//    else if([title isEqualToString:@"尊品寄售及优质回购"])
//    {
//        if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID131_UpdatedTime"]==nil)
//        {
//            [self showLoadingText];
//        }
//
//        [manager requestBuyBackDataSuccess:^(NSArray *data) {
//            BOOL isFirst = NO;
//            if([data count]==0)
//            {
//                return ;
//            }
//            else
//            {
//                /*写lastmodifyTime到沙盒*/
//                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//                if([userDefaults objectForKey:@"ViewID131_UpdatedTime"]==nil)
//                    isFirst = YES;
//                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID131_UpdatedTime"];
//                [userDefaults synchronize];
//                
//                NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"Content"];
//                NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
//                htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
//                [self hideLoadingText];
//                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"BannerURL"],@"BannerURL",
//                                     htmlStr,@"Content",
//                                     [[data objectAtIndex:0] objectForKey:@"ID"],@"ID",
//                                     [[data objectAtIndex:0] objectForKey:@"Name"],@"Name",
//                                     [[data objectAtIndex:0] objectForKey:@"UpdatedTime"],@"UpdatedTime",
//                                     nil];
//                
//                NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
//                
//                [[DBManager sharedManager] storeBuyBackData:dataTemp Sucess:^(NSMutableArray *storeData) {
//                    if(isFirst==YES)
//                        [self creatModel:title];
//                } fail:^(NSError *error) {
//                    [self hideLoadingText];
//                }];
//            }
//            
//        }failure:^(NSError *error){
//            [self hideLoadingText];
//        }];
//    }
//    else if([title isEqualToString:@"问题答疑"])
//    {
//        if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID179_UpdatedTime"]==nil)
//        {
//            [self showLoadingText];
//        }
//
//        [manager requestAnswerQuestionDataSuccess:^(NSArray *data) {
//            BOOL isFirst = NO;
//            if([data count]==0)
//            {
//                return ;
//            }
//            else
//            {
//                /*写lastmodifyTime到沙盒*/
//                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//                if([userDefaults objectForKey:@"ViewID179_UpdatedTime"]==nil)
//                {
//                    isFirst = YES;
//                    [self showLoadingText];
//                }
//                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID179_UpdatedTime"];
//                [userDefaults synchronize];
//                
//                NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"Content"];
//                NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
//                htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
//                [self hideLoadingText];
//                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"BannerURL"],@"BannerURL",
//                                     htmlStr,@"Content",
//                                     [[data objectAtIndex:0] objectForKey:@"ID"],@"ID",
//                                     [[data objectAtIndex:0] objectForKey:@"Name"],@"Name",
//                                     [[data objectAtIndex:0] objectForKey:@"UpdatedTime"],@"UpdatedTime",
//                                     nil];
//                
//                NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
//                [[DBManager sharedManager] storeAnswerQuestionData:dataTemp Sucess:^(NSMutableArray *storeData) {
//                    if(isFirst==YES)
//                        [self creatModel:title];
//                } fail:^(NSError *error) {
//                    [self hideLoadingText];
//                }];
//            }
//            
//        }failure:^(NSError *error){
//            [self hideLoadingText];
//        }];
//    }
//}


-(void)webViewDidStartLoad:(UIWebView *)webView {
    willLaunchSafari = NO;
}
/*
 * 给UIWebView 设置高度 自适应内容
 */
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self injectJavascript:webView];
    NSString *padding = @"document.body.style.margin='5px';document.body.style.padding = '5px'";
    [webView stringByEvaluatingJavaScriptFromString:padding];
    /*获取html body 的内容长度 计算高度*/
    NSString *height_str= [webView stringByEvaluatingJavaScriptFromString: @"document.body.offsetHeight"];
    int height = [height_str intValue];
    webView.frame = CGRectMake(webView.frame.origin.x,webView.frame.origin.y,webView.frame.size.width,height+50);
    
    /*设置WebView的superView=_bgScrollView的contentSize*/
    [_bgScrollView setContentSize:CGSizeMake(_bgScrollView.frame.size.width, 63 + height+ 50)];
    
    [self hideLoadingText];
    willLaunchSafari = YES;
}

/*
 * 点击网页图片,打开相册
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([self webViewImgIsClickedByRequest:request] == 0) {
        UINavigationController *nc = [self getAlbumPhotosFromImgClick:request];
        if (nc != nil) {
            nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:nc animated:YES completion:nil];
        }
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 1 && willLaunchSafari) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 2) {
        return YES;
    }
}
@end





