//
//  CustomSLPickerView.m
//  Courtyard1.1
//
//  Created by Wangmm on 13-1-21.
//
//

#import "CustomSLPickerView.h"
#import "SLPickerView.h"
@interface CustomSLPickerView()<SLPickerViewDelegate>

@property (nonatomic, strong) NSMutableDictionary *selectionStatesDic;
@property (nonatomic, strong) NSMutableArray *selectedEntriesArr;//选中的状态
@property (nonatomic, strong) SLPickerView *pickerView;
@property (nonatomic, strong) UIToolbar *toolBar;
@property (nonatomic, strong) UIView *optionView;
@end

@implementation CustomSLPickerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.selectionStatesDic = [[NSMutableDictionary alloc] initWithCapacity:16];
        self.selectedEntriesArr = [[NSMutableArray alloc] initWithCapacity:16];
        
        self.entriesArray = [[NSMutableArray alloc] initWithCapacity:16];
        self.itemDataArray = [[NSMutableArray alloc] initWithCapacity:16];
        self.entriesSelectedArray = [[NSMutableArray alloc] initWithCapacity:16];
        self.optionArray = [[NSArray alloc] init];
    }
    return self;
}


- (void)pickerShow
{
  //  entries = [[NSArray alloc] initWithObjects:@"Row 1", @"Row 2", @"Row 3", @"Row 4", @"Row 5", nil];

	for (NSString *key in self.entriesArray){
        BOOL isSelected = NO;
        for (NSString *keyed in self.entriesSelectedArray) {
            if ([key isEqualToString:keyed]) {
                isSelected = YES;
            }
        }
        [self.selectionStatesDic setObject:[NSNumber numberWithBool:isSelected] forKey:key];
    }
    
	// Init picker and add it to view
    if (!self.pickerView) {
        self.pickerView = [[SLPickerView alloc] initWithFrame:CGRectMake(0,260, 320, 260)];
    }
	self.pickerView.delegate = self;
	[self addSubview:self.pickerView];
    
    //创建工具栏
    NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:3];
	UIBarButtonItem *confirmBtn = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleDone target:self action:@selector(confirmPickView)];
	UIBarButtonItem *flexibleSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(pickerHide)];
    [items addObject:cancelBtn];
    [items addObject:flexibleSpaceItem];
    [items addObject:confirmBtn];
    
    if (self.toolBar==nil) {
        self.toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.pickerView.frame.origin.y - 44, 320, 44)];
    }
    self.toolBar.hidden = NO;
    self.toolBar.barStyle = UIBarStyleBlackTranslucent;
    self.toolBar.items = items;
    items = nil;
    [self addSubview:self.toolBar];
    self.optionView = [[UIView alloc] initWithFrame:CGRectMake(0, self.pickerView.frame.origin.y-44, 320, 44)];
    if(self.optionArray!=nil &&[self.optionArray count]!=0)
    {
        self.optionView.backgroundColor = [UIColor colorWithRed:144/255.f green:151/255.f blue:167/255.f alpha:1];
        
        for(int i = 0;i<3;i++)
        {
            UILabel *label = nil;
            label = [[UILabel alloc] initWithFrame:CGRectMake(0+i*106, 0, 320/3, 44)];
            label.backgroundColor = [UIColor clearColor];
            label.numberOfLines = 0;
            label.font = [UIFont systemFontOfSize:13.f];
            label.adjustsFontSizeToFitWidth = YES;
            label.textColor = [UIColor whiteColor];
            label.text = [self.optionArray objectAtIndex:i];
            label.textAlignment = NSTextAlignmentCenter;
            //label.contentMode = UIControlContentVerticalAlignmentTop;
            [self.optionView addSubview:label];
            
            UIView *image = [[UIView alloc] initWithFrame:CGRectMake((i+1)*106,0,1,44)];
            image.backgroundColor = [UIColor grayColor];
            [self.optionView addSubview:image];
        }
        [self addSubview:self.optionView];
    }
    [UIView animateWithDuration:0.5 animations:^{
        if(self.optionArray!=nil &&[self.optionArray count]!=0)
        {
            self.frame = CGRectMake(self.frame.origin.x, [UIScreen mainScreen].bounds.size.height - 304-20, self.frame.size.width, 304);
            self.pickerView.frame = CGRectMake(0, 88, 320, 304);
            self.optionView.frame = CGRectMake(0, self.pickerView.frame.origin.y-44, 320, 44);
            self.toolBar.frame = CGRectMake(0, self.pickerView.frame.origin.y-84, 320, 44);
        }
        else
        {
            self.pickerView.frame = CGRectMake(0, 44, 320, 304);
            self.toolBar.frame = CGRectMake(0, self.pickerView.frame.origin.y-44, 320, 44);
        }
    }];

}
- (void)pickerHide
{
    [UIView animateWithDuration:0.5 animations:^{
        if(self.optionArray!=nil &&[self.optionArray count]!=0)
        {
            
            self.alpha = 0.0;
            self.optionView.frame = CGRectMake(0, self.pickerView.frame.origin.y-44+304, 320, 44);
            self.pickerView.frame = CGRectMake(0, 260+44, 320, 260);
            self.toolBar.frame = CGRectMake(0, self.pickerView.frame.origin.y-44, 320, 44);
        }
        else
        {
            self.alpha = 0.0;
            self.pickerView.frame = CGRectMake(0, 260+44, 320, 260);
            self.toolBar.frame = CGRectMake(0, self.pickerView.frame.origin.y-44, 320, 44);
        }
    }];
}

-(void)confirmPickView
{
    int num = 0;
    for (NSString *row in [self.selectionStatesDic allKeys]) {
        if ([[self.selectionStatesDic objectForKey:row] boolValue]) {
            [self.selectedEntriesArr addObject:row];
            break;
        }
    }
    for(int i = 0;i<[self.entriesArray count];i++)
    {
        if([[self.entriesArray objectAtIndex:i] isEqualToString:[self.selectedEntriesArr lastObject]]){
            num = i;
            break;
        }
    }
    
    if ([self.multiPickerDelegate respondsToSelector:@selector(returnChoosedPickerString:)]) {
        [self.multiPickerDelegate returnChoosedPickerString:[NSDictionary dictionaryWithObjectsAndKeys:self.selectedEntriesArr,@"selectedEntriesArr",[self.itemDataArray objectAtIndex:num],@"id" ,nil]];
    }
    
    [self pickerHide];
}

#pragma mark -  SLPickerViewDelegate 


// Return the number of elements of your pickerview
-(NSInteger)numberOfRowsForPickerView:(SLPickerView *)pickerView
{
    return [self.entriesArray count];
}
// Return a plain UIString to display on the given row
- (NSString *)pickerView:(SLPickerView *)pickerView textForRow:(NSInteger)row
{
    return [self.entriesArray objectAtIndex:row];
}
// Return a boolean selection state on the given row
- (BOOL)pickerView:(SLPickerView *)pickerView selectionStateForRow:(NSInteger)row
{
    return [[self.selectionStatesDic objectForKey:[self.entriesArray objectAtIndex:row]] boolValue];
}

- (void)pickerView:(SLPickerView *)pickerView didCheckRow:(NSInteger)row {
	// Check whether all rows are checked or only one
	if (row == -1)
		for (id key in [self.selectionStatesDic allKeys])
			[self.selectionStatesDic setObject:[NSNumber numberWithBool:YES] forKey:key];
	else{
        for (NSString *eachRow in [self.selectionStatesDic allKeys]) {

                [self.selectionStatesDic setObject:[NSNumber numberWithBool:NO] forKey:eachRow];
            }
            [self.selectionStatesDic setObject:[NSNumber numberWithBool:YES] forKey:[self.entriesArray objectAtIndex:row]];

        
    }
}

- (void)pickerView:(SLPickerView *)pickerView didUncheckRow:(NSInteger)row {
	// Check whether all rows are unchecked or only one
	if (row == -1)
		for (id key in [self.selectionStatesDic allKeys])
			[self.selectionStatesDic setObject:[NSNumber numberWithBool:NO] forKey:key];
	else
		[self.selectionStatesDic setObject:[NSNumber numberWithBool:NO] forKey:[self.entriesArray objectAtIndex:row]];
}
@end
