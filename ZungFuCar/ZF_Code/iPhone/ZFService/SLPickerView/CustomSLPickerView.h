//
//  CustomSLPickerView.h
//  Courtyard1.1
//
//  Created by iZ on 13-1-21.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@protocol CustomSLPickerViewDelegate;

@interface CustomSLPickerView : UIView

@property (nonatomic, strong) NSArray *entriesArray;
@property (nonatomic, strong) NSArray *itemDataArray;

@property (nonatomic, strong) NSArray *entriesSelectedArray;
@property (nonatomic, strong) NSArray *optionArray;
@property (nonatomic, weak) id<CustomSLPickerViewDelegate> multiPickerDelegate;

- (void)pickerShow;
@end

@protocol CustomSLPickerViewDelegate <NSObject>
@required
-(void)returnChoosedPickerString:(NSDictionary *)selectedDic;
@end