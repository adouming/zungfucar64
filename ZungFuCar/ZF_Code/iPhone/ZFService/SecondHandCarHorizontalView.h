//
//  SecondHandCarHorizontalView.h
//  ZungFuCar
//
//  Created by lxm on 13-10-13.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

//横滚动


@class SecondHandCarVC;
@interface SecondHandCarHorizontalView : UIView<UITableViewDelegate,UITableViewDataSource>



@property (nonatomic,unsafe_unretained) SecondHandCarVC *controller;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,copy) NSArray *storeDataArray;



@end

