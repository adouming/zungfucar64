//
//  CalculatorVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-17.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "CalculatorVC.h"
#import "ZFStewardManager.h"
#import "DBManager.h"
#include <math.h>

@interface CalculatorVC ()

@end

@implementation CalculatorVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //1.1
    loanSchemeIndex = -1;
    monthCount = 1;
    _productDetailID = @"0";
    
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    [self._bgScrollView setContentSize:CGSizeMake(self._bgScrollView.frame.size.width, 800.0f)];
    
    //1.2
    entries = [[NSArray alloc] init];
    carTypes = [[NSArray alloc] init];
    _SchemeMuArr = [NSMutableArray array];
    
    priceInt = 0;
    firstRatio = 0;
    self.price.text = [NSString stringWithFormat:@"￥%d", priceInt];
    self.money1.text = [NSString stringWithFormat:@"￥%d", priceInt];
    self.money2.text = [NSString stringWithFormat:@"￥%d", priceInt];
    
    selectionStates = [[NSMutableDictionary alloc] init];
    
    /************************************************************************/
//    _netShowBenzDetailMuArr = [[DBManager sharedManager] getNetShowCarData:@"net_show_ben_chi"];
//    _netShowAMGDetailMuArr = [[DBManager sharedManager] getNetShowCarData:@"net_show_AMG"];
//    
//    for (int i=0; i<_netShowBenzDetailMuArr.count; i++) {
//        NSDictionary *carStyleDicTemp = [_netShowBenzDetailMuArr objectAtIndex:i];
//        if ([[carStyleDicTemp  objectForKey:@"CarType"] isKindOfClass:[NSArray class]]) {
//            NSArray *carTypeArrTemp = [carStyleDicTemp  objectForKey:@"CarType"];
//            for (int j=0; j<carTypeArrTemp.count; j++) {
//                NSArray *details = [[NSArray alloc]initWithArray:[[carTypeArrTemp objectAtIndex:j] valueForKey:@"Details"]];
//                for (int k=0; k<[details count]; k++) {
//                    NSString *carType = [[details objectAtIndex:k] objectForKey:@"ID"];
//                    [self getScheme:@"loan" withID:carType];
//                    [self getScheme:@"lease" withID:carType];
//                }
//            }
//        }
//    }
//    for (int i=0; i<_netShowAMGDetailMuArr.count; i++) {
//        NSDictionary *carStyleDicTemp = [_netShowAMGDetailMuArr objectAtIndex:i];
//        if ([[carStyleDicTemp  objectForKey:@"CarType"] isKindOfClass:[NSArray class]]) {
//            NSArray *carTypeArrTemp = [carStyleDicTemp  objectForKey:@"CarType"];
//            for (int j=0; j<carTypeArrTemp.count; j++) {
//                NSArray *details = [[NSArray alloc]initWithArray:[[carTypeArrTemp objectAtIndex:j] valueForKey:@"Details"]];
//                for (int k=0; k<[details count]; k++) {
//                    NSString *carType = [[details objectAtIndex:k] objectForKey:@"ID"];
//                    [self getScheme:@"loan" withID:carType];
//                    [self getScheme:@"lease" withID:carType];
//                }
//            }
//        }
//    }
    /************************************************************************/
    [self getCarData:1];
    [self getCarData:2];
    /************************************************************************/

    
}

#pragma mark - request

- (void)getScheme:(NSString *)scheme withID:(NSString *)ID
{
    
    [[ZFStewardManager sharedManager] getSchemeWithCase:scheme andCarID:ID Success:^(NSMutableArray *data) {
        NSMutableArray *schemeTempMuArr = [NSMutableArray array];
        for (int i=0; i<data.count; i++) {
            ZFSchemeData *temp = [ZFSchemeData dataWithJSON:[data objectAtIndex:i]];
            [schemeTempMuArr addObject:temp];
        }
        [[DBManager sharedManager] storeSchemeData:schemeTempMuArr key:ID scheme:scheme Sucess:^(NSMutableArray *storeData) {
            
        } fail:^(NSError *error) {
            
        }];
    } failue:^(NSError *error) {
        NSLog(@"Calculator error :%@", error);
    }];
    
}


-(void)getCarData:(int)type{
    
    AFHTTPRequestOperationManager *manger = [AFHTTPRequestOperationManager manager];
    manger.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manger.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"Http-X-U"];
    [manger.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"Http-X-P"];
    //    @"http://54.250.184.46/zf-web-ios/index.php/api/Product/fn/getproductByCategory/category/"
    [manger GET:[NSString stringWithFormat:@"%@%@%@%d", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_GETPRODUCTBYCATEGORY, type] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"App.net Global Stream: %@", responseObject);
        if ([[responseObject valueForKey:@"Code"] integerValue]==200) {
            NSMutableArray *dataSource = [[responseObject valueForKey:@"Data"] valueForKey:@"List"];
            if (type==1) {
                [[DBManager sharedManager] storeNetShowCarData:dataSource Sucess:nil fail:nil key:@"net_show_ben_chi"];
            }else if(type==2){
                [[DBManager sharedManager] storeNetShowCarData:dataSource Sucess:nil fail:nil key:@"net_show_AMG"];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Calculator error :%@", error);
    }];
    
}

- (void)didReceiveMemoryWarning
{
    cSLPickerView = nil;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    cSLPickerView = nil;
    [super viewDidUnload];
}

- (IBAction)exhibitionAction:(UIButton *)sender {
    
    CGPoint position = CGPointMake(0, ((UIButton *)sender).frame.origin.y-100);
    [self._bgScrollView setContentOffset:position animated:YES];
    
    _SchemeMuArr = [[DBManager sharedManager] getSchemeData:_productDetailID scheme:(self.case1.isSelected?@"loan":@"lease")];

    //点击后删除之前的PickerView
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[CustomSLPickerView class]]) {
            [view removeFromSuperview];
        }
    }
    customSLPickerViewType = 3;
//    entries = [[NSArray alloc] initWithObjects:@"12月       10%         2.99%", @"36月       20%         3.99%", @"48月       30%         4.99%",nil];
    
    pickerView.hidden = NO;
//    if (pickerView) {
//        pickerView.hidden = !pickerView.hidden;
//    }
    if (pickerView == nil) {
        pickerView = [[customPickerView alloc] initWithTitle:@"" delegate:self];
        pickerView.pickerDelegate = self;
    }
    pickerView.schemeList = _SchemeMuArr;
    pickerView.schemeType = self.case1.isSelected?@"loan":@"lease";
    pickerView.pickerStyle = pickerViewTypeLoan;
//    pickerView.optionArray = [NSArray arrayWithObjects:@"最长贷款期限",@"最低保证金比例",@"客户利率", nil];
    pickerView.optionArray = nil;
    if (self.case1.isSelected) {
        pickerView.optionArray = [NSArray arrayWithObjects:@"最长贷款期限", @"最低首付比例", @"客户利率", nil];
    } else {
        pickerView.optionArray = [NSArray arrayWithObjects:@"最长贷款期限", @"最低保证金比例", @"客户利率", nil];
    }

    [pickerView showInView:self.view];
    
}

#pragma mark - Delegate
//获取到选中的数据
-(void)returnChoosedPickerString:(NSDictionary *)selectedDic
//-(void)returnChoosedPickerString:(NSMutableArray *)selectedEntriesArr withIDStr:(NSString *)idStr
{
    NSMutableArray *selectedEntriesArr = [selectedDic objectForKey:@"selectedEntriesArr"];
    if(selectedEntriesArr==nil||[selectedEntriesArr count]==0)
        return;
    // 再次初始化选中的数据
    entriesSelected = [NSArray arrayWithArray:selectedEntriesArr];
    if(customSLPickerViewType == 1)
    {
        [self.carStyle setTitle:[NSString stringWithFormat:@"  %@",[selectedEntriesArr objectAtIndex:0]] forState:UIControlStateNormal];
        [self.carType setTitle:@"  " forState:UIControlStateNormal];
        self.price.text = [NSString stringWithFormat:@"￥%d", 0];
        self.money1.text = [NSString stringWithFormat:@"￥%d", 0];
        self.money2.text = [NSString stringWithFormat:@"￥%d", 0];
        carTypes = [[selectedDic objectForKey:@"id"] valueForKey:@"CarType"];
    }
    else if(customSLPickerViewType == 2)
    {
        [self.carType setTitle:[NSString stringWithFormat:@"  %@",[selectedEntriesArr objectAtIndex:0]] forState:UIControlStateNormal];
        priceInt = [[[selectedDic objectForKey:@"id"] valueForKey:@"Price"] integerValue];
        self.price.text = [NSString stringWithFormat:@"￥ %d", priceInt];
        if (self.case2.selected) {
            self.money1.text = [NSString stringWithFormat:@"￥%d", (int)(priceInt*(loanSchemeIndex+1)*0.1)];
        }else{
            self.money1.text = [NSString stringWithFormat:@"￥%d", 0];
        }
        self.money2.text = [NSString stringWithFormat:@"￥ %d", (int)(priceInt*firstRatio)];
    }else if(customSLPickerViewType == 3){
        [self.loanScheme setTitle:[NSString stringWithFormat:@"%@",[selectedEntriesArr objectAtIndex:0]] forState:UIControlStateNormal];
        loanSchemeIndex = [[selectedDic objectForKey:@"id"] integerValue];
        if (self.case2.selected) {
            self.money1.text = [NSString stringWithFormat:@"￥%d", (int)(priceInt*(loanSchemeIndex+1)*0.1)];
        }else{
            self.money1.text = [NSString stringWithFormat:@"￥%d", 0];
        }
        monthCount =12;
        switch (loanSchemeIndex) {
            case 0:
                self.monthCountSlider.maximumValue = (loanSchemeIndex+1)*12;
                break;
            case 1:
                self.monthCountSlider.maximumValue = (loanSchemeIndex+2)*12;
                break;
            case 2:
                self.monthCountSlider.maximumValue = (loanSchemeIndex+2)*12;
                break;
            default:
                break;
        }
        [self.monthCountSlider setValue:12.0];
        self.monthCountLable.text = [NSString stringWithFormat:@"%d", 12];
    }
}

- (IBAction)caseBtn:(id)sender
{
    if((UIButton *)sender == self.case1)
    {
        [self.case1 setSelected:YES];
        [self.case2 setSelected:NO];
        self.depositLabel.hidden = YES;
        self.money1.hidden = YES;
    }
    else
    {
        [self.case1 setSelected:NO];
        [self.case2 setSelected:YES];
        self.depositLabel.hidden = NO;
        self.money1.hidden = NO;
    }
}
- (IBAction)carStyle:(id)sender
{
    CGPoint position = CGPointMake(0, ((UIButton *)sender).frame.origin.y-100);
    [self._bgScrollView setContentOffset:position animated:YES];
    
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[CustomSLPickerView class]]) {
            [view removeFromSuperview];
        }
    }
    
    NSMutableArray *data = [NSMutableArray array];
    NSMutableArray *data1 = [NSMutableArray array];
    
    NSArray *benData = [[DBManager sharedManager]getCarListData:@"Banz_car_list"];
    NSArray *amgData = [[DBManager sharedManager]getCarListData:@"AMG_car_list"];
    
    for(int i=0;i<[benData count];i++)
    {
        [data addObject:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@%@", @"奔驰乘用车", [[benData objectAtIndex:i] valueForKey:@"Name"]]]];
        [data1 addObject:[benData objectAtIndex:i]];
    }
    for(int i=0;i<[amgData count];i++)
    {
        [data addObject:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@%@", @"AMG系列", [[amgData objectAtIndex:i] valueForKey:@"Name"]]]];
        [data1 addObject:[amgData objectAtIndex:i]];
    }
    
    pickerView.hidden = NO;
//    if (pickerView) {
//        pickerView.hidden = !pickerView.hidden;
//    }
    if (pickerView == nil) {
        pickerView = [[customPickerView alloc] initWithTitle:@"" delegate:self];
        pickerView.pickerDelegate = self;
    }
    pickerView.pickerStyle = pickerViewTypeCarSeries;
    pickerView.optionArray = nil;
    
    [pickerView showInView:self.view];
    
}
- (IBAction)carType:(id)sender
{
    
    CGPoint position = CGPointMake(0, ((UIButton *)sender).frame.origin.y-100);
    [self._bgScrollView setContentOffset:position animated:YES];
    
    pickerView.hidden = NO;
//    if (pickerView) {
//        pickerView.hidden = !pickerView.hidden;
//    }
    if (pickerView == nil) {
        pickerView = [[customPickerView alloc] initWithTitle:@"" delegate:self];
        pickerView.pickerDelegate = self;
    }
    pickerView.optionArray = nil;
    pickerView.carTypeData = _carTypeData;
    pickerView.pickerStyle = pickerViewTypeCarType;
    
    [pickerView showInView:self.view];
    
}

- (IBAction)sliderValueChanged:(id)sender{
    UISlider* control = (UISlider*)sender;
    if(control == self.mySlider){
        firstRatio = ((int)control.value)*10;
        self.perscet.text = [NSString stringWithFormat:@"%d%%",(int)(firstRatio)];
        self.money2.text = [NSString stringWithFormat:@"￥%d", (int)(priceInt*firstRatio/100)];
    }else if (control == self.monthCountSlider){
        [control setValue:(int)(control.value) animated:YES];
        monthCount = control.value;
        self.monthCountLable.text = [NSString stringWithFormat:@"%d", monthCount * 12];
    }
    if (monthCount == 0) {
        [self.mySlider setValue:0.0f animated:YES];
        self.perscet.text = [NSString stringWithFormat:@"%d%%",(int)(0*100)];
        self.money2.text = [NSString stringWithFormat:@"￥%d", (int)(priceInt*0)];
        self.moneyMonth.text = [NSString stringWithFormat:@"￥%d", 0];
    } else {
        self.moneyMonth.text = [NSString stringWithFormat:@"￥%d", (int)[self getEveryMonthMoney]];
    }
    
}

-(float)getEveryMonthMoney{
    if (loanSchemeIndex!=-1) {
        double powValue1 = pow((0.0299+loanSchemeIndex/100+1), monthCount*12);
        double powValue2 = pow((0.0299+loanSchemeIndex/100+1), monthCount*12-1);
        double money = priceInt*(1-firstRatio/100)*powValue1/powValue2;
        return money/(monthCount*12);
    }
    return 0;
}

#pragma mark - pickerView delegate
-(void)pickerViewDidSelectRow:(NSInteger)row withStyle:(pickerViewType)style andData:(id)data{
    switch (style) {
        case pickerViewTypeCarSeries:{
            int count = [[[DBManager sharedManager]getNetShowCarData:@"net_show_ben_chi"] count];
            NSString *name;
            if (row<=count) {
                name = [NSString stringWithFormat:@"  奔驰乘用车%@",[data valueForKey:@"Name"]];
            }else{
                name = [NSString stringWithFormat:@"  AMG系列%@",[data valueForKey:@"Name"]];
            }
            [self.carStyle setTitle:name forState:UIControlStateNormal];
            [self.carType setTitle:@"  " forState:UIControlStateNormal];
            self.price.text = [NSString stringWithFormat:@"￥%d", 0];
            self.money1.text = [NSString stringWithFormat:@"￥%d", 0];
            self.money2.text = [NSString stringWithFormat:@"￥%d", 0];
            _carTypeData = (NSDictionary *)data;
        }
            break;
        case pickerViewTypeCarType:{
            [self.carType setTitle:[NSString stringWithFormat:@"  %@",[data valueForKey:@"Name"]] forState:UIControlStateNormal];
            _productDetailID = [data objectForKey:@"ID"];
            priceInt = [[data valueForKey:@"Price"] integerValue];
            self.price.text = [NSString stringWithFormat:@"￥ %d", priceInt];
            if (self.case2.selected) {
                self.money1.text = [NSString stringWithFormat:@"￥%d", (int)(priceInt*((ZFSchemeData *)[_SchemeMuArr objectAtIndex:loanSchemeIndex]).LeasingDepositRate)];
            }else{
                self.money1.text = [NSString stringWithFormat:@"￥%d", 0];
            }
            self.money2.text = [NSString stringWithFormat:@"￥ %d", (int)(priceInt*firstRatio)];
        }
            break;
        case pickerViewTypeLoan:{
            [self.loanScheme setTitle:[NSString stringWithFormat:@"%@", data] forState:UIControlStateNormal];
            loanSchemeIndex = row;

            monthCount = 1;
            self.monthCountSlider.minimumValue = 1;
            self.mySlider.maximumValue = 10;
            
            if (loanSchemeIndex < _SchemeMuArr.count) {
                if (self.case2.selected) {
                    self.money1.text = [NSString stringWithFormat:@"￥%d", (int)(priceInt*((ZFSchemeData *)[_SchemeMuArr objectAtIndex:loanSchemeIndex]).LeasingDepositRate)];
                    self.monthCountSlider.maximumValue = ((ZFSchemeData *)[_SchemeMuArr objectAtIndex:loanSchemeIndex]).LeasingPeriod / 12;
                    self.mySlider.minimumValue = ((ZFSchemeData *)[_SchemeMuArr objectAtIndex:loanSchemeIndex]).LeasingDepositRate*10;
                }else{
                    self.money1.text = [NSString stringWithFormat:@"￥%d", 0];
                    self.monthCountSlider.maximumValue = ((ZFSchemeData *)[_SchemeMuArr objectAtIndex:loanSchemeIndex]).LoanPeriod / 12;
                    self.mySlider.minimumValue = ((ZFSchemeData *)[_SchemeMuArr objectAtIndex:loanSchemeIndex]).LoanRate*10;
                }
            }

            self.perscet.text = [NSString stringWithFormat:@"%d%%",((int)self.mySlider.minimumValue)*10];
            [self.monthCountSlider setValue:1.0];
            self.monthCountLable.text = [NSString stringWithFormat:@"%d", 12];
        }
            break;
        default:
            break;
    }
    
}

#pragma mark - action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([actionSheet isKindOfClass:[customPickerView class]]){
        pickerView.hidden = YES;
        if(buttonIndex == 0) {
            
        }else {
            
            
        }
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    cityPickerView.hidden = YES;
    pickerView.hidden = YES;
    cSLPickerView.hidden = YES;
}

@end







