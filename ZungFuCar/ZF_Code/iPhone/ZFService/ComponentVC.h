//
//  ComponentVC.h
//  ZungFuCar
//
//  Created by lxm on 13-10-16.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"
#import "ComponentHorizontalView.h"
#import "HorizontalScrollView.h"

@interface ComponentVC : AlexBaseViewController<HorizontalScrollViewDelegate>
{
    UIButton *btn1;
    UIButton *btn2;
    
    /**
     *	栏目
     */
    int currentIndex;
    
    /**
     *	表格row
     */
    int verticalViewRow;
    NSArray *storeDataArray;
    
    HorizontalScrollView *hScrollView;
    
    UIImageView *btnBgView;
}
//@property(strong , nonatomic)ComponentHorizontalView *verticalView;
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;
@property(nonatomic ,weak )NSDictionary *dic;

-(void)pushToDetail:(int )index;
@end
