//
//  CalculatorVC.h
//  ZungFuCar
//
//  Created by lxm on 13-10-17.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "CompanyProfileVC.h"
#import "TSLocateView.h"
#import "customPickerView.h"
#import "CustomSLPickerView.h"
#import "AlexBaseViewController.h"
@interface CalculatorVC : AlexBaseViewController<UITextFieldDelegate,UIActionSheetDelegate,CustomSLPickerViewDelegate, CustomPickerViewDelegate>{
    TSLocateView *cityPickerView;
    customPickerView *pickerView;
    CustomSLPickerView *cSLPickerView;
    NSArray *entries;
    NSArray *entriesSelected;
	NSMutableDictionary *selectionStates;
    
    int customSLPickerViewType;
    NSString *currentCarStyleItemID;
    int loanSchemeIndex;
    NSArray *carTypes;
    int priceInt;
    float firstRatio;
    int monthCount;
    NSDictionary *_carTypeData;
    NSString *_productDetailID;
    NSMutableArray *_SchemeMuArr;
    NSMutableArray *_netShowBenzDetailMuArr;
    NSMutableArray *_netShowAMGDetailMuArr;

}
@property(nonatomic,weak)IBOutlet UIButton *carStyle;
@property(nonatomic,weak)IBOutlet UIButton *carType;
@property(nonatomic,weak)IBOutlet UIButton *case1;
@property(nonatomic,weak)IBOutlet UIButton *case2;

@property(nonatomic,weak)IBOutlet UILabel *price;
@property(nonatomic,weak)IBOutlet UILabel *money1;
@property(nonatomic,weak)IBOutlet UILabel *perscet;
@property(nonatomic,weak)IBOutlet UILabel *money2;
@property (weak, nonatomic) IBOutlet UILabel *moneyMonth;
@property(nonatomic,weak)IBOutlet UISlider *mySlider;
@property (weak, nonatomic) IBOutlet UIButton *loanScheme;
@property (weak, nonatomic) IBOutlet UISlider *monthCountSlider;
@property (weak, nonatomic) IBOutlet UILabel *monthCountLable;
@property (weak, nonatomic) IBOutlet UILabel *depositLabel;
@property(nonatomic,weak)IBOutlet UIScrollView *_bgScrollView;
@property(nonatomic,weak)IBOutlet UILabel *_titleLabel;
@property BOOL isExtend2;
- (IBAction)caseBtn:(id)sender;
- (IBAction)carStyle:(id)sender;
- (IBAction)carType:(id)sender;
- (IBAction)sliderValueChanged:(id)sender;
@end
