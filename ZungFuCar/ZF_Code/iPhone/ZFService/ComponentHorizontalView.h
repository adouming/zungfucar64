//
//  ComponentHorizontalView.h
//  ZungFuCar
//
//  Created by lxm on 13-10-14.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

//横滚动


@class ComponentVC;
@interface ComponentHorizontalView : UIView<UITableViewDelegate,UITableViewDataSource>



@property (nonatomic,unsafe_unretained) ComponentVC *controller;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,copy) NSArray *storeDataArray;



@end

