//
//  GetAfterSalesData.m
//  ZungFuCar
//
//  Created by Ed Lee on 11/30/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "GetAfterSalesData.h"
#import "ZFSeriviceAPIManager.h"
#import "DBManager.h"

@implementation NSObject(GetAfterSalesData)

- (NSArray*)creatASModel:(NSString *)title
{
    NSArray *array = nil;
    if([title isEqualToString:@"精品"])
    {
        array = [[DBManager sharedManager] getBoutiqueData];
        if([array count] ==0 )
            return nil;
    }
    else if([title isEqualToString:@"配件"])
    {
        array = [[DBManager sharedManager] getPartsData];
        if([array count] ==0 )
            return nil;
    }
    else if([title isEqualToString:@"校园"])
    {
        array = [[DBManager sharedManager] getCampusRecruitmentData];
        if([array count] ==0 )
            return nil;
    }
    return array;
}

- (void)requestData:(NSString *)title completion:(void (^)(NSArray *titleData))titles
{
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    if([title isEqualToString:@"精品"])
    {
        [manager requestBoutiqueDataSuccess:^(NSArray *data) {
            BOOL isFirst = NO;
            if([data count]==0)
            {
                return ;
            }
            else
            {
                /*写lastmodifyTime到沙盒*/
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                if([userDefaults objectForKey:@"ViewID21_UpdatedTime"]==nil)
                    isFirst = YES;
                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID21_UpdatedTime"];
                [userDefaults synchronize];
                
                NSString *htmlStr = nil;
                NSDictionary *dic = nil;
                NSMutableArray *dataTemp = [[NSMutableArray alloc] init];
                for(int i=0;i<[data count];i++)
                {
                    
                    htmlStr = [[data objectAtIndex:i] objectForKey:@"Descp"];
                    NSString *imgReplaceStr = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
                    htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceStr];
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:
                           htmlStr,@"Descp",
                           [[data objectAtIndex:i] objectForKey:@"BannerURL"], @"BannerURL",
                           [[data objectAtIndex:i] objectForKey:@"ID"],@"ID",
                           [[data objectAtIndex:i] objectForKey:@"DisplayOrder"],@"DisplayOrder",
                           [[data objectAtIndex:i] objectForKey:@"Name"],@"Name",
                           [[data objectAtIndex:i] objectForKey:@"UpdatedTime"],@"UpdatedTime",
                           nil];
                    [dataTemp addObject:dic];
                }
                
                [[DBManager sharedManager] storeBoutiqueData:dataTemp Sucess:^(NSMutableArray *storeData) {
                    
                    if(isFirst==YES)
                        [self creatASModel:title];
                    
                    titles(dataTemp);
                } fail:^(NSError *error) {
                    
                }];
            }
            
            
        }failure:^(NSError *error){
            
        }];
        
    }
    else if([title isEqualToString:@"配件"])
    {
        [manager requestPartsDataSuccess:^(NSArray *data) {
            BOOL isFirst = NO;
            if([data count]==0)
            {
                return ;
            }
            else
            {
                /*写lastmodifyTime到沙盒*/
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                if([userDefaults objectForKey:@"ViewID19_UpdatedTime"]==nil)
                    isFirst = YES;
                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID19_UpdatedTime"];
                [userDefaults synchronize];
                
                NSString *htmlStr = nil;
                NSDictionary *dic = nil;
                NSMutableArray *dataTemp = [[NSMutableArray alloc] init];
                for(int i=0;i<[data count];i++)
                {
                    
                    htmlStr = [[data objectAtIndex:i] objectForKey:@"Descp"];
                    NSString *imgReplaceStr = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
                    htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceStr];
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:
                           htmlStr,@"Descp",
                           [[data objectAtIndex:i] objectForKey:@"BannerURL"], @"BannerURL",
                           [[data objectAtIndex:i] objectForKey:@"ID"],@"ID",
                           [[data objectAtIndex:i] objectForKey:@"DisplayOrder"],@"DisplayOrder",
                           [[data objectAtIndex:i] objectForKey:@"Name"],@"Name",
                           [[data objectAtIndex:i] objectForKey:@"UpdatedTime"],@"UpdatedTime",
                           nil];
                    [dataTemp addObject:dic];
                }
                [[DBManager sharedManager] storePartsData:dataTemp Sucess:^(NSMutableArray *storeData) {
                    if(isFirst==YES)
                        [self creatASModel:title];
                    titles(dataTemp);
                } fail:^(NSError *error) {
                    
                }];

            }
            
        }failure:^(NSError *error){
            
        }];
    }
    else if([title isEqualToString:@"校园"])
    {
        [manager requestCampusRecruitmentDataSuccess:^(NSArray *data) {
            BOOL isFirst = NO;
            if([data count]==0)
            {
                return ;
            }
            else
            {
                /*写lastmodifyTime到沙盒*/
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                if([userDefaults objectForKey:@"ViewID175_UpdatedTime"]==nil)
                    isFirst = YES;
                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID175_UpdatedTime"];
                [userDefaults synchronize];
                
                NSString *htmlStr = nil;
                NSDictionary *dic = nil;
                NSMutableArray *dataTemp = [[NSMutableArray alloc] init];
                for(int i=0;i<[data count];i++)
                {
                    
                    htmlStr = [[data objectAtIndex:i] objectForKey:@"Content"];
                    NSString *imgReplaceStr = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
                    htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceStr];
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:
                           htmlStr,@"Descp",
                           //                           [[data objectAtIndex:i] objectForKey:@"BannerURL"], @"BannerURL",
                           [[data objectAtIndex:i] objectForKey:@"ID"],@"ID",
                           [[data objectAtIndex:i] objectForKey:@"DisplayOrder"],@"DisplayOrder",
                           [[data objectAtIndex:i] objectForKey:@"Name"],@"Name",
                           [[data objectAtIndex:i] objectForKey:@"UpdatedTime"],@"UpdatedTime",
                           nil];
                    [dataTemp addObject:dic];
                }
                [[DBManager sharedManager] storeCampusRecruitmentData:dataTemp Sucess:^(NSMutableArray *storeData) {
                    if(isFirst==YES)
                        [self creatASModel:title];
                    titles(dataTemp);
                } fail:^(NSError *error) {
                    
                }];
//                titles(data);
            }
            
        }failure:^(NSError *error){
            
        }];
    }
}

@end
