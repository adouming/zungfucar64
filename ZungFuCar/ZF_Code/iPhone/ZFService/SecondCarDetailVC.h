//
//  SecondCarDetailVC.h
//  ZungFuCar
//
//  Created by lxm on 13-10-16.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "CompanyProfileVC.h"

@interface SecondCarDetailVC : MenuAbstractViewController<UIWebViewDelegate>
{
    __strong IBOutlet UIScrollView *_bgScrollView;
    __strong IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UIWebView *_contentWebView;
    __weak IBOutlet UIImageView *_bannerView;
    BOOL willLaunchSafari;
}

@property(nonatomic ,weak )NSDictionary *dic;
@end
