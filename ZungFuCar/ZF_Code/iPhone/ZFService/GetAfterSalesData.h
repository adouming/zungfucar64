//
//  GetAfterSalesData.h
//  ZungFuCar
//
//  Created by Ed Lee on 11/30/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SideBar.h"

@interface NSObject(GetAfterSalesData)


- (NSArray*)creatASModel:(NSString *)title;
//- (void)requestData:(NSString *)title;

- (void)requestData:(NSString *)title completion:(void (^)(NSArray *titleData))titles;

@end
