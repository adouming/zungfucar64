//
//  SecondHandCarCell.m
//  ZungFuCar
//
//  Created by lxm on 13-10-16.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "SecondHandCarCell.h"

@implementation SecondHandCarCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setData:(NSDictionary *)data
{
    self.nameLabel.text = [data objectForKey:@"Name"];
}

@end
