//
//  FinanceVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-6.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "FinanceVC.h"
#import "AboutZFAPIManager.h"
#import "DBManager.h"
#import "NSString+FilterHtml.h"
@interface FinanceVC ()

@end

@implementation FinanceVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _titleLabel.text = @"金融";
    _contentWebView.scrollView.scrollEnabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)creatModel
{
    NSArray *array = [[DBManager sharedManager] getFinanceData];
    if([array count] ==0 )
        return;
    NSString *contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
    NSString *bannerUrl = [[array objectAtIndex:0] objectForKey:@"BannerURL"];
    if([bannerUrl validatePng] &&![bannerUrl isEqualToString:@""])
        [_bannerView setImageWithURL:[NSURL URLWithString:[[array objectAtIndex:0] objectForKey:@"BannerURL"]]];

}

- (void)requestData
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID184_UpdatedTime"]==nil)
    {
        [self showLoadingText];
    }
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    [manager requestFinanceDataSuccess:^(NSArray *data) {
        BOOL isFirst = NO;
        if([data count]==0)
        {
            return ;
        }
        else
        {
            /*写lastmodifyTime到沙盒*/
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if([userDefaults objectForKey:@"ViewID184_UpdatedTime"]==nil)
                isFirst = YES;
            [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID184_UpdatedTime"];
            [userDefaults synchronize];
            
            NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"Content"];
//            NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
//            htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
            [self hideLoadingText];
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"BannerURL"],@"BannerURL",
                                 htmlStr,@"Content",
                                 [[data objectAtIndex:0] objectForKey:@"ID"],@"ID",
                                 [[data objectAtIndex:0] objectForKey:@"Name"],@"Name",
                                 [[data objectAtIndex:0] objectForKey:@"UpdatedTime"],@"UpdatedTime",
                                 nil];
            
            NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
            [[DBManager sharedManager] storeFinanceData:dataTemp Sucess:^(NSMutableArray *storeData) {
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
        }
        
    }failure:^(NSError *error){
        [self hideLoadingText];
    }];
}

@end
