//
//  ZFServiceVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-6.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "ZFServiceVC.h"
#import "CommonSingleWebViewController.h"
@interface ZFServiceVC ()

@end

@implementation ZFServiceVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    UIImage *image = nil;
    if(iPhone5)
    {
        image = [UIImage imageNamed:@"index_bg_iphone5@2x"];
    }
    else
    {
        image = [UIImage imageNamed:@"index_bg.png"];
    }
    [self.view addSubview:imageView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *	隐藏右边按钮
 */
-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}

//保险
- (IBAction)InsuranceVCAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:18];
    controler.title = @"保险";
    [self.navigationController pushViewController:controler animated:YES];
}
//大客户销售
- (IBAction)BigCustomerVCAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:19];
    controler.title = @"大客户销售";
    [self.navigationController pushViewController:controler animated:YES];
}

@end
