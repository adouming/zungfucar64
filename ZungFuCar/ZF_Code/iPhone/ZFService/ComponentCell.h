//
//  ComponentCell.h
//  ZungFuCar
//
//  Created by lxm on 13-10-14.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFStewardData.h"

@interface ComponentCell : UITableViewCell
@property (nonatomic ,weak)IBOutlet UIImageView *bgImageView;
@property (nonatomic ,weak)IBOutlet UILabel *nameLabel;
-(void)setData:(NSDictionary *)data;

@end
