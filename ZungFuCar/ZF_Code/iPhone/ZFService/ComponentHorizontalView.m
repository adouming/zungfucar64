//
//  NetShowClassHorizontalView.m
//  ZungFuCar
//
//  Created by lxm on 13-10-13.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "ComponentHorizontalView.h"
#import "ComponentCell.h"

#import "ComponentVC.h"


#define CELL_HEIGHT 280

@implementation ComponentHorizontalView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    
    return self;
}
-(void)awakeFromNib{
    
    [super awakeFromNib];
    
    [self initTableView];
    [self layoutTable];
    // Initialization code
    
}
-(void)initTableView{
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    /*旋转90*/
    _tableView.frame = CGRectMake(0, 0, self.frame.size.height, self.frame.size.width);
    _tableView.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    _tableView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    
    
}
-(void)layoutTable{
    
    _tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, _tableView.bounds.size.width-7);
    
}
#pragma mark - tableView dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return CELL_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.storeDataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int row = [indexPath row];
    static NSString *CellIdentifier = @"ComponentHorizontalViewCellIdentify";
    ComponentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ComponentCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
        [cell setData:[self.storeDataArray objectAtIndex:row]];
        
        // cell顺时针旋转90度
        cell.contentView.transform = CGAffineTransformMakeRotation(M_PI / 2);
        
        
    }else{
        [cell setData:[self.storeDataArray objectAtIndex:row]];
    }
    
    return cell;
}

#pragma mark - tableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [((ComponentVC*)self.controller) pushToDetail:[indexPath row]];
}



@end


