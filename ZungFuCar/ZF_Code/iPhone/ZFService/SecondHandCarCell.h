//
//  SecondHandCarCell.h
//  ZungFuCar
//
//  Created by lxm on 13-10-16.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFStewardData.h"

@interface SecondHandCarCell : UITableViewCell
@property (nonatomic ,weak)IBOutlet UIImageView *bgImageView;
@property (nonatomic ,weak)IBOutlet UILabel *nameLabel;
-(void)setData:(NSMutableArray *)data;

@end
