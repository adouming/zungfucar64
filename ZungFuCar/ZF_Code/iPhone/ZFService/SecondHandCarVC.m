//
//  SecondHandCarVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-15.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "SecondHandCarVC.h"
#import "SecondCarDetailVC.h"
@interface SecondHandCarVC ()

@end

@implementation SecondHandCarVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    /**
     *	非二级菜单显示按钮
     */
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    storeDataArray = [[NSMutableArray alloc]init];
    
    hScrollView = [[HorizontalScrollView alloc]initWithFrame:CGRectMake(0, 142, SCREEN_WIDTH, SCREEN_HEIGHT-142)];
    //hScrollView.frame = CGRectMake(0, 142, 270, 250);
    hScrollView.delegate = self;
    [self.view addSubview:hScrollView];
    
    [self createModel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    self.verticalView = nil;
    [super viewDidUnload];
}

-(void)initView{
    
//    //市场--左右
//    NSArray *nibArray2 = [[NSBundle mainBundle] loadNibNamed:@"SecondHandCarHorizontalView" owner:self options:nil];
//    self.verticalView = [nibArray2 objectAtIndex:0];
//    
//    self.verticalView.frame = CGRectMake(0, 100, 320, self.view.frame.size.height-180);
//    self.verticalView.controller = self;
//    //_classVerticalView.hidden = YES;
//    [self.view addSubview:self.verticalView];
    if ([storeDataArray count]>0) {
        [hScrollView clearAllView];
        [hScrollView initScrollView:storeDataArray type:4];
    }
    
}


#pragma mark - data
-(void)createModel{
    
    storeDataArray = [NSMutableArray arrayWithObjects:
                                        [NSDictionary dictionaryWithObjectsAndKeys:
                                         @"臻至服务",@"Name",
                                         @"臻至服务TestTestTestTestTestTest",@"Des", nil],
                                        [NSDictionary dictionaryWithObjectsAndKeys:
                                         @"五星评估",@"Name",
                                         @"五星评估TestTestTestTestTestTest",@"Des", nil],
                                        [NSDictionary dictionaryWithObjectsAndKeys:
                                         @"一站式置换",@"Name",
                                         @"一站式置换TestTestTestTestTestTest",@"Des", nil],
                                        [NSDictionary dictionaryWithObjectsAndKeys:
                                         @"星睿认证",@"Name",
                                         @"星睿认证TestTestTestTestTestTest",@"Des", nil],
                                        [NSDictionary dictionaryWithObjectsAndKeys:
                                         @"尊品寄售及优质回购",@"Name",
                                         @"尊品寄售及优质回购TestTestTestTestTestTest",@"Des", nil],
                                        [NSDictionary dictionaryWithObjectsAndKeys:
                                         @"问题答疑",@"Name",
                                         @"问题答疑TestTestTestTestTestTest",@"Des", nil], nil];
    [self initView];
    return;
    [[ZFStewardManager sharedManager] getDataSuccess:^(NSArray *data) {
        NSLog(@"%@",data);
        [self initView];
        storeDataArray = [NSMutableArray arrayWithObjects:@"1",@"2",@"3",@"4", nil];
        
    } failure:^(NSError *error) {
        
        NSLog(@"error");
        NSString *message = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"error" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }];
}

-(void)pushToDetail:(int )index{
    verticalViewRow = index;
    [self performSegueWithIdentifier:@"SecondHandCarVC->SecondCarDetailVC" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // 得到目标视图
    SecondCarDetailVC *viewController = segue.destinationViewController;
    viewController.dic = [storeDataArray objectAtIndex:verticalViewRow];
}

@end
