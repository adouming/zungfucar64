//
//  SecondHandCarVC.h
//  ZungFuCar
//
//  Created by lxm on 13-10-15.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"
#import "SecondHandCarHorizontalView.h"
#import "HorizontalScrollView.h"

@interface SecondHandCarVC : AlexBaseViewController<HorizontalScrollViewDelegate>
{
    /**
     *	表格row
     */
    int verticalViewRow;
    HorizontalScrollView *hScrollView;
    NSMutableArray *storeDataArray;
}
@property(strong , nonatomic)SecondHandCarHorizontalView *verticalView;

-(void)pushToDetail:(int )index;
@end
