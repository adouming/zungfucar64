//
//  ComponentCell.m
//  ZungFuCar
//
//  Created by lxm on 13-10-14.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "ComponentCell.h"
#import "ZFSeriviceAPIManager.h"
#import "UIImageView+AFNetworking.h"
@implementation ComponentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setData:(NSDictionary *)data
{
    /*Name*/
    self.nameLabel.text = [data objectForKey:@"Name"];
}

@end
