//
//  FinanceInsuranceVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-6.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "FinanceInsuranceVC.h"

@interface FinanceInsuranceVC ()

@end

@implementation FinanceInsuranceVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    /**
     *	非二级菜单显示按钮
     */
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    UIImage *image = nil;
    if(iPhone5)
    {
        image = [UIImage imageNamed:@"index_bg_iphone5@2x"];
    }
    else
    {
        image = [UIImage imageNamed:@"index_bg.png"];
    }
    [self.view addSubview:imageView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *	隐藏右边按钮
 */
-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}
@end
