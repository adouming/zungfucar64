//
//  InsuranceVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-6.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "InsuranceVC.h"
#import "AboutZFAPIManager.h"
#import "DBManager.h"
#import "NSString+FilterHtml.h"
@interface InsuranceVC ()

@end

@implementation InsuranceVC


- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _contentWebView.scrollView.scrollEnabled = NO;
    _titleLabel.text = @"保险";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/**
 * Model
 */
- (void)creatModel
{
    NSArray *array = [[DBManager sharedManager] getInsuranceData];
    if([array count] ==0 )
        return;
    NSString *contentStr = [[DBManager sharedManager] wrapperHtml:  [[array objectAtIndex:0] objectForKey:@"content"] ];
    [_contentWebView loadHTMLString: contentStr baseURL:[[DBManager sharedManager] baseUrl]];
    NSString *bannerUrl = [[array objectAtIndex:0] objectForKey:@"BannerURL"];
    if([bannerUrl validatePng] &&![bannerUrl isEqualToString:@""])
        [_bannerView setImageWithURL:[NSURL URLWithString:[[array objectAtIndex:0] objectForKey:@"BannerURL"]]];
}

- (void)requestData
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID136_UpdatedTime"]==nil)
    {
        [self showLoadingText];
    }
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    [manager requestInsuranceDataSuccess:^(NSArray *data) {
        BOOL isFirst = NO;
        if([data count]==0)
        {
            return ;
        }
        else
        {
            /*写lastmodifyTime到沙盒*/
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if([userDefaults objectForKey:@"ViewID136_UpdatedTime"]==nil)
                isFirst = YES;
            [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID136_UpdatedTime"];
            [userDefaults synchronize];
            
            NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"content"];
            //NSString *imgReplaceStr = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
            //htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceStr];
            //            [self.webView loadHTMLString:htmlStr baseURL:nil];
            [self hideLoadingText];
            /*重新封装数据*/
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[
                                [data objectAtIndex:0] objectForKey:@"img"],@"BannerURL",
                                 htmlStr,@"content",
                                 [[data objectAtIndex:0] objectForKey:@"id"],@"ID",
                                 [[data objectAtIndex:0] objectForKey:@"title"],@"Name",
                                 [[data objectAtIndex:0] objectForKey:@"updatetime"],@"UpdatedTime",
                                 nil];
            
            [self hideLoadingText];
            NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
            [[DBManager sharedManager] storeInsuranceData:dataTemp Sucess:^(NSMutableArray *storeData) {
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
        }
        
    }failure:^(NSError *error){
        [self hideLoadingText];
    }];
}
/*
 * 给UIWebView 设置高度 自适应内容
 */
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //[self injectJavascript:webView];
    
    //NSString *padding = @"document.body.style.margin='5px';document.body.style.padding = '5px'";
    //[webView stringByEvaluatingJavaScriptFromString:padding];
    
    /*获取html body 的内容长度 计算高度*/
    NSString *height_str= [webView stringByEvaluatingJavaScriptFromString: @"document.body.scrollHeight"];
    NSLog(@"body height: %@", height_str);
    int height = [height_str intValue];
    webView.frame = CGRectMake(webView.frame.origin.x,webView.frame.origin.y,webView.frame.size.width,height);
    
    /*设置WebView的superView=_bgScrollView的contentSize*/
    [_bgScrollView setContentSize:CGSizeMake(_bgScrollView.frame.size.width,CGRectGetMaxY(webView.frame))];
    willLaunchSafari = YES;
}
@end
