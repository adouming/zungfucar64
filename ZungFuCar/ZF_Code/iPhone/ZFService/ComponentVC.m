//
//  ComponentVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-16.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "ComponentVC.h"
#import "ZFSeriviceAPIManager.h"
#import "ComponentDetailVC.h"
#import "DBManager.h"
#import "HorizontalScrollView.h"

@interface ComponentVC ()

@end

@implementation ComponentVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];

    hScrollView = [[HorizontalScrollView alloc]initWithFrame:CGRectMake(0, 142, SCREEN_WIDTH, SCREEN_HEIGHT-142)];
    //hScrollView.frame = CGRectMake(0, 142, 270, 250);
    hScrollView.delegate = self;

    [self.view addSubview:hScrollView];
    
    btnBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 99, 320, 50)];
    btnBgView.backgroundColor = [UIColor clearColor];
    btnBgView.userInteractionEnabled = YES;
    [self.view addSubview:btnBgView];
    
    btn1 = [[UIButton alloc]initWithFrame:CGRectMake(-2, 0, 240, 45)];
    [btn1 setTitle:@"精品" forState:UIControlStateNormal];
    btn1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btn1.titleLabel.font = [UIFont systemFontOfSize:45.0f];
    [btn1 setTitleColor:RGBA(255, 255, 255, 1.0) forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(btn1:) forControlEvents:UIControlEventTouchUpInside];
    [btnBgView addSubview:btn1];
    
    btn2 = [[UIButton alloc]initWithFrame:CGRectMake(250, 0, 240, 45)];
    [btn2 setTitle:@"配件" forState:UIControlStateNormal];
    btn2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btn2.titleLabel.font = [UIFont systemFontOfSize:45.0f];
    [btn2 setTitleColor:RGBA(255, 255, 255, 0.5) forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(btn2:) forControlEvents:UIControlEventTouchUpInside];
    [btnBgView addSubview:btn2];
    
	// Do any additional setup after loading the view.
    if([[self.dic objectForKey:@"currentIndex"] intValue]==1)
    {
        currentIndex = 1;
        [self btn1:btn1];
    }
    else
    {
        currentIndex = 2;
        [self btn2:btn2];
    }
    
    /**
     *	非二级菜单显示按钮
     */
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;

    self.leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    self.rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [btnBgView addGestureRecognizer:self.leftSwipeGestureRecognizer];
    [btnBgView addGestureRecognizer:self.rightSwipeGestureRecognizer];
}

- (void)viewDidUnload
{
//    self.verticalView = nil;
    btnBgView = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/**
 *	精品
 */
- (void)btn1:(id)sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [btn1 setFrame:CGRectMake(-2, 0, 150, 50)];
    [btn2 setFrame:CGRectMake(245, 0, 150, 50)];
    [btn2 setTitleColor:RGBA(255, 255, 255, 0.5) forState:UIControlStateNormal];
    [btn1 setTitleColor:RGBA(255, 255, 255, 1.0) forState:UIControlStateNormal];
    [UIView commitAnimations];
    [self creatModel:@"精品"];
    [self requestData:@"精品"];
}
/**
 *	零件
 */
- (void)btn2:(id)sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [btn2 setFrame:CGRectMake(-2, 0, 150, 50)];
    [btn1 setFrame:CGRectMake(245, 0, 150, 50)];
    [btn1 setTitleColor:RGBA(255, 255, 255, 0.5) forState:UIControlStateNormal];
    [btn2 setTitleColor:RGBA(255, 255, 255, 1.0) forState:UIControlStateNormal];
    [UIView commitAnimations];
    [self creatModel:@"配件"];
    [self requestData:@"配件"];
}

-(void)pushToDetail:(int )index{
    verticalViewRow = index;
    [self performSegueWithIdentifier:@"ComponentVC->ComponentDetailVC" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // 得到目标视图
    ComponentDetailVC *viewController = segue.destinationViewController;
    // 传参
    [viewController setDic:[storeDataArray objectAtIndex:verticalViewRow]];
}

- (void)handleSwipes:(UISwipeGestureRecognizer *)sender
{
    if(currentIndex==1)
    {
        [self btn2:btn2];
        currentIndex = 2;
    }
    else if(currentIndex==2)
    {
        [self btn1:btn1];
        currentIndex = 1;
    }
}

- (void)creatModel:(NSString *)title
{
    NSArray *array = nil;
    if([title isEqualToString:@"精品"])
    {
        array = [[DBManager sharedManager] getBoutiqueData];
        if([array count] ==0 )
            return;
    }
    else if([title isEqualToString:@"配件"])
    {
        array = [[DBManager sharedManager] getPartsData];
        if([array count] ==0 )
            return;
    }
    storeDataArray = [[NSArray alloc]initWithArray:array];
    if ([storeDataArray count]>0) {
        [hScrollView clearAllView];
        [hScrollView initScrollView:storeDataArray type:0];
    }
}

- (void)requestData:(NSString *)title
{
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    if([title isEqualToString:@"精品"])
    {
        [manager requestBoutiqueDataSuccess:^(NSArray *data) {
            BOOL isFirst = NO;
            if([data count]==0)
            {
                return ;
            }
            else
            {
                /*写lastmodifyTime到沙盒*/
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                if([userDefaults objectForKey:@"ViewID21_UpdatedTime"]==nil)
                    isFirst = YES;
                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID21_UpdatedTime"];
                [userDefaults synchronize];
                
                NSString *htmlStr = nil;
                NSDictionary *dic = nil;
                NSMutableArray *dataTemp = [[NSMutableArray alloc] init];
                for(int i=0;i<[data count];i++)
                {
                
                htmlStr = [[data objectAtIndex:i] objectForKey:@"Descp"];
                //NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
                //htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
                dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                     htmlStr,@"Descp",
                                     [[data objectAtIndex:i] objectForKey:@"BannerURL"], @"BannerURL",
                                     [[data objectAtIndex:i] objectForKey:@"ID"],@"ID",
                                     [[data objectAtIndex:i] objectForKey:@"DisplayOrder"],@"DisplayOrder",
                                     [[data objectAtIndex:i] objectForKey:@"Name"],@"Name",
                                     [[data objectAtIndex:i] objectForKey:@"UpdatedTime"],@"UpdatedTime",
                                     nil];
                [dataTemp addObject:dic];
                }
                [[DBManager sharedManager] storeBoutiqueData:dataTemp Sucess:^(NSMutableArray *storeData) {
                    if(isFirst==YES)
                        [self creatModel:title];
                } fail:^(NSError *error) {
                    
                }];
            }
            
        }failure:^(NSError *error){
            
        }];
        
    }
    else if([title isEqualToString:@"配件"])
    {
        [manager requestPartsDataSuccess:^(NSArray *data) {
            BOOL isFirst = NO;
            if([data count]==0)
            {
                return ;
            }
            else
            {
                /*写lastmodifyTime到沙盒*/
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                if([userDefaults objectForKey:@"ViewID19_UpdatedTime"]==nil)
                    isFirst = YES;
                [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID19_UpdatedTime"];
                [userDefaults synchronize];
                
                NSString *htmlStr = nil;
                NSDictionary *dic = nil;
                NSMutableArray *dataTemp = [[NSMutableArray alloc] init];
                for(int i=0;i<[data count];i++)
                {
                    
                    htmlStr = [[data objectAtIndex:i] objectForKey:@"Descp"];
                    //NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
                    //htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
                    dic = [NSDictionary dictionaryWithObjectsAndKeys:
                           htmlStr,@"Descp",
                           [[data objectAtIndex:i] objectForKey:@"BannerURL"], @"BannerURL",
                           [[data objectAtIndex:i] objectForKey:@"ID"],@"ID",
                           [[data objectAtIndex:i] objectForKey:@"DisplayOrder"],@"DisplayOrder",
                           [[data objectAtIndex:i] objectForKey:@"Name"],@"Name",
                           [[data objectAtIndex:i] objectForKey:@"UpdatedTime"],@"UpdatedTime",
                           nil];
                    [dataTemp addObject:dic];
                }
                [[DBManager sharedManager] storePartsData:dataTemp Sucess:^(NSMutableArray *storeData) {
                    if(isFirst==YES)
                        [self creatModel:title];
                } fail:^(NSError *error) {
                    
                }];
            }
            
        }failure:^(NSError *error){
            
        }];
    }
}

@end
