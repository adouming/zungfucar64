//
//  NewsDetailController.m
//  ZungFuCar
//
//  Created by Ed Lee on 12/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//
#import "DBManager.h"
#import "NewsDetailController.h"
#import "ShareView.h"


@interface NewsDetailController ()

@end

@implementation NewsDetailController
@synthesize newsTitle, bannerUrl, date, descp, address, shortDesc, bannerName;

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
	self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 698);
    _contentWebView.delegate = self;
    _contentWebView.opaque = NO;
    _contentWebView.backgroundColor = [UIColor clearColor];
    _contentWebView.scrollView.scrollEnabled = NO;
    _contentWebView.dataDetectorTypes = UIDataDetectorTypeNone;
    NSLog(@"%f %f %f %f", self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [_bannerView setImageWithURL:[NSURL URLWithString:bannerUrl]];
    _titleLabel.text = newsTitle;
    
    if (date.length>11) {
        date = [date substringToIndex:10];
    }
    _dateLabel.text = date;
    
    if ([address isEqualToString:@""] || address==nil) {
        _lookButton.hidden = YES;
    }
    NSString *imgReplaceStr = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
    descp=[descp stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceStr];
    
//    if ([descp rangeOfString:@"style=\"padding\""].location == NSNotFound) {
//        descp = [NSString stringWithFormat:@"%@%@%@", @"<div style=\"padding:15px;\">", descp, @"</div>"];
//    }
    
    _contentWebView.delegate = self;
    [_contentWebView loadHTMLString:descp baseURL:[[DBManager sharedManager] baseUrl]];
}


-(void)webViewDidStartLoad:(UIWebView *)webView {
    [self showLoadingText];
    willLaunchSafari = NO;
}
/*
 * 给UIWebView 设置高度 自适应内容
 */
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self injectJavascript:webView];
    [webView stringByEvaluatingJavaScriptFromString: @"$('p').css({'color':'#ccc'});$('a').css({'color':'#ccc'})"];
    CGRect frame = webView.frame;
    frame.size.height = 1;
    webView.frame = frame;
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    CGFloat contentHeight = frame.size.height;
    webView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, contentHeight);
    NSLog(@"%f",frame.size.height);
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width,contentHeight +CGRectGetMaxY(_titleLabel.frame) + 200)];
    
    [self hideLoadingText];
    willLaunchSafari = YES;
}

/*
 * 点击网页图片,打开相册
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([self webViewImgIsClickedByRequest:request] == 0) {
        UINavigationController *nc = [self getAlbumPhotosFromImgClick:request];
        if (nc != nil) {
            nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:nc animated:YES completion:nil];
        }
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 1 && willLaunchSafari) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    } else {
        return YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)openLink:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:address]];
}

- (IBAction)shareByActivity:(id)sender {
    if (shareView==nil) {
        shareView = [[ShareView alloc]initWithImage:_bannerView.image imageUrl:bannerUrl text:shortDesc title:newsTitle bannerName:bannerName];
    }
    [shareView showInView:sender];
    
}
@end
