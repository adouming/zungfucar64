//
//  NewsSplitController.h
//  ZungFuCar
//
//  Created by Ed Lee on 12/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

// Subviews:
#import "SideBar.h"
#import "MenuAbstractViewController.h"
#import "NewsListCell.h"

// Category
#import "NSObject+ReadSubMenu.h"
#import "UIImageView+AFNetworking.h"
//#import "AFJSONRequestOperation.h"
#import "DBManager.h"

// VC
#import "NewsDetailController.h"

@interface NewsSplitController : MenuAbstractViewController <SideBarDelegate, UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *dataSource;
}

@property (strong, nonatomic) SideBar *sidebar;
@property (strong, nonatomic) NSMutableArray *sidebarMenuTitles;
@property (strong, nonatomic) UINavigationController *contentNaviCtrl;
//@property (strong, nonatomic) NSString *bgImageName;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITableView *newsTableView;
@property (strong, nonatomic) IBOutlet UIView *tableContainer;

@property NSInteger subIndex;
@property NSInteger topIndex;
@property NSInteger currentIndex;

@property BOOL pushFromNews;

@end
