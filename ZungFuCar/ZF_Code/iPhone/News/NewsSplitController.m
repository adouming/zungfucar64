//
//  NewsSplitController.m
//  ZungFuCar
//
//  Created by Ed Lee on 12/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NewsSplitController.h"

static NSString * const CellIdentifier = @"NewsListCellIdentifier";
extern int newsCount;
extern int privilegeCount;


@interface NewsSplitController ()

@end

@implementation NewsSplitController
@synthesize currentIndex, pushFromNews;

#define LEFTBUTTON          0
#define RIGHTBUTTON         1
#define TABLEW              312

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    CGFloat naviCtrlWidth;
    if (_sidebarMenuTitles.count>0) {
        _sidebar = [[SideBar alloc]init];
        [self.view addSubview:_sidebar];
        _sidebar.initialHighlight = currentIndex;
        _sidebar.delegate = self;
        [_sidebar addButtonsForSidebar:_sidebarMenuTitles fromTopIndex:_topIndex andSubIndex:_subIndex];
        naviCtrlWidth = self.view.frame.size.width - TABLEW - _sidebar.frame.size.width;
    } else {
        naviCtrlWidth = self.view.frame.size.width - TABLEW;
    }
    
    if (IOS7) {
        UIView *statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 20)];
        statusView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:statusView];
    }
    
    self.view.backgroundColor = RGB(42, 42, 42);
    
    _tableContainer.frame = CGRectMake(_sidebar.frame.size.width, IOS7?20:0, TABLEW, self.view.frame.size.height-(IOS7?70:50));
    
    
    UIViewController *vc = [[UIViewController alloc]init];
    self.contentNaviCtrl = [[UINavigationController alloc] initWithRootViewController:vc];
    self.contentNaviCtrl.view.frame = CGRectMake(_sidebar.frame.size.width + TABLEW, IOS7?20:0, naviCtrlWidth, self.view.frame.size.height-(IOS7?70:50));
    self.contentNaviCtrl.navigationBarHidden = YES;
    
    [self.view addSubview:self.contentNaviCtrl.view];
    
    self.contentNaviCtrl.view.backgroundColor = [UIColor clearColor];
    
    _newsTableView.backgroundColor = [UIColor clearColor];
    _newsTableView.backgroundView = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    newsCount = 0;
    privilegeCount = 0;
    //保存
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:newsCount] forKey:@"newsCount"];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:privilegeCount] forKey:@"privilegeCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)navigationBarWillAppear{
    if (pushFromNews) {
        if (currentIndex == LEFTBUTTON) {
            [self getLeftButtonData];
        } else if (currentIndex == RIGHTBUTTON) {
            [self getRightButtonData];
        }
    } else {
        [self getMemberDiscountData];
    }
    
    
    self.customNavigaitionBar.rightBtn.hidden = YES;
    [self.view bringSubviewToFront:self.customNavigaitionBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Side Bar Delegate Method
-(void)sideBarIsExpanded:(NSString *)expanded {
    CGPoint naviSlideToX;
    CGPoint tableSlideTox;
    if ([expanded isEqual:@"yes"]) {
        naviSlideToX    = CGPointMake(_sidebar.frame.size.width + TABLEW, 0);
        tableSlideTox   = CGPointMake(_sidebar.frame.size.width, 0);
    } else {
        naviSlideToX    = CGPointMake(16 + TABLEW, 0);
        tableSlideTox   = CGPointMake(16, 0);
    }
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.contentNaviCtrl.view.frame = CGRectMake(naviSlideToX.x, 20, self.view.frame.size.width-naviSlideToX.x, self.contentNaviCtrl.view.frame.size.height);
                         _tableContainer.frame = CGRectMake(tableSlideTox.x, 20, TABLEW, _tableContainer.frame.size.height);
                     }
                     completion:nil];
}

-(void)sideBarButtonIsClickedWithTag:(NSNumber *)tag
{
    if (pushFromNews) {
        NSInteger tagInt =  [tag integerValue];
        switch (tagInt) {
                // 市场活动
            case 10000:
                currentIndex = LEFTBUTTON;
                [self getLeftButtonData];
                break;
            case 10001:
                currentIndex = RIGHTBUTTON;
                [self getRightButtonData];
                break;
            default:
                break;
        }
    }
}

#pragma mark - 获取市场优惠数据
-(void)getLeftButtonData{
    _titleLabel.text = @"市场活动";
    _newsTableView.alpha = 0;
    
    AFHTTPRequestOperationManager *LeftManger = [AFHTTPRequestOperationManager manager];
    LeftManger.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [LeftManger.requestSerializer setValue:HTTP_X_U forHTTPHeaderField:@"Http-X-U"];
    [LeftManger.requestSerializer setValue:HTTP_X_P forHTTPHeaderField:@"Http-X-P"];
    
    [LeftManger GET:[NSString stringWithFormat:@"%@%@/list.php?id=10", ZF_BASE_URL, ZF_BASE_PATH] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"App.net Global Stream: %@", responseObject);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ([[responseObject valueForKey:@"Code"] integerValue]==200 && currentIndex==LEFTBUTTON) {
            dataSource = [[responseObject valueForKey:@"Data"] valueForKey:@"List"];
            if ([dataSource count]>0) {
                [_newsTableView reloadData];
                _newsTableView.alpha = 1;
                [self pushDetailView:0];
            }
            [[DBManager sharedManager] storeNewsListData:dataSource Sucess:nil fail:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"NewSplitController error :%@", error);
    }];
}

#pragma mark - 获取市场活动数据
-(void)getRightButtonData{
    _titleLabel.text = @"市场活动";
    _newsTableView.alpha = 0;
    
    AFHTTPRequestOperationManager *RightManger = [AFHTTPRequestOperationManager manager];
    RightManger.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [RightManger.requestSerializer setValue:HTTP_X_U forHTTPHeaderField:@"Http-X-U"];
    [RightManger.requestSerializer setValue:HTTP_X_P forHTTPHeaderField:@"Http-X-P"];
    
    [RightManger GET:[NSString stringWithFormat:@"%@%@/view/fn/getDataByviewid/viewid/7", ZF_BASE_URL, ZF_BASE_PATH] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"App.net Global Stream: %@", responseObject);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ([[responseObject valueForKey:@"Code"] integerValue]==200 && currentIndex==RIGHTBUTTON) {
            dataSource = [[responseObject valueForKey:@"Data"] valueForKey:@"List"];
            if ([dataSource count]>0) {
                [_newsTableView reloadData];
                _newsTableView.alpha = 1;
                [self pushDetailView:0];
            }
            [[DBManager sharedManager] storeModelPrivilegeListData:dataSource Sucess:nil fail:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"NewSplitController error :%@", error);
    }];
    
}

#pragma mark - 获取会员优惠数据
-(void)getMemberDiscountData{
    _titleLabel.text = @"会员优惠";
    _newsTableView.alpha = 0;
    
    AFHTTPRequestOperationManager *RightManger = [AFHTTPRequestOperationManager manager];
    RightManger.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [RightManger.requestSerializer setValue:HTTP_X_U forHTTPHeaderField:@"Http-X-U"];
    [RightManger.requestSerializer setValue:HTTP_X_P forHTTPHeaderField:@"Http-X-P"];
    
    [RightManger GET:[NSString stringWithFormat:@"%@%@/view/fn/getDataByviewid/viewid/23", ZF_BASE_URL, ZF_BASE_PATH] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"App.net Global Stream: %@", responseObject);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ([[responseObject valueForKey:@"Code"] integerValue]==200) {
            dataSource = [[responseObject valueForKey:@"Data"] valueForKey:@"List"];
            if ([dataSource count]>0) {
                [_newsTableView reloadData];
                _newsTableView.alpha = 1;
                [self pushDetailView:0];
            }
//            [[DBManager sharedManager] storeMemberDiscountData:dataSource Sucess:nil fail:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"NewSplitController error :%@", error);
    }];
    
}

#pragma mark - Tableview datasource & delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsListCell *cell = (NewsListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSDictionary *dic = [dataSource objectAtIndex:indexPath.row];
    NSString *bannerURL = [dic objectForKey:@"PromoImageURL"];
    NSString *time = [dic objectForKey:@"UpdatedTime"];
    if (time.length>11) {
        time = [time substringToIndex:10];
    }
    NSString *name = [dic objectForKey:@"Name"];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
        cell.titleLabel.text        = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        cell.dateLabel.text         = time;
        [cell.bannerImageView setImageWithURL:[NSURL URLWithString:bannerURL] placeholderImage:[UIImage imageNamed:@"loading"]];
    } else {
        [((UIImageView *)[cell.contentView viewWithTag:200]) setImageWithURL:[NSURL URLWithString:bannerURL] placeholderImage:[UIImage imageNamed:@"loading"]];
        ((UILabel *)[cell.contentView viewWithTag:201]).text = time;
        ((UILabel *)[cell.contentView viewWithTag:202]).text = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self pushDetailView:indexPath.row];
}

-(void)pushDetailView:(NSInteger)index {
    [self.contentNaviCtrl popViewControllerAnimated:NO];
    NewsDetailController *ndCtrl = [[NewsDetailController alloc]initWithNibName:@"NewsDetailController" bundle:nil];
    NSDictionary *dic = [dataSource objectAtIndex:index];
    ndCtrl.descp = [dic valueForKey:@"Descp"];
    ndCtrl.bannerUrl = [dic objectForKey:@"PromoImageURL"];
    ndCtrl.date = [dic objectForKey:@"UpdatedTime"];
    ndCtrl.newsTitle = [dic objectForKey:@"Name"];
    ndCtrl.address = [dic objectForKey:@"Address"];
    ndCtrl.shortDesc = [dic objectForKey:@"ShortDesc"];
    ndCtrl.bannerName = [dic objectForKey:@"BannerName"];
    [self.contentNaviCtrl pushViewController:ndCtrl animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 144.0f;
    
}

@end
