//
//  NewsMainVC.h
//  ZungFuCar
//
//  Created by kc on 13-10-6.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuAbstractViewController.h"
#import "HorizontalScrollView.h"

@interface NewsMainVC :MenuAbstractViewController<HorizontalScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>{
    UIButton *leftButton;
    UIButton *rightButton;
    int currentIndex;
    UITableView *newsTableView;
    NSMutableArray *dataSource;
    
    int viewType;
    HorizontalScrollView *hScrollView;
    int selectIndexSwap;
}
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;
@property int selectIndexSwap;
+ (NSString *)getTimeToShowWithTimestamp:(NSString *)timestamp;

@end
