//
//  NewsDetailController.h
//  ZungFuCar
//
//  Created by Ed Lee on 12/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

// Category
#import "UIImageView+AFNetworking.h"
#import "NSStringUtil.h"
#import "NSObject+AddJavascript.h"
#import "ShareView.h"

@interface NewsDetailController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate> {
    BOOL willLaunchSafari;
    ShareView *shareView;
}

@property (nonatomic) NSString *newsTitle;
@property (nonatomic) NSString *bannerUrl;
@property (nonatomic) NSString *date;
@property (nonatomic) NSString *descp;
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *bannerName;

@property (strong, nonatomic) IBOutlet UIImageView *bannerView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *lookButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIWebView *contentWebView;
@property (nonatomic) NSString *shortDesc;

- (IBAction)openLink:(id)sender;
- (IBAction)shareByActivity:(id)sender;

@end
