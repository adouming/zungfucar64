//
//  CurvyTextView.h
//  CurvyText
//
//  Created by Rob Napier on 8/28/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//




#import <UIKit/UIKit.h>


@class CurvyTextView;
@protocol CurvyTextViewDelegate <NSObject>

-(void)tagClick:(CurvyTextView*)aView;

@end



@interface CurvyTextView : UIView
{
    BOOL _isFirst;
}

@property(nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSAttributedString *attributedString;
@property (nonatomic,assign)id<CurvyTextViewDelegate> delegate;

-(void)buildAttributedString:(UIColor*)aColor;

@end
