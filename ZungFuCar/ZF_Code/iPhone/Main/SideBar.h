//
//  SideBar.h
//  ZungFuCar
//
//  Created by Ed Lee on 11/29/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SideBarDelegate <NSObject>

-(void)sideBarIsExpanded:(NSString*)expanded;
-(void)sideBarButtonIsClickedWithTag:(NSNumber*)tag;

@end

@interface SideBar : UIView

-(CGSize)addButtonsForSidebar:(NSMutableArray*)titleArray fromTopIndex:(NSInteger)topIndex andSubIndex:(NSInteger)subIndex;

@property(nonatomic,assign)id<SideBarDelegate> delegate;

@property(strong,nonatomic) UIView *allBtnContainer;
@property(nonatomic,strong) NSMutableArray *btnArray;
@property NSInteger topIndex;
@property NSInteger subIndex;

-(void)addFourthLevelFromData:(NSArray*)array atSideBtnIndex:(NSInteger)sideIndex;

@property NSInteger initialHighlight;

@end
