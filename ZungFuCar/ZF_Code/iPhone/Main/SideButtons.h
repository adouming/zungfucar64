//
//  SideButtons.h
//  ZungFuCar
//
//  Created by Ed Lee on 11/28/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SideButtonDelegate <NSObject>

-(void)sideBtnClickedWithIndex:(NSNumber*)menuIndex;

@end

@interface SideButtons : UIView

-(CGSize)addButtonsForSubmenu:(NSMutableArray*)titleArray fromTopIndex:(NSInteger)topIndex;

@property(nonatomic,assign)id<SideButtonDelegate> delegate;

@end
