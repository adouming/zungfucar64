//
//  bottomBarView.m
//  ZungFuCar
//
//  Created by Ed Lee on 11/28/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "BottomBarView_ipad.h"

@implementation BottomBarView_ipad

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *xib = [[NSBundle mainBundle] loadNibNamed:@"BottomBarView_ipad" owner:self options:nil];
        BottomBarView_ipad * myView = [xib objectAtIndex:0];
        [self addSubview:myView];
    }
    return self;
}

-(void)buttonClicked:(NSInteger)tag {
    
    if ([_delegate respondsToSelector:@selector(buttonClickedWithTag:)]) {
        [_delegate performSelector:@selector(buttonClickedWithTag:) withObject:[NSNumber numberWithInteger:tag]];
    }
}

- (IBAction)barButtonClicked:(id)sender {
    NSInteger tagInt = ((UIButton *)sender).tag;
    [self buttonClicked:tagInt];
}

//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
//    [self adjustViewsForOrientation:self.interfaceOrientation];
//}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation)orientation {
    if (orientation == UIInterfaceOrientationLandscapeLeft) {
        NSLog(@"Subview Landscape Left");
        self.transform = CGAffineTransformMakeRotation(-3.14/2);
        self.bounds = CGRectMake(0, 0, 1024, 50);
    }
    else if (orientation == UIInterfaceOrientationLandscapeRight) {
        NSLog(@"Subview Portrait Right");
        self.bounds = CGRectMake(0, 0, 1024, 50);
        self.transform = CGAffineTransformMakeRotation(3.14/2);
//        [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_bottomBar];
    }
}
@end
