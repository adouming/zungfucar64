//
//  MainVC.m
//  ZungFuCar
//
//  Created by lxm on 13-9-29.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "MainVC_Ipad.h"
#import "BenChiVC.h"

#define DEGREES_TO_RADIANS(x) (M_PI * x / 180.0)

extern int newsCount;
extern int privilegeCount;

@interface MainVC_Ipad ()

@end

@implementation MainVC_Ipad


-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleBlackOpaque;
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    NSString *lastTopIdxStr = [self readStringFromFile:@"clickedTopIndex.txt"];
    if (![lastTopIdxStr isEqualToString:@""]) {
        NSInteger lastTopIndex = [lastTopIdxStr integerValue];
        [self switchSideMenuOfIndex:lastTopIndex];
    }
    
    //Bottom Bar
    if (!_bottomBar) {
        _bottomBar = [[BottomBarView alloc]initWithFrame:CGRectZero];
        [[UIApplication sharedApplication].keyWindow addSubview:_bottomBar];
        
        _bottomBar.frame = CGRectMake(-487, 486, 1024, 50);
        _bottomBar.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
        _bottomBar.delegate = self;
    }
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_bottomBar];
}


-(void)setNewsImageHidden:(NSMutableArray *)aHiddens
{
    if (aHiddens.count>6) {
        return;
    }
    
    for (int i=0; i<aHiddens.count; i++) {
        //任孚管家，最新资讯，任孚服务，关于任孚，网上展厅，任孚会
        UIView *aView = [self.view viewWithTag:300+i];
        if (aView) {
            if ([[aHiddens objectAtIndex:i] integerValue] == 1) {
                aView.hidden = YES;
            }else{
                aView.hidden = NO;
            }
        }
    }
}

-(void)viewDidAppear:(BOOL)animated {
    NSLog(@"viewWillAppear");
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"IntroIsWatched"] == NO){
//        NSLog(@"First Launch!");
//        IntroVC *introVC = [[IntroVC alloc]initWithNibName:@"IntroVC" bundle:nil];
//        [self presentViewController:introVC animated:NO completion:nil];
//        [[UIApplication sharedApplication].keyWindow bringSubviewToFront:introVC.view];
//    }
    
    //hide news
    NSMutableArray *aArray = [NSMutableArray arrayWithObjects:@"1",@"1",@"0",@"1",@"1",@"1", nil];
    if ((newsCount == 0) && (privilegeCount == 0)) {
        [aArray replaceObjectAtIndex:2 withObject:@"1"];
    } else {
        [aArray replaceObjectAtIndex:2 withObject:@"0"];
    }
    
    [self setNewsImageHidden:aArray];
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    [self initSinaWeiboEngine];
    
    //提前获取展厅数据
    [self getCarData:1];
    [self getCarData:2];
    [[DataDBModel shareInstance] createDataModel];
}

- (IBAction)zuixinzixunAction:(id)sender { // Top Menu item 0
    [self switchSideMenuOfIndex:0];
}

- (IBAction)renfufuwuAction:(id)sender { // Top Menu item 1
    [self switchSideMenuOfIndex:1];
}

- (IBAction)guanyurenfuAction:(id)sender { // Top Menu item 2
    [self switchSideMenuOfIndex:2];
}

- (IBAction)wanshangzhantingAction:(id)sender { // Top Menu item 3
    [self switchSideMenuOfIndex:3];
}

- (IBAction)renfuhuiAction:(id)sender { // Top Menu item 4
    [self switchSideMenuOfIndex:4];
}

- (IBAction)renfuguanjianAction:(id)sender { // Top Menu item 5
    [self switchSideMenuOfIndex:5];
}

// 由左至右收回二级菜单并弹出另一个二级菜单
-(void)switchSideMenuOfIndex:(NSInteger)topIndex
{
    if (_sideBtns != nil && topIndex != _sideBtns.tag) {
        CGRect slideRight = CGRectMake(self.view.frame.size.width, (self.view.frame.size.height - _sideBtns.frame.size.height)/2, _sideBtns.frame.size.width, _sideBtns.frame.size.height);
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             _sideBtns.frame = slideRight;
                         }
                         completion:^(BOOL finished){
                             [_sideBtns removeFromSuperview];
                             _sideBtns = nil;
                             [self popOutSideMenuOfIndex:topIndex];
                         }
         ];
    } else if (topIndex != _sideBtns.tag || _sideBtns == nil) {
        [self popOutSideMenuOfIndex:topIndex];
    }
}

// 由右至左弹出二级菜单
-(void)popOutSideMenuOfIndex:(NSInteger)topIndex
{
    NSMutableArray *titleArray = [self subMenuTitlesOfIndex:topIndex];
    _sideBtns = [[SideButtons alloc] init];
    _sideBtns.tag = topIndex;
    _sideBtns.delegate = self;
    CGSize menuSize = [_sideBtns addButtonsForSubmenu:titleArray fromTopIndex:topIndex];
    [self.view addSubview:_sideBtns];
    _sideBtns.frame = CGRectMake(self.view.frame.size.width, (self.view.frame.size.height - menuSize.height)/2, menuSize.width, menuSize.height);
    CGRect slideLeft = CGRectMake(self.view.frame.size.width-menuSize.width, (self.view.frame.size.height - menuSize.height)/2, menuSize.width, menuSize.height);
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{_sideBtns.frame = slideLeft;} completion:nil];
}

#pragma mark - Side Buttons Delegate

-(void)sideBtnClickedWithIndex:(NSNumber *)menuIndex
{
    // 获取点击的二级菜单按钮的tag，格式为"x000x", 由SideButtons规定
    NSArray *topAndSub = [[NSString stringWithFormat:@"%d", [menuIndex integerValue]] componentsSeparatedByString:@"000"];
    NSInteger clickedTopMenuIndex = [[topAndSub objectAtIndex:0] integerValue] - 1;
    NSInteger clickedSubMenuIndex = [[topAndSub objectAtIndex:1] integerValue];
    
    // 根据一级index和二级index获取三级菜单题目]
    NSMutableArray *thirdLevelMenu = [[NSMutableArray alloc]init];
    if (clickedTopMenuIndex != 0) {
        thirdLevelMenu = [self thirdLevelMenuOfTopIndex:clickedTopMenuIndex andSubIndex:clickedSubMenuIndex];
    } else {
        thirdLevelMenu = nil;
    }
    
    // 保存topindex
    [self writeStringToFile:[NSString stringWithFormat:@"%d",clickedTopMenuIndex] toFile:@"clickedTopIndex.txt"];
    
    // 获取Action字段，由Submenus.plist生成，可修改
    //NSMutableArray *thisMenuActions = [self subMenuActionsOfIndex:clickedTopMenuIndex];
    //NSString *thisMenuStoryBoardName = [thisMenuActions objectAtIndex:clickedSubMenuIndex];
    //NSString *thisMenuStoryBoardID;
    
    AboutZFController *aboutZFCtrl = [[AboutZFController alloc]initWithNibName:@"AboutZFController" bundle:nil];
    aboutZFCtrl.sidebarMenuTitles = thirdLevelMenu;
    aboutZFCtrl.topIndex = clickedTopMenuIndex;
    aboutZFCtrl.subIndex = clickedSubMenuIndex;
    
    if (clickedTopMenuIndex == 2) { // 关于仁孚
        if (clickedSubMenuIndex != 4) { // 公司简介, 发展历程, 加入仁孚, 联系我们
            aboutZFCtrl.bgImageName = @"aboutzf_bg.png";
            [self.navigationController pushViewController:aboutZFCtrl animated:YES];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"授权微博"
                                                            message:@"你确定要授权于微博？"
                                                           delegate:self
                                                  cancelButtonTitle:@"取消"
                                                  otherButtonTitles:@"确定",nil];
            alert.tag = 101;
            [alert show];
        }
        
    } else if (clickedTopMenuIndex == 4) { //仁孚会
        // 关于仁孚会，会员手册, 注册登录
        if (clickedSubMenuIndex == 0) {
            NewsSplitController *newsSplitVC = [[NewsSplitController alloc]initWithNibName:@"NewsSplitController" bundle:nil];
            newsSplitVC.sidebarMenuTitles = [NSMutableArray arrayWithObjects:@"会员优惠", nil];
            newsSplitVC.currentIndex = clickedSubMenuIndex;
            newsSplitVC.pushFromNews = NO;
            [self.navigationController pushViewController:newsSplitVC animated:YES];
        } else if (clickedSubMenuIndex == 3) {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogined"]) {
                UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Club_iPad" bundle:nil];
                [self.navigationController pushViewController:[storyBoard instantiateViewControllerWithIdentifier:@"MemberCenterVCID"]
                                                     animated:YES];
            } else {
                UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Club_iPad" bundle:nil];
                [self.navigationController pushViewController:[storyBoard instantiateViewControllerWithIdentifier:@"LoginVCID"] animated:YES];
            }
        } else {
            aboutZFCtrl.bgImageName = @"zungfuclub_bg.png";
            [self.navigationController pushViewController:aboutZFCtrl animated:YES];
        }
        
    } else if (clickedTopMenuIndex == 1) { // 仁孚服务
        aboutZFCtrl.bgImageName = @"zfservice_bg.png";
        [self.navigationController pushViewController:aboutZFCtrl animated:YES];
        
    } else if (clickedTopMenuIndex == 0) { // 最新资讯
        NewsSplitController *newsSplitVC = [[NewsSplitController alloc]initWithNibName:@"NewsSplitController" bundle:nil];
        newsSplitVC.sidebarMenuTitles = [NSMutableArray arrayWithObjects:@"市场活动",@"车型优惠", nil];
        newsSplitVC.currentIndex = clickedSubMenuIndex;
        newsSplitVC.pushFromNews = YES;
        [self.navigationController pushViewController:newsSplitVC animated:YES];
    }else if (clickedTopMenuIndex==3){    //网上展厅
        BenChiVC *benchiVC = [[BenChiVC alloc]initWithType:clickedSubMenuIndex+1];
        [self.navigationController pushViewController:benchiVC animated:YES];
    }else {
        if (clickedTopMenuIndex == 5) {
            switch (clickedSubMenuIndex) {
                case 0:{ // 服务网络
                    StoreViewController *stVC = [[StoreViewController alloc] init];
                    [self.navigationController pushViewController:stVC animated:YES];
                }
                    break;
                case 1:  // 保养维修预约
                case 2:  // 试驾预约
                case 3:  // 24小时道路救援
                {
                    aboutZFCtrl.bgImageName = @"aboutzf_bg.png";
                    [self.navigationController pushViewController:aboutZFCtrl animated:YES];
                    break;
                }
                    
                default:
                    break;
            }
        }
    }
    
    if (_sideBtns != nil) {
        [_sideBtns removeFromSuperview];
        _sideBtns = nil;
    }
}

#pragma mark - Bottom Bar View Delegate

-(void)buttonClickedWithTag:(NSNumber*)tag {
    NSLog(@"Bottom button clicked with tag: %d", [tag integerValue]);
    [self writeStringToFile:@"" toFile:@"clickedTopIndex.txt"];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    switch ([tag integerValue]) {
        case 0:
        {
            [self sideBtnClickedWithIndex:@60001];
        }
            break;
        case 1:
        {
            [self sideBtnClickedWithIndex:@60002];
        }
            break;
        case 2:
        {
            NewsSplitController *newsSplitVC = [[NewsSplitController alloc]initWithNibName:@"NewsSplitController" bundle:nil];
            newsSplitVC.sidebarMenuTitles = [NSMutableArray arrayWithObjects:@"市场活动",@"车型优惠", nil];
            newsSplitVC.currentIndex = 0;
            [self.navigationController pushViewController:newsSplitVC animated:NO];
        }
            break;
        case 3:
        {
            NewsSplitController *newsSplitVC = [[NewsSplitController alloc]initWithNibName:@"NewsSplitController" bundle:nil];
            newsSplitVC.sidebarMenuTitles = [NSMutableArray arrayWithObjects:@"市场活动",@"车型优惠", nil];
            newsSplitVC.currentIndex = 1;
            [self.navigationController pushViewController:newsSplitVC animated:NO];
        }
            break;
        case 4:
        {
            AboutZFController *aboutZFCtrl = [[AboutZFController alloc]initWithNibName:@"AboutZFController" bundle:nil];
            aboutZFCtrl.topIndex = 2;
            aboutZFCtrl.subIndex = 3;
            aboutZFCtrl.bgImageName = @"aboutzf_bg.png";
            [self.navigationController pushViewController:aboutZFCtrl animated:NO];
        }
            break;
            
        default:
            break;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGRect slideRight = CGRectMake(self.view.frame.size.width, (self.view.frame.size.height - _sideBtns.frame.size.height)/2, _sideBtns.frame.size.width, _sideBtns.frame.size.height);
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _sideBtns.frame = slideRight;
                     }
                     completion:^(BOOL finished){
                         [_sideBtns removeFromSuperview];
                         _sideBtns = nil;
                     }
     ];
    
}

#pragma mark - Weibo Engine delegate

- (void)initSinaWeiboEngine
{
    _shareEngine = [[WBEngine alloc] initWithAppKey:kWBSDKDemoAppKey appSecret:kWBSDKDemoAppSecret];
    [_shareEngine setRootViewController: self];
    [_shareEngine setDelegate:self];
    [_shareEngine setRedirectURI:@"https://www.zfchina.com//"];
    [_shareEngine setIsUserExclusive:NO];
}

#pragma mark Authorize

- (void)engineAlreadyLoggedIn:(WBEngine *)engine
{
    if ([engine isUserExclusive])
    {
        UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
                                                           message:@"请先登出！"
                                                          delegate:nil
                                                 cancelButtonTitle:@"确定"
                                                 otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)engineDidLogIn:(WBEngine *)engine
{
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"你确定要关注？"
													  delegate:self
											 cancelButtonTitle:@"取消"
											 otherButtonTitles:@"确定",nil];
    [alertView setTag:102];
	[alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101)
    {
        if(buttonIndex==1)
        {
            [_shareEngine logIn];
        }
    }
    else if(alertView.tag == 102)
    {
        if(buttonIndex==1)
        {
            /*关注*/
            [_shareEngine loadRequestWithMethodName:@"friendships/create.json"
                                         httpMethod:@"POST"
                                             params:[NSDictionary dictionaryWithObjectsAndKeys:_shareEngine.accessToken,@"access_token",
                                                     kSinaSZAirUID,@"screen_name",
                                                     nil]
                                       postDataType:kWBRequestPostDataTypeNormal
                                   httpHeaderFields:nil];
        }
    }
}

- (void)engine:(WBEngine *)engine didFailToLogInWithError:(NSError *)error
{
    NSLog(@"didFailToLogInWithError: %@", error);
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"登录失败！"
													  delegate:nil
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
	[alertView show];
}

- (void)engineDidLogOut:(WBEngine *)engine
{
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"登出成功！"
													  delegate:self
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
    [alertView setTag:11];
	[alertView show];
}

- (void)engineNotAuthorized:(WBEngine *)engine
{
    
}

- (void)engineAuthorizeExpired:(WBEngine *)engine
{
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"请重新登录！"
													  delegate:nil
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
	[alertView show];
}

- (void)engine:(WBEngine *)engine requestDidSucceedWithResult:(id)result
{
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"关注成功！"
													  delegate:nil
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
	[alertView show];
}

- (void)engine:(WBEngine *)engine requestDidFailWithError:(NSError *)error
{
    if ([error.domain isEqualToString:@"WeiBoSDKErrorDomain"]
        &&[[error.userInfo objectForKey:@"error"] isEqualToString:@"already followed"]) {
        UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
                                                           message:@"您已关注！"
                                                          delegate:nil
                                                 cancelButtonTitle:@"确定"
                                                 otherButtonTitles:nil];
        [alertView show];
    }
    
}

////请求展厅数据
//-(void)getCarData:(int)type{
//    
//    AFHTTPRequestOperationManager *manger = [AFHTTPRequestOperationManager manager];
//    manger.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//    [manger.requestSerializer setValue:HTTP_X_U forHTTPHeaderField:@"Http-X-U"];
//    [manger.requestSerializer setValue:HTTP_X_P forHTTPHeaderField:@"Http-X-P"];
//    
//
//    
//    [manger GET:[NSString stringWithFormat:@"%@%@%@%d", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_GETCARLIST, type] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"App.net Global Stream: %@", responseObject);
//        if ([[responseObject valueForKey:@"Code"] integerValue]==200) {
//            NSMutableArray *dataSource = [[responseObject valueForKey:@"Data"] valueForKey:@"List"];
//            /////////////////////////////////
//            if (type==1) {
//                [[DBManager sharedManager] storeCarListData:dataSource Sucess:nil fail:nil key:@"Banz_car_list"];
//            }else if(type==2){
//                [[DBManager sharedManager] storeCarListData:dataSource Sucess:nil fail:nil key:@"AMG_car_list"];
//            }
//            /////////////////////////////////
//            for (int i=0; i<dataSource.count; i++) {
//                if ([[[dataSource objectAtIndex:i] objectForKey:@"CarType"] isKindOfClass:[NSMutableArray class]]) {
//                    
//                    NSMutableArray *carTypeList = [[dataSource objectAtIndex:i] objectForKey:@"CarType"];
//                    for (int j=0; j<carTypeList.count; j++) {
//                        if ([[carTypeList objectAtIndex:j] objectForKey:@"Details"]) {
//                            
//                            NSMutableArray *detailsList = [[carTypeList objectAtIndex:j] objectForKey:@"Details"];
//                            for (int k=0; k<detailsList.count; k++) {
//                                NSDictionary *carDic = [detailsList objectAtIndex:k];
//                                NSString *carID = [carDic objectForKey:@"ID"];
//                                /**************************/
//                                NSMutableArray *LoanList = [carDic objectForKey:@"Loanparameters"];
//                                NSMutableArray *LoanTempList = [NSMutableArray array];
//                                for (int m=0; m<LoanList.count; m++) {
//                                    ZFSchemeData *temp = [ZFSchemeData dataWithJSON:[LoanList objectAtIndex:m]];
//                                    [LoanTempList addObject:temp];
//                                }
//                                [[DBManager sharedManager] storeSchemeData:LoanTempList key:carID scheme:@"loan" Sucess:^(NSMutableArray *storeData) {
//                                    
//                                } fail:^(NSError *error) {
//                                    
//                                }];
//                                /**************************/
//                                NSMutableArray *LeaseList = [carDic objectForKey:@"Leaseparameters"];
//                                NSMutableArray *LeaseTempList = [NSMutableArray array];
//                                for (int m=0; m<LeaseList.count; m++) {
//                                    ZFSchemeData *temp = [ZFSchemeData dataWithJSON:[LeaseList objectAtIndex:m]];
//                                    [LeaseTempList addObject:temp];
//                                }
//                                [[DBManager sharedManager] storeSchemeData:LeaseTempList key:carID scheme:@"lease" Sucess:^(NSMutableArray *storeData) {
//                                    
//                                } fail:^(NSError *error) {
//                                    
//                                }];
//                                /**************************/
//                            }
//                            
//                        }
//                    }
//                }
//            }
//            
//            //            NSMutableArray *schemeTempMuArr = [NSMutableArray array];
//            //            for (int i=0; i<dataSource.count; i++) {
//            //                ZFSchemeData *temp = [ZFSchemeData dataWithJSON:[data objectAtIndex:i]];
//            //                [schemeTempMuArr addObject:temp];
//            //            }
//            //            [[DBManager sharedManager] storeSchemeData:schemeTempMuArr key:ID scheme:scheme Sucess:^(NSMutableArray *storeData) {
//            //
//            //            } fail:^(NSError *error) {
//            //
//            //            }];
//            /////////////////////////////////
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Calculator error :%@", error);
//    }];
//    
//}


//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
//    [_bottomBar adjustViewsForOrientation:self.interfaceOrientation];
//}

@end
