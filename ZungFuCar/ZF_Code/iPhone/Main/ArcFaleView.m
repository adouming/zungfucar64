//
//  ArcFaleView.m
//  Hello
//
//  Created by mac02 on 13-9-30.
//  Copyright (c) 2013年 liming. All rights reserved.
//

#import "ArcFaleView.h"
#import <math.h>


#import "CurvyTextView.h"


#import "SMRotaryWheel.h"



#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

// undefine for right handed operation
#define LEFTHANDED 1



@implementation ArcFaleView 

@synthesize controlView;
@synthesize buttons;



-(id)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        
        //UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
        
        
        
        
        SMRotaryWheel *wheel = [[SMRotaryWheel alloc] initWithFrame:CGRectMake(0, 0, 600, 600)
                                                        andDelegate:self
                                                       withSections:10];
        
        wheel.backgroundColor = [UIColor clearColor];
        wheel.myDelegate =self;
        wheel.userInteractionEnabled = YES;
        wheel.center = CGPointMake(160, 230);
        [self addSubview:wheel];
        
        //[[UIApplication sharedApplication].keyWindow addSubview:wheel];
        
        
        //[self addSubview:aView];
        
    }
    
    return self;
}

- (void) wheelDidChangeValue:(NSString *)newValue {
    
    //self.valueLabel.text = newValue;
    
}

-(void)tagClick:(NSNumber*)aNumber{
    
    if ([_delegate respondsToSelector:@selector(tagClick:)]) {
        [_delegate performSelector:@selector(tagClick:) withObject:aNumber];
    }
}




-(void)setCollections{
    
    
    
}


-(void)logRect:(CGRect)r msg:(NSString*)str {
    //NSLog(@"%@: %4.1f,%4.1f sz: %4.1f,%4.1f",str,r.origin.x,r.origin.y,r.size.width,r.size.height);
}

-(void)buttonPressed:(id)btn {
    UIButton *b = (UIButton*)btn;
    //NSLog(@"buttonPressed: %d",b.tag);
}

-(UIButton *)makeButton:(NSString *)imageName tag:(NSInteger)tag {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor clearColor];
    //UIImage *im = [UIImage imageNamed:imageName];
    [btn setTitle:imageName forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont systemFontOfSize:11]];
    //[btn setImage:im forState:UIControlStateNormal];
    [btn setShowsTouchWhenHighlighted:TRUE];
    [btn setTag:tag];
    CGRect r = CGRectMake(0,0, 70, 16);
//    CGRect r = CGRectMake(0,0, im.size.width, im.size.height);
    btn.frame=r;
    [btn addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return btn;
}


-(CurvyTextView*)makeView:(NSString *)imageName tag:(NSInteger)tag{
    
    CurvyTextView * aView = [[CurvyTextView alloc] initWithFrame:CGRectMake(0, 0, 70, 16)];
    
    aView.text = imageName;
    
    return aView;
    
}





@end



