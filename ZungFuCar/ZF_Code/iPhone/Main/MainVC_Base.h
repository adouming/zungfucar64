//
//  MainVC_Base.h
//  ZungFuCar
//
//  Created by adouming on 15/10/12.
//  Copyright © 2015年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArcView.h"
#import "IntroVC.h"
#import "MainVC_Base.h"
@interface MainVC_Base : UIViewController
-(void)getCarData:(int)type;
@end
