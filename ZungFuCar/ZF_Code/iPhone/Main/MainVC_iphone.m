//
//  MainVC.m
//  ZungFuCar
//
//  Created by lxm on 13-9-29.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "MainVC_iphone.h"
#import "NewsMainVC.h"
#import "SendhandVC.h"
#import "ReserveFormViewController.h"
#import "ReserveMainViewController.h"
#import "BootManager.h"
extern int newsCount;
extern int privilegeCount;

@interface MainVC_iphone ()

@end

@implementation MainVC_iphone

//-(id)initWithCoder:(NSCoder *)aDecoder{
//    if (self=[super initWithCoder:aDecoder]) {
//    }
//    return self;
//}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
    
    [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleBlackOpaque;
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:[ArcView sharedArcView]];
    
    
    //hide news
    NSMutableArray *aArray = [NSMutableArray arrayWithObjects:@"1",@"1",@"1",@"1",@"1",@"1", nil];
    if ((newsCount == 0) && (privilegeCount == 0)) {
        [aArray replaceObjectAtIndex:1 withObject:@"1"];
    } else {
        [aArray replaceObjectAtIndex:1 withObject:@"0"];
    }
    [self setNewsImageHidden:aArray];
}

-(void)viewDidAppear:(BOOL)animated {
    NSLog(@"viewWillAppear");
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"IntroIsWatched"] == NO){
//        NSLog(@"First Launch!");
//        IntroVC *introVC = [[IntroVC alloc]initWithNibName:@"IntroVC" bundle:nil];
//        [self presentViewController:introVC animated:NO completion:nil];
//        [[UIApplication sharedApplication].keyWindow bringSubviewToFront:introVC.view];
//    }
}

-(void)setNewsImageHidden:(NSMutableArray *)aHiddens{
    
    if (aHiddens.count>6) {
        return;
    }
    
    for (int i=0; i<aHiddens.count; i++) {
        
        //任孚管家，最新资讯，任孚服务，关于任孚，网上展厅，任孚会
        UIView *aView = [self.view viewWithTag:1000+i];
        if (aView) {
            if ([[aHiddens objectAtIndex:i] integerValue] == 1) {
                aView.hidden = YES;
            }else{
                aView.hidden = NO;
            }
        }
        
    }
}


- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    NSLog(@"mainvc viewDidLoad");
 
    if(IOS7)
    {
        UIView *statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 20)];
        statusView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:statusView];
    }
    
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeAction)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.delegate = self;
    [[UIApplication sharedApplication].keyWindow addGestureRecognizer:tapGestureRecognizer];
    [ArcView sharedArcView].delegate = self;
    [self.view addSubview:[ArcView sharedArcView]];
    
    //提前获取展厅数据
    [self getCarData:1];
    [self getCarData:2];
    [[DataDBModel shareInstance] createDataModel];
}

//取消特定view的点击监听事件
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    //    UIView *view = touch.view;
    //    if ([NSStringFromClass([view class]) isEqualToString:@"CurvyTextView"] || [NSStringFromClass([view class]) isEqualToString:@"UITableViewCellContentView"] || view.tag==1000 || view.tag==1001) {
    //
    //        return NO;
    //    }
    CGPoint point = [touch locationInView:[UIApplication sharedApplication].keyWindow];
    NSLog(@"point.x-->%f point.y-->%f", point.x, point.y);
    if (point.y<380||(point.x<50 && point.y<412)) {
        [self closeAction];
    }
    return NO;
}


-(void)closeAction{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"closeArcFaleView" object:nil];
}

- (IBAction)renfuguanjianAction:(id)sender {
    
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Steward" bundle:nil];
    [self.navigationController pushViewController:[stryBoard instantiateViewControllerWithIdentifier:@"StewardMainVCID"] animated:YES];
    
}

- (IBAction)renfuhuiAction:(id)sender {
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Club" bundle:nil];
    [self.navigationController pushViewController:[stryBoard instantiateViewControllerWithIdentifier:@"ClubMainVCID"] animated:YES];
}

- (IBAction)zuixinzixunAction:(id)sender {
    
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"News" bundle:nil];
    [self.navigationController pushViewController:[stryBoard instantiateViewControllerWithIdentifier:@"NewsMainVCID"] animated:YES];
}

- (IBAction)renfufuwuAction:(id)sender {
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"ZFService" bundle:nil];
    [self.navigationController pushViewController:[stryBoard instantiateViewControllerWithIdentifier:@"ZFServiceVCID"] animated:YES];
}

- (IBAction)guanyurenfuAction:(id)sender {
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"AboutZF" bundle:nil];
    [self.navigationController pushViewController:[stryBoard instantiateViewControllerWithIdentifier:@"AboutZFVCID"] animated:YES];
}

- (IBAction)wanshangzhantingAction:(id)sender {
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"NetShow" bundle:nil];
    [self.navigationController pushViewController:[stryBoard instantiateViewControllerWithIdentifier:@"NetShowVCID"] animated:YES];
}



-(void)tagClick:(NSNumber*)aNumber{
    
    NSLog(@"main vc tag=%d",[aNumber integerValue]);
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    switch ([aNumber integerValue]) {

        case 1:
        {
            UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Steward" bundle:nil];
            ReserveFormViewController *reserveVC = [stryBoard instantiateViewControllerWithIdentifier:@"TryReserveFormID"];
            reserveVC.reserveType = ReserveTypeTest;
            [self.navigationController pushViewController:reserveVC animated:YES];

        }
            break;
        case 2:
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Steward" bundle:nil];
            ReserveMainViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"serverReserveMenuID"];
            controler.reserveType = ReserveTypeService;
            [self.navigationController pushViewController:controler animated:YES];
        }
            break;

        case 3:
        {
            [self.navigationController popToRootViewControllerAnimated:NO];
            UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Human-resource" bundle:nil];
            
            SendhandVC *aVC = (SendhandVC*)[stryBoard instantiateViewControllerWithIdentifier:@"HRVC"];
            //aVC.selectIndexSwap = 0;
            [self.navigationController pushViewController:aVC animated:YES];
            
//            UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"AboutZF" bundle:nil];
//            [self.navigationController pushViewController:[stryBoard instantiateViewControllerWithIdentifier:@"ContactUSVCID"] animated:YES];
        }
            break;
            
        default:
            break;
    }
    
}




@end
