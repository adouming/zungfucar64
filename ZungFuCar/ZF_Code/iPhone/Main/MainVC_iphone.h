//
//  MainVC.h
//  ZungFuCar
//
//  Created by lxm on 13-9-29.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArcView.h"
#import "IntroVC.h"
#import "MainVC_Base.h"
@interface MainVC_iphone : MainVC_Base <ArcFaleViewDelegate, UIGestureRecognizerDelegate>
{
    
    
}

- (IBAction)renfuguanjianAction:(id)sender;
- (IBAction)renfuhuiAction:(id)sender;
- (IBAction)zuixinzixunAction:(id)sender;
- (IBAction)renfufuwuAction:(id)sender;
- (IBAction)guanyurenfuAction:(id)sender;
- (IBAction)wanshangzhantingAction:(id)sender;

@end
