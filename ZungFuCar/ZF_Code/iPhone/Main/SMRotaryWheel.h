//
//  SMRotaryWheel.h
//  RotaryWheelProject
//
//  Created by cesarerocchi on 2/10/12.
//  Copyright (c) 2012 studiomagnolia.com. All rights reserved.


#import <UIKit/UIKit.h>
#import "SMRotaryProtocol.h"
#import "CurvyTextView.h"



@class SMRotaryWheel;
@protocol SMRotaryWheelDelegate <NSObject>

-(void)tagClick:(NSNumber*)aNumber;

@end


@interface SMRotaryWheel : UIControl<CurvyTextViewDelegate>{
    int clickIndex;
}

@property (weak) id <SMRotaryProtocol> delegate;
@property (nonatomic, strong) UIView *container;
@property int numberOfSections;
@property CGAffineTransform startTransform;
@property (nonatomic, strong) NSMutableArray *cloves;
@property int currentValue;
@property float allAngle;
@property (nonatomic, assign)id<SMRotaryWheelDelegate> myDelegate;

- (id) initWithFrame:(CGRect)frame andDelegate:(id)del withSections:(int)sectionsNumber;


@end
