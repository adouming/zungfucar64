//
//  ArcView.h
//  HelloWorld
//
//  Created by mac02 on 13-9-30.
//  Copyright (c) 2013年 liming. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ArcFaleView.h"


@protocol ArcViewDelegate <NSObject>

-(void)tagClick:(NSNumber*)aNumber;

@end



@interface ArcView : UIView<ArcFaleViewDelegate>{
    
}

@property (nonatomic,assign)id<ArcFaleViewDelegate> delegate;
@property (strong, nonatomic) UIImageView *bg1;
@property (strong, nonatomic) UIImageView *bg2;
@property (strong, nonatomic) ArcFaleView *faleView;
@property (strong, nonatomic) UIButton *tapBtn;
+ (ArcView *)sharedArcView;

@end
