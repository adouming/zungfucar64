//
//  ArcFaleView.h
//  Hello
//
//  Created by mac02 on 13-9-30.
//  Copyright (c) 2013年 liming. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JCArcButtonWheelView.h"

#import "SMRotaryProtocol.h"
#import "SMRotaryWheel.h"

@class ArcFaleView;
@protocol ArcFaleViewDelegate <NSObject>

-(void)tagClick:(NSNumber*)aNumber;

@end



@interface ArcFaleView : UIView<SMRotaryProtocol,SMRotaryWheelDelegate>// <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate>

@property (nonatomic,retain) JCArcButtonWheelView *controlView;
@property (nonatomic,retain) NSMutableArray *buttons;

@property (nonatomic,assign) id<ArcFaleViewDelegate> delegate;
//@property (nonatomic, retain) UICollectionView * collectionView;
//@property (nonatomic, retain) NSMutableArray * imagesArray;
//@property (nonatomic, retain) RVCollectionViewLayout * collectionViewLayout;


-(void)setCollections;

@end
