//
//  SideButtons.m
//  ZungFuCar
//
//  Created by Ed Lee on 11/28/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "SideButtons.h"
extern int newsCount;
extern int privilegeCount;
@implementation SideButtons

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

-(CGSize)addButtonsForSubmenu:(NSMutableArray*)titleArray fromTopIndex:(NSInteger)topIndex
{
    UIImage *firstItemImgNormal     = [UIImage imageNamed:@"menu_normalbtn1"];
    UIImage *firstItemImgHighLight  = [UIImage imageNamed:@"menu_goldenbtn1"];
    UIImage *otherItemImgNormal     = [UIImage imageNamed:@"menu_normalbtn2"];
    UIImage *otherItemImgHighLight  = [UIImage imageNamed:@"menu_goldenbtn2"];
    CGFloat buttonH = firstItemImgNormal.size.height;
    CGFloat buttonW = firstItemImgNormal.size.width;
    
    for (int i=0; i<titleArray.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGFloat buttonY = buttonH * i;
        
        //显示新条目数量
        UIButton *countBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [countBtn setBackgroundImage:[UIImage imageNamed:@"slider1"] forState:UIControlStateNormal];
        countBtn.frame = CGRectMake(130, 20, 17, 17);
        countBtn.titleEdgeInsets = UIEdgeInsetsMake(1, 0, 0, -2);
        countBtn.hidden = YES;
        [button addSubview:countBtn];
        
        if (i==0) {
            [button setBackgroundImage:firstItemImgNormal       forState:UIControlStateNormal];
            [button setBackgroundImage:firstItemImgHighLight    forState:UIControlStateHighlighted];
        } else if ([[titleArray objectAtIndex:i] isEqual:@"24小时道路救援"]) {
            [button setBackgroundImage:[UIImage imageNamed:@"menu_24hourbtn"]forState:UIControlStateNormal];
        } else {
            [button setBackgroundImage:otherItemImgNormal       forState:UIControlStateNormal];
            [button setBackgroundImage:otherItemImgHighLight    forState:UIControlStateHighlighted];
        }
        
        if ([[titleArray objectAtIndex:i] isEqual:@"市场活动"]) {
            if (newsCount != 0) {
                [countBtn setTitle:[NSString stringWithFormat:@"%d", newsCount] forState:UIControlStateNormal];
                countBtn.hidden = NO;
            } else {
                countBtn.hidden = YES;
            }
        } else if ([[titleArray objectAtIndex:i] isEqual:@"车型优惠"]) {
            if (privilegeCount != 0) {
                [countBtn setTitle:[NSString stringWithFormat:@"%d", privilegeCount] forState:UIControlStateNormal];
                countBtn.hidden = NO;
            } else {
                countBtn.hidden = YES;
            }
        }
        
        [button setTitle:[titleArray objectAtIndex:i] forState:UIControlStateNormal];
        button.titleLabel.textColor = [UIColor whiteColor];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
        button.frame = CGRectMake(0, buttonY, buttonW, buttonH);
        NSString *tagStr = [NSString stringWithFormat:@"%d000%d", topIndex+1, i];
        button.tag = [tagStr integerValue];
        [button addTarget:self action:@selector(sideBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
    }
    
    CGFloat menuHeight = buttonH * titleArray.count;
    CGSize menuSize = CGSizeMake(buttonW, menuHeight);
    return menuSize;
}

-(void)sideBtnIsClickedOfIndex:(NSInteger)index
{
    if ([_delegate respondsToSelector:@selector(sideBtnClickedWithIndex:)]) {
        [_delegate performSelector:@selector(sideBtnClickedWithIndex:) withObject:[NSNumber numberWithInteger:index]];
    }
}

-(void)sideBtnIsClicked:(id)sender
{
    NSInteger tagInt = ((UIButton *)sender).tag;
    [self sideBtnIsClickedOfIndex:tagInt];
}


@end
