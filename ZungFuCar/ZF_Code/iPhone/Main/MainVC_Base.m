//
//  MainVC_Base.m
//  ZungFuCar
//
//  Created by adouming on 15/10/12.
//  Copyright © 2015年 Alex. All rights reserved.
//

#import "MainVC_Base.h"
#import "NewsMainVC.h"
#import "SendhandVC.h"
#import "ReserveFormViewController.h"
#import "BootManager.h"
@interface MainVC_Base ()

@end

@implementation MainVC_Base

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//请求展厅数据
-(void)getCarData:(int)type{
    
    AFHTTPRequestOperationManager *manger = [AFHTTPRequestOperationManager manager];
    manger.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manger.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"Http-X-U"];
    [manger.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"Http-X-P"];
    
    NSString* url = [NSString stringWithFormat:@"%@%@%@&type=%d", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_GETCARLIST, type];
    NSLog(@"%@", url);
    [manger GET: url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"App.net Global Stream: %@", responseObject);
        NSMutableArray *dataSource = responseObject;
        [[DBManager sharedManager] storeCarListData:dataSource Sucess:nil fail:nil key: ZF_ALL_car_type_list];
        /////////////////////////////////
        for (int i=0; i<dataSource.count; i++) {
            if ([[[dataSource objectAtIndex:i] objectForKey:@"CarType"] isKindOfClass:[NSMutableArray class]]) {
                NSMutableArray *carTypeList = [[dataSource objectAtIndex:i] objectForKey:@"CarType"];
                for (int j=0; j<carTypeList.count; j++) {
                    NSMutableArray *detailsList = [[carTypeList objectAtIndex:j] objectForKey:@"Details"];
                    for (int k=0; k<detailsList.count; k++) {
                        NSDictionary *carDic = [detailsList objectAtIndex:k];
                        NSString *carID = [carDic objectForKey:@"ID"];
                        /**************************/
                        NSMutableArray *LoanList = [carDic objectForKey:@"Loanparameters"];
                        NSMutableArray *LoanTempList = [NSMutableArray array];
                        for (int m=0; m<LoanList.count; m++) {
                            ZFSchemeData *temp = [ZFSchemeData dataWithJSON:[LoanList objectAtIndex:m]];
                            [LoanTempList addObject:temp];
                        }
                        [[DBManager sharedManager] storeSchemeData:LoanTempList key:carID scheme:@"loan" Sucess:^(NSMutableArray *storeData) {
                            
                        } fail:^(NSError *error) {
                            
                        }];
                        /**************************/
                        NSMutableArray *LeaseList = [carDic objectForKey:@"Leaseparameters"];
                        NSMutableArray *LeaseTempList = [NSMutableArray array];
                        for (int m=0; m<LeaseList.count; m++) {
                            ZFSchemeData *temp = [ZFSchemeData dataWithJSON:[LeaseList objectAtIndex:m]];
                            [LeaseTempList addObject:temp];
                        }
                        [[DBManager sharedManager] storeSchemeData:LeaseTempList key:carID scheme:@"lease" Sucess:^(NSMutableArray *storeData) {
                            
                        } fail:^(NSError *error) {
                            
                        }];
                        /**************************/
                    }
                }
            }
        }
        
        //            NSMutableArray *schemeTempMuArr = [NSMutableArray array];
        //            for (int i=0; i<dataSource.count; i++) {
        //                ZFSchemeData *temp = [ZFSchemeData dataWithJSON:[data objectAtIndex:i]];
        //                [schemeTempMuArr addObject:temp];
        //            }
        //            [[DBManager sharedManager] storeSchemeData:schemeTempMuArr key:ID scheme:scheme Sucess:^(NSMutableArray *storeData) {
        //
        //            } fail:^(NSError *error) {
        //
        //            }];
        /////////////////////////////////
        // }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Calculator error :%@", error);
    }];
    
}
@end
