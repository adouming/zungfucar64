//
//  SideBar.m
//  ZungFuCar
//
//  Created by Ed Lee on 11/29/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "SideBar.h"
#import "AlexMacro.h"

#define SIDEBARW 141
#define EXPBTNW 16
#define buttonH 40
#define buttonW (SIDEBARW - EXPBTNW)
#define TOPMARGIN 125

@implementation SideBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, SIDEBARW, 768);
        self.backgroundColor = RGB(54, 54, 54);
        UIButton *expandBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:expandBtn];
        [expandBtn setBackgroundImage:[UIImage imageNamed:@"sidebar.png"] forState:UIControlStateNormal];
        expandBtn.frame = CGRectMake(125, 0, 16, 768);
        [expandBtn addTarget:self action:@selector(sidebarExpand) forControlEvents:UIControlEventTouchUpInside];
        
        if (IOS7) {
            UIView *fakeTopBar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SIDEBARW, 20)];
            fakeTopBar.backgroundColor = [UIColor blackColor];
            [self addSubview:fakeTopBar];
        }
        
        _allBtnContainer = [[UIView alloc]initWithFrame:CGRectMake(0, TOPMARGIN, buttonW, 768-TOPMARGIN)];
        [self addSubview:_allBtnContainer];
    }
    return self;
}

-(CGSize)addButtonsForSidebar:(NSMutableArray*)titleArray fromTopIndex:(NSInteger)topIndex andSubIndex:(NSInteger)subIndex
{
    _topIndex = topIndex;
    _subIndex = subIndex;
    _btnArray = [[NSMutableArray alloc]init];
    for (int i=0; i<titleArray.count; i++) {
        UIView *btnContainer = [[UIView alloc]init];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        NSMutableDictionary *btnDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:button, @"third", nil, @"fourth", nil];
        [_btnArray insertObject:btnDic atIndex:i];
        
        CGFloat buttonY = buttonH * i;
        [button setBackgroundImage:[self buttonImageFromColor: [UIColor blackColor]] forState:UIControlStateHighlighted];
        [button setBackgroundImage:[self buttonImageFromColor: [UIColor blackColor]] forState:UIControlStateSelected];
        [button setTitle:[titleArray objectAtIndex:i] forState:UIControlStateNormal];
        button.titleLabel.textColor = [UIColor whiteColor];
        button.titleLabel.font = [UIFont systemFontOfSize:13];
        button.titleLabel. numberOfLines = 0; // Dynamic number of lines
        button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [button setTitleColor:HEXRGB(ZF_GoldColor) forState:UIControlStateHighlighted];
        [button setTitleColor:HEXRGB(ZF_GoldColor) forState:UIControlStateSelected];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 10);
        button.frame = CGRectMake(0, 0, buttonW, buttonH);
        NSString *tagStr = [NSString stringWithFormat:@"%d0%d0%d", topIndex+1, subIndex ,i];
        button.tag = [tagStr integerValue];
        [button addTarget:self action:@selector(sideBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        _initialHighlight = !_initialHighlight?0:_initialHighlight;
        if (i == _initialHighlight) {
            button.selected = YES;
        }
        
        btnContainer.frame = CGRectMake(0, buttonY, buttonW, buttonH);
        [btnContainer addSubview:button];
        [_allBtnContainer addSubview:btnContainer];
    }
    
    CGFloat menuHeight = buttonH * titleArray.count;
    CGSize menuSize = CGSizeMake(buttonW, menuHeight);
    return menuSize;
}

-(void)addFourthLevelFromData:(NSArray*)array atSideBtnIndex:(NSInteger)sideIndex {
    
    UIView *fourthBtnContainer = [[UIView alloc]init];
    
    NSMutableArray *fourthBtnArray = [[NSMutableArray alloc]init];
    for (int i=0; i<array.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGFloat buttonY = buttonH * i;
        [button setTitle:[[array objectAtIndex:i] objectForKey:@"Name"] forState:UIControlStateNormal];
        button.titleLabel.textColor = [UIColor whiteColor];
        button.titleLabel.font = [UIFont systemFontOfSize:13];
        button.titleLabel. numberOfLines = 0; // Dynamic number of lines
        button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [button setTitleColor:HEXRGB(ZF_GoldColor) forState:UIControlStateHighlighted];
        [button setTitleColor:HEXRGB(ZF_GoldColor) forState:UIControlStateSelected];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 35, 0, 0);
        button.frame = CGRectMake(0, buttonY, buttonW, buttonH);
        NSString *tagStr = [NSString stringWithFormat:@"%d0%d0%d0%d", _topIndex+1, _subIndex, sideIndex ,i];
        button.tag = [tagStr integerValue];
        [button addTarget:self action:@selector(sideBtnIsClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if (i==0) {
            button.selected = YES;
        }
        [fourthBtnContainer addSubview:button];
        [fourthBtnArray insertObject:button atIndex:i];
    }
    
    [[_btnArray objectAtIndex:sideIndex] setObject:fourthBtnArray forKey:@"fourth"];
    
    UIButton *btn = [[_btnArray objectAtIndex:sideIndex] objectForKey:@"third"];
    [btn.superview addSubview:fourthBtnContainer];
    btn.superview.clipsToBounds = YES;
    fourthBtnContainer.frame = CGRectMake(0, btn.frame.size.height, buttonW, buttonH * array.count);
    
    [self expandFourthMenuForIndex:0];
    //[self relayoutBtns];
}

- (UIImage *) buttonImageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

-(void)sideBtnIsClicked:(UIButton*)sender
{
    NSInteger tagInt = sender.tag;
    //BOOL toPushView;
    NSInteger toExpandIndex;
    NSLog(@"Clicked sidebar button tag: %d", tagInt);
    if (!sender.selected) {
        sender.selected = YES;
        
        if ([[NSString stringWithFormat:@"%d", tagInt] length] <= 5) { //点击第三级菜单
            for (int i=0; i<_btnArray.count; i++) {
                UIButton *btn = [[_btnArray objectAtIndex:i] objectForKey:@"third"];
                if ([btn isKindOfClass:[UIButton class]]) {
                    if (btn.tag != tagInt) {
                        btn.selected = NO;
                    }
                }
            }
            NSInteger clickedBtnIndex = [[[NSString stringWithFormat:@"%d", tagInt] substringFromIndex:4] integerValue];
            toExpandIndex = clickedBtnIndex;
            [self expandFourthMenuForIndex:toExpandIndex];
            
            NSArray *tempFourthAry = [[_btnArray objectAtIndex:clickedBtnIndex] objectForKey:@"fourth"];
            if (tempFourthAry.count>0) {
                tagInt = [[NSString stringWithFormat:@"%d00",sender.tag] integerValue];
            } else {
                tagInt = sender.tag;
            }
        }
        else
        { //点击四级菜单
            for (UIButton* btn in sender.superview.subviews) {
                if ([btn isKindOfClass:[UIButton class]]) {
                    if (btn.tag != tagInt) {
                        btn.selected = NO;
                    }
                }
            }
        }
        if ([_delegate respondsToSelector:@selector(sideBarButtonIsClickedWithTag:)]) {
            [_delegate performSelector:@selector(sideBarButtonIsClickedWithTag:) withObject:[NSNumber numberWithInteger:tagInt]];
        }
    }
}

-(void)sidebarExpand
{
    NSString *sideBarExpanded;
    if (self.frame.origin.x == 0) {
        CGRect slideLeft = CGRectMake(-125, 0, self.frame.size.width, self.frame.size.height);
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{self.frame = slideLeft;} completion:nil];
        sideBarExpanded = @"no";
    } else {
        CGRect slideRight = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{self.frame = slideRight;} completion:nil];
        sideBarExpanded = @"yes";
    }
    
    if ([_delegate respondsToSelector:@selector(sideBarIsExpanded:)]) {
        [_delegate performSelector:@selector(sideBarIsExpanded:) withObject:sideBarExpanded];
    }
}

-(void)relayoutBtns {
    CGFloat newThirdOrigin = 0;
    for (int i=0; i<_btnArray.count; i++) {
        NSArray *tempFourthAry = [[_btnArray objectAtIndex:i] objectForKey:@"fourth"];
        UIButton *thirdBtn = [[_btnArray objectAtIndex:i] objectForKey:@"third"];
        if ([thirdBtn isKindOfClass:[UIButton class]]) {
            thirdBtn.superview.frame = CGRectMake(0, newThirdOrigin, buttonW, buttonH *(tempFourthAry.count+1));
        }
        newThirdOrigin = newThirdOrigin + buttonH * (tempFourthAry.count + 1);
    }
}

-(void)expandFourthMenuForIndex:(NSInteger)index {
    //CGFloat upperBtnH;
    CGFloat newThirdOrigin;
    for (int i=0; i<_btnArray.count; i++) {
        if (i == index) {
            newThirdOrigin = buttonH * i;
            NSArray *tempFourthAry = [[_btnArray objectAtIndex:index] objectForKey:@"fourth"];
            UIButton *thirdBtn = [[_btnArray objectAtIndex:index] objectForKey:@"third"];
            if ([thirdBtn isKindOfClass:[UIButton class]]) {
                CGRect newFrame = CGRectMake(0, newThirdOrigin, buttonW, buttonH *(tempFourthAry.count+1));
                [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{thirdBtn.superview.frame = newFrame;} completion:nil];
            }
            newThirdOrigin = newThirdOrigin + buttonH * (tempFourthAry.count + 1);
        } else {
            UIButton *thirdBtn = [[_btnArray objectAtIndex:i] objectForKey:@"third"];
            if ([thirdBtn isKindOfClass:[UIButton class]]) {
                CGRect newFrame = CGRectMake(0, newThirdOrigin, buttonW, buttonH);
                [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{thirdBtn.superview.frame = newFrame;} completion:nil];
            }
            newThirdOrigin = newThirdOrigin + buttonH;
        }
    }
}

@end
