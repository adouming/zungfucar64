//
//  NSDateUtils.h
//  TarBar
//
//  Created by 刘 勇斌 on 13-4-15.
//  Copyright (c) 2013年 刘 勇斌. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateUtils : NSObject
+ (NSString *)stringFromDate:(NSDate *)date;
+(NSString *)stringFromTime:(NSDate *)date;
+(NSDate *)dateFromString:(NSString *)dateString;
+(NSString *)timeFromString:(NSString *)time;
+(BOOL)date1LaterDate2:(NSDate *)date1 date2:(NSDate *)date2;
+(NSString *)getNowYMD;
+(NSInteger)getNowWeekDay;
+(NSString *)stringOnlyDateFromDate:(NSDate *)date;
+(NSString *)getNowYMDHMS;
+(NSString *)getAge:(NSString *)dateString;
+(NSString *)getWeekDay:(NSInteger)i;
+(NSString *)getMonthEnglish:(NSInteger)i;
@end
