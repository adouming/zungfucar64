//
//  MainVC.h
//  ZungFuCar
//
//  Created by lxm on 13-9-29.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import <UIKit/UIKit.h>

// Subviews
#import "BottomBarView.h"
#import "SideButtons.h"

// Category
#import "NSObject+ReadSubMenu.h"
#import "NSObject+readAndWriteTxt.h"

// VC
#import "AboutZFController.h"
#import "ZFServiceVC.h"
#import "NewsSplitController.h"
#import "StoreViewController.h"
#import "IntroVC.h"

// Library
#import "WBEngine.h"
#import "MainVC_Base.h"

@interface MainVC_Ipad : MainVC_Base</*ArcFaleViewDelegate,*/BottomBarDelegate, SideButtonDelegate, WBEngineDelegate,UIGestureRecognizerDelegate>
{
    __strong WBEngine *_shareEngine;
    
}

- (IBAction)renfuguanjianAction:(id)sender;
- (IBAction)renfuhuiAction:(id)sender;
- (IBAction)zuixinzixunAction:(id)sender;
- (IBAction)renfufuwuAction:(id)sender;
- (IBAction)guanyurenfuAction:(id)sender;
- (IBAction)wanshangzhantingAction:(id)sender;

@property (strong, nonatomic) SideButtons *sideBtns;
@property (strong, nonatomic) BottomBarView *bottomBar;

@end
