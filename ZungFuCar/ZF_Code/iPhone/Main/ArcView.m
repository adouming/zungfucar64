//
//  ArcView.m
//  HelloWorld
//
//  Created by mac02 on 13-9-30.
//  Copyright (c) 2013年 liming. All rights reserved.
//

#import "ArcView.h"
#import "AppDelegate.h"

@implementation ArcView

+ (ArcView *)sharedArcView{
	static dispatch_once_t pred;
	static id shared_arcView = nil;
	
	dispatch_once(&pred, ^{
        
        CGRect mainFrame = [UIScreen mainScreen].bounds;
        shared_arcView = [[self alloc] initWithFrame:CGRectMake(0, mainFrame.size.height-88, 320, 88)];
        
//        [[UIApplication sharedApplication].keyWindow addSubview:shared_arcView];
    });
	
	return shared_arcView;
}


-(id)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        
        _bg1 = [[UIImageView alloc] initWithFrame:CGRectMake(38, frame.size.height-38, 246, 38)];
        _bg1.image = [UIImage imageNamed:@"index_scbtn"];
        [self addSubview:_bg1];
        
        _bg2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _bg2.image = [UIImage imageNamed:@"index_scbtn2"];
        _bg2.hidden = YES;
        _bg2.tag = 1000;
        _bg2.userInteractionEnabled = YES;
        [self addSubview:_bg2];
        
        _tapBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
        _tapBtn.center = CGPointMake(frame.size.width/2, frame.size.height-20);
        _tapBtn.backgroundColor = [UIColor clearColor];
        [_tapBtn addTarget:self action:@selector(tapAction) forControlEvents:UIControlEventTouchUpInside];
        _tapBtn.selected = NO;
        _tapBtn.tag = 1001;
        [self addSubview:_tapBtn];
        
        
//        ArcFaleView *aView = [[ArcFaleView alloc] initWithFrame:CGRectMake(10, 200, 300, 300)];
//        
//        [self.view addSubview:aView];
        //[aView setCollections];
        
        
//        _faleView = [[ArcFaleView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height-_tapBtn.frame.size.height)];
        _faleView = [[ArcFaleView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height-10)];
        _faleView.delegate = self;
        _faleView.userInteractionEnabled = YES;
        [self addSubview:_faleView];
        
        _faleView.hidden = YES;
        
       // [_faleView setCollections];
        
        [self bringSubviewToFront:_tapBtn];
        
        [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(closeFaleView) name:@"closeArcFaleView" object:nil];
    }
    return self;
}



-(void)tapAction{
    
    //NSLog(@"tapAction");
    
    _tapBtn.selected = !_tapBtn.selected;
    
    
    if (_tapBtn.selected) {
        
        CGRect mainFrame = [UIScreen mainScreen].bounds;
        //shared_arcView = [[self alloc] initWithFrame:CGRectMake(0, mainFrame.size.height-88, 320, 88)];
        self.frame = CGRectMake(0, mainFrame.size.height-88, 320, 88);
        
        _faleView.frame = CGRectMake(_faleView.frame.origin.x, _faleView.frame.origin.y, _faleView.frame.size.width, self.frame.size.height-10);
        
        
        _faleView.hidden = NO;
        _bg1.hidden = YES;
        _bg2.hidden = NO;
        
        // 淡入显示新View
        _faleView.alpha = 0.0f;
        [UIView beginAnimations:@"fadeIn" context:nil];
        //[self.view.window addSubview:subview];
        [UIView setAnimationDuration:1.0];
        _faleView.alpha = 1.0f;
        [UIView commitAnimations];

        
        _tapBtn.center = CGPointMake(self.frame.size.width/2, self.frame.size.height-20);
        _bg1.frame = CGRectMake(_bg1.frame.origin.x, self.frame.size.height-_bg1.frame.size.height, _bg1.frame.size.width, _bg1.frame.size.height);

 
        
    }else{
        [self closeFaleView];
    }
}

-(void)closeFaleView{
    
    _tapBtn.selected = NO;
    
    //收
    _bg1.hidden = NO;
    _bg2.hidden = YES;
    
    // 淡出移除当前View
    //        _faleView.alpha= 1.0f;
    //        [UIView beginAnimations:@"fadeIn" context:nil];
    //        [UIView setAnimationDuration:0.5];
    _faleView.hidden = YES;
    //        _faleView.alpha = 0.0f;
    //        [UIView commitAnimations];
    
    _faleView.frame = CGRectMake(_faleView.frame.origin.x, _faleView.frame.origin.y, _faleView.frame.size.width, 0);
    CGRect mainFrame = [UIScreen mainScreen].bounds;
    self.frame = CGRectMake(0, mainFrame.size.height-40, 320, 40);
    _tapBtn.center = CGPointMake(self.frame.size.width/2, self.frame.size.height-20);
    _bg1.frame = CGRectMake(_bg1.frame.origin.x, self.frame.size.height-_bg1.frame.size.height, _bg1.frame.size.width, _bg1.frame.size.height);
}

-(void)tagClick:(NSNumber*)aNumber{
    
    NSLog(@"arcView tag=%d",[aNumber integerValue]);
    
    {
        
        _tapBtn.selected = !_tapBtn.selected;
        
        
        _bg1.hidden = NO;
        _bg2.hidden = YES;
        
        // 淡出移除当前View
        _faleView.alpha= 1.0f;
        [UIView beginAnimations:@"fadeIn" context:nil];
        [UIView setAnimationDuration:0.5];
        
        //_faleView.hidden = YES;
        
        _faleView.alpha = 0.0f;
        [UIView commitAnimations];
        
        
        
        _faleView.frame = CGRectMake(_faleView.frame.origin.x, _faleView.frame.origin.y, _faleView.frame.size.width, 0);
    }
    
    
    
    if ([_delegate respondsToSelector:@selector(tagClick:)]) {
        [_delegate performSelector:@selector(tagClick:) withObject:aNumber];
    }
    
}


-(void)drawRect:(CGRect)rect{

    
    [super drawRect:rect];
    
    
}


@end
