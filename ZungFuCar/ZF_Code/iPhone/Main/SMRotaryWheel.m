//
//  SMRotaryWheel.m
//  RotaryWheelProject
//
//  Created by cesarerocchi on 2/10/12.
//  Copyright (c) 2012 studiomagnolia.com. All rights reserved.


#import "SMRotaryWheel.h"
#import <QuartzCore/QuartzCore.h>
#import "SMCLove.h"

#import "CurvyTextView.h"

@interface SMRotaryWheel()
    - (void)drawWheel;
    - (float) calculateDistanceFromCenter:(CGPoint)point;
    - (void) buildClovesEven;
    - (void) buildClovesOdd;
    - (UIImageView *) getCloveByValue:(int)value;
    //- (NSString *) getCloveName:(int)position;
@end

static float deltaAngle;
static float minAlphavalue = 1.0;
static float maxAlphavalue = 1.0;

@implementation SMRotaryWheel

@synthesize delegate, container, numberOfSections, startTransform, cloves, currentValue;


- (id) initWithFrame:(CGRect)frame andDelegate:(id)del withSections:(int)sectionsNumber {
    
    if ((self = [super initWithFrame:frame])) {
		
        self.currentValue = 0;
        self.numberOfSections = sectionsNumber;
        self.delegate = del;
		[self drawWheel];
        clickIndex = -1;
        _allAngle=0;
        
	}
    return self;
}



-(CurvyTextView*)makeView:(NSString *)imageName tag:(NSInteger)tag{
    CurvyTextView * aView = [[CurvyTextView alloc] initWithFrame:CGRectMake(0, 0, 105, 20)];
    aView.text = imageName;
    aView.userInteractionEnabled = YES;
    return aView;
}

- (void) drawWheel {

    container = [[UIView alloc] initWithFrame:self.frame];
    container.backgroundColor = [UIColor clearColor];
    
    CGFloat angleSize = 2*M_PI/numberOfSections;
    
    NSArray *texts = [NSArray arrayWithObjects:@" 试乘试驾预约" ,@" 保养维修预约" ,@"    加入仁孚",nil];
    
    //NSArray *texts = [NSArray arrayWithObjects:@"保养维修预约" ,@"试驾预约", @"市场活动",@"车型优惠",@"联系我们",@"保养维修预约" ,@"试驾预约", @"市场活动",@"车型优惠",@"联系我们",nil];
    
    
    for (int i = 0; i < texts.count; i++) {
        
        UIImageView *im = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"segment11.png"]];
        
        
        im.frame = CGRectMake(0, 0, 220, 220);
        im.backgroundColor = [UIColor clearColor];
//        if (i==0) {
//            im.backgroundColor = [UIColor lightGrayColor];
//        }
//        
//        if (i==9) {
//            im.backgroundColor = [UIColor yellowColor];
//        }
        
        //
        //im.layer.anchorPoint = CGPointMake(1.0f, 0.5f);
        im.layer.anchorPoint = CGPointMake(0.5f,1.0f);
        im.layer.position = CGPointMake(container.bounds.size.width/2.0-container.frame.origin.x, 
                                        container.bounds.size.height/2.0-container.frame.origin.y);
        im.transform = CGAffineTransformMakeRotation(angleSize*i);
        im.alpha = minAlphavalue;
        im.tag = i;
        
        if (i == 0) {
            im.alpha = maxAlphavalue;
        }

        CurvyTextView *aTextView = [self makeView:[texts objectAtIndex:i] tag:i];
        im.userInteractionEnabled = YES;
        aTextView.delegate = self;
        aTextView.tag = i+1;
        aTextView.center = CGPointMake(im.bounds.size.width/2, aTextView.center.y);
        
//        UIButton *aButton = [[UIButton alloc]initWithFrame:aTextView.frame];
//        [aButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"index_scbtntext%db", i+1]] forState:UIControlStateNormal];
//        [aButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"index_scbtntext%da", i+1]] forState:UIControlStateHighlighted];
//        [aButton addTarget:self action:@selector(tagClick) forControlEvents:UIControlEventTouchUpInside];
//        UIImageView *cloveImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
//        cloveImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon%i.png", i]];
//        cloveImage.backgroundColor = [UIColor blackColor];
//        [im addSubview:aButton];
        [im addSubview:aTextView];
        
        //NSLog(@"---%f,%f,%f,%f",im.frame.origin.x,im.frame.origin.y,im.frame.size.width,im.frame.size.height);
        
        //im.backgroundColor = [UIColor blackColor];
        
        [container addSubview:im];
        
        //NSLog(@"--%f,%f",im.frame.size.width,im.frame.size.height);
        
    }
    
    container.userInteractionEnabled = YES;
    [self addSubview:container];
    
    cloves = [NSMutableArray arrayWithCapacity:numberOfSections];
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:self.frame];
    bg.image = [UIImage imageNamed:@"bg11.png"];
    [self addSubview:bg];
    
    UIImageView *mask = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 58, 58)];
    mask.image =[UIImage imageNamed:@"centerButton11.png"] ;
    mask.center = self.center;
    mask.center = CGPointMake(mask.center.x, mask.center.y+3);
    [self addSubview:mask];
    
    if (numberOfSections % 2 == 0) {
        
        [self buildClovesEven];
        
    } else {
        
        [self buildClovesOdd];
        
    }
    
    
}


- (UIImageView *) getCloveByValue:(int)value {

    UIImageView *res;
    
    NSArray *views = [container subviews];
    
    for (UIImageView *im in views) {
        
        if (im.tag == value)
            res = im;
        
    }
    
    return res;
    
}

- (void) buildClovesEven {
    
    CGFloat fanWidth = M_PI*2/numberOfSections;
    CGFloat mid = 0;
    
    for (int i = 0; i < numberOfSections; i++) {
        
        SMClove *clove = [[SMClove alloc] init];
        clove.midValue = mid;
        clove.minValue = mid - (fanWidth/2);
        clove.maxValue = mid + (fanWidth/2);
        clove.value = i;
        
        
        if (clove.maxValue-fanWidth < - M_PI) {
            
            mid = M_PI;
            clove.midValue = mid;
            clove.minValue = fabsf(clove.maxValue);
            
        }
        
        mid -= fanWidth;
        
        
       // NSLog(@"cl is %@", clove);
        
        [cloves addObject:clove];
        
    }
    
}


- (void) buildClovesOdd {
    
    CGFloat fanWidth = M_PI*2/numberOfSections;
    CGFloat mid = 0;
    
    for (int i = 0; i < numberOfSections; i++) {
        
        SMClove *clove = [[SMClove alloc] init];
        clove.midValue = mid;
        clove.minValue = mid - (fanWidth/2);
        clove.maxValue = mid + (fanWidth/2);
        clove.value = i;
        
        mid -= fanWidth;
        
        if (clove.minValue < - M_PI) {
            
            mid = -mid;
            mid -= fanWidth; 
            
        }
        
                
        [cloves addObject:clove];
        
        //NSLog(@"cl is %@", clove);
        
    }
    
}

- (float) calculateDistanceFromCenter:(CGPoint)point {
    
    CGPoint center = CGPointMake(self.bounds.size.width/2.0f, self.bounds.size.height/2.0f);
	float dx = point.x - center.x;
	float dy = point.y - center.y;
	return sqrt(dx*dx + dy*dy);
    
}







-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //NSLog(@"my control touch begin");
    [super touchesBegan:touches withEvent:event];
    UITouch* touch = [touches anyObject];
    // Track the touch
    [self beginTrackingWithTouch:touch withEvent:event];
}


- (void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    [super touchesMoved:touches withEvent:event];
    // Get the only touch (multipleTouchEnabled is NO)
    UITouch* touch = [touches anyObject];
    // Track the touch
    [self continueTrackingWithTouch:touch withEvent:event];
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    // Get the only touch (multipleTouchEnabled is NO)
    UITouch* touch = [touches anyObject];
    // Track the touch
    [self endTrackingWithTouch:touch withEvent:event];
}

#pragma mark - control

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint touchPoint = [touch locationInView:self];
    float dist = [self calculateDistanceFromCenter:touchPoint];
    
    if (dist < 100 || dist > 220)
    {
        // forcing a tap to be on the ferrule
       // NSLog(@"ignoring tap (%f,%f)", touchPoint.x, touchPoint.y);
        return NO;
    }
    
	float dx = touchPoint.x - container.center.x;
	float dy = touchPoint.y - container.center.y;
	deltaAngle = atan2(dy,dx); 
    
    startTransform = container.transform;
    
    UIImageView *im = [self getCloveByValue:currentValue];
    im.alpha = minAlphavalue;
    
    CurvyTextView *aTextView = (CurvyTextView*)[container viewWithTag:currentValue+1];
    [aTextView buildAttributedString:[UIColor colorWithRed:172/255.0f green:109/255.0f blue:55/255.0f alpha:1.0f]];
    [aTextView setNeedsDisplay];
    
    
    return YES;
    
}




- (BOOL)continueTrackingWithTouch:(UITouch*)touch withEvent:(UIEvent*)event
{
        
	CGPoint pt = [touch locationInView:self];
    
    float dist = [self calculateDistanceFromCenter:pt];
    
    if (dist < 100 || dist > 220)
    {
        // a drag path too close to the center
        //NSLog(@"drag path too close to the center (%f,%f)", pt.x, pt.y);
        
        // here you might want to implement your solution when the drag 
        // is too close to the center
        // You might go back to the clove previously selected
        // or you might calculate the clove corresponding to
        // the "exit point" of the drag.

    }
	
	float dx = pt.x  - container.center.x;
	float dy = pt.y  - container.center.y;
	float ang = atan2(dy,dx);
    
    float angleDifference = deltaAngle - ang;
    
    CGAffineTransform oldTransForm = container.transform;
    container.transform = CGAffineTransformRotate(startTransform, -angleDifference);
    
    
    //_allAngle = _allAngle-angleDifference;
    //角度
   // NSLog(@"_allAngle==%f",_allAngle);
    
   // NSLog(@"cos==%f",acos(container.transform.a));
//    UIWebView * webView = (UIWebView *)[cell.contentView viewWithTag:10086];
//    CGAffineTransform transform = webView.transform;
//    NSLog(@"++++++++ transform(%f,%f,%f,%f,%f,%f)", transform.a, transform.b, transform.c, transform.d, transform.tx, transform.ty);
//    CGFloat rotate = acosf(transform.a);
//    if (transform.b < 0) {
//        rotate+= M_PI;
//    }
//    CGFloat degree = rotate/M_PI * 180;
//    NSLog(@"+++++++++ degree : %f", degree);
//    CGAffineTransform newTransForm = CGAffineTransformRotate(transform, -M_PI/8);
//    [webView setTransform:newTransForm];
    CGFloat newRotate = acosf(container.transform.a);
    if (container.transform.b < 0) {
        newRotate+= M_PI;
    }
    CGFloat newDegree = newRotate/M_PI * 180;
    //NSLog(@"+++++++++ newDegree : %f", newDegree);
    
    if (newDegree>180 && newDegree<260) {
        //container.transform = CGAffineTransformRotate(startTransform, -angleDifference);
    }else{
        container.transform= oldTransForm;
    }
    
    
    NSLog(@"continueTrackingWithTouch");
    clickIndex = -1;
    return YES;
	
}


- (void)endTrackingWithTouch:(UITouch*)touch withEvent:(UIEvent*)event
{
    NSLog(@"endTrackingWithTouch");
    CGFloat radians = atan2f(container.transform.b, container.transform.a);
    
    CGFloat newVal = 0.0;
    
    for (SMClove *c in cloves) {
        
        if (c.minValue > 0 && c.maxValue < 0) { // anomalous case
            
            if (c.maxValue > radians || c.minValue < radians) {
                
                if (radians > 0) { // we are in the positive quadrant
                    
                    newVal = radians - M_PI;
                    
                } else { // we are in the negative one
                    
                    newVal = M_PI + radians;                    
                    
                }
                currentValue = c.value;
                
            }
            
        }
        
        else if (radians > c.minValue && radians < c.maxValue) {
            
            newVal = radians - c.midValue;
            currentValue = c.value;
            
        }
        
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    
    CGAffineTransform t = CGAffineTransformRotate(container.transform, -newVal);
    container.transform = t;
    
    [UIView commitAnimations];
    
    //NSLog(@"-------%d",currentValue);
    
    for (int i=0; i<5; i++) {
        CurvyTextView *aTextView = (CurvyTextView*)[container viewWithTag:i+1];
        
        if (i==currentValue) {
            [aTextView buildAttributedString:[UIColor whiteColor]];
        }else{
            [aTextView buildAttributedString:[UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:100]];
        }
        
        [aTextView setNeedsDisplay];
    }
    
    /*
     CurvyTextView *aTextView = [self makeView:[texts objectAtIndex:i] tag:i];
     im.userInteractionEnabled = YES;
     aTextView.delegate = self;
     aTextView.tag = i+1;
     aTextView.center = CGPointMake(im.bounds.size.width/2, aTextView.center.y);
     
     //        UIImageView *cloveImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
     //        cloveImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon%i.png", i]];
     //        cloveImage.backgroundColor = [UIColor blackColor];
     [im addSubview:aTextView];
     
     
     //NSLog(@"---%f,%f,%f,%f",im.frame.origin.x,im.frame.origin.y,im.frame.size.width,im.frame.size.height);
     
     //im.backgroundColor = [UIColor blackColor];
     
     [container addSubview:im];
     */
    
    //NSLog(@"---end");
    
//    [self.delegate wheelDidChangeValue:[self getCloveName:currentValue]];
//    
//    UIImageView *im = [self getCloveByValue:currentValue];
//    im.alpha = maxAlphavalue;
    
    if (clickIndex!=-1) {
        if ([_myDelegate respondsToSelector:@selector(tagClick:)]) {
            [_myDelegate performSelector:@selector(tagClick:) withObject:[NSNumber numberWithInt:clickIndex]];
        }
        [self performSelector:@selector(reset:) withObject:[NSNumber numberWithInt:clickIndex]  afterDelay:0.25];
    }
    
}

//- (NSString *) getCloveName:(int)position {
//    
//    NSString *res = @"";
//    
//    switch (position) {
//        case 0:
//            res = @"Circles";
//            break;
//            
//        case 1:
//            res = @"Flower";
//            break;
//            
//        case 2:
//            res = @"Monster";
//            break;
//            
//        case 3:
//            res = @"Person";
//            break;
//            
//        case 4:
//            res = @"Smile";
//            break;
//            
//        case 5:
//            res = @"Sun";
//            break;
//            
//        case 6:
//            res = @"Swirl";
//            break;
//            
//        case 7:
//            res = @"3 circles";
//            break;
//            
//        case 8:
//            res = @"Triangle";
//            break;
//            
//        default:
//            break;
//    }
//    
//    return res;
//}



-(void)tagClick:(CurvyTextView *)aView{
    
    
    clickIndex = aView.tag;
    //NSLog(@"tag=%d",aView.tag);
    if (clickIndex!=currentValue+1) {
        clickIndex = -1;
    }

}


-(void)reset:(NSNumber*)aNumber{
    
    for (int i=0; i<5; i++) {
        CurvyTextView *aTextView = (CurvyTextView*)[container viewWithTag:i+1];
        
        if (i==[aNumber integerValue]-1) {
            [aTextView buildAttributedString:[UIColor whiteColor]];
        }else{
            [aTextView buildAttributedString:[UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:100]];
        }
        
        [aTextView setNeedsDisplay];
    }
}


@end
