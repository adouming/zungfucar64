//
//  bottomBarView.h
//  ZungFuCar
//
//  Created by Ed Lee on 11/28/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BottomBarDelegate <NSObject>

-(void)buttonClickedWithTag:(NSNumber*)tag;

@end

@interface BottomBarView : UIView

//+ (BottomBarView *)sharedBottomView;

@property(nonatomic,assign)id<BottomBarDelegate> delegate;
- (IBAction)barButtonClicked:(id)sender;

- (void) adjustViewsForOrientation:(UIInterfaceOrientation)orientation;

@end
