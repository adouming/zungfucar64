//
//  AboutZFVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-2.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "AboutZFVC.h"
#import "AppDelegate.h"
#import "CommonSingleWebViewController.h"
@interface AboutZFVC ()

@end

@implementation AboutZFVC
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view
    /*背景图片*/
    
    
    /*新浪微博*/
    [self initSinaWeiboEngine];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    _shareEngine = nil;
    _bgImageView = nil;
    [super viewDidUnload];
}
/**
 *	隐藏右边按钮
 */
-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}

/**
 *	关注微博
 *
 *	@param	sender	按钮
 */
- (IBAction)AttentionWeiboBtnClicked:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"授权微博"
                                                    message:@"你确定要授权于微博？"
                                                   delegate:self
                                          cancelButtonTitle:@"取消"
                                          otherButtonTitles:@"确定",nil];
    alert.tag = 101;
    [alert show];
}

///**
// *	发展历程
// *
// *	@param	sender	按钮
// */
//- (IBAction)companyHistory:(id)sender
//{
//    // Init the pages texts, and pictures.
////    ICETutorialPage *layer0 = [[ICETutorialPage alloc] initWithSubTitle:@"Picture 1"
////                                                            description:@"1994年\n仁孚中国网络发展\n首个销售及服务网点"
////                                                            pictureName:nil
////                                                                  point:CGPointMake(35, 200)];
//    ICETutorialPage *layer1 = [[ICETutorialPage alloc] initWithSubTitle:@"Picture 1"
//                                                            description:@"1994年\n仁孚中国网络发展\n首个销售及服务网点"
//                                                            pictureName:@"history_1994pic.png"
//                                                                    point:CGPointMake(5, 200)];
//    ICETutorialPage *layer2 = [[ICETutorialPage alloc] initWithSubTitle:@"Picture 2"
//                                                            description:@"1995年\n正式成立"
//                                                            pictureName:@"history_1995pic.png"
//                               point:CGPointMake(5, 200)];
//    ICETutorialPage *layer3 = [[ICETutorialPage alloc] initWithSubTitle:@"Picture 3"
//                                                            description:@"2002年\n深圳鹏星行"
//                                                            pictureName:@"history_2013pic.png"
//                               point:CGPointMake(5, 300)];
//    ICETutorialPage *layer4 = [[ICETutorialPage alloc] initWithSubTitle:@"Picture 4"
//                                                            description:@"2003年\n。。。"
//                                                            pictureName:@"banner3.png"
//                               point:CGPointMake(10, 100)];
//    ICETutorialPage *layer5 = [[ICETutorialPage alloc] initWithSubTitle:@"Picture 5"
//                                                            description:@"2004年\n。。。"
//                                                            pictureName:@"banner4.png"
//                               point:CGPointMake(10, 200)];
//
//    // Set the common style for SubTitles and Description (can be overrided on each page).
//    ICETutorialLabelStyle *subStyle = [[ICETutorialLabelStyle alloc] init];
//    [subStyle setFont:TUTORIAL_SUB_TITLE_FONT];
//    [subStyle setTextColor:TUTORIAL_LABEL_TEXT_COLOR];
//    [subStyle setLinesNumber:TUTORIAL_SUB_TITLE_LINES_NUMBER];
//    [subStyle setOffset:TUTORIAL_SUB_TITLE_OFFSET];
//
//    ICETutorialLabelStyle *descStyle = [[ICETutorialLabelStyle alloc] init];
//    [descStyle setFont:TUTORIAL_DESC_FONT];
//    [descStyle setTextColor:TUTORIAL_LABEL_TEXT_COLOR];
//    [descStyle setLinesNumber:TUTORIAL_DESC_LINES_NUMBER];
//    [descStyle setOffset:TUTORIAL_DESC_OFFSET];
//
//    // Load into an array.
//    NSArray *tutorialLayers = @[layer1,layer2,layer3];
//
//    // Override point for customization after application launch.
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//        self.iceViewController = [[ICETutorialController alloc] initWithNibName:@"ICETutorialController_iPhone"
//                                                                      bundle:nil
//                                                                    andPages:tutorialLayers];
//    }
//
//    // Set the common styles, and start scrolling (auto scroll, and looping enabled by default)
//    [self.iceViewController setCommonPageSubTitleStyle:subStyle];
//    [self.iceViewController setCommonPageDescriptionStyle:descStyle];
//
//    // Set button 1 action.
//    [self.iceViewController setButton1Block:^(UIButton *button){
//        NSLog(@"Button 1 pressed.");
//    }];
//
//    // Set button 2 action, stop the scrolling.
//    __unsafe_unretained typeof(self) weakSelf = self;
//    [self.iceViewController setButton2Block:^(UIButton *button){
//        NSLog(@"Button 2 pressed.");
//        NSLog(@"Auto-scrolling stopped.");
//
//        [weakSelf.iceViewController stopScrolling];
//    }];
//
//    // Run it.
//    [self.iceViewController startScrolling];
//
//    [self.navigationController pushViewController:self.iceViewController animated:YES];
//}

/**
 * 初始化新浪微博引擎
 */
- (void)initSinaWeiboEngine
{
    _shareEngine = [[WBEngine alloc] initWithAppKey:kWBSDKDemoAppKey appSecret:kWBSDKDemoAppSecret];
    [_shareEngine setRootViewController: self];
    [_shareEngine setDelegate:self];
    [_shareEngine setRedirectURI:@"https://www.zfchina.com//"];
    [_shareEngine setIsUserExclusive:NO];
}

#pragma mark - WBEngineDelegate Methods

#pragma mark Authorize

- (void)engineAlreadyLoggedIn:(WBEngine *)engine
{
    if ([engine isUserExclusive])
    {
        UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
                                                           message:@"请先登出！"
                                                          delegate:nil
                                                 cancelButtonTitle:@"确定"
                                                 otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)engineDidLogIn:(WBEngine *)engine
{
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"你确定要关注？"
													  delegate:self
											 cancelButtonTitle:@"取消"
											 otherButtonTitles:@"确定",nil];
    [alertView setTag:102];
	[alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101)
    {
        if(buttonIndex==1)
        {
            [_shareEngine logIn];
        }
    }
    else if(alertView.tag == 102)
    {
        if(buttonIndex==1)
        {
            /*关注*/
            [_shareEngine loadRequestWithMethodName:@"friendships/create.json"
                                         httpMethod:@"POST"
                                             params:[NSDictionary dictionaryWithObjectsAndKeys:_shareEngine.accessToken,@"access_token",
                                                     kSinaSZAirUID,@"screen_name",
                                                     nil]
                                       postDataType:kWBRequestPostDataTypeNormal
                                   httpHeaderFields:nil];
        }
    }
}

- (void)engine:(WBEngine *)engine didFailToLogInWithError:(NSError *)error
{
    NSLog(@"didFailToLogInWithError: %@", error);
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"登录失败！"
													  delegate:nil
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
	[alertView show];
}

- (void)engineDidLogOut:(WBEngine *)engine
{
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"登出成功！"
													  delegate:self
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
    [alertView setTag:11];
	[alertView show];
}

- (void)engineNotAuthorized:(WBEngine *)engine
{
    
}

- (void)engineAuthorizeExpired:(WBEngine *)engine
{
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"请重新登录！"
													  delegate:nil
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
	[alertView show];
}

- (void)engine:(WBEngine *)engine requestDidSucceedWithResult:(id)result
{
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"关注成功！"
													  delegate:nil
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
	[alertView show];
}

- (void)engine:(WBEngine *)engine requestDidFailWithError:(NSError *)error
{
    if ([error.domain isEqualToString:@"WeiBoSDKErrorDomain"]
        &&[[error.userInfo objectForKey:@"error"] isEqualToString:@"already followed"]) {
        UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
                                                           message:@"您已关注！"
                                                          delegate:nil
                                                 cancelButtonTitle:@"确定"
                                                 otherButtonTitles:nil];
        [alertView show];
    }
    
}
//公司简介
- (IBAction)companyprofileAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:2];
    controler.title = @"公司简介";
    [self.navigationController pushViewController:controler animated:YES];
}
//发展历程
- (IBAction)historyAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:3];
    controler.title = @"发展历程";
    [self.navigationController pushViewController:controler animated:YES];
}
//企业文化
- (IBAction)companycultureAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:4];
    controler.title = @"企业文化";
    [self.navigationController pushViewController:controler animated:YES];
}
//服务网络
- (IBAction)servicenetworkAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:5];
    controler.title = @"服务网络";
    [self.navigationController pushViewController:controler animated:YES];
}
//社会责任
- (IBAction)societyrecruitmentkAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:6];
    controler.title = @"社会责任";
    [self.navigationController pushViewController:controler animated:YES];
}
//员工关怀
- (IBAction)staffconcernAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:7];
    controler.title = @"员工关怀";
    [self.navigationController pushViewController:controler animated:YES];
}
@end
