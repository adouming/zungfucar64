//
//  CompanyProfileVC.h
//  ZungFuCar
//
//  Created by lxm on 13-10-3.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
@interface CompanyProfileVC : MenuAbstractViewController<UIWebViewDelegate>
{
    __strong IBOutlet UIScrollView *_bgScrollView;/*scrollview 需要拉动*/
    __strong IBOutlet UILabel *_contentLabel;/*无效*/
    __strong IBOutlet UILabel *_titleLabel;/*banner下方显示文字*/
    __weak IBOutlet UIWebView *_contentWebView;/*内容*/
    __weak IBOutlet UIView *_topView;
    __weak IBOutlet UIImageView *_bannerView;/*banner scaleToFit*/
    BOOL willLaunchSafari;
}
/**
 *	自适应ContentLabel高度
 *
 *	@param	text	内容
 */
//- (void)resizeContentLabel:(NSString *)text;
- (void)creatModel;

@end
