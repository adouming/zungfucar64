//
//  AboutZFVC.h
//  ZungFuCar
//
//  Created by lxm on 13-10-2.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
#import "WBEngine.h"
#import "ICETutorialController.h"
@interface AboutZFVC : MenuAbstractViewController<WBEngineDelegate>
{
    __strong IBOutlet UIImageView *_bgImageView;
    __strong WBEngine *_shareEngine;
}
@property (strong, nonatomic) ICETutorialController *iceViewController;
/**
 *	关注微博
 *
 *	@param	sender	按钮
 */
- (IBAction)AttentionWeiboBtnClicked:(id)sender;

/**
 *	发展历程
 *
 *	@param	sender	按钮
 */
//- (IBAction)companyHistory:(id)sender;
@end
