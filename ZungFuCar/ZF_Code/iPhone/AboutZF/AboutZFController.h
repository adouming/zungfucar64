//
//  AboutZFController.h
//  ZungFuCar
//
//  Created by Ed Lee on 11/29/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

// Subviews:
#import "SideBar.h"
#import "MenuAbstractViewController.h"

// Category
#import "NSObject+ReadSubMenu.h"

// VC
#import "CompanyProfileVC.h"
#import "SinglePageController.h"

// Data
#import "GetAfterSalesData.h"
#import "CalculatorVC.h"
#import "ReserveFormViewController_ipad.h"
#import "RoadRescueViewController.h"

@interface AboutZFController : MenuAbstractViewController <SideBarDelegate>{
    CalculatorVC *calculatorCtl;
    ReserveFormViewController_ipad *reserveCtl;
}

@property (strong, nonatomic) SideBar *sidebar;

@property (strong, nonatomic) NSMutableArray *sidebarMenuTitles;

@property (strong, nonatomic) UINavigationController *contentNaviCtrl;

@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) NSString *bgImageName;
@property (strong, nonatomic) NSArray *jingpinData;
@property (strong, nonatomic) NSArray *peijianData;
@property (strong, nonatomic) NSArray *xiaoYuanData;

@property NSInteger subIndex;
@property NSInteger topIndex;

@end
