//
//  ContactUSVC.h
//  ZungFuCar
//
//  Created by lxm on 13-10-5.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuAbstractViewController.h"
#import "HorizontalScrollView.h"

@interface ServiceNetwork :MenuAbstractViewController<HorizontalScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>{
    UIButton *leftButton;
    UIButton *rightButton;
    int currentIndex;
    UITableView *newsTableView;
    NSMutableArray *dataSource;
    
    int viewType;
    HorizontalScrollView *hScrollView;
    int selectIndexSwap;
}
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;
@property int selectIndexSwap;
+ (NSString *)getTimeToShowWithTimestamp:(NSString *)timestamp;

@end
