//
//  HistoryViewController.m
//  ZungFuCar
//
//  Created by GuoChengHao on 13-11-17.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "HistoryViewController.h"
#import "AboutZFAPIManager.h"
#import "DBManager.h"
#import "NSString+FilterHtml.h"

@interface HistoryViewController ()

@end

@implementation HistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/**
 * Model
 */
- (void)creatModel
{
    NSArray *array = [[DBManager sharedManager] getDevelopmentHistoryData];
    if([array count] ==0 )
        return;
    NSString *contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
    NSString *bannerUrl = [[array objectAtIndex:0] objectForKey:@"BannerURL"];
    if([bannerUrl validatePng] &&![bannerUrl isEqualToString:@""])
        [_bannerView setImageWithURL:[NSURL URLWithString:[[array objectAtIndex:0] objectForKey:@"BannerURL"]]];
}

/**
 *  请求数据
 */
- (void)requestData
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID4_UpdatedTime"]==nil)
    {
        [self showLoadingText];
    }

    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    [manager requestDevelopmentHistoryDataSuccess:^(NSArray *data) {
        BOOL isFirst = NO;
        if([data count]==0)
        {
            return ;
        }
        else
        {
            /*写lastmodifyTime到沙盒*/
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if([userDefaults objectForKey:@"ViewID4_UpdatedTime"]==nil)
                isFirst = YES;
            [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID4_UpdatedTime"];
            [userDefaults synchronize];
           
            /*content url 图片不规范 需要附加http*/
            NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"content"];
//            NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
//            htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
            [self hideLoadingText];
            
            
            /*重新封装数据*/
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[
                                 [data objectAtIndex:0] objectForKey:@"content"],@"BannerURL",
                                 htmlStr,@"Content",
                                 [[data objectAtIndex:0] objectForKey:@"id"],@"ID",
                                 [[data objectAtIndex:0] objectForKey:@"title"],@"Name",
                                 [[data objectAtIndex:0] objectForKey:@"updatetime"],@"UpdatedTime",
                                 nil];
            
            NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
            /*存储数据*/
            [[DBManager sharedManager] storeDevelopmentHistoryData:dataTemp Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
        }
        
    }failure:^(NSError *error){
        NSLog(@"History failed : %@", error);
        [self hideLoadingText];
    }];
}

/*
 * 给UIWebView 设置高度 自适应内容
 */


@end
