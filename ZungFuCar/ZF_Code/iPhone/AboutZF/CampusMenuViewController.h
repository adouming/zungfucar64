//
//  CampusMenuViewController.h
//  ZungFuCar
//
//  Created by GuoChengHao on 13-11-24.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
#import "CampusRecruitmentVC.h"

@interface CampusMenuViewController : MenuAbstractViewController

@end
