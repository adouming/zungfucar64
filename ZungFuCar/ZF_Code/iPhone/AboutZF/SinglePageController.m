//
//  SinglePageController.m
//  ZungFuCar
//
//  Created by Ed Lee on 11/29/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "SinglePageController.h"
#import "AboutZFAPIManager.h"
#import "DBManager.h"
#import "NSObject+AddJavascript.h"

#define EMAILBTNW 300
#define ZF_GoldColor  0xba8047
@interface SinglePageController ()

@end

@implementation SinglePageController


- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
	
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    _contentWebView.delegate = self;
    _contentWebView.opaque = NO;
    _contentWebView.backgroundColor = [UIColor clearColor];
    _contentWebView.scrollView.scrollEnabled = NO;
    _contentWebView.dataDetectorTypes = UIDataDetectorTypeNone;
    
    _webViewContainer.backgroundColor = RGBA(255, 255, 255, 0.8);
    _titleLabel.text = _titleStr;
    [self showLoadingText];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EMailCall:) name:@"sendEmail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reserveCall) name:@"reserveCall" object:nil];
}

- (void)reserveCall
{
    [self loadLocalDataToWebviewOfIndex:_localDataIndex];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear");
    /*读取数据 请求数据*/
    //    if (!_isLoadLocalData) {
    //        //[self creatModel];
    //        [self requestData];
    //    } else {
    //        [self loadLocalDataToWebviewOfIndex:_localDataIndex];
    //    }
    [self creatModel];
    [self loadLocalDataToWebviewOfIndex:_localDataIndex];
    [self requestData];
    
    _emailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _emailBtn.frame = CGRectMake((self.view.frame.size.width-EMAILBTNW)/2, 630, EMAILBTNW, 34);
    [_emailBtn setBackgroundImage:[self buttonImageFromColor:HEXRGB(ZF_GoldColor)] forState:UIControlStateNormal];
    [_emailBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:_emailBtn];
    _emailBtn.hidden = YES;
    
    if (_dbMethodTag == 203) { //联系我们加按钮
        [_emailBtn setTitle:@"info@zfchina.com" forState:UIControlStateNormal];
        [_emailBtn addTarget:self action:@selector(emailButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    } else if (_dbMethodTag == 30201) { //打开网页去社会招聘
        [_emailBtn setTitle:@"查看详情" forState:UIControlStateNormal];
        [_emailBtn addTarget:self action:@selector(gotoSocialRecruit) forControlEvents:UIControlEventTouchUpInside];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    _bgScrollView = nil;
    _titleLabel = nil;
    [super viewDidUnload];
}

/**
 * Model
 */
- (void)creatModel
{
    NSArray *array = [[NSArray alloc] init];
    switch (_dbMethodTag) {
        case 200:
            array = [[DBManager sharedManager] getCompanyProfileData];
            break;
        case 201:
            array = [[DBManager sharedManager] getDevelopmentHistoryData];
            break;
        case 202:
            array = [[DBManager sharedManager] getTalentTrainingData];
            break;
        case 203:
            array = [[DBManager sharedManager] getContactUSData];
            break;
        case 400:
            array = [[DBManager sharedManager] getAboutClubData];
            break;
        case 401:
            array = [[DBManager sharedManager] getMemberBookData];
            break;
        case 100:
            array = [[DBManager sharedManager] getRealSeriviceData];
            break;
        case 101:
            array = [[DBManager sharedManager] getFinanceData];
            break;
        case 103:
            array = [[DBManager sharedManager] getBigCustomerData];
            break;
            //        case 30200:
            //            array = [[DBManager sharedManager] getSocietyRecruitmentData];
            //            break;
        case 30201:
            array = [[DBManager sharedManager] getSocietyRecruitmentData];
            break;
            
            //仁孚服务
            //        case 102:
            //            array = [[DBManager sharedManager] getBoutiqueData];
            //            break;
            //        case 2020100:
            //            array = [[DBManager sharedManager] getPartsData];
            //            break;
        case 20001:
            array = [[DBManager sharedManager] getSecondCarData:30];
            break;
        case 20002:
            array = [[DBManager sharedManager] getSecondCarData:31];
            break;
        case 20003:
            array = [[DBManager sharedManager] getSecondCarData:36];
            break;
        case 20004:
            array = [[DBManager sharedManager] getSecondCarData:33];
            break;
        case 20005:
            array = [[DBManager sharedManager] getSecondCarData:35];
            break;
        case 20101:
            array = [[DBManager sharedManager] getInsuranceData];
            break;
        default:
            break;
    }
    
    if([array count] ==0 )
        return;
    
    if([array count] ==0 )
        return;
    NSString *contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
    NSLog(@"web content: %@", contentStr);
}

- (void)isFirstToStoreData:(BOOL)isFirst withData:(NSMutableArray *)data
{
    switch (_dbMethodTag) {
        case 200:
        {
            [[DBManager sharedManager] storeCompanyProfileData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 201:
        {
            //            array = [[DBManager sharedManager] getDevelopmentHistoryData];
            [[DBManager sharedManager] storeDevelopmentHistoryData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 202:
        {
            //            array = [[DBManager sharedManager] getTalentTrainingData];
            [[DBManager sharedManager] storeTalentTrainingData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 203:
        {
            //            array = [[DBManager sharedManager] getContactUSData];
            [[DBManager sharedManager] storeContactUSData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 400:
        {
            //            array = [[DBManager sharedManager] getAboutClubData];
            [[DBManager sharedManager] storeAboutClubData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 401:
        {
            //            array = [[DBManager sharedManager] getMemberBookData];
            [[DBManager sharedManager] storeMemberBookData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 100:
        {
            //            array = [[DBManager sharedManager] getRealSeriviceData];
            [[DBManager sharedManager] storeRealSeriviceData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 101:
        {
            //            array = [[DBManager sharedManager] getFinanceData];
            [[DBManager sharedManager] storeFinanceData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 103:
        {
            //            array = [[DBManager sharedManager] getBigCustomerData];
            [[DBManager sharedManager] storeBigCustomerData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 30201:
        {
            //            array = [[DBManager sharedManager] getSocietyRecruitmentData];
            [[DBManager sharedManager] storeSocietyRecruitmentData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 20001:
        {
            //            array = [[DBManager sharedManager] getFiveStarData];
            //[[DBManager sharedManager] storeSecondCarData:<#(NSMutableArray *)#> cid:<#(int)#> Sucess:<#^(NSMutableArray *storeData)sucess#> fail:<#^(NSError *error)failure#>
            [[DBManager sharedManager] storeSecondCarData:data cid:30 Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 20002:
        {
            //            array = [[DBManager sharedManager] getOneChangeData];
            [[DBManager sharedManager] storeSecondCarData:data cid:31  Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 20003:
        {
            //            array = [[DBManager sharedManager] getXingRuiData];
            [[DBManager sharedManager] storeSecondCarData:data cid:36  Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 20004:
        {
            //            array = [[DBManager sharedManager] getBuyBackData];
            [[DBManager sharedManager] storeSecondCarData:data cid:33  Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 20005:
        {
            //            array = [[DBManager sharedManager] getAnswerQuestionData];
            [[DBManager sharedManager] storeSecondCarData:data cid:35  Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        case 20101:
        {
            //            array = [[DBManager sharedManager] getInsuranceData];
            [[DBManager sharedManager] storeInsuranceData:data Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
            break;
        }
        default:
            break;
    }
}

/**
 *  请求数据
 */
- (void)requestData
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"ViewID%@_UpdatedTime",_viewID]]==nil)
    {
        [self showLoadingText];
    }
    
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    [manager requestWebViewDataWithViewID:_viewID success:^(NSArray *data) {
        BOOL isFirst = NO;
        if([data count]==0)
        {
            [self hideLoadingText];
            return ;
        }
        else
        {
            /*写lastmodifyTime到沙盒*/
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if([userDefaults objectForKey:[NSString stringWithFormat:@"ViewID%@_UpdatedTime",_viewID]]==nil)
                isFirst = YES;
            [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:[NSString stringWithFormat:@"ViewID%@_UpdatedTime",_viewID]];
            [userDefaults synchronize];
            
            /*content url 图片不规范 需要附加http*/
            NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"Content"];
            //NSString *imgReplaceStr = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
            //htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceStr];
            NSLog(@"web content: %@", htmlStr);
            //[_contentWebView loadHTMLString:htmlStr baseURL:nil];
            [self hideLoadingText];
            /*重新封装数据*/
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"BannerURL"],@"BannerURL",
                                 htmlStr,@"Content",
                                 [[data objectAtIndex:0] objectForKey:@"ID"],@"ID",
                                 [[data objectAtIndex:0] objectForKey:@"Name"],@"Name",
                                 [[data objectAtIndex:0] objectForKey:@"UpdatedTime"],@"UpdatedTime",
                                 nil];
            
            NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
            /*存储数据*/
            [self isFirstToStoreData:isFirst withData:dataTemp];
        }
        
    }failure:^(NSError *error){
        [self hideLoadingText];
    }];
}

-(void)loadLocalDataToWebviewOfIndex:(NSInteger)index
{
    if (_localData.count>0) {
        NSString *htmlStr = [[_localData objectAtIndex:index] objectForKey:@"Descp"];
        NSLog(@"web content: %@", htmlStr);
        //[_contentWebView loadHTMLString:htmlStr baseURL:nil];
        [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: htmlStr] baseURL:[[DBManager sharedManager] baseUrl]];
    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    //    [self showLoadingText];
    willLaunchSafari = NO;
}

/*
 * 给UIWebView 设置高度 自适应内容
 */
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    /*获取html body 的内容长度 计算高度*/
    //NSString *height_str= [webView stringByEvaluatingJavaScriptFromString: @"document.body.scrollHeight"];
    //NSLog(@"body height: %@", height_str);
    //int height = [height_str intValue] + 50;
    //webView.frame = CGRectMake(webView.frame.origin.x,webView.frame.origin.y,webView.frame.size.width,height);
    
    /*设置WebView的superView=_bgScrollView的contentSize*/
    //[_bgScrollView setContentSize:CGSizeMake(_bgScrollView.frame.size.width,height)];
    //_webViewContainer.frame = CGRectMake(_webViewContainer.frame.origin.x, _webViewContainer.frame.origin.y, _webViewContainer.frame.size.width, height);
    //[webView sizeToFit];
    //[_bgScrollView setContentSize:CGSizeMake(_bgScrollView.frame.size.width,webView.frame.size.height)];
    NSString *padding = @"document.body.style.margin='5px';document.body.style.padding = '5px'";
    [webView stringByEvaluatingJavaScriptFromString:padding];
    
    [self injectJavascript:webView];
    [webView stringByEvaluatingJavaScriptFromString: @"$('p').css({'color':'#000'});$('a').css({'color':'#000'})"];
    CGRect frame = webView.frame;
    frame.size.height = 1;
    webView.frame = frame;
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    CGFloat contentHeight = frame.size.height+350;
    webView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, contentHeight);
    NSLog(@"%f,%f",frame.size.height, frame.size.width);
    [_bgScrollView setContentSize:CGSizeMake(_bgScrollView.frame.size.width,contentHeight)];
    _webViewContainer.frame = CGRectMake(_webViewContainer.frame.origin.x, _webViewContainer.frame.origin.y, _webViewContainer.frame.size.width, contentHeight);
    
    [self hideLoadingText];
    willLaunchSafari = YES;
    
    if (_dbMethodTag == 203 || _dbMethodTag == 30201) {
        _emailBtn.hidden = NO;
    }
}

/*
 * 点击网页图片,打开相册
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([self webViewImgIsClickedByRequest:request] == 0) {
        UINavigationController *nc = [self getAlbumPhotosFromImgClick:request];
        if (nc != nil) {
            nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:nc animated:YES completion:nil];
        }
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 1 && willLaunchSafari) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 2) {
        return YES;
    }
}


/**
 *	邮箱按钮
 */
- (void)emailButtonClicked
{
    [self EMailCall:@"info@zfchina.com"];
}


#pragma mark - MFMailComposeViewController 发送邮件 系统邮件
/**
 *	contactUS 发送email
 *
 *	@param	email	email地址
 */
-(void)EMailCall:(NSString*)email
{
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet:email];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
}

/**
 *	展示邮箱发送控件UI
 *
 *	@param	email	email地址
 */
-(void)displayComposerSheet:(NSString*)email
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:@""];
	
	
	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:email];
	NSArray *ccRecipients = [NSArray arrayWithObjects:@"", @"", nil];
	NSArray *bccRecipients = [NSArray arrayWithObject:@""];
	
	[picker setToRecipients:toRecipients];
	[picker setCcRecipients:ccRecipients];
	[picker setBccRecipients:bccRecipients];
	
	// Attach an image to the email
	//	NSString *path = [[NSBundle mainBundle] pathForResource:@"rainy" ofType:@"png"];
	//    NSData *myData = [NSData dataWithContentsOfFile:path];
	//	[picker addAttachmentData:myData mimeType:@"image/png" fileName:@"rainy"];
	
	// Fill out the email body text
	NSString *emailBody = @"";
	[picker setMessageBody:emailBody isHTML:NO];
	
	[self presentModalViewController:picker animated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	
	// Notifies users about errors associated with the interface
    NSString *msg;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			DOLOG(@"Result: canceled");
            msg = @"邮件发送取消";
			break;
		case MFMailComposeResultSaved:
			DOLOG(@"Result: saved");
            msg = @"邮件保存成功";
			break;
		case MFMailComposeResultSent:
			DOLOG( @"Result: sent");
            msg = @"邮件发送成功";
			break;
		case MFMailComposeResultFailed:
			DOLOG( @"Result: failed");
            msg = @"邮件发送失败";
			break;
		default:
			DOLOG( @"Result: not sent");
            msg = @"邮件未发送";
			break;
	}
    [self alertWithTitle:nil msg:msg];
	[self dismissModalViewControllerAnimated:YES];
}

/**
 *	Launches the Mail application on the device.
 */
-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=Hello from California!";
	NSString *body = @"&body=It is raining in sunny California!";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

/**
 *	alertView
 *
 *	@param	_title_	标题
 *	@param	msg	内容
 */
- (void) alertWithTitle: (NSString *)_title_ msg: (NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:_title_
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:@"确定"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)gotoSocialRecruit {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://zfchina.com/index.php/list/sjobs"]];
}

- (UIImage *) buttonImageFromColor:(UIColor *)color{
    CGRect rect = CGRectMake(0, 0, EMAILBTNW, 34);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end
