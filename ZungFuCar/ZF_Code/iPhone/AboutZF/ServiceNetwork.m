//
//  ContactUSVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-5.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "ServiceNetwork.h"
#import "MyUILabel.h"
#import "UIImageView+AFNetworking.h"
#import "AFJSONRequestOperation.h"
#import "DBManager.h"
#import "NewsDetailVC.h"

extern int newsCount;
extern int privilegeCount;

@implementation ServiceNetwork
@synthesize selectIndexSwap;

#define LEFTBUTTON          0
#define RIGHTBUTTON         1
#define VIEW_TYPE_LIST      2
#define VIEW_TYPE_PAGE      3
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view bringSubviewToFront:self.customNavigaitionBar];
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    self.customNavigaitionBar.itemType = NavigationItemTypeHomeAndSwitch;
    
    currentIndex = LEFTBUTTON;
    viewType = VIEW_TYPE_PAGE;
    
    int yPos = 99;
    
    UIImageView *buttonBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, yPos, 320, 45)];
    buttonBgView.userInteractionEnabled = YES;
    [self.view addSubview:buttonBgView];
    
    leftButton = [[UIButton alloc]initWithFrame:CGRectMake(-2, 0, 240, 45)];
    [leftButton setTitle:@"服务网络" forState:UIControlStateNormal];
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    leftButton.titleLabel.font = [UIFont systemFontOfSize:25.0f];
    leftButton.tag = LEFTBUTTON;
    [leftButton setTitleColor:RGBA(255, 255, 255, 1.0) forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    [buttonBgView addSubview:leftButton];
    
    
    self.leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    self.rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [buttonBgView addGestureRecognizer:self.leftSwipeGestureRecognizer];
    [buttonBgView addGestureRecognizer:self.rightSwipeGestureRecognizer];
    
    [self getData:8];
    
    hScrollView = [[HorizontalScrollView alloc]initWithFrame:CGRectMake(0, 142, SCREEN_WIDTH, SCREEN_HEIGHT-142)];
    //hScrollView.frame = CGRectMake(0, 142, 270, 250);
    hScrollView.delegate = self;
    
    
    if ([dataSource count]>0) {
        [hScrollView initScrollView:dataSource type:7];
    }
    [self.view addSubview:hScrollView];
    
    newsTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 140, 320, SCREEN_HEIGHT-140) style:UITableViewStylePlain];
    newsTableView.dataSource = self;
    newsTableView.delegate = self;
    newsTableView.backgroundColor = [UIColor clearColor];
    newsTableView.backgroundView = nil;
    newsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    newsTableView.showsVerticalScrollIndicator = NO;
    newsTableView.hidden = YES;
    [self.view addSubview:newsTableView];
    
    if (selectIndexSwap==1) {
        [self clickButton:rightButton];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    newsCount = 0;
    privilegeCount = 0;
    //保存
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:newsCount] forKey:@"newsCount"];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:privilegeCount] forKey:@"privilegeCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)handleSwipes:(UISwipeGestureRecognizer *)sender
{
    if(currentIndex==LEFTBUTTON)
    {
        [self clickButton:rightButton];
        currentIndex = RIGHTBUTTON;
    }
    else if(currentIndex==RIGHTBUTTON)
    {
        [self clickButton:leftButton];
        currentIndex = LEFTBUTTON;
    }
}

-(void)clickButton:(UIButton *)sender{
    int tag = sender.tag;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    if (tag==LEFTBUTTON && currentIndex==RIGHTBUTTON) {
        rightButton.frame = CGRectMake(250, 0, 240, 45);
        leftButton.frame = CGRectMake(-2, 0, 240, 45);
        [rightButton setTitleColor:RGBA(255, 255, 255, 0.5) forState:UIControlStateNormal];
        [leftButton setTitleColor:RGBA(255, 255, 255, 1.0) forState:UIControlStateNormal];
        [self getData:8];
    }else if(tag==RIGHTBUTTON && currentIndex==LEFTBUTTON){
        rightButton.frame = CGRectMake(-2, 0, 240, 45);
        leftButton.frame = CGRectMake(250, 0, 240, 45);
        [leftButton setTitleColor:RGBA(255, 255, 255, 0.5) forState:UIControlStateNormal];
        [rightButton setTitleColor:RGBA(255, 255, 255, 1.0) forState:UIControlStateNormal];
        [self getData:7];
    }
    [UIView commitAnimations];
    currentIndex = tag;
}

-(void)getData:(int)viewId{
    if (viewId==8) {
        dataSource = [[DBManager sharedManager] getNewsListData];
        [self getLeftButtonData];
    }else if(viewId==7){
        dataSource = [[DBManager sharedManager] getModelPrivilegeListData];
        //[self getRightButtonData];
    }
    [newsTableView reloadData];
    [hScrollView clearAllView];
    [hScrollView initScrollView:dataSource type:7];
}

#pragma mark - 获取服务网络数据
-(void)getLeftButtonData{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@%d", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID, 5]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:DEFAULTHttp_X_U forHTTPHeaderField:@"Http-X-U"];
    [request addValue:DEFAULTHttp_X_P forHTTPHeaderField:@"Http-X-P"];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"App.net Global Stream: %@", JSON);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ([[JSON valueForKey:@"list"] count] > 0 && currentIndex==LEFTBUTTON) {
            dataSource = [JSON valueForKey:@"list"];
            if ([dataSource count]>0) {
                [newsTableView reloadData];
                [hScrollView clearAllView];
                [hScrollView initScrollView:dataSource type:7];
            }
            [[DBManager sharedManager] storeNewsListData:dataSource Sucess:nil fail:nil];
        }
    } failure:nil];
    [operation start];
}


#pragma mark - Tableview datasource & delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150.0f;
    
}

+ (NSString *)getTimeToShowWithTimestamp:(NSString *)timestamp

{
    double publishLong = [timestamp doubleValue];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    [formatter setDateFormat:@"yyyy.MM.dd HH:mm:ss"];
    
    
    
    NSDate *publishDate = [NSDate dateWithTimeIntervalSince1970:publishLong];
    
    
    
    NSDate *date = [NSDate date];
    
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    
    NSInteger interval = [zone secondsFromGMTForDate:date];
    
    publishDate = [publishDate  dateByAddingTimeInterval: interval];
    
    NSString *publishString = [formatter stringFromDate:publishDate];
    
    NSLog(@"date is %@", publishString);
    return publishString;
    //return @"abc";
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell_list";
    int nRow = indexPath.row;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSDictionary *dic = [dataSource objectAtIndex:nRow];
    NSString *bannerURL = [NSString stringWithFormat:@"%@/%@", ZF_BASE_URL, [dic objectForKey:@"dimg"] ];
    NSString *time = [ServiceNetwork getTimeToShowWithTimestamp: [dic objectForKey:@"updatetime"]];
    if (time.length>11) {
        time = [time substringToIndex:10];
    }
    NSString *name = [dic objectForKey:@"title"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIImageView *listBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 148)];
        listBgView.image = [UIImage imageNamed:@"latestnews_listbg"];
        [cell.contentView addSubview:listBgView];
        
        UIImageView *bannerImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 275, 330)];
        [bannerImageView setImageWithURL:[NSURL URLWithString:bannerURL] placeholderImage: [UIImage imageNamed:@"loading.png"]];
        bannerImageView.contentMode = UIViewContentModeScaleAspectFill;
        bannerImageView.clipsToBounds = YES;
        bannerImageView.tag = 200;
        [cell.contentView addSubview:bannerImageView];
        
        UIImageView *titleBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 95, 320, 50)];
        titleBgView.image = [UIImage imageNamed:@"title_list_bg"];
        [cell.contentView addSubview:titleBgView];
        
        UIImageView *bgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 160, 275, 170)];
        //bgView.image = [UIImage imageNamed:@"latestnews_textbg.png"];
        //[cell.contentView addSubview:bgView];
        
        MyUILabel *titleLabel = [[MyUILabel alloc]initWithFrame:CGRectMake(10, 260, 255, 50)];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.textAlignment = NSTextAlignmentRight;
        titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        titleLabel.text = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        titleLabel.tag = 202;
        titleLabel.numberOfLines = 2;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping|NSLineBreakByTruncatingTail;
        titleLabel.verticalAlignment = VerticalAlignmentTop;
        [cell.contentView addSubview:titleLabel];
        
        UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 300, 255, 12)];
        dateLabel.textAlignment = NSTextAlignmentRight;
        dateLabel.text = time;
        dateLabel.font = [UIFont boldSystemFontOfSize:11.0f];
        dateLabel.textColor = HEXRGB(0x444444);
        dateLabel.tag = 201;
        [cell.contentView addSubview:dateLabel];
        
        listBgView.hidden = NO;
        titleBgView.hidden = NO;
        bannerImageView.frame = CGRectMake(10, 10, 300, 120);
        titleLabel.frame = CGRectMake(95, 100, 200, 20);
        titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
        dateLabel.frame = CGRectMake(25, 120, 270, 15);
    }else{
        [((UIImageView *)[cell.contentView viewWithTag:200]) setImageWithURL:[NSURL URLWithString:bannerURL] placeholderImage:nil];
        ((UILabel *)[cell.contentView viewWithTag:201]).text = time;
        ((MyUILabel *)[cell.contentView viewWithTag:202]).text = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = indexPath.row;
    [self pushToDetail:row];
}

-(void)pushToDetail:(int )index{
    NSLog(@"pushToDetail:%d", index);
    NSDictionary *dic = [dataSource objectAtIndex:index];
    NSString* descp =[NSString stringWithFormat: @"%@<br/><div style='background: #222 none repeat scroll 0 0;color: white;height: auto;opacity: 0.8;padding: 10px 10px 10px 10px' id='vdd357' class='storedetail'><div class='storeinfo'><p id='storename'> %@ </p><p id='storeadd' style='height: 34px;margin-bottom: 8px;margin-top: 5px;padding: 5px;'>%@ </p><p>%@</p></div></div>", dic[@"content"],dic[@"tel"],dic[@"addr"],dic[@"jszc"] ];
    //NSString *descp = [NSString stringWithFormat:@"%@<br/>电话：%@<br/>地址: %@<br/> %@", dic[@"content"],dic[@"tel"],dic[@"addr"],dic[@"jszc"]];
    NSString* dimg = [dic objectForKey:@"dimg"];
    NSString *bannerURL = [NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH, dimg];
    NSString *time = [ServiceNetwork getTimeToShowWithTimestamp: [dic objectForKey:@"updatetime"]];
    NSString *name = [dic objectForKey:@"title"];
    NSString *address = [dic objectForKey:@"Address"];
    NSString *shortDesc = [dic objectForKey:@"info"];
    NSString *bannerName = [dic objectForKey:@"source"];
    NewsDetailVC *newsDetailView = [[NewsDetailVC alloc]initWithTitle:name bannerUrl:bannerURL date:time descp:descp address:address shortDesc:shortDesc bannerName:bannerName];
    [self.navigationController pushViewController:newsDetailView animated:YES];
}

#pragma mark - navigationBarDelegate
-(void)navigationBarDidPressAtIndex:(NSInteger)index{
    switch (index) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
            if (viewType==VIEW_TYPE_PAGE) {
                viewType = VIEW_TYPE_LIST;
                newsTableView.hidden = NO;
                hScrollView.hidden = YES;
            }else{
                viewType = VIEW_TYPE_PAGE;
                newsTableView.hidden = YES;
                hScrollView.hidden = NO;
            }
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
