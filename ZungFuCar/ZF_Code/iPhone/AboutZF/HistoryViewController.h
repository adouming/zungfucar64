//
//  HistoryViewController.h
//  ZungFuCar
//
//  Created by GuoChengHao on 13-11-17.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "CompanyProfileVC.h"

@interface HistoryViewController : CompanyProfileVC <UIWebViewDelegate>

@end
