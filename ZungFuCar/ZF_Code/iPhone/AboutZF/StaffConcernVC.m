//
//  StaffConcernVC.m
//  ZungFuCar
//
//  Created by adouming on 15/6/17.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "StaffConcernVC.h"
#import "AboutZFAPIManager.h"
#import "DBManager.h"
#import "NSString+FilterHtml.h"
@interface StaffConcernVC ()

@end

@implementation StaffConcernVC
/**
 * Model
 */
- (void)creatModel
{
    NSArray *array = [[DBManager sharedManager] getStaffConcernData];
    if([array count] ==0 )
        return;
    NSString *contentStr = @"";
    for (NSDictionary* obj in [[array objectAtIndex:0] objectForKey:@"list"]) {
        NSString* rowStr = [NSString stringWithFormat:@"%@%@%@", [obj objectForKey:@"title"], @"<hr>",  [obj objectForKey:@"content"]];
        contentStr = [NSString stringWithFormat:@"%@%@", contentStr, rowStr];
    }
    //NSString *contentStr = [[[[array objectAtIndex:0] objectForKey:@"list"] objectAtIndex:0] objectForKey:@"content"];
    [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
//    NSString *bannerUrl = [[array objectAtIndex:0] objectForKey:@"BannerURL"];
//    if([bannerUrl validatePng] &&![bannerUrl isEqualToString:@""])
//        [_bannerView setImageWithURL:[NSURL URLWithString:[[array objectAtIndex:0] objectForKey:@"BannerURL"]]];
}

/**
 *  请求数据
 */
- (void)requestData
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID6_UpdatedTime"]==nil)
    {
        [self showLoadingText];
    }
    
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    [manager requestStaffConcernDataSuccess:^(NSArray *data) {
        BOOL isFirst = NO;
        if([data count]==0)
        {
            return ;
        }
        else
        {
            /*写lastmodifyTime到沙盒*/
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if([userDefaults objectForKey:@"ViewID6_UpdatedTime"]==nil)
                isFirst = YES;
            //[userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID4_UpdatedTime"];
            //[userDefaults synchronize];
            
            /*content url 图片不规范 需要附加http*/
            NSDictionary *list = [[data objectAtIndex:0] objectForKey:@"list"];
//            NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
//            htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
            [self hideLoadingText];
            
            
            /*重新封装数据*/
            NSDictionary *dic =  @{@"list":list};
            
            NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
            /*存储数据*/
            [[DBManager sharedManager] storeStaffConcernData:dataTemp Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
        }
        
    }failure:^(NSError *error){
        NSLog(@"History failed : %@", error);
        [self hideLoadingText];
    }];
}


@end
