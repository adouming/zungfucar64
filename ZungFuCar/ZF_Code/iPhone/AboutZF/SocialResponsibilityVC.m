//
//  SocialResponsibilityVCViewController.m
//  ZungFuCar
//
//  Created by adouming on 15/6/17.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "SocialResponsibilityVC.h"
#import "AboutZFAPIManager.h"
#import "DBManager.h"
#import "NSObject+AddJavascript.h"
#import "NSString+FilterHtml.h"
@interface SocialResponsibilityVC ()

@end

@implementation SocialResponsibilityVC
/**
 * Model
 */
- (void)creatModel
{
    NSArray *array = [[DBManager sharedManager] getSocialResponsibilityData];
    if([array count] ==0 )
        return;
    NSString *contentStr = @"";
    for (NSDictionary* obj in [[array objectAtIndex:0] objectForKey:@"list"]) {
        NSString* rowStr = [NSString stringWithFormat:@"%@%@%@", [obj objectForKey:@"title"], @"<hr>",  [obj objectForKey:@"content"]];
        contentStr = [NSString stringWithFormat:@"%@%@", contentStr, rowStr];
    }
    //NSString *contentStr = [[[[array objectAtIndex:0] objectForKey:@"list"] objectAtIndex:0] objectForKey:@"content"];
    [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
    //    NSString *bannerUrl = [[array objectAtIndex:0] objectForKey:@"BannerURL"];
    //    if([bannerUrl validatePng] &&![bannerUrl isEqualToString:@""])
    //        [_bannerView setImageWithURL:[NSURL URLWithString:[[array objectAtIndex:0] objectForKey:@"BannerURL"]]];
}

/**
 *  请求数据
 */
- (void)requestData
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID4_UpdatedTime"]==nil)
    {
        [self showLoadingText];
    }
    
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    [manager requestSocialResponsibilitySuccess:^(NSArray *data) {
        BOOL isFirst = NO;
        if([data count]==0)
        {
            return ;
        }
        else
        {
            /*写lastmodifyTime到沙盒*/
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if([userDefaults objectForKey:@"ViewID4_UpdatedTime"]==nil)
                isFirst = YES;
            //[userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID4_UpdatedTime"];
            //[userDefaults synchronize];
            
            /*content url 图片不规范 需要附加http*/
            NSDictionary *list = [[data objectAtIndex:0] objectForKey:@"list"];
            //            NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
            //            htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
            [self hideLoadingText];
            
            
            /*重新封装数据*/
            NSDictionary *dic =  @{@"list":list};
            
            NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
            /*存储数据*/
            [[DBManager sharedManager] storeStaffConcernData:dataTemp Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
        }
        
    }failure:^(NSError *error){
        NSLog(@"History failed : %@", error);
        [self hideLoadingText];
    }];
}
/*
 * 给UIWebView 设置高度 自适应内容
 */
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self injectJavascript:webView];
    
    NSString *padding = @"document.body.style.margin='5px';document.body.style.padding = '5px'";
    [webView stringByEvaluatingJavaScriptFromString:padding];
    
    /*获取html body 的内容长度 计算高度*/
    NSString *height_str= [webView stringByEvaluatingJavaScriptFromString: @"document.body.scrollHeight"];
    NSLog(@"body height: %@", height_str);
    int height = [height_str intValue];
    webView.frame = CGRectMake(webView.frame.origin.x,webView.frame.origin.y,webView.frame.size.width,height);
    
    /*设置WebView的superView=_bgScrollView的contentSize*/
    [_bgScrollView setContentSize:CGSizeMake(_bgScrollView.frame.size.width,CGRectGetMaxY(webView.frame))];
    willLaunchSafari = YES;
}
/*
 * 点击网页图片,打开相册
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([self webViewImgIsClickedByRequest:request] == 0) {
        UINavigationController *nc = [self getAlbumPhotosFromImgClick:request];
        if (nc != nil) {
            nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:nc animated:YES completion:nil];
        }
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 1 && willLaunchSafari) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 2) {
        return YES;
    }
}

@end
