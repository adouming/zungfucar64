//
//  SinglePageController.h
//  ZungFuCar
//
//  Created by Ed Lee on 11/29/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface SinglePageController : UIViewController <UIWebViewDelegate,UIScrollViewDelegate,MFMailComposeViewControllerDelegate> {
    BOOL willLaunchSafari;
}

@property (strong, nonatomic) IBOutlet UIWebView *contentWebView;
@property (strong, nonatomic) IBOutlet UIScrollView *bgScrollView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) NSString *titleStr;
@property (strong, nonatomic) IBOutlet UIView *webViewContainer;

@property (strong, nonatomic) NSString *viewID;

@property BOOL isLoadLocalData;
@property NSInteger localDataIndex;
@property (strong, nonatomic) NSArray *localData;
@property NSInteger dbMethodTag;
@property (strong, nonatomic) UIButton *emailBtn;

@end
