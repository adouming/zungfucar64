//
//  AboutZFController.m
//  ZungFuCar
//
//  Created by Ed Lee on 11/29/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AboutZFController.h"

@interface AboutZFController ()

@end

@implementation AboutZFController
@synthesize contentNaviCtrl;

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    CGFloat naviCtrlWidth;
    if (_sidebarMenuTitles.count>0) {
        _sidebar = [[SideBar alloc]init];
        [self.view addSubview:_sidebar];
        _sidebar.delegate = self;
        [_sidebar addButtonsForSidebar:_sidebarMenuTitles fromTopIndex:_topIndex andSubIndex:_subIndex];
        naviCtrlWidth = self.view.frame.size.width - _sidebar.frame.size.width;
    } else {
        naviCtrlWidth = self.view.frame.size.width;
    }
    
    _bgImageView.image = [UIImage imageNamed:_bgImageName];
    
    if (IOS7) {
        UIView *statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 20)];
        statusView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:statusView];
    }
    
    UIViewController *vc = [[UIViewController alloc]init];
    
    self.contentNaviCtrl = [[UINavigationController alloc] initWithRootViewController:vc];
    self.contentNaviCtrl.view.frame = CGRectMake(_sidebar.frame.size.width, IOS7?20:0, naviCtrlWidth, self.view.frame.size.height-(IOS7?70:50));
    self.contentNaviCtrl.navigationBarHidden = YES;
    
    [self.view addSubview:self.contentNaviCtrl.view];
    
    self.contentNaviCtrl.view.backgroundColor = [UIColor clearColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goHome) name:@"GOHOME" object:nil];
}

- (void) goHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    SinglePageController *spCtrl = [[SinglePageController alloc]initWithNibName:@"SinglePageController" bundle:nil];
    spCtrl.titleStr = [[self subMenuTitlesOfIndex:_topIndex] objectAtIndex:_subIndex];
    
    if (_topIndex == 2) { //关于仁孚
        switch (_subIndex) {
            case 0:
                spCtrl.viewID = @"146";
                spCtrl.dbMethodTag = 200;
                [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
                break;
            case 1:
                spCtrl.viewID = @"4";
                spCtrl.dbMethodTag = 201;
                [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
                break;
            case 2:
            {
                spCtrl.viewID = @"148";
                spCtrl.dbMethodTag = 202;
                spCtrl.titleStr = [[self thirdLevelMenuOfTopIndex:2 andSubIndex:_subIndex] objectAtIndex:0];
                
                // Add fourth level menu
                _xiaoYuanData = [self creatASModel:@"校园"];
                if(_xiaoYuanData.count>0) {
                    [_sidebar addFourthLevelFromData:_xiaoYuanData atSideBtnIndex:2];
                }
                
                [self requestData:@"校园" completion:^(NSArray *titleData) {
                    NSLog(@"%@",titleData[0]);
                    [_sidebar addFourthLevelFromData:titleData atSideBtnIndex:2];
                    _xiaoYuanData = titleData;
                }];
                
                [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
                break;
            }
            case 3:
                spCtrl.viewID = @"182";
                spCtrl.dbMethodTag = 203;
                [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
                break;
                /*
                 case 4:
                 spCtrl.viewID = @"146";
                 break;
                 */
            default:
                break;
        }
    } else if (_topIndex == 4) { //仁孚会
        switch (_subIndex) {
            case 2:
                spCtrl.viewID = @"183";
                spCtrl.dbMethodTag = 400;
                [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
                break;
            case 1:
                spCtrl.viewID = @"181";
                spCtrl.dbMethodTag = 401;
                [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
                break;
            case 3:
            {
                // push login and register
                
                break;
            }
            default:
                break;
        }
    } else if (_topIndex == 1) { // 仁孚服务
        _jingpinData = [[NSArray alloc]init];
        _peijianData = [[NSArray alloc]init];
        switch (_subIndex) {
            case 0:
            {
                spCtrl.viewID = @"177";
                spCtrl.dbMethodTag = 100;
                spCtrl.titleStr = [[self thirdLevelMenuOfTopIndex:1 andSubIndex:_subIndex] objectAtIndex:0];
                [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
                break;
            }
                
            case 1:
            {
                spCtrl.viewID = @"184";
                spCtrl.dbMethodTag = 101;
                spCtrl.titleStr = [[self thirdLevelMenuOfTopIndex:1 andSubIndex:_subIndex] objectAtIndex:0];
                [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
                break;
            }
                
            case 2:
            {
                spCtrl.dbMethodTag = 102;
                _jingpinData = [self creatASModel:@"精品"];
                
                if(_jingpinData.count>0) {
                    [_sidebar addFourthLevelFromData:_jingpinData atSideBtnIndex:0];
                    spCtrl.titleStr = [[_jingpinData objectAtIndex:0] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _jingpinData;
                    spCtrl.localDataIndex = 0;
                }
                
                [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
                
                [self requestData:@"精品" completion:^(NSArray *titleData) {
                    [_sidebar addFourthLevelFromData:titleData atSideBtnIndex:0];
                    _jingpinData = titleData;
                    spCtrl.titleStr = [[_jingpinData objectAtIndex:0] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _jingpinData;
                    spCtrl.localDataIndex = 0;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"reserveCall" object:nil];
                }];
                
                _peijianData = [self creatASModel:@"配件"];
                if(_peijianData.count>0) {
                    [_sidebar addFourthLevelFromData:_peijianData atSideBtnIndex:1];
                }
                [self requestData:@"配件" completion:^(NSArray *titleData) {
                    [_sidebar addFourthLevelFromData:titleData atSideBtnIndex:1];
                    _peijianData = titleData;
                }];
                
                break;
            }
                
            case 3:
            {
                spCtrl.viewID = @"16";
                spCtrl.dbMethodTag = 103;
                [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
                
                break;
            }
            default:
                break;
        }
    } else if (_topIndex == 5) {
        switch (_subIndex) {
            case 1:
            {
                if (reserveCtl==nil) {
                    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Steward_iPad" bundle:nil];
                    reserveCtl = [storyBoard instantiateViewControllerWithIdentifier:@"ReserveFormVCID"];
                }
                reserveCtl.reserveType = ReserveTypeService;
                reserveCtl.reserverServerType = ReserveTypeServiceTypeConst;
                //reserveCtl.isExtend2 = YES;
                [self.contentNaviCtrl pushViewController:reserveCtl animated:NO];
                break;
            }
                
            case 2:
            {
                if (reserveCtl==nil) {
                    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Steward_iPad" bundle:nil];
                    reserveCtl = [storyBoard instantiateViewControllerWithIdentifier:@"ReserveFormVCID"];
                }
                reserveCtl.reserveType = ReserveTypeTest;
                reserveCtl.isExtend2 = NO;
                //                reserveCtl.reserverServerType = ReserveTypeServiceTypeConst;
                [self.contentNaviCtrl pushViewController:reserveCtl animated:NO];
                
                break;
            }
            case 3:
            {
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Steward_iPad" bundle:nil];
                RoadRescueViewController *roadCtl = [storyBoard instantiateViewControllerWithIdentifier:@"RoadRescueVCID"];
                [self.contentNaviCtrl pushViewController:roadCtl animated:NO];
            }
            default:
                break;
        }
    }
    
    self.customNavigaitionBar.rightBtn.hidden = YES;
    [self.view bringSubviewToFront:self.customNavigaitionBar];
}

#pragma mark - Side Bar Delegate Method
-(void)sideBarIsExpanded:(NSString *)expanded {
    CGPoint naviSlideToX;
    if ([expanded isEqual:@"yes"]) {
        naviSlideToX = CGPointMake(_sidebar.frame.size.width, 0);
    } else {
        naviSlideToX = CGPointMake(16, 0);
    }
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.contentNaviCtrl.view.frame = CGRectMake(naviSlideToX.x, IOS7?20:0, self.view.frame.size.width-naviSlideToX.x, self.contentNaviCtrl.view.frame.size.height);
                         if (calculatorCtl) {
                             if (naviSlideToX.x==16) {
                                 //[calculatorCtl scanCalculVCViewFrame:NO];
                             }else{
                                 //[calculatorCtl scanCalculVCViewFrame:YES];
                             }
                         }
                         if (reserveCtl) {
                             if (naviSlideToX.x==16) {
                                 [reserveCtl scanCalculVCViewFrame:NO];
                             }else{
                                 [reserveCtl scanCalculVCViewFrame:YES];
                             }
                         }
                     }
                     completion:nil];
}

-(void)sideBarButtonIsClickedWithTag:(NSNumber *)tag
{
    if (_topIndex == 5) {
        [self.contentNaviCtrl popToRootViewControllerAnimated:NO];
        switch ([tag integerValue]) {
            case 60100:
            {
                if (reserveCtl==nil) {
                    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Steward_iPad" bundle:nil];
                    reserveCtl = [storyBoard instantiateViewControllerWithIdentifier:@"ReserveFormVCID"];
                }
                reserveCtl.reserveType = ReserveTypeService;
                reserveCtl.reserverServerType = ReserveTypeServiceTypeConst;
                reserveCtl.isExtend2 = YES;
                [self.contentNaviCtrl pushViewController:reserveCtl animated:NO];
                break;
            }
            case 60101:
            {
                if (reserveCtl==nil) {
                    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Steward_iPad" bundle:nil];
                    reserveCtl = [storyBoard instantiateViewControllerWithIdentifier:@"ReserveFormVCID"];
                }
                reserveCtl.reserveType = ReserveTypeTest;
                reserveCtl.isExtend2 = YES;
                //                reserveCtl.reserverServerType = ReserveTypeServiceTypeConst;
                [self.contentNaviCtrl pushViewController:reserveCtl animated:NO];
                break;
            }
            default:
                break;
        }
    } else if (_topIndex == 1) {
        [self.contentNaviCtrl popViewControllerAnimated:NO];
        if ([tag integerValue] == 20102) {
            if (calculatorCtl==nil) {
                UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"ZFService_iPad" bundle:nil];
                calculatorCtl = [storyBoard instantiateViewControllerWithIdentifier:@"CalculatorVCID"];
                calculatorCtl.isExtend2 = YES;
            }
            [self.contentNaviCtrl pushViewController:calculatorCtl animated:NO];
        }
        else {
            SinglePageController *spCtrl = [[SinglePageController alloc]initWithNibName:@"SinglePageController" bundle:nil];
            NSArray *bigTitleArray = [self thirdLevelMenuOfTopIndex:_topIndex andSubIndex:_subIndex];
            NSInteger tagInt =  [tag integerValue];
            spCtrl.dbMethodTag = tagInt;
            switch (tagInt) {
                    //仁孚服务
                case 20000:
                    spCtrl.viewID = @"177";
                    spCtrl.titleStr = bigTitleArray[0];
                    break;
                case 20001:
                    spCtrl.viewID = @"178";
                    spCtrl.titleStr = bigTitleArray[1];
                    break;
                case 20002:
                    spCtrl.viewID = @"127";
                    spCtrl.titleStr = bigTitleArray[2];
                    break;
                case 20003:
                    spCtrl.viewID = @"126";
                    spCtrl.titleStr = bigTitleArray[3];
                    break;
                case 20004:
                    spCtrl.viewID = @"131";
                    spCtrl.titleStr = bigTitleArray[4];
                    break;
                case 20005:
                    spCtrl.viewID = @"179";
                    spCtrl.titleStr = bigTitleArray[5];
                    break;
                case 20100:
                    spCtrl.viewID = @"184";
                    spCtrl.titleStr = bigTitleArray[0];
                    break;
                case 20101:
                    spCtrl.viewID = @"136";
                    spCtrl.titleStr = bigTitleArray[1];
                    break;
                    
                    //精品:手表时钟, 箱包, 服饰, 生活, 户外
                case 2020000:
                    spCtrl.titleStr = [[_jingpinData objectAtIndex:0] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _jingpinData;
                    spCtrl.localDataIndex = 0;
                    break;
                case 2020001:
                    spCtrl.titleStr = [[_jingpinData objectAtIndex:1] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _jingpinData;
                    spCtrl.localDataIndex = 1;
                    break;
                case 2020002:
                    spCtrl.titleStr = [[_jingpinData objectAtIndex:2] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _jingpinData;
                    spCtrl.localDataIndex = 2;
                    break;
                case 2020003:
                    spCtrl.titleStr = [[_jingpinData objectAtIndex:3] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _jingpinData;
                    spCtrl.localDataIndex = 3;
                    break;
                case 2020004:
                    spCtrl.titleStr = [[_jingpinData objectAtIndex:4] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _jingpinData;
                    spCtrl.localDataIndex = 4;
                    break;
                    
                case 2020100:
                    spCtrl.titleStr = [[_peijianData objectAtIndex:0] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _peijianData;
                    spCtrl.localDataIndex = 0;
                    break;
                case 2020101:
                    spCtrl.titleStr = [[_peijianData objectAtIndex:1] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _peijianData;
                    spCtrl.localDataIndex = 1;
                    break;
                case 2020102:
                    spCtrl.titleStr = [[_peijianData objectAtIndex:2] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _peijianData;
                    spCtrl.localDataIndex = 2;
                    break;
                case 2020103:
                    spCtrl.titleStr = [[_peijianData objectAtIndex:3] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _peijianData;
                    spCtrl.localDataIndex = 3;
                    break;
                case 2020104:
                    spCtrl.titleStr = [[_peijianData objectAtIndex:4] objectForKey:@"Name"];
                    spCtrl.isLoadLocalData = YES;
                    spCtrl.localData = _peijianData;
                    spCtrl.localDataIndex = 4;
                    break;
                default:
                    break;
            }
            [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
        }
    } else if (_topIndex == 2) {
        [self.contentNaviCtrl popViewControllerAnimated:NO];
        SinglePageController *spCtrl = [[SinglePageController alloc]initWithNibName:@"SinglePageController" bundle:nil];
        NSArray *bigTitleArray = [self thirdLevelMenuOfTopIndex:_topIndex andSubIndex:_subIndex];
        NSInteger tagInt =  [tag integerValue];
        spCtrl.dbMethodTag = tagInt;
        switch (tagInt) {
            case 30200:
                spCtrl.viewID = @"148";
                spCtrl.titleStr = bigTitleArray[0];
                break;
            case 30201:
                spCtrl.viewID = @"185";
                spCtrl.titleStr = bigTitleArray[1];
                break;
            case 30202:
                spCtrl.titleStr = [[_xiaoYuanData objectAtIndex:0] objectForKey:@"Name"];
                spCtrl.isLoadLocalData = YES;
                spCtrl.localData = _xiaoYuanData;
                spCtrl.localDataIndex = 0;
                break;
            case 3020200:
                spCtrl.titleStr = [[_xiaoYuanData objectAtIndex:0] objectForKey:@"Name"];
                spCtrl.isLoadLocalData = YES;
                spCtrl.localData = _xiaoYuanData;
                spCtrl.localDataIndex = 0;
                break;
            case 3020201:
                spCtrl.titleStr = [[_xiaoYuanData objectAtIndex:1] objectForKey:@"Name"];
                spCtrl.isLoadLocalData = YES;
                spCtrl.localData = _xiaoYuanData;
                spCtrl.localDataIndex = 1;
                break;
            case 3020202:
                spCtrl.titleStr = [[_xiaoYuanData objectAtIndex:2] objectForKey:@"Name"];
                spCtrl.isLoadLocalData = YES;
                spCtrl.localData = _xiaoYuanData;
                spCtrl.localDataIndex = 2;
                break;
            case 3020203:
                spCtrl.titleStr = [[_xiaoYuanData objectAtIndex:3] objectForKey:@"Name"];
                spCtrl.isLoadLocalData = YES;
                spCtrl.localData = _xiaoYuanData;
                spCtrl.localDataIndex = 3;
                break;
            case 3020204:
                spCtrl.titleStr = [[_xiaoYuanData objectAtIndex:4] objectForKey:@"Name"];
                spCtrl.isLoadLocalData = YES;
                spCtrl.localData = _xiaoYuanData;
                spCtrl.localDataIndex = 4;
                break;
            default:
                break;
        }
        [self.contentNaviCtrl pushViewController:spCtrl animated:NO];
    }
}

@end
