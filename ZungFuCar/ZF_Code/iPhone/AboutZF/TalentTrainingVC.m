//
//  TalentTrainingVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-3.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "TalentTrainingVC.h"
#import "AboutZFAPIManager.h"
#import "DBManager.h"
#import "NSString+FilterHtml.h"
@implementation TalentTrainingVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    /**
     *	非二级菜单显示按钮
     */
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    [_titleLabel setText:@"人才培训和发展"] ;
    [_contentWebView setDelegate:self];
}

/**
 * Model
 */
- (void)creatModel
{
    /*获取数据*/
    NSArray *array = [[DBManager sharedManager] getTalentTrainingData];
    if([array count] ==0 )
        return;
    /*content */
    NSString *contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
    /*banner */
    NSString *bannerUrl = [[array objectAtIndex:0] objectForKey:@"BannerURL"];
    if([bannerUrl validatePng]&&![bannerUrl isEqualToString:@""])
        [_bannerView setImageWithURL:[NSURL URLWithString:[[array objectAtIndex:0] objectForKey:@"BannerURL"]]];
}

/**
 * 请求数据
 */
- (void)requestData
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID148_UpdatedTime"]==nil)
    {
        [self showLoadingText];
    }
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    [manager requestTalentTrainingDataSuccess:^(NSArray *data) {
        BOOL isFirst = NO;
        if([data count]==0)
        {
            return ;
        }
        else
        {
            /*写lastmodifyTime到沙盒*/
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if([userDefaults objectForKey:@"ViewID148_UpdatedTime"]==nil)
                isFirst = YES;
            [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID148_UpdatedTime"];
            [userDefaults synchronize];
            
            /*解析content 拼接URL*/
            NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"Content"];
//            NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
//            htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
            [self hideLoadingText];
            /*重新封装*/
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"BannerURL"],@"BannerURL",
                                 htmlStr,@"Content",
                                 [[data objectAtIndex:0] objectForKey:@"ID"],@"ID",
                                 [[data objectAtIndex:0] objectForKey:@"Name"],@"Name",
                                 [[data objectAtIndex:0] objectForKey:@"UpdatedTime"],@"UpdatedTime",
                                 nil];
            
            /*归档*/
            NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
            [[DBManager sharedManager] storeTalentTrainingData:dataTemp Sucess:^(NSMutableArray *storeData) {
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
        }
        
    }failure:^(NSError *error){
        [self hideLoadingText];
    }];
}

@end
