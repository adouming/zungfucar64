//
//  ICETutorialController.m
//
//
//  Created by Patrick Trillsam on 25/03/13.
//  Copyright (c) 2013 Patrick Trillsam. All rights reserved.
//

#import "ICETutorialController.h"
#import <QuartzCore/QuartzCore.h>

@interface ICETutorialController ()

@end

@implementation ICETutorialController
@synthesize autoScrollEnabled = _autoScrollEnabled;
@synthesize autoScrollLooping = _autoScrollLooping;
@synthesize autoScrollDurationOnPage = _autoScrollDurationOnPage;
@synthesize commonPageSubTitleStyle = _commonPageSubTitleStyle;
@synthesize commonPageDescriptionStyle = _commonPageDescriptionStyle;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        _autoScrollEnabled = YES;
        _autoScrollLooping = YES;
        _autoScrollDurationOnPage = TUTORIAL_DEFAULT_DURATION_ON_PAGE;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
             andPages:(NSArray *)pages{
    self = [self initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        _pages = pages;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
                pages:(NSArray *)pages
         button1Block:(ButtonBlock)block1
         button2Block:(ButtonBlock)block2{
    self = [self initWithNibName:nibNameOrNil bundle:nibBundleOrNil andPages:pages];
    if (self){
        _button1Block = block1;
        _button2Block = block2;
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    UIImage *bgImage=nil;
    bgImage = [UIImage imageNamed:@"history_bg_iphone4.png"];
    CGRect frame = CGRectMake(0, 0, bgImage.size.width, bgImage.size.height);
	UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:frame];
	backgroundImageView.image = bgImage;
    [self.backgroundImageView setImage:bgImage];
    
    _windowSize = [[UIScreen mainScreen] bounds].size;
    
    // ScrollView configuration.
    [_scrollView setContentSize:CGSizeMake([self numberOfPages] * _windowSize.width,
                                           _scrollView.contentSize.height)];
    
    [_scrollView setPagingEnabled:YES];
    [_scrollView setShowsVerticalScrollIndicator:NO];
    [_scrollView setShowsHorizontalScrollIndicator:NO];
    [_scrollView setTag:noDisableVerticalScrollTag];
    // PageControl configuration.
    [_pageControl setNumberOfPages:[self numberOfPages]];
    [_pageControl setCurrentPage:-1];
    [_pageControl setHidden:YES];
    [_scrollView setContentOffset:CGPointMake(-1 * _windowSize.width,0)
                         animated:YES];
    //[_scrollView setBackgroundColor:[UIColor clearColor]];
    //[self.view bringSubviewToFront:_scrollView];
    //
    
    // Overlays.
    [self setOverlayTexts];
    
    // Preset the origin state.
    [self setOriginLayersState];

    // Run the auto-scrolling.
    [self autoScrollToNextPage];
    _currentPageIndex = -1;
    [self overlayStarwithIndex];

    /**
     *	非二级菜单显示按钮
     */
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;

    UIView *bgView  = [[UIView alloc] initWithFrame:self.view.bounds];
    bgView.tag =111;
    [bgView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:bgView];
}

/**
 *	隐藏右边按钮
 */
-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions
- (void)setButton1Block:(ButtonBlock)block{
    _button1Block = block;
}

- (void)setButton2Block:(ButtonBlock)block{
    _button2Block = block;
}

- (IBAction)didClickOnButton1:(id)sender{
    if (_button1Block)
        _button1Block(sender);
}

- (IBAction)didClickOnButton2:(id)sender{
    if (_button2Block)
        _button2Block(sender);
}

- (IBAction)didClickOnPageControl:(UIPageControl *)sender {
    _currentState = ScrollingStateManual;
    
    // Make the scrollView animation.
    [_scrollView setContentOffset:CGPointMake(sender.currentPage * _windowSize.width,0)
                         animated:YES];
    
    // Set the PageControl on the right page.
    [_pageControl setCurrentPage:sender.currentPage];
}

#pragma mark - Pages
// Set the list of pages (ICETutorialPage)
- (void)setPages:(NSArray *)pages{
    _pages = pages;
}

- (NSUInteger)numberOfPages{
    if (_pages)
        return [_pages count];
    
    return 0;
}

#pragma mark - Animations
- (void)animateScrolling{
    if (_currentState & ScrollingStateManual)
        return;
    // Jump to the next page...
    int nextPage = _currentPageIndex + 1;
    if (nextPage == [self numberOfPages]){
        // ...stop the auto-scrolling or...
        if (!_autoScrollLooping){
            _currentState = ScrollingStateManual;
            return;
        }
        
        // ...jump to the first page.
        nextPage = 0;
        _currentState = ScrollingStateLooping;
        
        // Set alpha on layers.
        //[self setLayersPrimaryAlphaWithPageIndex:0];
        //[self setBackLayerPictureWithPageIndex:-1];
    } else {
        _currentState = ScrollingStateAuto;
    }
    
    // Make the scrollView animation.
    [_scrollView setContentOffset:CGPointMake(nextPage * _windowSize.width,0)
                         animated:YES];
    
    // Set the PageControl on the right page.
    [_pageControl setCurrentPage:nextPage];
    
 
    
    // Call the next animation after X seconds.
    [self autoScrollToNextPage];
    
    for(UIView *label in _scrollView.subviews)
    {
        if([label isKindOfClass:[UIImageView class]])
        {
            [label setFrame:CGRectOffset(label.frame, 5, 0)];
        }
    }
//    int y=0;
//    for(UIView *label in _scrollView.subviews)
//    {
//        if([label isKindOfClass:[UIImageView class]])
//        {
//            [label setFrame:CGRectMake(y*_windowSize.width, label.frame.origin.y, label.frame.size.width, label.frame.size.height)];
//            y++;
//        }
//    }
}

// Call the next animation after X seconds.
- (void)autoScrollToNextPage{
    if (_autoScrollEnabled)
        [self performSelector:@selector(animateScrolling)
                   withObject:nil
                   afterDelay:_autoScrollDurationOnPage];
}

#pragma mark - Scrolling management
// Run it.
- (void)startScrolling{
    [self autoScrollToNextPage];
}

// Manually stop the scrolling
- (void)stopScrolling{
    [[self.view viewWithTag:111] removeFromSuperview];

    _currentState = ScrollingStateManual;
}

#pragma mark - State management
// State.
- (ScrollingState)getCurrentState{
    return _currentState;
}

#pragma mark - Overlay management
// Setup the Title Label.
- (void)setOverlayTitle{
    // ...or change by an UIImageView if you need it.
    [_overlayTitle setText:@""];
}

// Setup the SubTitle/Description style/text.
- (void)setOverlayTexts{
    int index = 0;    
    for(ICETutorialPage *page in _pages){
        // SubTitles.
        if ([[[page subTitle] text] length]){
            UILabel *subTitle = [self overlayLabelWithText:[[page subTitle] text]
                                                     layer:[page subTitle]
                                               commonStyle:_commonPageSubTitleStyle
                                                     index:index];
            //‘[_scrollView addSubview:subTitle];
        }
        
        if(page.pictureName!=nil)
        {
            [_scrollView addSubview:[self overlayImageView:[UIImage imageNamed:page.pictureName] withPoint:page.point withIndex:index]];
            [_scrollView addSubview:[self overlayImagewithIndex:index]];
            
        }
        
        // Description.
        if ([[[page description] text] length]){
            UILabel *description = [self overlayLabelWithText:[[page description] text]
                                                        layer:[page description]
                                                  commonStyle:_commonPageDescriptionStyle
                                                        index:index];
            description.tag=index;
            //[_scrollView addSubview:description];
        }
        

        
        index++;
    }
}

- (void )overlayStarwithIndex
{
    UIImage *image=[UIImage imageNamed:@"history_stars"];
    NSArray *array = [NSArray arrayWithObjects:@"445",@"438",
                      @"420",
                      @"348",@"404",@"389",@"365",@"330",
                      @"335",@"270",@"238",@"216",@"174", nil];
    for (int i=0; i<13; i++) {
        NSLog(@"%f",[[array objectAtIndex:i] floatValue]);
        UIImageView *imageViewTemp = [[UIImageView alloc] initWithFrame:
                                      CGRectMake(320,[[array objectAtIndex:i] floatValue]/2,
                                                 image.size.width,
                                                 image.size.height)];
        
        imageViewTemp.tag =1001+i;
        [imageViewTemp setImage:image];
        [self.view addSubview:imageViewTemp];
    }
}


- (UIImageView *)overlayImagewithIndex:(NSUInteger)index
{
    UIImage *image=nil;
    CGPoint point ;
    if(index == 0)
    {
        image = [UIImage imageNamed:@"history_1994text.png"];
        point = CGPointMake(10, 190);
    }
    else if (index == 1)
    {
        image = [UIImage imageNamed:@"history_1995text.png"];
        point = CGPointMake(10, 180);
    }
    else if (index == 2)
    {
        image = [UIImage imageNamed:@"history_2013text.png"];
        point = CGPointMake(10, 200);
    }
    UIImageView *imageViewTemp = [[UIImageView alloc] initWithFrame:CGRectMake(point.x+(index  * _windowSize.width),
                                                                               point.y,
                                                                               image.size.width>320?380:image.size.width,
                                                                               image.size.height)];
    [imageViewTemp setImage:image];
    return imageViewTemp;
}

- (UIImageView *)overlayImageView:(UIImage*)image
                        withPoint:(CGPoint)point
                        withIndex:(NSUInteger)index
{
    UIImageView *imageViewTemp = [[UIImageView alloc] initWithFrame:CGRectMake(point.x+(index  * _windowSize.width),
                                                                           480-image.size.height,
                                                                               image.size.width>320?380:image.size.width,
                                                                           image.size.height)];
    [imageViewTemp setImage:image];
    return imageViewTemp;
}

- (UILabel *)overlayLabelWithText:(NSString *)text
                            layer:(ICETutorialLabelStyle *)style
                      commonStyle:(ICETutorialLabelStyle *)commonStyle
                            index:(NSUInteger)index{
    // SubTitles.
    UILabel *overlayLabel = [[UILabel alloc] initWithFrame:CGRectMake((index  * _windowSize.width),
                                                                      _windowSize.height - [commonStyle offset],
                                                                      _windowSize.width,
                                                                      TUTORIAL_LABEL_HEIGHT)];
    //[overlayLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [overlayLabel setNumberOfLines:[commonStyle linesNumber]];
    [overlayLabel setBackgroundColor:[UIColor clearColor]];
    [overlayLabel setTextAlignment:NSTextAlignmentLeft];  
    // Datas and style.
    [overlayLabel setText:text];
    [style font] ? [overlayLabel setFont:[style font]] :
                   [overlayLabel setFont:[commonStyle font]];
    if ([style textColor])
        [overlayLabel setTextColor:[style textColor]];
    else
        [overlayLabel setTextColor:[commonStyle textColor]];
  
    return overlayLabel;
}


// Preset the origin state.
- (void)setOriginLayersState{
    _currentState = ScrollingStateAuto;
    [self setLayersPicturesWithIndex:0];
}

// Setup the layers with the page index.
- (void)setLayersPicturesWithIndex:(NSInteger)index{
    _currentPageIndex = index;
}

// Animate the fade-in/out (Cross-disolve) with the scrollView translation.
- (void)disolveBackgroundWithContentOffset:(float)offset{
    if (_currentState & ScrollingStateLooping){
        // Jump from the last page to the first.
        [self scrollingToFirstPageWithOffset:offset];
    } else {
        // Or just scroll to the next/previous page.
        [self scrollingToNextPageWithOffset:offset];
    }
}

// Handle alpha on layers when the auto-scrolling is looping to the first page.
- (void)scrollingToFirstPageWithOffset:(float)offset{
    // Compute the scrolling percentage on all the page.
    offset = (offset * _windowSize.width) / (_windowSize.width * [self numberOfPages]);
    
    // Scrolling finished...
    if (offset == 0){
        // ...reset to the origin state.
        [self setOriginLayersState];
        return;
    }
    
    /**
     *	播放完一次后停止
     */
    [self stopScrolling];
}

// Handle alpha on layers when we are scrolling to the next/previous page.
- (void)scrollingToNextPageWithOffset:(float)offset{
    // Current page index in scrolling.
    NSInteger page = (int)(offset);
    
    // Keep only the float value.
    float alphaValue = offset - (int)offset;
    
    // This is only when you scroll to the right on the first page.
    // That will fade-in black the first picture.
    if (alphaValue < 0 && _currentPageIndex == 0){
        return;
    }
    
    // Switch pictures, and imageView alpha.
    if (page != _currentPageIndex)
        [self setLayersPicturesWithIndex:page];
}

#pragma mark - ScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // Get scrolling position, and send the alpha values.
    [scrollView flashScrollIndicators];
    float scrollingPosition = scrollView.contentOffset.x / _windowSize.width;
    [self disolveBackgroundWithContentOffset:scrollingPosition];

    if (_scrollView.isTracking)
        _currentState = ScrollingStateManual;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView;   // called on finger up as we are moving
{
    /**
     *	label 回归原位
     */
    [self labelMoveBack];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    // Update the page index.
    [_pageControl setCurrentPage:_currentPageIndex];
    
    /**
     *	scrollview 移动完文字移动
     */
    [self labelMoveAnimation];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    if(_currentPageIndex==0)
    {
        for(int i =0 ;i<13;i++)
        {
            UIImageView *view = (UIImageView *)[self.view viewWithTag:1001+i];
            [view setFrame:CGRectOffset(view.frame, 320, 0)];
        }
    }
    else if(_currentPageIndex==1)
    {
        for(int i =3 ;i<13;i++)
        {
            UIImageView *view = (UIImageView *)[self.view viewWithTag:1001+i];
            [view setFrame:CGRectOffset(view.frame, 320, 0)];
        }
        UIImageView *imageView1 = (UIImageView *)[self.view viewWithTag:1001];
        [imageView1 setHidden:NO];
        [imageView1 setFrame:CGRectMake(208, 222, imageView1.frame.size.width, imageView1.frame.size.height)];
        UIImageView *imageView2 = (UIImageView *)[self.view viewWithTag:1002];
        [imageView2 setHidden:NO];
        [imageView2 setFrame:CGRectMake(240, 219, imageView1.frame.size.width, imageView1.frame.size.height)];
    }
    //    else if(_currentPageIndex==2)
    //    {
    //        UIImageView *imageView3 = (UIImageView *)[self.view viewWithTag:1003];
    //        [imageView setFrame:CGRectMake(268, 210, imageView.frame.size.width, imageView.frame.size.height)];
    //    }
    else if(_currentPageIndex==2)
    {
        UIImageView *imageView4 = (UIImageView *)[self.view viewWithTag:1004];
        [imageView4 setHidden:NO];
        [imageView4 setFrame:CGRectMake(603/2, 348/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView5 = (UIImageView *)[self.view viewWithTag:1005];
        [imageView5 setHidden:NO];
        [imageView5 setFrame:CGRectMake(469/2, 404/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView6 = (UIImageView *)[self.view viewWithTag:1006];
        [imageView6 setHidden:NO];
        [imageView6 setFrame:CGRectMake(417/2, 389/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView7 = (UIImageView *)[self.view viewWithTag:1007];
        [imageView7 setHidden:NO];
        [imageView7 setFrame:CGRectMake(449/2, 365/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView8 = (UIImageView *)[self.view viewWithTag:1008];
        [imageView8 setHidden:NO];
        [imageView8 setFrame:CGRectMake(472/2, 330/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        
        UIImageView *imageView9 = (UIImageView *)[self.view viewWithTag:1009];
        [imageView9 setHidden:NO];
        [imageView9 setFrame:CGRectMake(393/2, 335/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView10 = (UIImageView *)[self.view viewWithTag:1010];
        [imageView10 setHidden:NO];
        [imageView10 setFrame:CGRectMake(421/2, 270/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView11 = (UIImageView *)[self.view viewWithTag:1011];
        [imageView11 setHidden:NO];
        [imageView11 setFrame:CGRectMake(425/2, 238/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView12 = (UIImageView *)[self.view viewWithTag:1012];
        [imageView12 setHidden:NO];
        [imageView12 setFrame:CGRectMake(337/2, 216/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView13 = (UIImageView *)[self.view viewWithTag:1013];
        [imageView13 setHidden:NO];
        [imageView13 setFrame:CGRectMake(273/2, 174/2, imageView4.frame.size.width, imageView4.frame.size.height)];
    }
    [UIView commitAnimations];

}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView; // called when setContentOffset/scrollRectVisible:animated: finishes. not called if not animating
{
    /**
     *	scrollview 移动完文字移动
     */
    NSLog(@"currentIndex=%d",_currentPageIndex);

    [self labelMoveAnimation];
}

/**
 *	文字移动
 */
- (void)labelMoveAnimation
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    if(_currentPageIndex==0)
    {
        for(int i =0 ;i<13;i++)
        {
            UIImageView *view = (UIImageView *)[self.view viewWithTag:1001+i];
            [view setHidden:YES];
        }
    }
    else if(_currentPageIndex==1)
    {

        UIImageView *imageView1 = (UIImageView *)[self.view viewWithTag:1001];
        [imageView1 setHidden:NO];
        [imageView1 setFrame:CGRectMake(208, 222, imageView1.frame.size.width, imageView1.frame.size.height)];
        UIImageView *imageView2 = (UIImageView *)[self.view viewWithTag:1002];
        [imageView2 setHidden:NO];
        [imageView2 setFrame:CGRectMake(240, 219, imageView1.frame.size.width, imageView1.frame.size.height)];
    }
//    else if(_currentPageIndex==2)
//    {
//        UIImageView *imageView3 = (UIImageView *)[self.view viewWithTag:1003];
//        [imageView setFrame:CGRectMake(268, 210, imageView.frame.size.width, imageView.frame.size.height)];
//    }
    else if(_currentPageIndex==2)
    {
        UIImageView *imageView4 = (UIImageView *)[self.view viewWithTag:1004];
        [imageView4 setHidden:NO];
        [imageView4 setFrame:CGRectMake(603/2, 348/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView5 = (UIImageView *)[self.view viewWithTag:1005];
        [imageView5 setHidden:NO];
        [imageView5 setFrame:CGRectMake(469/2, 404/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView6 = (UIImageView *)[self.view viewWithTag:1006];
        [imageView6 setHidden:NO];
        [imageView6 setFrame:CGRectMake(417/2, 389/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView7 = (UIImageView *)[self.view viewWithTag:1007];
        [imageView7 setHidden:NO];
        [imageView7 setFrame:CGRectMake(449/2, 365/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView8 = (UIImageView *)[self.view viewWithTag:1008];
        [imageView8 setHidden:NO];
        [imageView8 setFrame:CGRectMake(472/2, 330/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        
        UIImageView *imageView9 = (UIImageView *)[self.view viewWithTag:1009];
        [imageView9 setHidden:NO];
        [imageView9 setFrame:CGRectMake(393/2, 335/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView10 = (UIImageView *)[self.view viewWithTag:1010];
        [imageView10 setHidden:NO];
        [imageView10 setFrame:CGRectMake(421/2, 270/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView11 = (UIImageView *)[self.view viewWithTag:1011];
        [imageView11 setHidden:NO];
        [imageView11 setFrame:CGRectMake(425/2, 238/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView12 = (UIImageView *)[self.view viewWithTag:1012];
        [imageView12 setHidden:NO];
        [imageView12 setFrame:CGRectMake(337/2, 216/2, imageView4.frame.size.width, imageView4.frame.size.height)];
        UIImageView *imageView13 = (UIImageView *)[self.view viewWithTag:1013];
        [imageView13 setHidden:NO];
        [imageView13 setFrame:CGRectMake(273/2, 174/2, imageView4.frame.size.width, imageView4.frame.size.height)];
    }
    [UIView commitAnimations];

    for(UIView *label in _scrollView.subviews)
    {
        if([label isKindOfClass:[UIImageView class]])
        {
            
            {
                [UIView beginAnimations:nil context:nil];
                //设定动画持续时间
                [UIView setAnimationDuration:0.7];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                //动画的内容
                [label setFrame:CGRectOffset(label.frame, -5, 0)];
                //[label setFrame:CGRectMake(i*_windowSize.width+30, label.frame.origin.y, label.frame.size.width, label.frame.size.height)];
                //动画结束
                [UIView commitAnimations];
            }
        }
    }
}

- (void)labelMoveBack
{
    for(UIView *label in _scrollView.subviews)
    {
        if([label isKindOfClass:[UIImageView class]])
        {
            [label setFrame:CGRectOffset(label.frame, 5, 0)];

//            [label setFrame:CGRectMake(i*_windowSize.width+0, label.frame.origin.y, label.frame.size.width, label.frame.size.height)];
        }
    }
}

- (void)starMove
{
    
}

@end
