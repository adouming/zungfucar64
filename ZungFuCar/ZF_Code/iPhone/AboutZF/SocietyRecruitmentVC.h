//
//  SocietyRecruitmentVC.h
//  ZungFuCar
//
//  Created by lxm on 13-10-5.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "CampusRecruitmentVC.h"

@interface SocietyRecruitmentVC : CampusRecruitmentVC
{
}
/**
 *	进入社会招聘详情
 */
- (IBAction)openSocietyUrl:(id)sender;
@end
