//
//  CampusMenuViewController.m
//  ZungFuCar
//
//  Created by GuoChengHao on 13-11-24.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "CampusMenuViewController.h"

@interface CampusMenuViewController ()

@end

@implementation CampusMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button action

- (IBAction)chooseDetail:(id)sender {
    int select = ((UIButton *)sender).tag;
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"AboutZF" bundle:nil];
    CampusRecruitmentVC *campusRecruitmentCtl = [stryBoard instantiateViewControllerWithIdentifier:@"CampusRecruitmentVCID"];
    [self.navigationController pushViewController:campusRecruitmentCtl animated:YES];
    switch (select) {
        case 90:
            campusRecruitmentCtl.contentNum = 0;
            break;
        case 91:
            campusRecruitmentCtl.contentNum = 1;
            break;
        case 92:
            campusRecruitmentCtl.contentNum = 2;
            break;
        case 93:
            campusRecruitmentCtl.contentNum = 3;
            break;
        case 94:
            campusRecruitmentCtl.contentNum = 4;
            break;
        default:
            break;
    }
}



/**
 *	隐藏右边按钮
 */
-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}

@end
