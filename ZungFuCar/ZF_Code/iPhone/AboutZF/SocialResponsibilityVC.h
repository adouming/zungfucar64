//
//  SocialResponsibilityVCViewController.h
//  ZungFuCar
//
//  Created by adouming on 15/6/17.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyProfileVC.h"
@interface SocialResponsibilityVC   :  CompanyProfileVC <UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *bgImgView;

@end
