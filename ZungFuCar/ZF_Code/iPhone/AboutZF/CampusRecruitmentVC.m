//
//  CampusRecruitmentVC.m
//  ZungFuCar
//
//  Created by lxm on 13-10-5.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "CampusRecruitmentVC.h"
#import "DBManager.h"
#import "AboutZFAPIManager.h"
#import "NSString+FilterHtml.h"

@interface CampusRecruitmentVC ()

@end

@implementation CampusRecruitmentVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    /**
     *	非二级菜单显示按钮
     */
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    _titleLabel.text = @"校园招聘";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    _detailBtn = nil;
    [super viewDidUnload];
}

/**
 *	调整detailbutton
 */
- (void)resizeDetailButton
{
    [_detailBtn setFrame:CGRectMake(_detailBtn.frame.origin.x,
                                    _bgScrollView.contentSize.height-_detailBtn.frame.size.height,_detailBtn.frame.size.width,
                                    _detailBtn.frame.size.height)];
}

/**
 *	进入校园招聘详情
 */
- (IBAction)openCampusUrl:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.baidu.com"]];
}

/**
 * Model
 */
- (void)creatModel
{
    /*从DB中获取数据*/
    NSArray *array = [[DBManager sharedManager] getCampusRecruitmentData];
    if([array count] ==0 )
        return;
    if ([[array objectAtIndex:0] count] == 0) {
        return;
    }
    /*获取内容显示*/
    NSString *contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
    /*获取bannerURL 显示 banner*/
    NSString *bannerUrl = [[array objectAtIndex:0] objectForKey:@"BannerURL"];
    if([bannerUrl validatePng] &&![bannerUrl isEqualToString:@""])
        [_bannerView setImageWithURL:[NSURL URLWithString:[[array objectAtIndex:0] objectForKey:@"BannerURL"]]];
}

/*请求数据*/
- (void)requestData
{
    //    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID175_UpdatedTime"]==nil)
    //    {
    [self showLoadingText];
    //    }
    
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    
    [manager requestCampusRecruitmentDataSuccess:^(NSArray *data) {
        BOOL isFirst = NO;
        if([data count]==0)
        {
            return ;
        }
        else
        {
            /*写lastmodifyTime到沙盒*/
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if([userDefaults objectForKey:@"ViewID175_UpdatedTime"]==nil)
                isFirst = YES;
            [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:@"ViewID175_UpdatedTime"];
            [userDefaults synchronize];
            
            //            NSString *htmlStr = @"<p>\
            //            <big>人才培养与发展</big></p>\
            //            <p style=\"text-align: center\">\
            //            <img alt=\"\" src=\"images/articalimgs/JOINZF_01_03.jpg\" style=\"width: 194px; height: 129px;\" /> <img alt=\"\" src=\"images/articalimgs/JOINZF_01_02.jpg\" style=\"width: 230px; height: 129px\" /> <img alt=\"\" src=\"images/articalimgs/JOINZF_01_01.jpg\"style=\"width: 230px; height: 129px;\" /></p>\
            //            <p>\
            //            我们始终坚信人才是企业持续发展最关键的因素，也是最重要的资产。因此仁孚集团每年在人才发展与培养方面均投入了巨大的时间、精力与成本，以保持并提升内部人员的质素，配合持续和长远业务发展的人才需求。</p>\
            //            <p>\
            //            为了储备充足的领袖和管理人才，集团制定了长期、中期、短期人才培养和发展策略。期望通过严谨的甄选和考核，挖掘具有管理潜质的人员，为他们规划和提供管理技能提升的学习机会，快速具备管理人员应有的技能和经验，以随时担任总经理或部门经理等关键管理岗位职责。</p>\
            //            <p>\
            //            <br />\
            //            &nbsp;</p>";
            //            htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:@"src=\"http://54.250.184.46/zf-web-ios/"];
            //            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"http://54.250.184.46/zf-web-ios/",@"BannerURL",
            //                                 htmlStr,@"Content",
            //                                 @"624",@"ID",
            //                                 @"articletitle-624",@"Name",
            //                                 @"2013-09-14 11:45:52",@"UpdatedTime",
            //                                 nil];
            /*解析Content 拼接URL*/
            NSString *htmlStr = [[data objectAtIndex:self.contentNum] objectForKey:@"Content"];
            //NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
            //htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
            //[_contentWebView loadHTMLString:htmlStr baseURL:nil];
            [self hideLoadingText];
            /*重新封装*/
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"BannerURL"],@"BannerURL",
                                 htmlStr,@"Content",
                                 [[data objectAtIndex:0] objectForKey:@"ID"],@"ID",
                                 [[data objectAtIndex:0] objectForKey:@"Name"],@"Name",
                                 [[data objectAtIndex:0] objectForKey:@"UpdatedTime"],@"UpdatedTime",
                                 nil];
            /*归档*/
            NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
            
            [[DBManager sharedManager] storeCampusRecruitmentData:dataTemp Sucess:^(NSMutableArray *storeData) {
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
        }
        
    }failure:^(NSError *error){
        [self hideLoadingText];
    }];
}

/*
 * 给UIWebView 设置高度 自适应内容
 */

@end
