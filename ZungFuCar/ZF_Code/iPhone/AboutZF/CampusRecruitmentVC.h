//
//  CampusRecruitmentVC.h
//  ZungFuCar
//
//  Created by lxm on 13-10-5.
//  Copyright (c) 2013年 lxm. All rights reserved.
//

#import "CompanyProfileVC.h"

@interface CampusRecruitmentVC : CompanyProfileVC
{
    /*招聘详情按钮*/
    __strong IBOutlet UIButton *_detailBtn;
}

@property int contentNum;

/**
 *	调整detailbutton
 */
- (void)resizeDetailButton;

/**
 *	进入校园招聘详情
 */
- (IBAction)openCampusUrl:(id)sender;
@end
