//
//  PriceShowViewController.m
//  ZungFuCar
//
//  Created by adouming on 15/7/21.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "PriceShowViewController.h"
#import "AboutZFAPIManager.h"
#import "NSObject+AddJavascript.h"
#import "NSString+FilterHtml.h"

@interface PriceShowViewController ()

@end

@implementation PriceShowViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
   
    
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    //_webView.scrollView.scrollEnabled = NO;
    // Do any additional setup after loading the view.
    /*读取数据 请求数据*/
    [self creatModel];
    [self requestData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    
    [self setContentScrollView:nil];
    [self setTitleLable:nil];
    [super viewDidUnload];
}

/**
 * Model
 */
- (void)creatModel
{
    NSArray *array = [[DBManager sharedManager] getPriceTableData];
    if([array count] == 0 )
        return;
    NSString *contentStr = [[array objectAtIndex:0] objectForKey:@"content"];
    [self.webView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
    NSString *bannerUrl = [[array objectAtIndex:0] objectForKey:@"BannerURL"];
    if([bannerUrl validatePng] &&![bannerUrl isEqualToString:@""])
        [self.bannerView setImageWithURL:[NSURL URLWithString:[[array objectAtIndex:0] objectForKey:@"BannerURL"]]];
}

/**
 *  请求数据
 */
- (void)requestData
{
    //    [self showLoadingText];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ViewID150_UpdatedTime"]==nil)
    {
        [self showLoadingText];
    }
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    [manager requestPriceTableDataSuccess:^(NSArray *data) {
        BOOL isFirst = NO;
        if([data count]==0)
        {
            return ;
        }
        else
        {
            /*写lastmodifyTime到沙盒*/
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if([userDefaults objectForKey:@"ViewID150_UpdatedTime"]==nil)
            {
                isFirst = YES;
            }
            [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"updatetime"] forKey:@"ViewID150_UpdatedTime"];
            [userDefaults synchronize];
            
            //            NSString *htmlStr = @"<p>\
            //            <big>人才培养与发展</big></p>\
            //            <p style=\"text-align: center\">\
            //            <img alt=\"\" src=\"images/articalimgs/JOINZF_01_03.jpg\" style=\"width: 194px; height: 129px;\" /> <img alt=\"\" src=\"images/articalimgs/JOINZF_01_02.jpg\" style=\"width: 230px; height: 129px\" /> <img alt=\"\" src=\"images/articalimgs/JOINZF_01_01.jpg\"style=\"width: 230px; height: 129px;\" /></p>\
            //            <p>\
            //            我们始终坚信人才是企业持续发展最关键的因素，也是最重要的资产。因此仁孚集团每年在人才发展与培养方面均投入了巨大的时间、精力与成本，以保持并提升内部人员的质素，配合持续和长远业务发展的人才需求。</p>\
            //            <p>\
            //            为了储备充足的领袖和管理人才，集团制定了长期、中期、短期人才培养和发展策略。期望通过严谨的甄选和考核，挖掘具有管理潜质的人员，为他们规划和提供管理技能提升的学习机会，快速具备管理人员应有的技能和经验，以随时担任总经理或部门经理等关键管理岗位职责。</p>\
            //            <p>\
            //            <br />\
            //            &nbsp;</p>";
            //            htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:@"src=\"http://54.250.184.46/zf-web-ios/"];
            //            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"http://54.250.184.46/zf-web-ios/",@"BannerURL",
            //                                 htmlStr,@"Content",
            //                                 @"624",@"ID",
            //                                 @"articletitle-624",@"Name",
            //                                 @"2013-09-14 11:45:52",@"UpdatedTime",
            //                                 nil];
            
            /*content url 图片不规范 需要附加http*/
            NSString *htmlStr = [[data objectAtIndex:0] objectForKey:@"content"];
            //NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
            //htmlStr=[htmlStr stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
            [self hideLoadingText];
            /*重新封装数据*/
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[[data objectAtIndex:0] objectForKey:@"url"],@"BannerURL",
                                 htmlStr,@"content",
                                 [[data objectAtIndex:0] objectForKey:@"id"],@"ID",
                                 [[data objectAtIndex:0] objectForKey:@"title"],@"Name",
                                 [[data objectAtIndex:0] objectForKey:@"updatetime"],@"UpdatedTime",
                                 nil];
            
            NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
            /*存储数据*/
            [[DBManager sharedManager] storePriceTableData:dataTemp Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
        }
        
    }failure:^(NSError *error){
        [self hideLoadingText];
    }];
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    willLaunchSafari = NO;
}
/*
 * 给UIWebView 设置高度 自适应内容
 */
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    willLaunchSafari = YES;
    return;
    
    [self injectJavascript:webView];
    NSString *padding = @"document.body.style.margin='5px';document.body.style.padding = '5px'";
    [webView stringByEvaluatingJavaScriptFromString:padding];
    /*获取html body 的内容长度 计算高度*/
    NSString *height_str= [webView stringByEvaluatingJavaScriptFromString: @"document.body.scrollHeight"];
    int height = [height_str intValue];
    webView.frame = CGRectMake(webView.frame.origin.x,webView.frame.origin.y,webView.frame.size.width,height+200);
    
    /*设置WebView的superView=contentScrollView的contentSize*/
    [self.contentScrollView setContentSize:CGSizeMake(self.contentScrollView.frame.size.width, CGRectGetMaxY(webView.frame))];
    
    willLaunchSafari = YES;
}

/*
 * 点击网页图片,打开相册
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([self webViewImgIsClickedByRequest:request] == 0) {
        UINavigationController *nc = [self getAlbumPhotosFromImgClick:request];
        if (nc != nil) {
            nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:nc animated:YES completion:nil];
        }
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 1 && willLaunchSafari) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 2) {
        return YES;
    }
}




@end