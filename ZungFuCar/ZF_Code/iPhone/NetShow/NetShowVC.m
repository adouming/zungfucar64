//
//  NetShowVc.m
//  ZungFuCar
//
//  Created by 刘 勇斌 on 13-11-12.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "NetShowVC.h"
#import "BenChiVC.h"
#import "ReserveFormViewController.h"
@interface NetShowVC ()

@end

@implementation NetShowVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/**
 *	隐藏右边按钮
 */
-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}

- (IBAction)benChiAction:(id)sender {
    BenChiVC *benChiView = [[BenChiVC alloc]initWithType:13];
    [self.navigationController pushViewController:benChiView animated:YES];
}

- (IBAction)amgAction:(id)sender {
    BenChiVC *benChiView = [[BenChiVC alloc]initWithType:14];
    [self.navigationController pushViewController:benChiView animated:YES];
}

- (IBAction)smartAction:(id)sender {
    BenChiVC *benChiView = [[BenChiVC alloc]initWithType:16];
    [self.navigationController pushViewController:benChiView animated:YES];
}

- (IBAction)tryAction:(id)sender {
  
        UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Steward" bundle:nil];
        ReserveFormViewController *reserveVC = [stryBoard instantiateViewControllerWithIdentifier:@"TryReserveFormID"];
        reserveVC.reserveType = ReserveTypeTest;
        [self.navigationController pushViewController:reserveVC animated:YES];
        return;
    
}

- (IBAction)priceAction:(id)sender {
    BenChiVC *benChiView = [[BenChiVC alloc]initWithType:150];
    [self.navigationController pushViewController:benChiView animated:YES];
}
@end









