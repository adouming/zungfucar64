//
//  BenChiVC.m
//  ZungFuCar
//
//  Created by 刘 勇斌 on 13-11-13.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "BenChiVC.h"
#import "AFJSONRequestOperation.h"
#import "UIImageView+AFNetworking.h"
#import "DBManager.h"
#import "MyUILabel.h"
#import "CarDetailVC.h"
#import "SmartCarDetailVC.h"
#import "SelectTableViewCell.h"

@interface BenChiVC ()

@end

@implementation BenChiVC
@synthesize carType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view bringSubviewToFront:self.customNavigaitionBar];
    }
    return self;
}

-(id)initWithType:(int)type{
    carType = type;
    self = [super init];
    return self;
}
- (void) controllerPressed:(id)sender {
    UISegmentedControl *segmentControl =(UISegmentedControl *)sender;
    int selectedSegment = segmentControl.selectedSegmentIndex;
    NSLog(@"Segment %d selected\n", selectedSegment);
}
- (NSString*)getTitleWithType:(int)type {
    NSString* title = nil;
    if (carType==13) {
        title = @"奔驰乘用车";
        
    }else if(carType==14){
        title = @"AMG系列";                
    }else if (carType==16){
        title = @"smart";
        
        
    }
    return title;
}
- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    CGRect bgFrame;
    if (IOS7) {
        bgFrame = CGRectMake(0, 20, SCREEN_WIDTH, SCREEN_HEIGHT);
    }else{
        bgFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    
    UIImageView *bgView = [[UIImageView alloc]initWithFrame:bgFrame];
    bgView.image = [UIImage imageNamed:@"sl_bg.png"];
    [self.view addSubview:bgView];
    
    
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    dataSource = [[NSMutableArray alloc]init];
    
    UILabel *tipLabel = [[UILabel alloc]initWithFrame:CGRectMake(-2, 99, 299, 45)];
    if (carType==13) {
        tipLabel.text = @"奔驰乘用车";
        dataSource = [[DBManager sharedManager]getNetShowCarData:@"net_show_ben_chi"];
    }else if(carType==14){
        tipLabel.text = @"AMG系列";
        dataSource = [[DBManager sharedManager]getNetShowCarData:@"net_show_AMG"];
    }else if (carType==16){
        tipLabel.text = @"smart";
        dataSource = [[DBManager sharedManager]getNetShowCarData:@"net_show_smart"];
    }
    else if (carType==15){
        tipLabel.text = @"试乘试驾预约";
        dataSource = [[DBManager sharedManager]getNetShowCarData:@"net_show_try"];
    }
    else if (carType==150){
        tipLabel.text = @"价格表";
        dataSource = [[DBManager sharedManager]getNetShowCarData:@"net_show_price"];
    }
    
    tipLabel.backgroundColor = [UIColor clearColor];
    tipLabel.textColor = [UIColor whiteColor];
    tipLabel.font = [UIFont boldSystemFontOfSize:45.0f];
    tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:tipLabel];
    
    hScrollView = [[HorizontalScrollView alloc]initWithFrame:CGRectMake(0, 142, SCREEN_WIDTH, SCREEN_HEIGHT-142)];
    //hScrollView.frame = CGRectMake(0, 142, 270, 250);
    hScrollView.delegate = self;
    if ([dataSource count]>0) {
        if ([dataSource count]>0) {
            if (carType==3) {
                [hScrollView initScrollView:dataSource type:0];
            }else{
                [hScrollView initScrollView:dataSource type:1];
            }
        }
    }
    [self.view addSubview:hScrollView];
    
    
    NSMutableArray* all_car_type_list = [[DBManager sharedManager]getNetShowCarData:@"ZF_ALL_car_type_list"];
    


    
    _selctTableView = [[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 105-20, 55, 115, SCREEN_HEIGHT - 70 - 50) style:UITableViewStylePlain];
    [self.view addSubview:_selctTableView];
    
    [_selctTableView setDataSource:self];
    [_selctTableView setDelegate:self];
    [_selctTableView setBackgroundColor:[UIColor clearColor]];
    [_selctTableView setShowsVerticalScrollIndicator:NO];
    
    //[_selctTableView.layer setCornerRadius:10.0f];
    [_selctTableView.layer setMasksToBounds:YES];
    //[_selctTableView.layer setBorderWidth:0.0f];
    //[_selctTableView.layer setBorderColor:[UIColor clearColor].CGColor];
    [_selctTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    NSString* title = [self getTitleWithType:carType];
    
    
    
    //self.carArray = [[subItems allKeys] sortedArrayUsingSelector:@selector(compare:)];
    self.carMap = [[NSMutableDictionary alloc] init];
    self.carArray = [[NSMutableArray alloc] init];
    for (NSDictionary* dict in all_car_type_list) {
        NSString* name = [dict objectForKey:@"name"];
        NSString* cateId = [dict objectForKey:@"cid"];
        if([name containsString:title]) {
        
            NSString* newName = [name stringByReplacingOccurrencesOfString:title withString:@""];
            newName = [newName stringByReplacingOccurrencesOfString:@" - " withString:@""];
            [self.carArray addObject:newName];
            
            [self.carMap setObject:cateId forKey:newName];
            
            
        }
    }

    
    
    NSString* itemTitle = [_carArray objectAtIndex:0] ;
    NSInteger* itemKey = [[self.carMap objectForKey:itemTitle] integerValue];
    
    
    
    [self loadData:itemKey];
    
  
    //[self loadData:77];
    
    
    
    }
- (void)loadData:(NSInteger)itemKey {
    
    
    
    
    //https://www.zfchina.com/api/list.php?id=77
    NSString *urlString = nil;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    AFHTTPRequestOperationManager *request = [AFHTTPRequestOperationManager manager];
    [request.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"Http-X-U"];
    [request.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"Http-X-P"];
    request.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    urlString = [NSString stringWithFormat:@"%@%@%@%d", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID, itemKey];
     [request GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        //NSLog(@"App.net Global Stream: %@", responseObject);
        NSLog(@"operation %@", operation);
        [self setTableViewData:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    

}
#pragma makr - tableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *strId = @"cell";
    SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strId];
    if (!cell) {
        cell = [[SelectTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strId] ;
        
    }
    
    //取消选中颜色
    
　　UIView *backView = [[UIView alloc] initWithFrame:cell.frame];
　　cell.selectedBackgroundView = backView;
　　cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];

　　//取消边框线

　　[cell setBackgroundView:[[UIView alloc] init]];          //取消边框线
    cell.backgroundColor = [UIColor clearColor];
    
    [cell.carNameLable setText:[_carArray objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_carArray count];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString* title = [self getTitleWithType:carType];
    
    NSString* itemTitle = [_carArray objectAtIndex:indexPath.row];
    //NSDictionary* subItems = [_carMap objectForKey:itemTitle];
    NSInteger* itemKey = [[_carMap objectForKey:itemTitle] integerValue];
    
    [self loadData:itemKey];
    NSLog(@"%d", itemKey);
}
-(void)setTableViewData:(id) data{
    if ([[data valueForKey:@"list"] count] > 0) {
        dataSource = [data valueForKey:@"list"];
        [hScrollView clearAllView];
        if ([dataSource count]>0) {
            if (carType==3) {
                [hScrollView initScrollView:dataSource type:0];
            }else{
                [hScrollView initScrollView:dataSource type:1];
            }
        }
        if (carType==13) {
            [[DBManager sharedManager] storeNetShowCarData:dataSource Sucess:nil fail:nil key:@"net_show_ben_chi"];
        }else if(carType==14){
            [[DBManager sharedManager] storeNetShowCarData:dataSource Sucess:nil fail:nil key:@"net_show_AMG"];
        }else if (carType==16){
            [[DBManager sharedManager] storeNetShowCarData:dataSource Sucess:nil fail:nil key:@"net_show_smart"];
        }
        else if (carType==15){
            [[DBManager sharedManager] storeNetShowCarData:dataSource Sucess:nil fail:nil key:@"net_show_try"];
        }else if (carType==150){
            [[DBManager sharedManager] storeNetShowCarData:dataSource Sucess:nil fail:nil key:@"net_show_price"];
        }
    }
}

-(void)pushToDetail:(int )index{
    NSDictionary *dic = [dataSource objectAtIndex:index];
    NSString *carName = [dic valueForKey:@"title"];
    if (carType==3) {
        carName = [dic valueForKey:@"title"];
    }
    NSString *carId = [dic valueForKey:@"id"];
    NSString *bannerURL = [dic objectForKey:@"img"];
    NSArray *carInfo = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@",dic[@"h0"],[self cssTitle:dic[@"h"]],dic[@"content"],[self cssTitle:dic[@"h2"]],dic[@"content2"],[self cssTitle:dic[@"h3"]],dic[@"content3"],[self cssTitle:dic[@"h4"]],dic[@"content4"],dic[@"h5"],dic[@"content5"],dic[@"h6"],dic[@"content6"]];
    int carPrice = ([dic objectForKey:@"price"] && ![[dic objectForKey:@"price"] isEqual:[NSNull null]] ) ? [[dic objectForKey:@"price"] integerValue] : 0;
    if (carType==3) {
    
        SmartCarDetailVC *smartCarDetailVC = [[SmartCarDetailVC alloc] initWithTitle:@"相关活动" bannerUrl:@"" date:@"" descp:carInfo address:@"" shortDesc:@"" bannerName:@""];
        [self.navigationController pushViewController:smartCarDetailVC animated:YES];
    }else if (carType==14) {
    
        NSString* priceText = nil;
        if(carPrice>0) {
            priceText = [NSString stringWithFormat:@"市场指导价:￥%.1lf万起", carPrice/10000.0];
        }
        else {
            priceText = @"市场指导价:不详";
        }
        NSString *imgReplaceURL = [NSString stringWithFormat:@"%@%@%@",  @"http://www.zfchina.com", ZF_BASE_IMAGE_PATH, bannerURL];
        CarDetailVC *carDetailVC = [[CarDetailVC alloc] initWithTitle:carName bannerUrl:imgReplaceURL date:priceText descp:carInfo address:@"" shortDesc:@"" bannerName:@""];
        //CarDetailVC *carDetailVC = [[CarDetailVC alloc] initWithType:carName rightTitle:@"相关活动" productid:carId carType:carInfo bannerURL:bannerURL carType:carType carPrice: carPrice];
        [self.navigationController pushViewController:carDetailVC animated:YES];
    }else{

        
        NSString* priceText = nil;
        if(carPrice>0) {
            priceText = [NSString stringWithFormat:@"市场指导价:￥%.1lf万起", carPrice/10000.0];
        }
        else {
            priceText = @"市场指导价:不详";
        }
        NSString *imgReplaceURL = [NSString stringWithFormat:@"%@%@%@",  @"http://www.zfchina.com", ZF_BASE_IMAGE_PATH, bannerURL];
        CarDetailVC *carDetailVC = [[CarDetailVC alloc] initWithTitle:carName bannerUrl:imgReplaceURL date:priceText descp:carInfo address:@"" shortDesc:@"" bannerName:@""];
        //CarDetailVC *carDetailVC = [[CarDetailVC alloc] initWithType:carName rightTitle:@"相关活动" productid:carId carType:carInfo bannerURL:bannerURL carType:carType carPrice: carPrice];
        [self.navigationController pushViewController:carDetailVC animated:YES];
    }
}
-(NSString*)cssTitle:(NSString*)title {
    return [NSString stringWithFormat:@"<p style='border-right-width:0px;text-transform:none;background-color:#ffffff;text-indent:0px;margin:0px;letter-spacing:normal;font:12px/15px ' helvetica='' neue',='' helvetica,='' arial,='' 微软雅黑,='' 'microsoft='' yahei',='' stheiti,='' sans-serif;white-space:normal;border-top-width:0px;border-bottom-width:0px;color:#000000;border-left-width:0px;word-spacing:0px;font-size-adjust:none;font-stretch:normal;-webkit-text-stroke-width:0px;padding:0px;'=''><var style='border-bottom:#d9cbad 3px double;line-height:35px;font-size:16px;font-weight:bold;padding-bottom:2px;'>%@</var></p>", title];
    
}
#pragma mark - navigationBarDelegate
-(void)navigationBarDidPressAtIndex:(NSInteger)index{
    switch (index) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
            
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end







