//
//  NetShowVc.h
//  ZungFuCar
//
//  Created by 刘 勇斌 on 13-11-12.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"

@interface NetShowVC : MenuAbstractViewController
{
    UIImageView *_bgImageView;
}
- (IBAction)benChiAction:(id)sender;
- (IBAction)amgAction:(id)sender;
- (IBAction)smartAction:(id)sender;
- (IBAction)tryAction:(id)sender;
- (IBAction)priceAction:(id)sender;

@end
