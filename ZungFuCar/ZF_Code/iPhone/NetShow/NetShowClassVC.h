//
//  NetShowClassVC.h
//  ZungFuCar
//  网上展厅 --子菜单(点击 奔驰乘用车 进入模块)
//  Created by kc on 13-10-13.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AlexBaseViewController.h"


@interface NetShowClassVC : AlexBaseViewController

@property (strong, nonatomic)NSString *titleString;

@property (nonatomic,assign)NSInteger vcType;

-(void)pushToDetail:(NSInteger)row;

@end
