//
//  CarDetailVC.m
//  ZungFuCar
//
//  Created by 刘 勇斌 on 13-11-13.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "CarDetailVC.h"
#import "UIImageView+AFNetworking.h"
#import "DBManager.h"
#import "NSStringUtil.h"
#import "NewsDetailVC.h"
#import "ReserveFormViewController.h"
#import "CalculatorVC.h"
#import "NSObject+AddJavascript.h"
#import "WXApi.h"

@interface CarDetailVC ()

@end

@implementation CarDetailVC
@synthesize title, bannerUrl, date, descp, address, shortDesc, bannerName;
#define CARICONTAG   10

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view bringSubviewToFront:self.customNavigaitionBar];
    }
    return self;
}

-(id)initWithTitle:(NSString *)title2 bannerUrl:(NSString *)bannerUrl2 date:(NSString *)date2 descp:(NSString *)descp2 address:(NSString *)address2 shortDesc:(NSString *)shortDesc2 bannerName:(NSString *)bannerName2{
    self.descp = descp2;
    self.title = title2;
    self.bannerUrl = bannerUrl2;
    self.date = date2;
    self.address = address2;
    self.shortDesc = shortDesc2;
    self.bannerName = bannerName2;
    self = [super init];
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    NSLog(@"NewsDetailVC load");
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    scrollView = [[UIScrollView alloc]init];
    scrollView.bounces = NO;
    if (IOS7) {
        scrollView.frame = CGRectMake(0, 20, SCREEN_WIDTH, SCREEN_HEIGHT);
    }else{
        scrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    scrollView.backgroundColor = HEXRGB(0xeaece9);
    scrollView.userInteractionEnabled = YES;
    [self.view addSubview:scrollView];
    
    int yPos = 0;
    int imgHeight = SCREEN_WIDTH*0.47;
    UIImageView *carIconView = [[UIImageView alloc]initWithFrame:CGRectMake(0, yPos, SCREEN_WIDTH, imgHeight)];
    //设置圆角
    //carIconView.layer.cornerRadius = 8;
    //carIconView.layer.masksToBounds = YES;
    
    //自适应图片宽高比例
    carIconView.contentMode = UIViewContentModeScaleAspectFit;
    
    [carIconView setImageWithURL:[NSURL URLWithString:bannerUrl]];
    carIconView.tag = CARICONTAG;
    [scrollView addSubview:carIconView];
    
    yPos = yPos+imgHeight+10;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, yPos, 300, 20)];
    titleLabel.text = title;
    titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    titleLabel.backgroundColor = [UIColor clearColor];
    [scrollView addSubview:titleLabel];
    
    yPos = yPos+23;
    
    UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, yPos, 300, 15)];
//    if (date.length>11) {
//        date = [date substringToIndex:10];
//    }
    dateLabel.text = date;
    dateLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    dateLabel.textColor = HEXRGB(0x444444);
    dateLabel.backgroundColor = [UIColor clearColor];
    [scrollView addSubview:dateLabel];
    
    yPos = yPos+23;
    
    UIButton *lookButton = [[UIButton alloc]initWithFrame:CGRectMake(20, yPos, 100, 30)];
    [lookButton setImage:[UIImage imageNamed:@"video"] forState:UIControlStateNormal];
    [lookButton setTitle:@" 点击观看" forState:UIControlStateNormal];
    lookButton.titleLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    [lookButton setTitleColor:HEXRGB(0x444444) forState:UIControlStateNormal];
    lookButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [lookButton addTarget:self action:@selector(openLink) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:lookButton];
    
    UIButton *shareButton = [[UIButton alloc]initWithFrame:CGRectMake(140, yPos, 100, 30)];
    [shareButton setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
    [shareButton setTitle:@" 点击分享" forState:UIControlStateNormal];
    shareButton.titleLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    [shareButton setTitleColor:HEXRGB(0x444444) forState:UIControlStateNormal];
    shareButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [shareButton addTarget:self action:@selector(shareByActivity:) forControlEvents:UIControlEventTouchUpInside];
//    [scrollView addSubview:shareButton];
    
    if ([NSStringUtil isBlankString:address]) {
        lookButton.hidden = YES;
        shareButton.frame = CGRectMake(20, yPos, 100, 30);
    }
    
    //NSString *imgReplaceURL = [NSString stringWithFormat:@"src=\"%@%@", ZF_BASE_URL, ZF_BASE_IMAGE_PATH];
    //descp=[descp stringByReplacingOccurrencesOfString:@"src=\"" withString:imgReplaceURL];
    
    if (![descp hasPrefix:@"<div style=\"padding:15px;\">"]) {
        descp = [NSString stringWithFormat:@"<div style=\"padding:15px;\">%@</div>", descp];
    }
    
    yPos = yPos+40;
    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, yPos, SCREEN_WIDTH, SCREEN_HEIGHT -20 )];
    
    [webView loadHTMLString: [[DBManager sharedManager] wrapperHtml:descp] baseURL:[[DBManager sharedManager] baseUrl]];
    webView.delegate = self;
    [(UIScrollView *)[[webView subviews] objectAtIndex:0] setBounces:NO];
    [(UIScrollView *)[[webView subviews] objectAtIndex:0] setScrollEnabled:NO];
    [scrollView addSubview:webView];
    
    [self showLoadingText];
    [self initSinaWeiboEngine];
}

- (void)shareByActivity:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"分享到新浪微博", @"分享到微信", @"分享到微信朋友圈",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self shareToSina];
    }else if (buttonIndex == 1) {
        [self shareToWx:WXSceneSession];
    }else if(buttonIndex == 2) {
        [self shareToWx:WXSceneTimeline];
    }
}

-(void)shareToSina{
    if (_shareEngine.accessToken) {
        //        [self showLoadingText];
        NSString *status = self.shortDesc;
        if (status.length>140) {
            status = [NSString stringWithFormat:@"%@...", [status substringToIndex:136]];
        }
        UIImage *image = ((UIImageView *)[scrollView viewWithTag:CARICONTAG]).image;
        /**********************弹出发布内容展示窗口**************************/
        UIView *sendView = [[UIView alloc] init];
        sendView.frame = CGRectMake(20, 64, SCREEN_WIDTH-40, SCREEN_HEIGHT-120);
        sendView.tag = 100;
        sendView.layer.cornerRadius = 10;
        sendView.layer.borderWidth = 8;
        sendView.layer.borderColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.8] CGColor];
        sendView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:sendView];
        
        UIImageView *shareImgView = [[UIImageView alloc] initWithImage:image];
        shareImgView.frame = CGRectMake((sendView.frame.size.width-image.size.width/2)/2,
                                        15,
                                        image.size.width/2,
                                        image.size.height/2);
        [sendView addSubview:shareImgView];
        
        UITextView *shareText = [[UITextView alloc] init];
        shareText.frame = CGRectMake(20,
                                     shareImgView.frame.origin.y+shareImgView.frame.size.height,
                                     sendView.frame.size.width-40,
                                     sendView.frame.size.height-shareImgView.frame.size.height-80);
        shareText.editable = NO;
        shareText.text = status;
        [sendView addSubview:shareText];
        
        UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [sendBtn setTitle:@"分享" forState:UIControlStateNormal];
        sendBtn.frame = CGRectMake((sendView.frame.size.width-150)/2,
                                   sendView.frame.size.height-50,
                                   60,
                                   30);
        [sendBtn addTarget:self action:@selector(shareToSinaAction) forControlEvents:UIControlEventTouchUpInside];
        [sendView addSubview:sendBtn];
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        cancelBtn.frame = CGRectMake(sendBtn.frame.origin.x+sendBtn.frame.size.width+30,
                                     sendView.frame.size.height-50,
                                     60,
                                     30);
        [cancelBtn addTarget:self action:@selector(hideShare) forControlEvents:UIControlEventTouchUpInside];
        [sendView addSubview:cancelBtn];
        /************************************************/
        //        [_shareEngine sendWeiBoWithText:[NSString stringWithFormat:@"%@%@", status, bannerName] image:image];
        
    }else{
        [_shareEngine logIn];
    }
}

- (void)hideShare
{
    [(UIView *)[self.view viewWithTag:100] removeFromSuperview];
}

- (void)shareToSinaAction
{
    [self showLoadingText];
    NSString *status = self.shortDesc;
    if (status.length>140) {
        status = [NSString stringWithFormat:@"%@...", [status substringToIndex:136]];
    }
    UIImage *image = ((UIImageView *)[scrollView viewWithTag:CARICONTAG]).image;
    [_shareEngine sendWeiBoWithText:[NSString stringWithFormat:@"%@%@", status, bannerName] image:image];
}

- (void)shareToWx:(int)_scene{
    
    //    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    //    req.text = [NSString stringWithFormat:@"%@%@", self.shortDesc, self.bannerUrl];
    //    req.bText = YES;
    //    req.scene = _scene;
    
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = @"仁孚中国";
    if (_scene==WXSceneTimeline) {
        message.title = title;
    }
    message.description = self.shortDesc;
    
    UIImage *image = ((UIImageView *)[scrollView viewWithTag:CARICONTAG]).image;
    CGSize size = {120, 120};
    UIGraphicsBeginImageContext(size);
    CGRect rect = {{0,0}, size};
    [image drawInRect:rect];
    UIImage *compressedImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [message setThumbImage:compressedImg];
    
    WXWebpageObject *ext = [WXWebpageObject object];
    
    if (bannerName) {
        ext.webpageUrl = bannerName;
    }else{
        ext.webpageUrl = @"https://www.zfchina.com//";
    }
    
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = _scene;
    
    [WXApi sendReq:req];
}

/**
 * 初始化新浪微博引擎
 */
- (void)initSinaWeiboEngine
{
    _shareEngine = [[WBEngine alloc] initWithAppKey:kWBSDKDemoAppKey appSecret:kWBSDKDemoAppSecret];
    [_shareEngine setRootViewController: self];
    [_shareEngine setDelegate:self];
    [_shareEngine setRedirectURI:@"https://www.zfchina.com//"];
    [_shareEngine setIsUserExclusive:NO];
}

#pragma mark - WBEngineDelegate Methods
#pragma mark Authorize
- (void)engineAlreadyLoggedIn:(WBEngine *)engine
{
    if ([engine isUserExclusive])
    {
        UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
                                                           message:@"请先登出！"
                                                          delegate:nil
                                                 cancelButtonTitle:@"确定"
                                                 otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)engineDidLogIn:(WBEngine *)engine
{
    [self showLoadingText];
    NSString *status = self.shortDesc;
    if (status.length>140) {
        status = [status substringToIndex:139];
    }
    UIImage *image = ((UIImageView *)[scrollView viewWithTag:CARICONTAG]).image;
    [_shareEngine sendWeiBoWithText:status image:image];
}

- (void)engine:(WBEngine *)engine didFailToLogInWithError:(NSError *)error
{
    NSLog(@"didFailToLogInWithError: %@", error);
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"登录失败！"
                                                      delegate:nil
                                             cancelButtonTitle:@"确定"
                                             otherButtonTitles:nil];
    [alertView show];
}

- (void)engineAuthorizeExpired:(WBEngine *)engine
{
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"请重新登录！"
                                                      delegate:nil
                                             cancelButtonTitle:@"确定"
                                             otherButtonTitles:nil];
    [alertView show];
}

- (void)engine:(WBEngine *)engine requestDidSucceedWithResult:(id)result
{
    [self hideLoadingText];
    [self hideShare];
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"分享成功！"
                                                      delegate:nil
                                             cancelButtonTitle:@"确定"
                                             otherButtonTitles:nil];
    [alertView show];
}

- (void)engine:(WBEngine *)engine requestDidFailWithError:(NSError *)error
{
    [self hideLoadingText];
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"分享失败！"
                                                      delegate:nil
                                             cancelButtonTitle:@"确定"
                                             otherButtonTitles:nil];
    [alertView show];
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    willLaunchSafari = NO;
}
/*
 * 给UIWebView 设置高度 自适应内容
 */
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self injectJavascript:webView];
    NSString *padding = @"document.body.style.margin='5px';document.body.style.padding = '5px'";
    [webView stringByEvaluatingJavaScriptFromString:padding];
    /*获取html body 的内容长度 计算高度*/
    NSString *height_str= [webView stringByEvaluatingJavaScriptFromString: @"document.body.offsetHeight"];
    int height = [height_str intValue];
    webView.frame = CGRectMake(webView.frame.origin.x, webView.frame.origin.y, webView.frame.size.width, height+130);
    /*设置WebView的superView=_bgScrollView的contentSize*/
    [scrollView setContentSize:CGSizeMake(320, 350 + height)];
    [self hideLoadingText];
    willLaunchSafari = YES;
}
/*
 * 点击网页图片,打开相册
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([self webViewImgIsClickedByRequest:request] == 0) {
        UINavigationController *nc = [self getAlbumPhotosFromImgClick:request];
        if (nc != nil) {
            nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:nc animated:YES completion:nil];
        }
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 1 && willLaunchSafari) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 2) {
        return YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

-(void)openLink{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:address]];
}
@end




