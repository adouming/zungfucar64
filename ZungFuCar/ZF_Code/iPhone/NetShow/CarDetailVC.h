//
//  CarDetailVC.h
//  ZungFuCar
//
//  Created by 刘 勇斌 on 13-11-13.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
#import "CollapseClick.h"
#import "WBEngine.h"
#import "MyUILabel.h"

@interface CarDetailVC : MenuAbstractViewController<UIWebViewDelegate, UIActionSheetDelegate, WBEngineDelegate>{
    UIScrollView *scrollView;
    WBEngine *_shareEngine;
    BOOL willLaunchSafari;
}
-(id)initWithTitle:(NSString *)title2 bannerUrl:(NSString *)bannerUrl2 date:(NSString *)date2 descp:(NSString *) descp2 address:(NSString *)address2 shortDesc:(NSString *)shortDesc2 bannerName:(NSString *)bannerName2;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *bannerUrl;
@property (nonatomic) NSString *date;
@property (nonatomic) NSString *descp;
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *shortDesc;
@property (nonatomic) NSString *bannerName;
@end





