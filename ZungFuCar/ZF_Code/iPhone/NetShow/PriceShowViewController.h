//
//  PriceShowViewController.h
//  ZungFuCar
//
//  Created by adouming on 15/7/21.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlexBaseViewController.h"
@interface PriceShowViewController : AlexBaseViewController<UIScrollViewDelegate, UIWebViewDelegate> {
    BOOL willLaunchSafari;
}

@property (strong, nonatomic) IBOutlet UIImageView *bannerView;
@property (strong, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (strong, nonatomic) IBOutlet UILabel *titleLable;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
