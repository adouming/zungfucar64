//
//  BenChiVC.h
//  ZungFuCar
//
//  Created by 刘 勇斌 on 13-11-13.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
#import "HorizontalScrollView.h"
#import "SelectTableViewCell.h"

@interface BenChiVC : MenuAbstractViewController<HorizontalScrollViewDelegate,UITableViewDataSource, UITableViewDelegate>
{
    UIImageView *_bgImageView;
    NSMutableArray *dataSource;
    
    HorizontalScrollView *hScrollView;
    
}
-(id)initWithType:(int)type;
@property int carType;
@property NSMutableArray* carArray;
@property NSMutableDictionary* carMap;
@property (nonatomic, retain) UITableView *selctTableView;
@end
