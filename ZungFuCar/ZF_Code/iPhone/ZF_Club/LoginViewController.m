//
//  LoginViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/5/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "LoginViewController.h"
#import "MemberCenterViewController.h"
#import "ZFClubManager.h"
#import "ZFStewardManager.h"
#import "ZFUserInfo.h"
#import "ForgetPasswordViewController.h"
#import "RegisterViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
	// Do any additional setup after loading the view.
    NSUserDefaults *_userDefaults = [NSUserDefaults standardUserDefaults];
    _rembered = [_userDefaults boolForKey:@"rembered"];
    if (_rembered) {
        self.remember.selected = _rembered;
        self.email.text = [_userDefaults objectForKey:@"loginEmail"];
        self.password.text = [_userDefaults objectForKey:@"loginPassword"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    
    [self setEmail:nil];
    [self setPassword:nil];
    [self setRemember:nil];
    [self setLoginBtn:nil];
    [self setRegisterBtn:nil];
    [super viewDidUnload];
}

#pragma mark - Action

- (IBAction)remberAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    _rembered = sender.selected;
}

- (IBAction)forgetBtn:(id)sender {
    ForgetPasswordViewController *forgetPasswordCtl = [[ForgetPasswordViewController alloc] init];
    [self.navigationController pushViewController:forgetPasswordCtl animated:YES];
}

- (IBAction)loginAction:(id)sender {
    
    [self.email resignFirstResponder];
    [self.password resignFirstResponder];
    
    if ((self.email.text.length != 0) && (self.password.text.length != 0)) {
        [self showLoadingText];
        
        [[ZFClubManager sharedManager] postLoginWithUser:self.email.text andPassword:[self.password.text MD5Encry:self.password.text] Success:^(NSDictionary *response) {
            NSLog(@"login success with %@", response);
            //记录账号密码
            //写入NSUserDefaults文件
            NSUserDefaults *_userDefaults = [NSUserDefaults standardUserDefaults];
            [_userDefaults setBool:_rembered forKey:@"rembered"];
            [_userDefaults setObject:self.email.text forKey:@"loginEmail"];
            [_userDefaults setObject:self.password.text forKey:@"loginPassword"];
            [_userDefaults setBool:YES forKey:@"isLogined"];
            [_userDefaults synchronize];
            
            
            NSString *userID = [[[response objectForKey:@"Data"] objectForKey:@"Member"] objectForKey:@"ID"];
            [[ZFStewardManager sharedManager] getPersonInfoWithUserID:userID Success:^(ZFUserInfoData *data) {
                
                ZFUserInfoData *userData = data;
                [[DBManager sharedManager] storeUserData:userData Sucess:^(ZFUserInfoData *storeData) {
                    [[ZFUserInfo shareInstance] setUserData:storeData];
                    [self hideLoadingText];
                    [self.navigationController popViewControllerAnimated:NO];
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTICENAME_LOGIN_SUCESS object:nil];
                } fail:^(NSError *error) {
                    [self hideLoadingText];
                }];
            } failure:^(NSError *error) {
                [self hideLoadingText];
                UIAlertView *failureView = [[UIAlertView alloc] initWithTitle:@""
                                                                      message:@"获取用户信息失败！"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [failureView show];
                
            }];
        } failure:^(NSError *error) {
            NSLog(@"login Fail=%@",error);
            [self hideLoadingText];
            UIAlertView *failureView = [[UIAlertView alloc] initWithTitle:@""
                                                                  message:@"账号或密码错误"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [failureView show];
            
        }];
    } else {
        UIAlertView *failureView = [[UIAlertView alloc] initWithTitle:@""
                                                              message:@"账号或密码不能为空。"
                                                             delegate:nil
                                                    cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [failureView show];
    }
    
}

- (IBAction)registerAction:(id)sender {
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Club" bundle:nil];
    RegisterViewController *registerCtl = [stryBoard instantiateViewControllerWithIdentifier:@"registerVCID"];
    //    [self.navigationController pushViewController:registerCtl animated:YES];
    registerCtl.pageType = 1;
    [self.navigationController pushViewController:registerCtl animated:YES];
}
#pragma mark - textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.view.frame;
    rect.origin.y = -50;
    self.view.frame = rect;
    [UIView commitAnimations];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.view.frame;
    rect.origin.y = 0;
    self.view.frame = rect;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.email resignFirstResponder];
    [self.password resignFirstResponder];
}


@end
