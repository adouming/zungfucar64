//
//  ZFUserInfo.h
//  ZungFuCar
//
//  Created by Alex Peng on 11/9/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZFStewardData.h"

//Username: robertchen2004@gmail.com
//password: test1234
@interface ZFUserInfo : NSObject
+ (ZFUserInfo *)shareInstance;
- (ZFUserInfoData *)userData;
+ (ZFUserInfoData *)userInfoData;

@property (nonatomic,retain) ZFUserInfoData *userData;
@end
