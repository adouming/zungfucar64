//
//  ZFUserInfo.m
//  ZungFuCar
//
//  Created by Alex Peng on 11/9/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ZFUserInfo.h"
#import "DBManager.h"


@implementation ZFUserInfo

static ZFUserInfo *share_UserInfo = nil;
+ (ZFUserInfo *)shareInstance{
    static dispatch_once_t pred;
	dispatch_once(&pred, ^{
        share_UserInfo = [[self alloc] init];
    });
	return share_UserInfo;
    
}
-(id)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (ZFUserInfoData *)userData{
    return _userData;
}
+ (ZFUserInfoData *)userInfoData{
    ZFUserInfoData *userData = [[DBManager sharedManager] getUserData];
    return userData;
}
@end
