//
//  MemberDiscountViewController.h
//  ZungFuCar
//
//  Created by Ed Lee on 6/10/14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
#import "HorizontalScrollView.h"


@interface MemberDiscountViewController :MenuAbstractViewController<HorizontalScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>{
    UIButton *leftButton;
    UIButton *rightButton;
    int currentIndex;
    UITableView *newsTableView;
    NSMutableArray *dataSource;
    
    int viewType;
    HorizontalScrollView *hScrollView;
    int selectIndexSwap;
}
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;
@property int selectIndexSwap;
@end