//
//  ForgetPasswordViewController.h
//  ZungFuCar
//
//  Created by GuoChengHao on 13-11-14.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"

@interface ForgetPasswordViewController : AlexBaseViewController<UITextFieldDelegate>

{
    UILabel *emailLab;
}

@property (strong, nonatomic) UIButton *sendBtn;
@property (strong, nonatomic) UITextField *emailTF;

@end
