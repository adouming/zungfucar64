//
//  RegisterViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/6/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"
#import "MutilColorLable.h"
#import "customPickerView.h"
@interface RegisterViewController : AlexBaseViewController<UITextFieldDelegate,UIActionSheetDelegate,CustomPickerViewDelegate>{
    customPickerView *pickerView;
    NSString *cityID;
    NSString *exhibitID;
    NSString *industryID;
    NSString *jobID;
    NSString *city;
    NSString *exhibit;
    NSString *industry;
    NSString *job;
    BOOL isAgree;
    IBOutlet UILabel *nickNameAlert;
    IBOutlet UILabel *emailAlert;
    IBOutlet UILabel *passwordAlert;
    IBOutlet UILabel *rePasswordAlert;
    IBOutlet UILabel *cityAlert;
    IBOutlet UILabel *exhibitionAlert;
    IBOutlet UILabel *securityCodeAlert;
    IBOutlet UILabel *agreeAlert;
    ZFUserInfoData *_userInfo;
    NSInteger _userID;
}
@property (retain,nonatomic) ZFNetServerCityExhibitionData *cityTypeData;
@property(nonatomic,copy) NSString *titleText;
@property (strong, nonatomic) IBOutlet UIScrollView *contenScrollView;
@property (strong, nonatomic) IBOutlet MutilColorLable *tipAttLabel;
@property (strong, nonatomic) IBOutlet MutilColorLable *emailAttLabel;
@property (strong, nonatomic) IBOutlet MutilColorLable *nameAttLabel;
@property (strong, nonatomic) IBOutlet MutilColorLable *passwordAttLabel;
@property (strong, nonatomic) IBOutlet MutilColorLable *rePasswordAttLabel;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passowrdTextField;
@property (strong, nonatomic) IBOutlet UITextField *rePasswordTextField;
@property (strong, nonatomic) IBOutlet UIButton *isCarOwnerBtn;
@property (strong, nonatomic) IBOutlet UIButton *notCarOwnerBtn;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *cityBtn;
@property (strong, nonatomic) IBOutlet UIButton *exhibitionBtn;
@property (strong, nonatomic) IBOutlet UIButton *jobBtn;
@property (strong, nonatomic) IBOutlet UIButton *industryBtn;
@property (strong, nonatomic) IBOutlet UITextField *nickNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *cellphoneTextField;
@property (strong, nonatomic) IBOutlet UITextField *SecurityCodeTextField;
@property (strong, nonatomic) IBOutlet UIButton *sirBtn;
@property (strong, nonatomic) IBOutlet UIButton *womenBtn;
@property (strong, nonatomic) IBOutlet UIButton *agreeBtn;
@property int pageType;
@property (strong, nonatomic) IBOutlet UILabel *yanzhenLab;
@property (strong, nonatomic) IBOutlet UIButton *getBtn;
@property (strong, nonatomic) IBOutlet UIButton *agreeText;
@property (strong, nonatomic) IBOutlet UIButton *regeditBtn;
@property (strong, nonatomic) IBOutlet UILabel *SecurityCodeLab;

@end
