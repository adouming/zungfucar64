//
//  NoticeTableViewCell.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/10/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFClubData.h"
@interface NoticeTableViewCell : UITableViewCell <UIWebViewDelegate>{

}
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *deleBtn;
//@property (strong, nonatomic) IBOutlet UILabel *detailLabel;
@property (strong, nonatomic) IBOutlet UIWebView *detailWeb;
@property (weak, nonatomic) IBOutlet UIButton *readButton;
@property (strong, nonatomic) IBOutlet UILabel *detailLab;

-(void)setData:(id)noticeData pagetype:(int)pagetype;
@end
