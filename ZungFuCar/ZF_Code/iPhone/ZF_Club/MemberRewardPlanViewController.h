//
//  MemberRewardPlanViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/9/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"
#import "NoticeViewController.h"

@interface MemberRewardPlanViewController : AlexBaseViewController<UIScrollViewDelegate, UIWebViewDelegate> {
    BOOL willLaunchSafari;
}

@property (strong, nonatomic) IBOutlet UIImageView *bannerView;
@property (strong, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (strong, nonatomic) IBOutlet UILabel *titleLable;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end