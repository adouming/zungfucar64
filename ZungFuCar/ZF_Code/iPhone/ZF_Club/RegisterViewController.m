//
//  RegisterViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/6/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "RegisterViewController.h"
#import "ZFUserInfo.h"
#import "NSStringUtil.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    [self updateCode:self.getBtn];
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    self.titleLabel.text = @"    新会员注册";
    if (self.titleText) {
        self.titleLabel.text = self.titleText;
    }
    //    _pageType = 1;
    self.emailTextField.text = @"";
    self.nickNameTextField.text = @"";
    self.passowrdTextField.text = @"";
    self.firstNameTextField.text = @"";
    self.isCarOwnerBtn.selected = YES;
    city = @"";
    exhibit = @"";
    self.firstNameTextField.text = @"";
    self.nameTextField.text = @"";
    self.sirBtn.selected = YES;
    self.cellphoneTextField.text = @"";
    industry = @"";
    job = @"";
    self.SecurityCodeTextField.text = @"";
    isAgree = self.agreeBtn.selected;
    
    [self setObjects];
    [self chooseType];
    //    [self getCityExhibitionData];
    //    [self getIndustryJobData];
    [[DataDBModel shareInstance] createDataModel];
}

#pragma mark - data
-(void)getCityExhibitionData{
    [[ZFStewardManager sharedManager] getCitySucess:^(NSMutableArray *data) {
        [[DBManager sharedManager]storeCityData:data Sucess:nil fail:nil];
    } failure:^(NSError *error) {
        NSLog(@"error--》%@", error);
    }];
}

#pragma mark - 获取行业、职业数据
-(void)getIndustryJobData{
    //    @"http://54.250.184.46/zf-web-ios/index.php/api/Booking/fn/getInfo"
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    AFHTTPRequestOperationManager *request = [AFHTTPRequestOperationManager manager];
    [request.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"Http-X-U"];
    [request.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"Http-X-P"];
    
    [request GET:[NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_GET_BOOKINFO] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"getIndustryJobData: %@", responseObject);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if ([[responseObject valueForKey:@"Code"] integerValue]==200) {
            NSMutableArray *dataSourceIndustry = [[[responseObject valueForKey:@"Data"] valueForKey:@"List"] valueForKey:@"Industry"];
            if ([dataSourceIndustry count]>0) {
                [[DBManager sharedManager]storeIndustryData:dataSourceIndustry Sucess:nil fail:nil];
            }
            NSMutableArray *dataSourceJob = [[[responseObject valueForKey:@"Data"] valueForKey:@"List"] valueForKey:@"Job"];
            if ([dataSourceJob count]>0) {
                [[DBManager sharedManager]storeJobData:dataSourceJob Sucess:nil fail:nil];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    
    [self setTipAttLabel:nil];
    [self setEmailAttLabel:nil];
    [self setNameAttLabel:nil];
    [self setPasswordAttLabel:nil];
    [self setRePasswordAttLabel:nil];
    [self setEmailTextField:nil];
    [self setNameTextField:nil];
    [self setPassowrdTextField:nil];
    [self setRePasswordTextField:nil];
    [self setContenScrollView:nil];
    [self setIsCarOwnerBtn:nil];
    [self setNotCarOwnerBtn:nil];
    [self setTitleLabel:nil];
    [self setCityBtn:nil];
    [self setExhibitionBtn:nil];
    [self setJobBtn:nil];
    [self setIndustryBtn:nil];
    [self setNameTextField:nil];
    [self setNickNameTextField:nil];
    [self setFirstNameTextField:nil];
    [self setCellphoneTextField:nil];
    [self setSecurityCodeTextField:nil];
    [self setSirBtn:nil];
    [self setWomenBtn:nil];
    [super viewDidUnload];
}

#pragma mark - set
-(void)setObjects{
    CGRect conternRect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);//self.contenScrollView.frame;
    //    if (iPhone5) {
    //        conternRect.size.height += iPhone5Step;
    //    }
    self.contenScrollView.frame = conternRect;
    self.contenScrollView.contentSize = CGSizeMake(320, 910);
    
    {
        NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
        [d setObject:HEXRGB(ZF_GoldColor) forKey:[NSValue valueWithRange:NSMakeRange(1, 1)]];
        _tipAttLabel.attributedDic = d;
    }
    {
        NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
        [d setObject:HEXRGB(ZF_GoldColor) forKey:[NSValue valueWithRange:NSMakeRange(2, 1)]];
        _emailAttLabel.attributedDic = d;
    }
    {
        NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
        [d setObject:HEXRGB(ZF_GoldColor) forKey:[NSValue valueWithRange:NSMakeRange(2, 1)]];
        _nameAttLabel.attributedDic = d;
    }
    {
        NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
        [d setObject:HEXRGB(ZF_GoldColor) forKey:[NSValue valueWithRange:NSMakeRange(2, 1)]];
        _passwordAttLabel.attributedDic = d;
    }
    {
        NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
        [d setObject:HEXRGB(ZF_GoldColor) forKey:[NSValue valueWithRange:NSMakeRange(4, 1)]];
        _rePasswordAttLabel.attributedDic = d;
    }
}

- (void)chooseType
{
    if (self.pageType == 2) {
        _userInfo = [[DBManager sharedManager] getUserData];
        if (_userInfo == Nil) {
            _userID = 0;
        }
        if(_userInfo.userID != 0)
        {
            NSUserDefaults *_userDefaults = [NSUserDefaults standardUserDefaults];
            _userID = _userInfo.userID;
            self.emailTextField.text = ((NSNull *)_userInfo.email == [NSNull null]) ? @"" : _userInfo.email;
            self.nickNameTextField.text = ((NSNull *)_userInfo.nickName == [NSNull null])?@"":_userInfo.nickName;
            self.passowrdTextField.text = [_userDefaults objectForKey:@"loginPassword"];
            self.rePasswordTextField.text = [_userDefaults objectForKey:@"loginPassword"];
            self.firstNameTextField.text = ((NSNull *)_userInfo.firstName == [NSNull null]) ? @"" : _userInfo.firstName;
            self.nameTextField.text = ((NSNull *)_userInfo.lastName == [NSNull null])? @"" : _userInfo.lastName;
            if ([_userInfo.gender isEqualToString:@"0"]) {
                self.sirBtn.selected = YES;
                self.womenBtn.selected = NO;
            }else{
                self.sirBtn.selected = NO;
                self.womenBtn.selected = YES;
            }
            
            self.cellphoneTextField.text = ((NSNull *)_userInfo.homePhone == [NSNull null])? @"":_userInfo.homePhone;
            industry = [NSString stringWithFormat:@"%d", _userInfo.otherJob];
            job = [NSString stringWithFormat:@"%d", _userInfo.job];
            self.SecurityCodeTextField.text = @"";
            //        isAgree = self.agreeBtn.selecte
            
            //获取登陆用户所属行业
            int industryCount = [[[DBManager sharedManager] getIndustryData] count];
            for (int i=0; i<industryCount; i++) {
                ZFNetServerIndustryData *industryData = [[[DBManager sharedManager] getIndustryData] objectAtIndex:i];
                if ([industryData.value integerValue] == _userInfo.otherJob) {
                    [self.industryBtn setTitle:industryData.displayName forState:UIControlStateNormal];
                    industryID = industryData.value;
                    break;
                }
            }
            
            //获取登陆用户所从事职业
            int jobCount = [[[DBManager sharedManager] getJobData] count];
            for (int i=0; i<jobCount; i++) {
                ZFNetServerJobData *jobData = [[[DBManager sharedManager] getJobData] objectAtIndex:i];
                if ([jobData.value integerValue] == _userInfo.job) {
                    [self.jobBtn setTitle:jobData.displayName forState:UIControlStateNormal];
                    jobID = jobData.value;
                    break;
                }
            }
            
            //城市、展厅初始化
            int cityCount = [[[DBManager sharedManager] getCityData] count];
            for (int i=0; i<cityCount; i++) {
                ZFNetServerCityExhibitionData *cityData = [[[DBManager sharedManager] getCityData] objectAtIndex:i];
                for (int j=0; j<cityData.exhibitionArray.count; j++) {
                    ZFNetServerExhibitionData *exhibitData = [cityData.exhibitionArray objectAtIndex:j];
                    if (exhibitData.exhibitionID == _userInfo.storeID) {
                        //设置店铺
                        [self.exhibitionBtn setTitle:exhibitData.location forState:UIControlStateNormal];
                        exhibitID = [NSString stringWithFormat:@"%d", exhibitData.exhibitionID];
                        //设置城市
                        [self.cityBtn setTitle:cityData.name forState:UIControlStateNormal];
                        cityID = cityData.cityid;
                        break;
                    }
                }
            }
            //            //城市、展厅初始化
            //            cityID = [_userDefaults objectForKey:[NSString stringWithFormat:@"cityID%d", _userID]];
            //            if (![NSStringUtil isBlankString:cityID]) {
            //                exhibitID = [_userDefaults objectForKey:[NSString stringWithFormat:@"exhibitID%d", _userID]];
            //                city = [_userDefaults objectForKey:[NSString stringWithFormat:@"city%d", _userID]];
            //                exhibit = [_userDefaults objectForKey:[NSString stringWithFormat:@"exhibit%d", _userID]];
            //                [self.cityBtn setTitle:city forState:UIControlStateNormal];
            //                [self.exhibitionBtn setTitle:exhibit forState:UIControlStateNormal];
            //            }
        }
        self.SecurityCodeLab.hidden = YES;
        self.yanzhenLab.hidden = YES;
        self.SecurityCodeTextField.hidden = YES;
        self.agreeBtn.hidden = YES;
        self.getBtn.hidden = YES;
        self.agreeText.hidden = YES;
        [self.regeditBtn setTitle:@"提交" forState:UIControlStateNormal];
        self.regeditBtn.frame = CGRectMake(100, 705, 120, 35);
        
    }
}

#pragma mark - Action
- (IBAction)hideKeyBoardAction:(id)sender {
    
    //    [self.emailTextField resignFirstResponder];
    //    [self.nameTextField resignFirstResponder];
    //    [self.passowrdTextField resignFirstResponder];
    //    [self.rePasswordTextField resignFirstResponder];
    
}

- (IBAction)isCarOwnerAction:(UIButton *)sender {
    if ([sender isEqual:_isCarOwnerBtn]) {
        _isCarOwnerBtn.selected = YES;
        _isCarOwnerBtn.userInteractionEnabled = NO;
        _notCarOwnerBtn.selected = !_isCarOwnerBtn.selected;
        _notCarOwnerBtn.userInteractionEnabled = !_isCarOwnerBtn.userInteractionEnabled;
    }
    if ([sender isEqual:_notCarOwnerBtn]) {
        _notCarOwnerBtn.selected = YES;
        _notCarOwnerBtn.userInteractionEnabled = NO;
        _isCarOwnerBtn.selected = !_notCarOwnerBtn.selected;
        _isCarOwnerBtn.userInteractionEnabled = !_notCarOwnerBtn.userInteractionEnabled;
    }
}

- (IBAction)genderAction:(UIButton *)sender {
    if ([sender isEqual:_sirBtn]) {
        _sirBtn.selected = YES;
        _sirBtn.userInteractionEnabled = NO;
        _womenBtn.selected = !_sirBtn.selected;
        _womenBtn.userInteractionEnabled = !_sirBtn.userInteractionEnabled;
    }
    if ([sender isEqual:_womenBtn]) {
        _womenBtn.selected = YES;
        _womenBtn.userInteractionEnabled = NO;
        _sirBtn.selected = !_womenBtn.selected;
        _sirBtn.userInteractionEnabled = !_womenBtn.userInteractionEnabled;
    }
}


- (IBAction)showPickerView:(UIButton *)sender {
    
    pickerView.hidden = NO;
    
    CGPoint position = CGPointMake(0, sender.frame.origin.y-100);
    [self.contenScrollView setContentOffset:position animated:YES];
    
    //    [UIView beginAnimations:@"text" context:nil];
    //    [UIView setAnimationDuration:0.5];
    //    CGRect rect = self.contenScrollView.frame;
    //    rect.origin.y = -100;
    //    self.contenScrollView.frame = rect;
    //    [UIView commitAnimations];
    
    self.contenScrollView.scrollEnabled = NO;
    
    if ([self.emailTextField isFirstResponder]) {
        [self.emailTextField resignFirstResponder];
    }
    else if ([self.nameTextField isFirstResponder]) {
        [self.nameTextField resignFirstResponder];
    }
    else if ([self.passowrdTextField isFirstResponder]) {
        [self.passowrdTextField resignFirstResponder];
    }
    else if ([self.rePasswordTextField isFirstResponder]) {
        [self.rePasswordTextField resignFirstResponder];
    }
    else if ([self.nickNameTextField isFirstResponder]) {
        [self.nickNameTextField resignFirstResponder];
    }
    else if ([self.firstNameTextField isFirstResponder]) {
        [self.firstNameTextField resignFirstResponder];
    }
    else if ([self.cellphoneTextField isFirstResponder]) {
        [self.cellphoneTextField resignFirstResponder];
    }
    else if ([self.SecurityCodeTextField isFirstResponder]) {
        [self.SecurityCodeTextField resignFirstResponder];
    }
    
    //    if (pickerView) {
    //        pickerView.hidden = !pickerView.hidden;
    //    }
    
    if (pickerView == nil) {
        pickerView = [[customPickerView alloc] initWithTitle:@"" delegate:self];
        pickerView.pickerDelegate = self;
    }
    
    
    if ([sender isEqual:_cityBtn]) {
        pickerView.pickerStyle = pickerViewTypeCity;
    }
    if ([sender isEqual:_exhibitionBtn]) {
        pickerView.cityData = self.cityTypeData;
        pickerView.pickerStyle = pickerViewTypeExhibit;
    }
    if ([sender isEqual:_jobBtn]) {
        pickerView.pickerStyle = pickerViewTypeJobReserve;
    }
    if ([sender isEqual:_industryBtn]) {
        pickerView.pickerStyle = pickerViewTypeIndustryReserve;
    }
    
    [pickerView showInView:self.view];
    
}

- (IBAction)agreeAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    isAgree = sender.selected;
}

- (IBAction)agreeTextBtn:(id)sender {
    
}

- (BOOL)infoIntegrityCheck
{
    if ([self isValidateEmail:self.emailTextField.text]) {
        emailAlert.hidden = YES;
    } else {
        if ([self.emailTextField.text length] == 0) {
            emailAlert.text = @"必填";
        } else {
            emailAlert.text = @"有误";
        }
        emailAlert.hidden = NO;
    }
    if ([self.nickNameTextField.text length] == 0) {
        nickNameAlert.hidden = NO;
    } else {
        nickNameAlert.hidden = YES;
    }
    
    if ([self.passowrdTextField.text length] == 0 && self.pageType == 1) {
        passwordAlert.hidden = NO;
    } else {
        passwordAlert.hidden = YES;
    }
    
    if ([self.rePasswordTextField.text length] == 0 && self.pageType == 1) {
        rePasswordAlert.hidden = NO;
    } else {
        rePasswordAlert.hidden = YES;
    }
    
    if ([self.cityBtn.titleLabel.text isEqualToString:@"选择"] && _isCarOwnerBtn.isSelected) {
        cityAlert.hidden = NO;
        city = @"";
    } else {
        cityAlert.hidden = YES;
    }
    
    if ([self.exhibitionBtn.titleLabel.text isEqualToString:@"选择"] && _isCarOwnerBtn.isSelected) {
        exhibitionAlert.hidden = NO;
        exhibit = @"";
    } else {
        exhibitionAlert.hidden = YES;
    }
    
    if ([self.industryBtn.titleLabel.text isEqualToString:@"选择"]) {
        industry = @"";
    }
    
    if ([self.jobBtn.titleLabel.text isEqualToString:@"选择"]) {
        job = @"";
    }
    
    if (isAgree) {
        agreeAlert.hidden = YES;
    } else {
        agreeAlert.hidden = NO;
    }
    
    if (self.pageType == 1) {
        
        if ([self.SecurityCodeTextField.text isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"Code"]]) {
            securityCodeAlert.hidden = YES;
        } else {
            if ([self.SecurityCodeTextField.text length] == 0) {
                securityCodeAlert.text = @"必填";
            } else {
                securityCodeAlert.text = @"有误";
            }
            securityCodeAlert.hidden = NO;
        }
        
    }
    
    return (emailAlert.hidden && passwordAlert.hidden && rePasswordAlert.hidden && cityAlert.hidden && exhibitionAlert.hidden && agreeAlert.hidden && securityCodeAlert.hidden);
}

- (BOOL)isValidateEmail:(NSString *)email
{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}

- (BOOL)validateNumeric:(NSString *)str2validate
{
    NSString *phoneRegex = @"[0-9]{1,11}";
    
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",phoneRegex];
    
    return [phoneTest evaluateWithObject:str2validate];
}

- (IBAction)registerAction:(id)sender {
    //注册
    if (self.pageType == 1 && [self infoIntegrityCheck]) {
        if ([self.passowrdTextField.text isEqualToString:self.rePasswordTextField.text]) {
            NSDictionary *userInfoDic = [[NSDictionary alloc] initWithObjectsAndKeys:self.emailTextField.text, @"Email",
                                         self.nickNameTextField.text, @"NickName",
                                         self.passowrdTextField.text, @"Password",
                                         cityID, @"City",
                                         exhibitID, @"StoreID",
                                         self.firstNameTextField.text, @"FirstName",
                                         self.nameTextField.text, @"LastName",
                                         self.sirBtn.selected?@"0":@"1", @"Gender",
                                         self.cellphoneTextField.text, @"HomePhone",
                                         industryID, @"OtherJob",
                                         jobID, @"Job",
                                         self.SecurityCodeTextField.text, @"VerifyCode", nil];
            
            NSLog(@"%@", userInfoDic);
            [self showLoadingText];
            [[ZFClubManager sharedManager] postRegisterWithInfo:userInfoDic Success:^(NSDictionary *response) {
                
                NSLog(@"%@", response);
                //记录账号密码
                //写入NSUserDefaults文件
                NSUserDefaults *_userDefaults = [NSUserDefaults standardUserDefaults];
                [_userDefaults setObject:self.emailTextField.text forKey:@"loginEmail"];
                [_userDefaults setObject:self.passowrdTextField.text forKey:@"loginPassword"];
                [_userDefaults setBool:YES forKey:@"isLogined"];
                [_userDefaults synchronize];
                
                NSString *userID = [[[response objectForKey:@"Data"] objectForKey:@"List"] objectForKey:@"ID"];
                
                [[ZFStewardManager sharedManager] getPersonInfoWithUserID:userID Success:^(ZFUserInfoData *data) {
                    
                    ZFUserInfoData *userData = data;
                    [[DBManager sharedManager] storeUserData:userData Sucess:^(ZFUserInfoData *storeData) {
                        [[ZFUserInfo shareInstance] setUserData:storeData];
                        [self hideLoadingText];
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogined"];
                        [self.navigationController popViewControllerAnimated:NO];
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTICENAME_LOGIN_SUCESS object:nil];
                        
                        //保存城市、展厅信息
                        if (![NSStringUtil isBlankString:cityID]) {
                            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                            [userDefaults setObject:city forKey:[NSString stringWithFormat:@"city%d", _userID]];
                            [userDefaults setObject:exhibit forKey:[NSString stringWithFormat:@"exhibit%d", _userID]];
                            [userDefaults setObject:cityID forKey:[NSString stringWithFormat:@"cityID%d", _userID]];
                            [userDefaults setObject:exhibitID forKey:[NSString stringWithFormat:@"exhibitID%d", _userID]];
                            [userDefaults synchronize];
                        }
                        
                    } fail:^(NSError *error) {
                        [self hideLoadingText];
                    }];
                } failure:^(NSError *error) {
                    [self hideLoadingText];
                    UIAlertView *failureView = [[UIAlertView alloc] initWithTitle:@""
                                                                          message:@"获取用户信息失败！"
                                                                         delegate:nil
                                                                cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [failureView show];
                    
                }];
                
            } failure:^(NSError *error) {
                
                [self hideLoadingText];
            
                UIAlertView *failAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"注册失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [failAlert show];
                
            }];
            
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"两次输入的密码不一致。" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } else if (self.pageType == 2 && [self infoIntegrityCheck]) {
        //修改设置
        if ([self.passowrdTextField.text isEqualToString:self.rePasswordTextField.text]) {
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys: self.emailTextField.text, @"Email", self.nickNameTextField.text, @"NickName", [self.passowrdTextField.text MD5Encry:self.passowrdTextField.text], @"Password", cityID, @"City", exhibitID, @"StoreID", self.firstNameTextField.text, @"FirstName", self.nameTextField.text, @"LastName", self.sirBtn.selected?@"0":@"1", @"Gender", self.cellphoneTextField.text, @"HomePhone", industryID, @"OtherJob", jobID, @"Job", nil];
            ////////////////////////////////
            // 1
            //
            //
            [self showLoadingText];
            
            //            NSString *BaseURLString = ZF_BASE_URL;
            //            NSURL *baseURL = [NSURL URLWithString:BaseURLString];
            
            
            ////////////////////////////////////
            AFHTTPRequestOperationManager *put = [AFHTTPRequestOperationManager manager];
            [put.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"HTTP-X-U"];
            [put.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"HTTP-X-P"];
            put.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            
            [put POST:[NSString stringWithFormat:@"%@/index.php/apipost/Member/id/%d", ZF_BASE_URL,_userID] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [[ZFStewardManager sharedManager] getPersonInfoWithUserID:[NSString stringWithFormat:@"%d", _userID] Success:^(ZFUserInfoData *data) {
                    ZFUserInfoData *userData = data;
                    [[DBManager sharedManager] storeUserData:userData Sucess:^(ZFUserInfoData *storeData) {
                        //                                        [[ZFUserInfo shareInstance] setUserData:storeData];
                        [self hideLoadingText];
                        
                        //保存城市、展厅信息
                        if (![NSStringUtil isBlankString:cityID]) {
                            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                            [userDefaults setObject:city forKey:[NSString stringWithFormat:@"city%d", _userID]];
                            [userDefaults setObject:exhibit forKey:[NSString stringWithFormat:@"exhibit%d", _userID]];
                            [userDefaults setObject:cityID forKey:[NSString stringWithFormat:@"cityID%d", _userID]];
                            [userDefaults setObject:exhibitID forKey:[NSString stringWithFormat:@"exhibitID%d", _userID]];
                            [userDefaults synchronize];
                        }
                        
                        UIAlertView *succ = [[UIAlertView alloc] initWithTitle:@""
                                                                       message:@"提交成功！"
                                                                      delegate:nil
                                                             cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [succ show];
                    } fail:^(NSError *error) {
                        [self hideLoadingText];
                    }];
                } failure:^(NSError *error) {
                    [self hideLoadingText];
                    UIAlertView *failureView = [[UIAlertView alloc] initWithTitle:@""
                                                                          message:@"获取用户信息失败！"
                                                                         delegate:nil
                                                                cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [failureView show];
                    
                }];
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"error with %@", error);
                [self hideLoadingText];
                UIAlertView *failureView = [[UIAlertView alloc] initWithTitle:@""
                                                                      message:@"发送失败"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [failureView show];
            }];
            
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"两次输入的密码不一致。" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"表单填写有误，请核对后再次发送" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (IBAction)updateCode:(id)sender {
    //获取一个随机整数，范围在[from,to），包括from，不包括to
    NSString *codeNum = [NSString stringWithFormat:@"%d", (int)(1000 + (arc4random() % (9999 - 1000 + 1)))];
    [[NSUserDefaults standardUserDefaults] setObject:codeNum forKey:@"Code"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.SecurityCodeLab.text = codeNum;
}

#pragma mark - action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.contenScrollView.frame;
    rect.origin.y = 0;
    self.contenScrollView.frame = rect;
    [UIView commitAnimations];
    self.contenScrollView.scrollEnabled = YES;
    
    if ([actionSheet isKindOfClass:[customPickerView class]]){
        pickerView.hidden = YES;
        if(buttonIndex == 0) {
            
        }else {
            
            
        }
    }
    
}
#pragma mark - textfield delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    pickerView.hidden = YES;
    
    CGPoint position = CGPointMake(0, textField.frame.origin.y-150);
    [self.contenScrollView setContentOffset:position animated:YES];
    
    //    [UIView beginAnimations:@"text" context:nil];
    //    [UIView setAnimationDuration:0.5];
    //    CGRect rect = self.view.frame;
    //    rect.origin.y = -100;
    //    self.view.frame = rect;
    //    [UIView commitAnimations];
    
    self.contenScrollView.scrollEnabled = NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.view.frame;
    rect.origin.y = 0;
    self.view.frame = rect;
    [UIView commitAnimations];
    self.contenScrollView.scrollEnabled = YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.emailTextField resignFirstResponder];
    [self.nameTextField resignFirstResponder];
    [self.passowrdTextField resignFirstResponder];
    [self.rePasswordTextField resignFirstResponder];
    [self.nickNameTextField resignFirstResponder];
    [self.firstNameTextField resignFirstResponder];
    [self.cellphoneTextField resignFirstResponder];
    [self.SecurityCodeTextField resignFirstResponder];
    pickerView.hidden = YES;
    
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.contenScrollView.frame;
    rect.origin.y = 0;
    self.contenScrollView.frame = rect;
    [UIView commitAnimations];
    self.contenScrollView.scrollEnabled = YES;
}

#pragma mark - pickerView delegate
-(void)pickerViewDidSelectRow:(NSInteger)row withStyle:(pickerViewType)style andData:(id)data{
    
    switch (style) {
            
        case pickerViewTypeCity:{
            ZFNetServerCityExhibitionData *stData = (ZFNetServerCityExhibitionData *)data;
            [self.cityBtn setTitle:stData.name forState:UIControlStateNormal];
            city = stData.name;
            cityID = stData.cityid;
            self.cityTypeData = stData;
        }
            
            break;
            
        case pickerViewTypeExhibit:{
            ZFNetServerExhibitionData *stData = (ZFNetServerExhibitionData *)data;
            [self.exhibitionBtn setTitle:stData.location forState:UIControlStateNormal];
            exhibit = stData.location;
            exhibitID = [NSString stringWithFormat:@"%d", stData.exhibitionID];
            
        }
            
            break;
        case pickerViewTypeIndustryReserve:{
            ZFNetServerIndustryData *stData = (ZFNetServerIndustryData *)data;
            industry = stData.displayName;
            industryID = stData.value;
            [self.industryBtn setTitle:industry forState:UIControlStateNormal];
            
        }
            
            break;
        case pickerViewTypeJobReserve:{
            ZFNetServerJobData *stData = (ZFNetServerJobData *)data;
            job = stData.displayName;
            jobID = stData.value;
            [self.jobBtn setTitle:job forState:UIControlStateNormal];
            
        }
            
            break;
            
        default:
            break;
    }
    
}

@end
