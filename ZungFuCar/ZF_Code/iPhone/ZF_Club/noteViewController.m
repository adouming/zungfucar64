//
//  noteViewController.m
//  ZungFuCar
//
//  Created by GuoChengHao on 13-12-20.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "noteViewController.h"

@interface noteViewController ()

@end

@implementation noteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
