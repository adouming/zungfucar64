//
//  MemberCenterViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/9/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"
#import "NoticeViewController.h"

@interface MemberCenterViewController : AlexBaseViewController< UIAlertViewDelegate>

{
    ZFUserInfoData *_userInfo;
    NSString *_noteCount;
    NoticeViewController *noticeVC;
    ZFNoticeMutableArray *noticeDataArray;
    ZFNoticeMutableArray *bookDataArray;
}

@property (strong, nonatomic) IBOutlet UIImageView *titleBgImageView;
@property (strong, nonatomic) IBOutlet UIImageView *userImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *noteCountBtn;

@end
