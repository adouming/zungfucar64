//
//  UIScrollView+UITouch.h
//  ZungFuCar
//
//  Created by GuoChengHao on 13-11-13.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (UITouch)

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

@end
