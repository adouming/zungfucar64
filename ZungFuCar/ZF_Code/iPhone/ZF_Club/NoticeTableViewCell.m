//
//  NoticeTableViewCell.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/10/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NoticeTableViewCell.h"
#import "NSStringUtil.h"

@implementation NoticeTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib{
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setData:(id)noticeData pagetype:(int)pagetype{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *readIdString = [userDefaults valueForKey:@"readIdString"];
    if (pagetype == 1) {
        self.detailLab.hidden = NO;
        
        self.detailWeb.hidden = YES;
        
        self.detailLab.lineBreakMode = NSLineBreakByWordWrapping|NSLineBreakByTruncatingTail;
        self.detailLab.numberOfLines = 0;
        
        ZFMemberNoticeeData *zFMemberNoticeeDataTmp = (ZFMemberNoticeeData *)noticeData;
        NSString *subjectString = zFMemberNoticeeDataTmp.subject;
        NSRange range = [subjectString rangeOfString:@"@"];
        if (range.location!=NSNotFound) {
            self.titleLabel.text = zFMemberNoticeeDataTmp.sendBy;
        }else{
            self.titleLabel.text = zFMemberNoticeeDataTmp.subject;
        }
        
        NSString *htmlString = zFMemberNoticeeDataTmp.body;
//        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];
        NSString *resultStr = [NSStringUtil filterHTML:htmlString];
        resultStr = [resultStr stringByReplacingOccurrencesOfString:@"\r" withString:@" "];
        
        UIFont *font = [UIFont systemFontOfSize:15.0f];
        //设置一个行高上限]
        CGSize size = CGSizeMake(300,2000);
        //计算实际frame大小，并将label的frame变成实际大小
        CGSize labelsize = [resultStr sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping|NSLineBreakByTruncatingTail];
        self.detailLab.frame = CGRectMake(10, 30, 300, labelsize.height+30);
        
        NSDictionary *linkAndContentDic = [self getLink:resultStr];
        self.detailLab.text = [linkAndContentDic valueForKey:@"content"];
        
        [[self.detailLab viewWithTag:100] removeFromSuperview];
        
        NSString *linkString = [linkAndContentDic valueForKey:@"link"];
        if (linkString) {
            UIButton *linkButtton = [UIButton buttonWithType:UIButtonTypeCustom];
            linkButtton.frame = CGRectMake(100, self.detailLab.frame.size.height-40, 100, 15);
            [linkButtton setTitle:@"点击连接" forState:UIControlStateNormal];
            linkButtton.accessibilityIdentifier = linkString;
            [linkButtton addTarget:self action:@selector(openLink:) forControlEvents:UIControlEventTouchUpInside];
            [linkButtton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            linkButtton.tag = 100;
            [self.detailLab addSubview:linkButtton];
        }
        
        int noticeId = zFMemberNoticeeDataTmp.noticeID;
        if (readIdString) {
            NSRange range = [readIdString rangeOfString:[NSString stringWithFormat:@"%d" , noticeId]];
            if (range.location == NSNotFound) {
                [self.readButton setImage:[UIImage imageNamed:@"radiobtn2.png"] forState:UIControlStateNormal];
                if (zFMemberNoticeeDataTmp.isread==1) {
                    [self.readButton setImage:[UIImage imageNamed:@"radiobtn1.png"] forState:UIControlStateNormal];
                }else{
                    [self.readButton setImage:[UIImage imageNamed:@"radiobtn2.png"] forState:UIControlStateNormal];
                }
            }else{
                [self.readButton setImage:[UIImage imageNamed:@"radiobtn1.png"] forState:UIControlStateNormal];
            }
        }
    } else if (pagetype == 2) {
        self.detailWeb.hidden = YES;
        self.detailLab.hidden = NO;
        ZFMemberBookData *tmp = (ZFMemberBookData *)noticeData;
        NSString *str = [[NSString alloc] init];
        if ([tmp.type isEqualToString: @"1"]) {
            str = @"预约试驾成功";
        } else if ([tmp.type isEqualToString:@"2"]){
            str = @"维修保养预约成功";
        }
        self.titleLabel.text = @"预约成功";
        self.detailLab.text = [NSString stringWithFormat:@"    %@提醒您,您的%@%@,请尊贵的客户您按时出席。", tmp.storeName, tmp.scheduleTime, str];

        int noticeId = tmp.bookID;
        if (readIdString) {
            NSRange range = [readIdString rangeOfString:[NSString stringWithFormat:@"%d" , noticeId]];
            if (range.location == NSNotFound) {
                [self.readButton setImage:[UIImage imageNamed:@"radiobtn1.png"] forState:UIControlStateNormal];
            }else{
                [self.readButton setImage:[UIImage imageNamed:@"radiobtn2.png"] forState:UIControlStateNormal];
            }
        }
    }else{
        self.hidden = YES;
    }
}

-(NSDictionary *)getLink:(NSString *)contentString{
    NSMutableString *contentMutableString = [NSMutableString stringWithFormat:@"%@", contentString];
    if (![NSStringUtil isBlankString:contentMutableString]) {
        NSRange range = [contentMutableString rangeOfString:@"http://"];
        if (range.location == NSNotFound) {
            range = [contentMutableString rangeOfString:@"https://"];
        }
        if (range.location!=NSNotFound) {
            NSString *content = [contentMutableString substringToIndex:range.location];
            NSRange linkRange = NSMakeRange(range.location, contentString.length-range.location);
            NSString *link = [contentMutableString substringWithRange:linkRange];
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:content,@"content", link, @"link", nil];
            return dic;
        }
    }
    NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:contentString, @"content", nil];
    return dic2;
}

-(void)openLink:(UIButton *)button{
    NSString *urlString = [button.accessibilityIdentifier stringByReplacingOccurrencesOfString:@" " withString:@""];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    [[UIApplication sharedApplication] openURL:url];
}



@end




