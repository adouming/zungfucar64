//
//  ForgetPasswordViewController.m
//  ZungFuCar
//
//  Created by GuoChengHao on 13-11-14.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "ForgetPasswordViewController.h"

@interface ForgetPasswordViewController ()

@end

@implementation ForgetPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
	// Do any additional setup after loading the view.
    
    UIImageView *logoImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zungfuelitez_logo.png"]];
    logoImgView.frame = CGRectMake(84, 70, 142, 34);
    [self.view addSubview:logoImgView];
    
    emailLab = [[UILabel alloc] initWithFrame:CGRectMake(28, 131, 70, 21)];
    emailLab.backgroundColor = [UIColor clearColor];
    emailLab.textColor = [UIColor blackColor];
    emailLab.text = @"注册邮箱";
    emailLab.textAlignment = UITextAlignmentRight;
    emailLab.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:emailLab];
    
    self.emailTF = [[UITextField alloc] init];
    [self.emailTF setBackground:[UIImage imageNamed:@"textbox.png"]];
    self.emailTF.frame = CGRectMake(106, 129, 141, 25);
    self.emailTF.font = [UIFont systemFontOfSize:14];
    self.emailTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.emailTF.keyboardType = UIKeyboardTypeASCIICapable;
    self.emailTF.returnKeyType = UIReturnKeyDone;
    self.emailTF.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailTF.delegate = self;
    [self.view addSubview:self.emailTF];
    
    self.sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sendBtn.frame = CGRectMake(89.5, 184, 141, 33);
    [self.sendBtn setBackgroundImage:[UIImage imageNamed:@"golden_btn.png"] forState:UIControlStateNormal];
    [self.sendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [self.sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.sendBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.sendBtn addTarget:self action:@selector(sendEmail) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.sendBtn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button action
- (void)sendEmail
{
    [self.emailTF resignFirstResponder];
    [self showLoadingText];
    
    //    // 1
    //    //http://54.250.184.46/zf-web-ios/index.php/api/Member/fn/forgotPassword
    //

    NSDictionary *parameters = [NSDictionary dictionaryWithObject:self.emailTF.text forKey:@"Email"];
    
    // 2
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:@"text/html" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"HTTP-X-U"];
    [manager.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"HTTP-X-P"];
//    @"http://54.250.184.46/zf-web-ios/index.php/api/Member/fn/forgotPassword"
    [manager POST:[NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_FORGETPASSWORD] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"forget success with operation :%@", operation);
        NSLog(@"forget success with responseObject :%@", responseObject);
        
        [self hideLoadingText];
        UIAlertView *successView = [[UIAlertView alloc] initWithTitle:@""
                                                              message:@"邮件发送成功"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [successView show];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideLoadingText];
        NSLog(@"error with %@", error);
        NSLog(@"%@", operation);
        UIAlertView *failureView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"邮件发送失败"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [failureView show];
    }];
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.emailTF resignFirstResponder];
}


@end
