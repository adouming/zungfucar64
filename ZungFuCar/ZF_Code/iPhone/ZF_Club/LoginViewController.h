//
//  LoginViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/5/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"

@interface LoginViewController : AlexBaseViewController<UITextFieldDelegate>

{
    BOOL _rembered;
}

@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UIButton *remember;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *registerBtn;

@end
