//
//  MemberCenterViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/9/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "MemberCenterViewController.h"
#import "NoticeViewController.h"
#import "RegisterViewController.h"

@interface MemberCenterViewController ()

@end

@implementation MemberCenterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    _userInfo = [[DBManager sharedManager] getUserData];
    
    [self downloadNotification];
    //提前下载城市与工作类型数据，以期进入设置能设置按钮显示名
    [[DataDBModel shareInstance] createDataModel];
    
	// Do any additional setup after loading the view.
    self.titleLabel.text = [_userInfo.gender isEqualToString:@"0"] ? [NSString stringWithFormat:@"您好！%@先生", _userInfo.lastName] : [NSString stringWithFormat:@"您好！%@女士", _userInfo.lastName];
    self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1];
    noticeDataArray = [[ZFNoticeMutableArray alloc]init];
    bookDataArray = [[ZFNoticeMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTitleBgImageView:nil];
    [self setUserImageView:nil];
    [self setTitleLabel:nil];
    [super viewDidUnload];
}

#pragma mark - get Notification
- (void)downloadNotification
{
    NSString *userIDTmp = [NSString stringWithFormat:@"%d", _userInfo.userID];
    //获取通知信息
    [[ZFClubManager sharedManager] getMemberNoticeWithMemberID:userIDTmp Sucess:^(ZFNoticeMutableArray *data) {
        
        //保存信息
        [[DBManager sharedManager] storeNoticeData:data Sucess:^(ZFNoticeMutableArray *storeData) {
            NSString *deleteIdString = [[NSUserDefaults standardUserDefaults]valueForKey:@"deleteIdString"];
            if (deleteIdString!=nil) {
                NSMutableArray *tempArray = [[NSMutableArray alloc]init];
                for (id temp in storeData) {
                    ZFMemberNoticeeData * tmp = (ZFMemberNoticeeData *)temp;
                    NSRange range = [deleteIdString rangeOfString:[NSString stringWithFormat:@"%d" , tmp.noticeID]];
                    if (range.location != NSNotFound) {
                        [tempArray addObject:temp];
                    }else{
                        if ([tmp.subject hasSuffix:@"预约"]) {
                            [bookDataArray addObject:temp];
                        }else{
                            [noticeDataArray addObject:temp];
                        }
                    }
                }
                [storeData removeObjectsInArray:tempArray];
            }else{
                for (id temp in storeData) {
                    ZFMemberNoticeeData * tmp = (ZFMemberNoticeeData *)temp;
                    if ([tmp.subject hasSuffix:@"预约"]) {
                        [bookDataArray addObject:temp];
                    }else{
                        [noticeDataArray addObject:temp];
                    }
                }
            }
            //成功保存信息
            NSLog(@"success %@", storeData);
            _noteCount = [NSString stringWithFormat:@"%d", noticeDataArray.count];
            [self.noteCountBtn setTitle:_noteCount forState:UIControlStateNormal];
        } fail:^(NSError *error) {
            //保存不成功
            NSLog(@"error %@", error);
        }];
    } failure:^(NSError *error) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:@"获取通知失败！" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [errorAlert show];
    }];
}

#pragma mark - button action

- (IBAction)gotoNote:(id)sender {
    if (noticeVC==nil) {
        UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Club" bundle:nil];
        noticeVC = [stryBoard instantiateViewControllerWithIdentifier:@"noticeVCID"];
    }
    noticeVC.pageType = 1;
    noticeVC.titleText = @"通知";
    noticeVC.noticeDataArray = noticeDataArray;
    [noticeVC setData];
    [self.navigationController pushViewController:noticeVC animated:YES];
}
- (IBAction)gotoSet:(id)sender {
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Club" bundle:nil];
    RegisterViewController *registerVC = [stryBoard instantiateViewControllerWithIdentifier:@"registerVCID"];
    registerVC.titleText = @"   我的设置";
    registerVC.pageType = 2;
    [self.navigationController pushViewController:registerVC animated:YES];
}
- (IBAction)gotoMyBook:(id)sender {
    if (noticeVC==nil) {
        UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Club" bundle:nil];
        noticeVC = [stryBoard instantiateViewControllerWithIdentifier:@"noticeVCID"];
    }
    noticeVC.pageType = 1;
    noticeVC.titleText = @"我的预约";
    noticeVC.noticeDataArray = bookDataArray;
    [noticeVC setData];
    [self.navigationController pushViewController:noticeVC animated:YES];
}
- (IBAction)logout:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"您已成功退出登录" message:@"" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    alert.tag = 104;
    [alert show];
}

#pragma mark - alert delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 104) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLogined"];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

@end
