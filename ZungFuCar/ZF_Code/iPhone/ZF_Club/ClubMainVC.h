//
//  ClubMainVC.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/5/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "MenuAbstractViewController.h"

@interface ClubMainVC : MenuAbstractViewController
@property (weak, nonatomic) IBOutlet UIButton *loginAndRegisterButton;

@end
