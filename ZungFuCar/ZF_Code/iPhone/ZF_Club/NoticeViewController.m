//
//  NoticeViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/9/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NoticeViewController.h"
#import "NoticeTableViewCell.h"
#import "NSStringUtil.h"

#define CELLHIGHT 150
@interface NoticeViewController ()

@end

@implementation NoticeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated{
    //    if (self.pageType == 1) {
    //        self.noticeDataArray = [[DBManager sharedManager] getNoticeData];
    //    } else if (self.pageType == 2) {
    //        self.noticeDataArray = [[DBManager sharedManager] getBookData];
    //    }
    
}
- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    self.titleLabel.text = self.titleText;
    [self.bannerImageView setImage:iPhone5 ? [UIImage imageNamed:@"sl_bg_iphone5.png"] : [UIImage imageNamed:@"sl_bg.png"]];
    userDefaults = [NSUserDefaults standardUserDefaults];
    //    [self createModel];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
}

-(void)setData{
    self.titleLabel.text = self.titleText;
    deleteIdString = [userDefaults valueForKey:@"deleteIdString"];
    readIdString = [userDefaults valueForKey:@"readIdString"];
    [self resetNoticeDataArray];
    [self.tableView reloadData];
}

-(void)createModel{
    ZFUserInfoData *userInfo = [[DBManager sharedManager] getUserData];
    NSString *userIDStr = [NSString stringWithFormat:@"%d", userInfo.userID];
    if (self.pageType == 1) {
        [[ZFClubManager sharedManager] getMemberNoticeWithMemberID:userIDStr Sucess:^(ZFNoticeMutableArray *data) {
            [[DBManager sharedManager] storeNoticeData:data Sucess:^(ZFNoticeMutableArray *storeData) {
                
                //成功保存信息
                NSLog(@"success %@", storeData);
                self.noticeDataArray = storeData;
                [self resetNoticeDataArray];
            } fail:^(NSError *error) {
                //保存不成功
                NSLog(@"error %@", error);
            }];
            
        } failure:^(NSError *error) {
            NSLog(@"error=%@",error);
            UIAlertView *failAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"数据请求失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [failAlert show];
        }];
        
    } else if (self.pageType == 2) {
        
        [[ZFClubManager sharedManager] getMemberBookDataWithMemberID:userIDStr Sucess:^(ZFBookMutableArray *data) {
            
            if (data != nil) {
                [[DBManager sharedManager] storeBookData:data Sucess:^(ZFBookMutableArray *storeData) {
                    //保存成功
                    self.noticeDataArray = storeData;
                    [self resetNoticeDataArray];
                } fail:^(NSError *error) {
                    //保存失败
                    
                }];
            }
            
        } failure:^(NSError *error) {
            //获取失败
            NSLog(@"error=%@", error);
            UIAlertView *failAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"数据请求失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [failAlert show];
        }];
    }
    
}

//-(void)setData:(NSDictionary *)param{
//    NSString *title = [param objectForKey:@"title"];
//    self.titleLabel.text = title;
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView dataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.pageType == 1) {
        ZFMemberNoticeeData * tmp = (ZFMemberNoticeeData *)[self.noticeDataArray objectAtIndex:indexPath.row];
        NSString *resultStr = [NSStringUtil filterHTML:tmp.body];
        resultStr = [resultStr stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        UIFont *font = [UIFont systemFontOfSize:15.0f];
        //设置一个行高上限]
        CGSize size = CGSizeMake(300,2000);
        //计算实际frame大小，并将label的frame变成实际大小
        CGSize labelsize = [resultStr sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping|NSLineBreakByTruncatingTail];
        return labelsize.height+80;
    } else if (self.pageType == 2) {
        ZFMemberBookData *tmp = (ZFMemberBookData *)[self.noticeDataArray objectAtIndex:indexPath.row];
        return CELLHIGHT;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.noticeDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"cellIdentify";
    NoticeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        cell = [[NoticeTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    [cell setData:[self.noticeDataArray objectAtIndex:indexPath.row] pagetype:self.pageType];
    [cell.deleBtn addTarget:self action:@selector(deleteButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell.readButton addTarget:self action:@selector(readButton:) forControlEvents:UIControlEventTouchUpInside];
    if (((ZFMemberNoticeeData *)[self.noticeDataArray objectAtIndex:indexPath.row]).isread) {
        [cell.readButton setImage:[UIImage imageNamed:@"radiobtn1"] forState:UIControlStateNormal];
    } else {
        [cell.readButton setImage:[UIImage imageNamed:@"radiobtn2"] forState:UIControlStateNormal];
    }
    cell.deleBtn.tag = indexPath.row;
    cell.readButton.tag = indexPath.row;
    return cell;
}

#pragma mark - tableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setBannerImageView:nil];
    [self setTitleLabel:nil];
    [super viewDidUnload];
}
- (void)deleteButton:(id)sender {
    UIButton *button = (UIButton *)sender;
    int row = button.tag;
    int bookId;
    if (self.pageType==1) {
        ZFMemberNoticeeData *tmp = (ZFMemberNoticeeData *)[self.noticeDataArray objectAtIndex:row];
        bookId = tmp.noticeID;
    }else{
        ZFMemberBookData *tmp = (ZFMemberBookData *)[self.noticeDataArray objectAtIndex:row];
        bookId = tmp.bookID;
    }
    deleteIdString = [NSString stringWithFormat:@"%@%d", deleteIdString, bookId];
    [userDefaults setValue:deleteIdString forKey:@"deleteIdString"];
    [userDefaults synchronize];
    [self.noticeDataArray removeObjectAtIndex:row];
    [self.tableView reloadData];
}
-(void)readButton:(id)sender{
    UIButton *button = (UIButton *)sender;
    int row = button.tag;
    int bookId;
    if (self.pageType==1) {
        ZFMemberNoticeeData *tmp = (ZFMemberNoticeeData *)[self.noticeDataArray objectAtIndex:row];
        bookId = tmp.noticeID;
    }else{
        ZFMemberBookData *tmp = (ZFMemberBookData *)[self.noticeDataArray objectAtIndex:row];
        bookId = tmp.bookID;
    }
    NSRange range = [readIdString rangeOfString:[NSString stringWithFormat:@"%d" , bookId]];
    if (range.location == NSNotFound || readIdString==nil) {
        readIdString = [NSString stringWithFormat:@"%@%d", readIdString, bookId];
        [userDefaults setValue:readIdString forKey:@"readIdString"];
        [button setImage:[UIImage imageNamed:@"radiobtn1.png"] forState:UIControlStateNormal];
        ((ZFMemberNoticeeData *)[self.noticeDataArray objectAtIndex:row]).isread = 1;
    }else{
//        readIdString = [readIdString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%d", bookId] withString:@""];
//        [userDefaults setValue:readIdString forKey:@"readIdString"];
//        [button setImage:[UIImage imageNamed:@"radiobtn2.png"] forState:UIControlStateNormal];
    }
    [userDefaults synchronize];
    [self updateIsRead:row];
}

-(void)updateIsRead:(int)index{
    int bookId;
    ZFMemberNoticeeData *tmp = (ZFMemberNoticeeData *)[self.noticeDataArray objectAtIndex:index];
    bookId = tmp.noticeID;
    int read=1;
    if (tmp.isread==1) {
        read = 0;
    }
    
    NSDictionary *dic = @{@"ID": [NSString stringWithFormat:@"%d", bookId], @"isread": @"1"};
    
    AFHTTPRequestOperationManager *put = [AFHTTPRequestOperationManager manager];
    [put.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"HTTP-X-U"];
    [put.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"HTTP-X-P"];
    [put.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    put.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
//    [put POST:[NSString stringWithFormat:@"%@%@/api/emaillog/fn/updateisReadFlag", ZF_BASE_URL, ZF_BASE_PATH] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        
//        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%d", bookId] dataUsingEncoding:NSUTF8StringEncoding] name:@"ID"];
//        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%d", read] dataUsingEncoding:NSUTF8StringEncoding] name:@"isread"];
//        
//    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        NSLog(@"responseObject-->%@",responseObject);
//        NSLog(@"保存已读状态成功");
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        NSLog(@"operation %@", operation);
//        NSLog(@"保存已读状态失败 %@", error);
//        
//    }];
    [put POST:[NSString stringWithFormat:@"%@%@/api/emaillog/fn/updateisReadFlag", ZF_BASE_URL, ZF_BASE_PATH] parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"responseObject-->%@",responseObject);
        NSLog(@"保存已读状态成功");
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"operation %@", operation);
        NSLog(@"保存已读状态失败 %@", error);
        
    }];

}

-(void)resetNoticeDataArray{
    if (deleteIdString==nil) {
        return;
    }
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    for (id temp in self.noticeDataArray) {
        if (self.pageType==1) {
            ZFMemberNoticeeData * tmp = (ZFMemberNoticeeData *)temp;
            NSRange range = [deleteIdString rangeOfString:[NSString stringWithFormat:@"%d" , tmp.noticeID]];
            if (range.location != NSNotFound) {
                [tempArray addObject:temp];
            }
        }else{
            ZFMemberBookData *tmp = (ZFMemberBookData *)temp;
            NSRange range = [deleteIdString rangeOfString:[NSString stringWithFormat:@"%d" , tmp.bookID]];
            if (range.location != NSNotFound) {
                [tempArray addObject:temp];
            }
        }
    }
    [self.noticeDataArray removeObjectsInArray:tempArray];
}
@end








