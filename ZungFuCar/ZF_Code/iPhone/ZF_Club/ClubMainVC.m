//
//  ClubMainVC.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/5/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ClubMainVC.h"
#import "MemberCenterViewController.h"
#import "LoginViewController.h"
#import "CommonSingleWebViewController.h"
@interface ClubMainVC ()

@end

@implementation ClubMainVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidload");
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSucess:) name:NOTICENAME_LOGIN_SUCESS object:nil];
    self.customNavigaitionBar.rightBtn.hidden = YES;
    self.customNavigaitionBar.itemType = NavigationItemTypeHome;
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:animated];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogined"]) {
        [self.loginAndRegisterButton setTitle:@"用户中心" forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loginSucess:(NSNotification *)notice{
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Club" bundle:nil];
    [self.navigationController pushViewController:[stryBoard instantiateViewControllerWithIdentifier:@"MemberCenterVCID"] animated:YES];
//    MemberCenterViewController * memberCenterCtl = [[MemberCenterViewController alloc] init];
//    [self.navigationController pushViewController:memberCenterCtl animated:YES];
    
}

- (IBAction)isLogined:(id)sender {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogined"]) {
        UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Club" bundle:nil];
        [self.navigationController pushViewController:[stryBoard instantiateViewControllerWithIdentifier:@"MemberCenterVCID"]
                                             animated:YES];
    } else {
        UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Club" bundle:nil];
        [self.navigationController pushViewController:[stryBoard instantiateViewControllerWithIdentifier:@"LoginViewID"]
                                             animated:YES];
    }
}

//会员章程
- (IBAction)memberBookAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:43];
    controler.title = @"会员章程";
    [self.navigationController pushViewController:controler animated:YES];
}
@end
