//
//  NoticeViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/9/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"
#import "ZFClubManager.h"
@interface NoticeViewController : AlexBaseViewController<UITableViewDataSource,UITableViewDelegate>{
    NSUserDefaults *userDefaults;
    NSString *deleteIdString;
    NSString *readIdString;
}
@property (nonatomic,copy) NSString *titleText;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIImageView *bannerImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) ZFNoticeMutableArray *noticeDataArray;
@property int pageType;
-(void)setData;

//-(void)setData:(NSDictionary *)param;
@end
