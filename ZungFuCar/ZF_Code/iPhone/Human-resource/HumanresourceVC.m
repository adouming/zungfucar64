//
//  ShVC.m
//  ZungFuCar
//
//  Created by adouming on 15/8/20.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "HumanresourceVC.h"
#import "CommonSingleWebViewController.h"
#import "StarSmartCenterViewController.h"
@interface HumanresourceVC ()

@end

@implementation HumanresourceVC

- (void)viewDidLoad {
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    [self.customNavigaitionBar setItemType: NavigationItemTypeBack ];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - actions

//集团总部
- (IBAction)jituanzongbuAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Human-resource" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"HRCommWebViewID"];
    [controler setContentId:40];
    controler.title = @"集团总部";
    [self.navigationController pushViewController:controler animated:YES];
}
//各分公司
- (IBAction)gefengongsiAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Human-resource" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"HRCommWebViewID"];
    [controler setContentId:41];
    controler.title = @"各分公司";
    [self.navigationController pushViewController:controler animated:YES];
}
//毕业生工作机会
- (IBAction)gongzuojihuiAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Human-resource" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"HRCommWebViewID"];
    [controler setContentId:42];
    controler.title = @"毕业生工作机会";
    [self.navigationController pushViewController:controler animated:YES];
}
//招聘流程
- (IBAction)zhaopingliuchengAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Human-resource" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"HRCommWebViewID"];
    [controler setContentId:50];
    controler.title = @"招聘流程";
    [self.navigationController pushViewController:controler animated:YES];
}
//职位申请
- (IBAction)gangweishenqingAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Human-resource" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"HRCommWebViewID"];
    [controler setContentId:51];
    controler.title = @"职位申请";
    [self.navigationController pushViewController:controler animated:YES];
}
//常见问题解答
- (IBAction)faqAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Human-resource" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"HRCommWebViewID"];
    [controler setContentId:52];
    controler.title = @"常见问题解答";
    [self.navigationController pushViewController:controler animated:YES];
}
//联系我们
- (IBAction)contactusAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Human-resource" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"HRCommWebViewID"];
    [controler setContentId:53];
    controler.title = @"联系我们";
    [self.navigationController pushViewController:controler animated:YES];
}


@end
