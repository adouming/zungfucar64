//
//  ShVC.h
//  ZungFuCar
//
//  Created by adouming on 15/8/20.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
#import "WBEngine.h"
#import "ICETutorialController.h"
@interface HumanresourceVC : MenuAbstractViewController<WBEngineDelegate>
{
    __strong IBOutlet UIImageView *_bgImageView;
    __strong WBEngine *_shareEngine;
}
@property (strong, nonatomic) ICETutorialController *iceViewController;
/**
 *	关注微博
 *
 *	@param	sender	按钮
 */
- (IBAction)AttentionWeiboBtnClicked:(id)sender;

/**
 *	发展历程
 *
 *	@param	sender	按钮
 */
//- (IBAction)companyHistory:(id)sender;
@end
