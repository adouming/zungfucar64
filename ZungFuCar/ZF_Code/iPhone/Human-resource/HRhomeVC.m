//
//  ShVC.m
//  ZungFuCar
//
//  Created by adouming on 15/8/20.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "HRhomeVC.h"
#import "CommonSingleWebViewController.h"
#import "StarSmartCenterViewController.h"
@interface HRhomeVC ()

@end

@implementation HRhomeVC

- (void)viewDidLoad {
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    UIStoryboard* sb = self.storyboard;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - actions

//人才培训与发展
- (IBAction)qinzhifuwuAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Human-resource" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"HRCommWebViewID"];
    [controler setContentId:37];
    controler.title = @"人才培训与发展";
    [self.navigationController pushViewController:controler animated:YES];
}


@end
