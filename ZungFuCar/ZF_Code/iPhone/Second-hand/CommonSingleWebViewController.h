//
//  CommWebViewController.h
//  ZungFuCar
//
//  Created by adouming on 15/9/16.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
@interface CommonSingleWebViewController : MenuAbstractViewController <UIWebViewDelegate>
{
    __strong IBOutlet UIScrollView *_bgScrollView;/*scrollview 需要拉动*/
    __strong IBOutlet UILabel *_contentLabel;/*无效*/
    __strong IBOutlet UILabel *_titleLabel;/*banner下方显示文字*/
    __weak IBOutlet UIWebView *_contentWebView;/*内容*/
    __weak IBOutlet UIImageView *_bannerView;/*banner scaleToFit*/
    __weak IBOutlet UIView *_topView;
    BOOL willLaunchSafari;
    NSString* viewtitle;
    int _cid;
}
- (void)setContentId:(int)contentID;
- (void)setTitle:(NSString *)title;
- (void)creatModel;
@end
