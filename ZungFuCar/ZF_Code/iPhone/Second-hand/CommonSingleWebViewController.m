//
//  CommWebViewController.m
//  ZungFuCar
//
//  Created by adouming on 15/9/16.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "CommonSingleWebViewController.h"
#import "AboutZFAPIManager.h"
#import "DBManager.h"
#import "NSObject+AddJavascript.h"
#import "NSString+FilterHtml.h"
@interface CommonSingleWebViewController ()

@end

@implementation CommonSingleWebViewController
- (void)setContentId:(int)contentID {
    _cid = contentID;
}
- (void)setTitle:(NSString *)title {
    viewtitle = title;
}
- (void)viewDidLoad {
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    //auto resize
    CGRect rect = CGRectMake(0, 0,  [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height);
    NSLog(@"%@", NSStringFromCGRect(rect));
    [self.view setFrame: rect];
    
    [_bgScrollView setFrame:rect];
    
    rect = CGRectMake(_contentWebView.frame.origin.x, _contentWebView.frame.origin.y,  [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - _contentWebView.frame.origin.y );
    NSLog(@"%@", NSStringFromCGRect(rect));
    [_contentWebView setFrame:rect];
    
    [_topView setFrame:CGRectMake(_topView.frame.origin.x, _topView.frame.origin.y, [UIScreen mainScreen].bounds.size.width, _topView.frame.size.height) ];
    
    //init
    [_titleLabel setFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y, [UIScreen mainScreen].bounds.size.width, _titleLabel.frame.size.height) ];
    
    _titleLabel.text = viewtitle;
    // Do any additional setup after loading the view.
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    [self.backgroundImageView removeFromSuperview];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _contentWebView.scrollView.scrollEnabled = NO;
    
    /*读取数据 请求数据*/
    [self creatModel];
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload
{
    _bgScrollView = nil;
    _contentLabel = nil;
    _titleLabel = nil;
    [super viewDidUnload];
}

/**
 * Model
 */
- (void)creatModel
{
    NSArray *array = [[DBManager sharedManager] getSecondCarData: _cid];
    if([array count] ==0 )
        return;
    NSString *contentStr = [[array objectAtIndex:0] objectForKey:@"Content"];
    [_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: contentStr] baseURL:[[DBManager sharedManager] baseUrl]];
    NSLog(@"web content: %@", contentStr);
    NSString *bannerUrl = [[array objectAtIndex:0] objectForKey:@"BannerURL"];
    if([bannerUrl validatePng] &&![bannerUrl isEqualToString:@""])
        [_bannerView setImageWithURL:[NSURL URLWithString:[[array objectAtIndex:0] objectForKey:@"BannerURL"]]];
}
/**
 *  请求数据
 */
- (void)requestData
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:ViewID146_UpdatedTime]==nil)
    {
        [self showLoadingText];
    }
    AboutZFAPIManager *manager = [[AboutZFAPIManager alloc] init];
    //[manager requestSecondCarDataSuccess:<#^(NSArray *data)success#> cid:<#(int)#> failure:<#^(NSError *error)failure#>]
    [manager requestSecondCarDataSuccess:^(NSArray *data) {
        BOOL isFirst = NO;
        if([data count]==0)
        {
            return ;
        }
        else
        {
            /*写lastmodifyTime到沙盒*/
            NSString* sandboxID = [NSString stringWithFormat:@"ViewID%d_UpdatedTime",_cid];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            if([userDefaults objectForKey:sandboxID]==nil)
                isFirst = YES;
            [userDefaults setObject:[[data objectAtIndex:0] objectForKey:@"UpdatedTime"] forKey:sandboxID];
            [userDefaults synchronize];

            
            /*content url 图片不规范 需要附加http*/
           
            NSString *htmlStr =[NSString stringWithFormat:@"%@", [[data objectAtIndex:0] objectForKey:@"content"]];
            
            //_contentWebView.scalesPageToFit = true; //scalesPageToFit];
            //[_contentWebView loadHTMLString: [[DBManager sharedManager] wrapperHtml: htmlStr] baseURL:nil];
            NSLog(@"web content: %@", htmlStr);
            [self hideLoadingText];
            /*重新封装数据*/
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[
                                                                            [data objectAtIndex:0] objectForKey:@"content"],@"BannerURL",
                                 htmlStr,@"Content",
                                 [[data objectAtIndex:0] objectForKey:@"id"],@"ID",
                                 [[data objectAtIndex:0] objectForKey:@"title"],@"Name",
                                 [[data objectAtIndex:0] objectForKey:@"updatetime"],@"UpdatedTime",
                                 nil];
            
            NSMutableArray *dataTemp =[[NSMutableArray alloc] initWithObjects:dic, nil];
            /*存储数据*/
            [[DBManager sharedManager] storeSecondCarData:dataTemp cid:_cid Sucess:^(NSMutableArray *storeData) {
                /*判断是否是第一次读取，如果是需要reload UI*/
                if(isFirst==YES)
                    [self creatModel];
            } fail:^(NSError *error) {
                
            }];
        }
        
    }
    cid:_cid
    failure:^(NSError *error){
        [self hideLoadingText];
    }];
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    willLaunchSafari = NO;
}

/*
 * 给UIWebView 设置高度 自适应内容
 */
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self injectJavascript:webView];
    
    NSString *padding = @"document.body.style.margin='5px';document.body.style.padding = '5px'";
    [webView stringByEvaluatingJavaScriptFromString:padding];
    
    /*获取html body 的内容长度 计算高度*/
    NSString *height_str= [webView stringByEvaluatingJavaScriptFromString: @"document.body.scrollHeight"];
    NSLog(@"body height: %@", height_str);
    int height = [height_str intValue];
    webView.frame = CGRectMake(webView.frame.origin.x,webView.frame.origin.y,webView.frame.size.width,height);
    
    /*设置WebView的superView=_bgScrollView的contentSize*/
    [_bgScrollView setContentSize:CGSizeMake(_bgScrollView.frame.size.width,CGRectGetMaxY(webView.frame))];
    willLaunchSafari = YES;
}
/*
 * 点击网页图片,打开相册
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([self webViewImgIsClickedByRequest:request] == 0) {
        UINavigationController *nc = [self getAlbumPhotosFromImgClick:request];
        if (nc != nil) {
            nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:nc animated:YES completion:nil];
        }
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 1 && willLaunchSafari) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    } else if ([self webViewImgIsClickedByRequest:request] == 2) {
        return YES;
    }
}

@end
