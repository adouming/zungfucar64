//
//  ShVC.m
//  ZungFuCar
//
//  Created by adouming on 15/8/20.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "SendhandVC.h"
#import "CommonSingleWebViewController.h"
#import "StarSmartCenterViewController.h"
@interface SendhandVC ()

@end

@implementation SendhandVC

- (void)viewDidLoad {
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    UIStoryboard* sb = self.storyboard;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - actions

//臻至服务
- (IBAction)qinzhifuwuAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:29];
    controler.title = @"臻至服务";
    [self.navigationController pushViewController:controler animated:YES];
}
//五星评估
- (IBAction)fiveStarAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:30];
    controler.title = @"五星评估";
    [self.navigationController pushViewController:controler animated:YES];
}
//一站式置换
- (IBAction)onestepExchangeAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:31];
    controler.title = @"一站式置换";
    [self.navigationController pushViewController:controler animated:YES];
}
//星睿认证
- (IBAction)xinruiauthorAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:32];
    controler.title = @"星睿认证";
    [self.navigationController pushViewController:controler animated:YES];
}
//优质回购
- (IBAction)goodbacksellAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:33];
    controler.title = @"优质回购";
    [self.navigationController pushViewController:controler animated:YES];
}
//尊品寄售
- (IBAction)goodsMailAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:34];
    controler.title = @"尊品寄售";
    [self.navigationController pushViewController:controler animated:YES];
}
//问题答疑
- (IBAction)questionAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
    CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
    [controler setContentId:35];
    controler.title = @"问题答疑";
    [self.navigationController pushViewController:controler animated:YES];
}
//星睿中心
- (IBAction)xinruiCenterAction:(id)sender {
    StarSmartCenterViewController *sscView = [[StarSmartCenterViewController alloc] init];
    [self.navigationController pushViewController:sscView animated:YES];
    
// UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Second-hand" bundle:nil];
// CommonSingleWebViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"CommWebViewID"];
//    [controler setContentId:36];
//    controler.title = @"星睿中心";
//// controler.reserverServerType = ReserveTypeServiceTypeMend;
// [self.navigationController pushViewController:controler animated:YES];
 }

@end
