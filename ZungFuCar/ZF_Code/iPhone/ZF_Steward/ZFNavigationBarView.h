//
//  ZFNavigationBarView.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZFNavigationBarDelegate <NSObject>

@optional
-(void)navigationBarDidPressAtIndex:(NSInteger)index;
-(void)navigationBarDidAppear;
-(void)navigationbarWillAppear;

@end

@interface ZFNavigationBarView : UIView
@property (strong, nonatomic) IBOutlet UIButton *leftBtn;
@property (strong, nonatomic) IBOutlet UIButton *rightBtn;
@property (nonatomic,unsafe_unretained) id<ZFNavigationBarDelegate> delegate;
@property (nonatomic,strong) UINavigationController *navigationController;

- (IBAction)itemPressAction:(id)sender;

@end
