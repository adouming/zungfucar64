//
//  ReserveFormViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/4/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"
#import "NetServerListTableHeaderView.h"
#import "customPickerView.h"
//#import "isBecomeMemberModel.h"

@interface ReserveFormViewController : AlexBaseViewController<UIActionSheetDelegate,UITextFieldDelegate,CustomPickerViewDelegate, UIAlertViewDelegate>
{
    
    customPickerView *pickerView;
    int selectedTypeId;
    UIDatePicker *datepick;
    BOOL _isAgreeRule;
    NSString *carTypeID;
    NSString *carModelID;
    NSString *cityID;
    NSString *storeID;
    NSString *ExhibitName;
    NSString *industryName;
    NSString *jobName;
    ZFUserInfoData *_userInfo;
    int _userID;
    IBOutlet UILabel *_needSeries;
    IBOutlet UILabel *_needStyle;
    IBOutlet UILabel *_needCity;
    IBOutlet UILabel *_needStore;
    IBOutlet UILabel *_needDate;
    IBOutlet UILabel *_needTime;
    IBOutlet UILabel *_needNick;
    IBOutlet UILabel *_needGender;
    IBOutlet UILabel *_needEmail;
    IBOutlet UILabel *_needPhone;
    IBOutlet UILabel *_needAgree;
    IBOutlet UILabel *_needCode;
    
}

//预约类型选择参数
@property (nonatomic, assign) ReserveType reserveType;
@property (nonatomic, assign) ReserveServiceType reserverServerType;

//picker选择数据记录
@property (retain, nonatomic) NSArray *carTypeData;
@property (retain, nonatomic) ZFNetServerCityExhibitionData *cityTypeData;


//界面元素变量
@property (strong, nonatomic) IBOutlet UITextField *otherCarStyle;
@property (strong, nonatomic) IBOutlet UITextField *codeTF;
@property (strong, nonatomic) IBOutlet UILabel *CodeLab;
@property (strong, nonatomic) IBOutlet UIButton *CodeBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *contentView;
@property (strong, nonatomic) IBOutlet UIImageView *bannerImageView;
@property (strong, nonatomic) IBOutlet UIButton *commit;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *carSeriesBtn;
@property (strong, nonatomic) IBOutlet UIButton *carStyleBtn;
@property (strong, nonatomic) IBOutlet UIButton *cityBtn;
@property (strong, nonatomic) IBOutlet UIButton *exhibitBtn;
@property (strong, nonatomic) IBOutlet UIButton *dateBtn;
@property (strong, nonatomic) IBOutlet UIButton *timeBtn;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UIButton *industryBtn;
@property (strong, nonatomic) IBOutlet UIButton *jobBtn;
@property (strong, nonatomic) IBOutlet UIButton *manBtn;
@property (strong, nonatomic) IBOutlet UIButton *womanBtn;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *telTextField;
@property (strong, nonatomic) IBOutlet UIButton *agreeBtn;

-(void)pickerViewDidSelectRow:(NSInteger)row withStyle:(pickerViewType)style andData:(id)data;

@end
