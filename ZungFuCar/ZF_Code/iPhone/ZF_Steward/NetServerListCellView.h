//
//  NetServerListCellView.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NetServerListCellView : UIView
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *contenLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleNoticeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
-(void)reSetFrame:(CGRect)frame;
@end
