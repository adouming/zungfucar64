//
//  BeMemberViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/5/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"

@interface BeMemberViewController : AlexBaseViewController<UITextFieldDelegate, UIAlertViewDelegate>

{
    
}
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *rePassword;

@end
