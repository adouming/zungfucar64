//
//  RoadRescueViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/5/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"
#import <MapKit/MapKit.h>

@interface RoadRescueViewController : AlexBaseViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate, CLLocationManagerDelegate>
{
    ZFNetServerStoreData *_nearestData;
    ZFNetServerStoreData *_userData;
    CLLocationManager *locationManager;
}
@property ZFNetServerStoreMutableArray *dataArray;
@property (nonatomic,strong) UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
