//
//  NetServerListTableHeaderView.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DOMacro.h"
@interface NetServerListTableHeaderView : UIView

@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;

@property (strong, nonatomic) IBOutlet UILabel *titleLable;

@end
