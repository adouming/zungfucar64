//
//  RoadRescueViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/5/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "RoadRescueViewController.h"
#import "RoadRescueCell.h"
#import "NSStringUtil.h"
#import "MapLocation.h"

@interface RoadRescueViewController ()

@end

@implementation RoadRescueViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
	[self.backgroundImageView setImage:iPhone5 ? [UIImage imageNamed:@"index_bg_iphone5"] : [UIImage imageNamed:@"index_bg.png"]];
    // always insert background at bottom
    [self.view insertSubview:self.backgroundImageView atIndex:0];
    [self initTableView];
    [self updateLocation];
    [self getNearestData];
}

-(void)updateLocation{
    locationManager = [[CLLocationManager alloc] init];//创建位置管理器
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    //启动位置更新
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    double longitude = ((CLLocation *)[locations objectAtIndex:0]).coordinate.longitude;
    double latitude = ((CLLocation *)[locations objectAtIndex:0]).coordinate.latitude;
    [MapLocation shareInstance].latitude = latitude;
    [MapLocation shareInstance].longitude = longitude;
    NSLog(@"Latitude = %f", latitude);
    NSLog(@"Longitude = %f", longitude);
    [manager stopUpdatingLocation];
    [self getNearestData];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"locationManager error");
}

-(void)getNearestData{
    int exhibitID;
    //取所属店铺数据
    ZFUserInfoData *userInfo = [[DBManager sharedManager] getUserData];
    if (userInfo.storeID==0) {
        exhibitID = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"exhibitID%d", userInfo.userID]] intValue];
    }else{
        exhibitID = userInfo.storeID;
    }
    
    NSMutableArray *sourceArray = [[DBManager sharedManager] getStoreData];
    if ([sourceArray count]>0) {
        NSMutableArray *dicArray = [[NSMutableArray alloc]initWithCapacity:[sourceArray count]];
        for (ZFNetServerStoreData *store in sourceArray) {
            NSString *lat = store.latitude;
            NSString *lng = store.longtitude;
            double distance;
            if ([NSStringUtil isBlankString:lat] || [NSStringUtil isBlankString:lat]) {
                distance = MAXFLOAT;
            }else{
                distance = [[MapLocation shareInstance] getDistanceFromLocationLatitude:lat andLongitude:lng];
            }
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:store forKey:@"store"];
            [dic setObject:[NSNumber numberWithDouble:distance] forKey:@"distance"];
            [dicArray addObject:dic];
            
            if (userInfo) {
                if (store.storeID == exhibitID) {
                    _userData = store;
                }
            }
        }
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sorter count:1];
        NSArray *sortedArray = [dicArray sortedArrayUsingDescriptors:sortDescriptors];
        _nearestData = [[sortedArray objectAtIndex:0] valueForKey:@"store"];
        [_tableView reloadData];
    }else{
        [self createModel];
    }
}

-(void)createModel{
    [[ZFStewardManager sharedManager] getStoreWithPage:@"" andPageSize:@"" Success:^(NSMutableArray *data) {
        [[DBManager sharedManager] storeStoreData:data Sucess:^(NSMutableArray *storeData) {
            [self getNearestData];
            [_tableView reloadData];
        } fail:^(NSError *error) {
            NSLog(@"error=%@",error.description);
        }];
    } failure:^(NSError *error) {
        NSString *message = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"error" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        NSLog(@"error");
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}
- (void)viewDidUnload {
    [self setNameLabel:nil];
    [self setTableView:nil];
    [super viewDidUnload];
}
-(void)initTableView{
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableHeaderView = nil;
    _tableView.tableFooterView = nil;
}

#pragma mark - tableView dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_nearestData) {
        return 1;
    }else{
        return 0;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return RoadRescueTableRowHight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_userData) {
        return 2;
    } else {
        return 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int row = [indexPath row];
    
    static NSString *CellIdentifier = @"identifier";
    RoadRescueCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[RoadRescueCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:
                CellIdentifier];
        if (row == 0) {
            [cell setData:@"最近您的仁孚店" subtitle:_nearestData.location];
        } else if (row == 1) {
            [cell setData:@"您所属的仁孚店" subtitle:_userData.location];
        }
        
    } else {
        if (row == 0) {
            [cell setData:@"最近您的仁孚店" subtitle:_nearestData.location];
        } else if (row == 1) {
            [cell setData:@"您所属的仁孚店" subtitle:_userData.location];
        }
    }
    
    return cell;
}

#pragma mark - tableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"确定拨打" message:_nearestData.phone delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        aler.tag = 100;
        [aler show];
    } else if (indexPath.row == 1) {
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"确定拨打" message:_userData.phone delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        aler.tag = 101;
        [aler show];
    }
    
}

#pragma mark - alertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 100) {
        switch (buttonIndex) {
            case 0:
                break;
            case 1:
            {
                NSString *phone = [NSString stringWithFormat:@"tel://%@", [_nearestData.phone stringByReplacingOccurrencesOfString:@" " withString:@""]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone]];
            }
                break;
            default:
                break;
        }
    } else if (alertView.tag == 101) {
        switch (buttonIndex) {
            case 0:
                break;
            case 1:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_userData.phone]];
                break;
            default:
                break;
        }
    }
}

@end
