//
//  DataDBModel.m
//  ZungFuCar
//
//  Created by Alex Peng on 11/8/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "DataDBModel.h"
#import "DBManager.h"
@implementation DataDBModel
static DataDBModel *share_Model = nil;
+ (DataDBModel *)shareInstance{
    static dispatch_once_t pred;
	dispatch_once(&pred, ^{
        share_Model = [[self alloc] init];
    });
	return share_Model;
    
}
//根据城市获得体验店
- (void)createExhibitionModel:(int)cityID type:(ReserveType)type {
    
    [[ZFStewardManager sharedManager] getExhibitionSucess:^(ZFReserveCityExhibitionMutableArray *data) {
        [[DBManager sharedManager] storeExhibitionData:data
                                                   cid:cityID
                                                Sucess:^(NSMutableArray *storeData) {
            
        } fail:^(NSError *error) {
            
        }];
    } failure:^(NSError *error) {
        
    } cid:cityID type:type];

}
//获得城市
- (void)createCityModel{
    [[ZFStewardManager sharedManager] getCitySucess:^(ZFReserveCityExhibitionMutableArray *data) {
        [[DBManager sharedManager] storeCityData:data Sucess:^(NSMutableArray *storeData) {
            
        } fail:^(NSError *error) {
            
        }];
    } failure:^(NSError *error) {
        
    }];
}
//获得车型
- (void)createCarModelModel:(int)cid {
    [[ZFStewardManager sharedManager] getCarModelWithID: cid Success:^(ZFNetServerCarTypeDictionary *data) {
        //NSMutableArray *arraydata = [data objectForKey:@"1"];
        [[DBManager sharedManager] storeCarModelData:data cid:cid Sucess:^(NSMutableArray *storeData) {
            
        } fail:^(NSError *error) {
            
        }];
    } failure:^(NSError *error) {
        
    }];
}
//获得车系
- (void)createCarModel{
    [[ZFStewardManager sharedManager] getCarTypeWithType:@"1" Success:^(ZFNetServerCarTypeDictionary *data) {
        NSMutableArray *arraydata = [data objectForKey:@"1"];
        [[DBManager sharedManager] storeCarData:arraydata Sucess:^(NSMutableArray *storeData) {
            
        } fail:^(NSError *error) {
            
        }];
    } failure:^(NSError *error) {
        
    }];
//    [[ZFStewardManager sharedManager] getCarTypeWithType:@"2" Success:^(ZFNetServerCarTypeDictionary *data) {
//        NSMutableArray *arraydata = [data objectForKey:@"2"];
//        [[DBManager sharedManager] storeAMGCarData:arraydata Sucess:^(NSMutableArray *storeData) {
//            
//        } fail:^(NSError *error) {
//            
//        }];
//    } failure:^(NSError *error) {
//        
//    }];
//    [[ZFStewardManager sharedManager] getCarTypeWithType:@"3" Success:^(ZFNetServerCarTypeDictionary *data) {
//        NSMutableArray *arraydata = [data objectForKey:@"3"];
//        [[DBManager sharedManager] storeSmartCarData:arraydata Sucess:^(NSMutableArray *storeData) {
//            
//        } fail:^(NSError *error) {
//            
//        }];
//    } failure:^(NSError *error) {
    
//    }];

}
- (void)createReserveInfoModel{
    [[ZFStewardManager sharedManager] getReserveInfoSucess:^(NSDictionary *data) {
        NSMutableArray *serviceTimeData = [data objectForKey:@"ServiceTime"];
        NSMutableArray *testTimeData = [data objectForKey:@"TestTime"];
        NSMutableArray *jobData = [data objectForKey:@"Job"];
        NSMutableArray *industryData = [data objectForKey:@"Industry"];
      
        if (serviceTimeData) {
            [[DBManager sharedManager] storeServiceTimeData:serviceTimeData Sucess:^(NSMutableArray *storeData) {
                
            } fail:^(NSError *error) {
                
            }];
        }
        if (testTimeData) {
            [[DBManager sharedManager] storeTestTimeData:testTimeData Sucess:^(NSMutableArray *storeData) {
                
            } fail:^(NSError *error) {
                
            }];
        }
        if (jobData) {
            [[DBManager sharedManager] storeJobData:jobData Sucess:^(NSMutableArray *storeData) {
                
            } fail:^(NSError *error) {
                
            }];
        }
        if (industryData) {
            [[DBManager sharedManager] storeIndustryData:industryData Sucess:^(NSMutableArray *storeData) {
                
            } fail:^(NSError *error) {
                
            }];
        }
        
        
    } failure:^(NSError *error) {
        
    }];
}
- (void)createDataModel{
    [self createCarModel];
    [self createReserveInfoModel];
    [self createCityModel];

}

-(id)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}
@end
