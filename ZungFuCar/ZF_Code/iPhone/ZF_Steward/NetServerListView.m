//
//  NetServerListView.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NetServerListView.h"
#import "NetServerListCell.h"

#import "NetServerDetailViewController.h"

#import "NetServerViewController.h"

#define HEADER_HEIGHT  125
#define CELL_HEIGHT 95

@implementation NetServerListView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
  
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"NetServerListView" owner:self options:nil];
        UIView *nibView = [nibArray objectAtIndex:0];
        if (nibView.superview == nil) {
            [self addSubview:nibView];
        }
        [self initTableView];
        [self layoutTable];
        // Initialization code
    }
    
    return self;
}
-(void)awakeFromNib{
    
    [super awakeFromNib];

   
}
-(void)initTableView{
    heightArray = [NSMutableArray array];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableHeaderView = nil;
    _tableView.tableFooterView = nil;

    
}
-(void)layoutTable{
    CGRect rect = self.bounds;
    rect.size.height = HEADER_HEIGHT;
//    _headerView.bounds = rect;
    CGRect rect1 = self.bounds;
    rect1.origin.y = 113;
    rect1.size.height = self.bounds.size.height - HEADER_HEIGHT;
    _tableView.frame = rect1;
}
#pragma mark - tableView dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = [NetServerListCell CellData:self.storeDataArray heightForRowAtIndexPath:indexPath];
    [heightArray addObject:[NSNumber numberWithFloat:height]];
    height = CELL_HEIGHT;
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.storeDataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int row = [indexPath row];
    static NSString *CellIdentifier = @"serverlistIdentifer";
    NetServerListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[NetServerListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:
                CellIdentifier];
    }
    [cell setData:[self.storeDataArray objectAtIndex:row] andHeight:[heightArray objectAtIndex:row] row:row];
    return cell;
}

#pragma mark - tableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NetServerDetailViewController *detailVC = [[NetServerDetailViewController alloc] initWithNibName:@"NetServerDetailViewController" bundle:nil];
    detailVC.dataSource = [self.storeDataArray objectAtIndex:indexPath.row];
    [self.controller.navigationController pushViewController:detailVC animated:YES];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
