//
//  ReserveFormViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/4/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"
#import "NetServerListTableHeaderView.h"
#import "customPickerView.h"
//#import "isBecomeMemberModel.h"
#import "MenuAbstractViewController.h"
#import "noteViewController.h"

@interface ReserveFormViewController_ipad : MenuAbstractViewController<UIActionSheetDelegate,UITextFieldDelegate,CustomPickerViewDelegate, UIAlertViewDelegate>
{
    customPickerView *pickerView;
    UIDatePicker *datepick;
    BOOL _isAgreeRule;
    NSString *carTypeID;
    NSString *carModelID;
    NSString *cityID;
    NSString *exhibitID;
    NSString *industryID;
    NSString *jobID;
    ZFUserInfoData *_userInfo;
    int _userID;
    IBOutlet UILabel *_needSeries;
    IBOutlet UILabel *_needStyle;
    IBOutlet UILabel *_needCity;
    IBOutlet UILabel *_needStore;
    IBOutlet UILabel *_needDate;
    IBOutlet UILabel *_needTime;
    IBOutlet UILabel *_needNick;
    IBOutlet UILabel *_needGender;
    IBOutlet UILabel *_needEmail;
    IBOutlet UILabel *_needPhone;
    IBOutlet UILabel *_needAgree;
    IBOutlet UILabel *_needCode;
    
    noteViewController *_noteCtl;
    
}

@property (strong, nonatomic) IBOutlet UITextField *codeTF;
@property (strong, nonatomic) IBOutlet UILabel *CodeLab;
@property (strong, nonatomic) IBOutlet UIButton *CodeBtn;
@property (nonatomic,assign) ReserveType reserveType;
@property (nonatomic,assign) ReserveServiceType reserverServerType;
@property (strong, nonatomic) IBOutlet UIScrollView *contentView;
@property (retain,nonatomic) NSDictionary *carTypeData;
@property (retain,nonatomic) ZFNetServerCityExhibitionData *cityTypeData;
@property (strong, nonatomic) IBOutlet UIImageView *bannerImageView;
@property (strong, nonatomic) IBOutlet UIButton *commit;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *carSeriesBtn;
@property (strong, nonatomic) IBOutlet UIButton *carStyleBtn;
@property (strong, nonatomic) IBOutlet UIButton *cityBtn;
@property (strong, nonatomic) IBOutlet UIButton *exhibitBtn;
@property (strong, nonatomic) IBOutlet UIButton *dateBtn;
@property (strong, nonatomic) IBOutlet UIButton *timeBtn;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UIButton *industryBtn;
@property (strong, nonatomic) IBOutlet UIButton *jobBtn;
@property (strong, nonatomic) IBOutlet UIButton *manBtn;
@property (strong, nonatomic) IBOutlet UIButton *womanBtn;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *telTextField;
@property (strong, nonatomic) IBOutlet UIButton *agreeBtn;

@property (weak, nonatomic) IBOutlet UIView *reserveContentVIewBg;
@property bool showBack;
@property bool isExtend2;
@property (weak, nonatomic) IBOutlet UIView *reserveContentView;
-(void)scanCalculVCViewFrame:(BOOL)isExtend;
-(void)pickerViewDidSelectRow:(NSInteger)row withStyle:(pickerViewType)style andData:(id)data;

@end
