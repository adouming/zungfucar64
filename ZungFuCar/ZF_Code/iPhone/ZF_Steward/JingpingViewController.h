//
//  JingpingViewController.h
//  ZungFuCar
//
//  Created by adouming on 15/8/17.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
#import "HorizontalScrollView.h"
#import "SelectTableViewCell.h"

@interface JingpingViewController : MenuAbstractViewController<HorizontalScrollViewDelegate,UITableViewDataSource, UITableViewDelegate>
{
    UIImageView *_bgImageView;
    NSMutableArray *dataSource;
    
    HorizontalScrollView *hScrollView;
    
}
-(id)initWithType:(int)type;
@property int carType;
@property NSArray* carArray;
@property NSDictionary* carMap;
@property (nonatomic, retain) UITableView *selctTableView;
@end
