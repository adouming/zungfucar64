//
//  ReserveMainViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/4/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "MenuAbstractViewController.h"

@interface ReserveMainViewController : MenuAbstractViewController

@property (nonatomic,assign) ReserveType reserveType;
@property (nonatomic,assign) ReserveServiceType reserverServerType;

@end
