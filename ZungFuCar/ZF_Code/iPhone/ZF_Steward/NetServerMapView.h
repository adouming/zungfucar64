//
//  NetServerMapView.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CallOutAnnotationVifew.h"
#import "CalloutMapAnnotation.h"
#import "BasicMapAnnotation.h"

@class NetServerViewController;
@interface NetServerMapView : UIView<MKMapViewDelegate, CLLocationManagerDelegate>{
    NSString *_userlongitude;
    NSString *_userLatitude;
    MKAnnotationView *myAnnotation;
    
    CalloutMapAnnotation *_calloutAnnotation;
	CalloutMapAnnotation *_previousdAnnotation;
    ZFNetServerStoreData *nowData;
}

@property (nonatomic,copy) NSArray *storeDataArray;
@property (nonatomic,unsafe_unretained) NetServerViewController *controller;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
-(void)drawAnnotation;
@end