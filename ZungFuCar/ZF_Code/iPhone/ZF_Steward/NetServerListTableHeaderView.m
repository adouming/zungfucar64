//
//  NetServerListTableHeaderView.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NetServerListTableHeaderView.h"

@implementation NetServerListTableHeaderView

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *nibName;
        if (iPhone) {
            nibName = @"NetServerListTableHeaderView";
        }else if (iPad){
            nibName = @"NetServerListTableHeaderView_iPad";
        }
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        UIView *nibView = [nibArray objectAtIndex:0];
        [self addSubview:nibView];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSString *nibName;
        if (iPhone) {
            nibName = @"NetServerListTableHeaderView";
        }else if (iPad){
            nibName = @"NetServerListTableHeaderView_iPad";
        }
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        UIView *nibView = [nibArray objectAtIndex:0];
        [self addSubview:nibView];
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    [self setObject];
}
-(void)setObject{
    
//    _bgImageView.image = [UIImage imageNamed:@"carlist_bg.png"];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
