//
//  BeMemberViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/5/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "BeMemberViewController.h"
#import "ZFUserInfo.h"

@interface BeMemberViewController ()

@end

@implementation BeMemberViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Action
- (IBAction)cancelAction:(id)sender {
    
    [self.password resignFirstResponder];
    [self.rePassword resignFirstResponder];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
- (IBAction)commitAction:(id)sender {
    [self.password resignFirstResponder];
    [self.rePassword resignFirstResponder];
    if ([self.password.text isEqualToString:self.rePassword.text] && (self.password.text.length != 0)) {
        
        [self regeditNew];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"两次输入的密码不相同或者为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:Nil, nil];
        [alert show];
    }
}

- (void)regeditNew
{
    ////////////////////////////////
    // 1
    //
    //
    [self showLoadingText];
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [[NSUserDefaults standardUserDefaults] objectForKey:@"isBecomeEmail"], @"Email",
                                [self.password.text MD5Encry:self.password.text], @"Password",
                                @"", @"VerifyCode",
                                nil];
    
    AFHTTPRequestOperationManager *login = [AFHTTPRequestOperationManager manager];
    [login.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"HTTP-X-U"];
    [login.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"HTTP-X-P"];
    login.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [login POST:[NSString stringWithFormat:@"%@%@%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_BECOMEMEMBER] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *responseDataDic = [[NSDictionary alloc] initWithDictionary:responseObject];
        
        //记录账号密码
        //写入NSUserDefaults文件
        NSUserDefaults *_userDefaults = [NSUserDefaults standardUserDefaults];
        [_userDefaults setObject:[_userDefaults objectForKey:@"isBecomeEmail"] forKey:@"loginEmail"];
        [_userDefaults setObject:self.password.text forKey:@"loginPassword"];
        [_userDefaults setBool:YES forKey:@"isLogined"];
        [_userDefaults synchronize];
        
        NSString *userID = [[[responseDataDic objectForKey:@"Data"] objectForKey:@"Member"] objectForKey:@"ID"];
        [[ZFStewardManager sharedManager] getPersonInfoWithUserID:userID Success:^(ZFUserInfoData *data) {
            ZFUserInfoData *userData = data;
            [[DBManager sharedManager] storeUserData:userData Sucess:^(ZFUserInfoData *storeData) {
                [[ZFUserInfo shareInstance] setUserData:storeData];
                [self hideLoadingText];
                UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"注册成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                successAlert.tag = 20;
                [successAlert show];
                
            } fail:^(NSError *error) {
                [self hideLoadingText];
            }];
        } failure:^(NSError *error) {
            [self hideLoadingText];
            UIAlertView *failureView = [[UIAlertView alloc] initWithTitle:@""
                                                                  message:@"获取用户信息失败！"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [failureView show];
            
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error with %@", error);
        [self hideLoadingText];
        UIAlertView *failureView = [[UIAlertView alloc] initWithTitle:@""
                                                              message:@"注册失败"
                                                             delegate:nil
                                                    cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [failureView show];
    }];
    
}

#pragma mark - alert delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 20) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - Navigation Delegate
-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
    
}
-(void)navigationBarDidPressAtIndex:(NSInteger)index{
    switch (index) {
        case 0:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}

#pragma mark - textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.view.frame;
    rect.origin.y = -100;
    self.view.frame = rect;
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.view.frame;
    rect.origin.y = 0;
    self.view.frame = rect;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.password resignFirstResponder];
    [self.rePassword resignFirstResponder];
}

@end
