//
//  NetServerListView.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetServerListTableHeaderView.h"
#import "AlexMacro.h"


@class NetServerViewController;
@interface NetServerListView : UIView<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *heightArray;
}


@property (nonatomic,unsafe_unretained) NetServerViewController *controller;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,copy) NSArray *storeDataArray;
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;

@end
