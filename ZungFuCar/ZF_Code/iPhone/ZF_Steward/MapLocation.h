//
//  MapLocation.h
//  ZungFuCar
//
//  Created by Alex Peng on 11/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapLocation : NSObject


@property (nonatomic,unsafe_unretained) double latitude;
@property (nonatomic,unsafe_unretained) double longitude;
@property (nonatomic,retain) NSString *nearLatitude;
@property (nonatomic,retain) NSString *nearLongitude;
+ (MapLocation *)shareInstance;
+ (NSString *)latestStoreID;
-(double)getDistanceFromLocationLatitude:(NSString *)latitude andLongitude:(NSString *)longitude;
@end
