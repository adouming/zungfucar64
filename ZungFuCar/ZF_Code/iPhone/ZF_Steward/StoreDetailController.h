//
//  StoreDetailController.h
//  ZungFuCar
//
//  Created by Ed Lee on 12/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFStewardData.h"

@interface StoreDetailController : UIViewController <UIAlertViewDelegate> {
    NSString *hotTell;
    NSString *emergenceTell;
}

@property (strong, nonatomic) IBOutlet UIImageView *banerView;
@property (strong, nonatomic) IBOutlet UITextView *addressValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *serviceHourLabel;
@property (strong, nonatomic) IBOutlet UIImageView *serviceImageView;
@property (strong, nonatomic) IBOutlet UILabel *netUrlValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailValueLabel;
@property (strong, nonatomic) IBOutlet UIButton *callServiceBtn;
@property (strong, nonatomic) IBOutlet UIButton *callEmergencyBtn;
@property (strong, nonatomic) IBOutlet UITextView *titleText;
@property (strong, nonatomic) IBOutlet UILabel *emergencyLabel;
- (IBAction)nativeNavi:(id)sender;
- (IBAction)callService:(id)sender;
- (IBAction)callEmergency:(id)sender;

@property (strong,nonatomic) ZFNetServerStoreData *dataSource;

@end
