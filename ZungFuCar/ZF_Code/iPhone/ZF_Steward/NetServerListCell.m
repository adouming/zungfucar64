//
//  NetServerListCell.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NetServerListCell.h"
#import "UIImageView+AFNetworking.h"
#import "MapLocation.h"
#import "UILabel+autoResize.h"
#define CELL_HEIGHT_iPhone 100
#define CELL_HEIGHT_iPad   125
static NSMutableArray *heightArray=nil;
@implementation NetServerListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        if (iPhone) {
            _cellView = [[NetServerListCellView alloc] initWithFrame:CGRectMake(0, 0, 320, CELL_HEIGHT_iPhone)];
        }else if (iPad){
            _cellView = [[NetServerListCellView alloc] initWithFrame:CGRectMake(0, 0, 300, CELL_HEIGHT_iPad)];
        }
        
//        nearestStoreID = [[MapLocation latestStoreID] integerValue];
        nearestStoreID = 0;
        [self.contentView addSubview:_cellView];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        heightArray = [NSMutableArray array];
        // Initialization code
    }
    return self;
}
-(void)setData:(ZFNetServerStoreData *)data andHeight:(NSNumber *)height row:(int)row{
//    CGRect frame = self.bounds;
//    frame.size.height = [height floatValue];
//    [self.cellView reSetFrame:frame];
//    [self.cellView.iconImageView setImageWithURL:[NSURL URLWithString:data.sImageUrl] placeholderImage:[UIImage imageNamed:@"radiobtn1.png"]];
    
    CGRect titleRect = CGRectMake(35, 15, 170, 20);
    CGRect noticeRect = CGRectMake(210, 18, 95, 18);
    self.cellView.titleLabel.text = data.location;
//    if (data.storeID == nearestStoreID) {
    if (row == 0) {
        self.cellView.titleNoticeLabel.hidden = NO;
        self.cellView.titleNoticeLabel.text = @"最近您的经销商";
        CGSize size = CGSizeMake(MAXFLOAT, 20);
        CGSize labelsize = [self.cellView.titleLabel.text sizeWithFont:self.cellView.titleLabel.font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
        if (labelsize.width>titleRect.size.width) {
            self.cellView.titleLabel.frame = titleRect;
        }else{
            titleRect.size.width = labelsize.width;
            self.cellView.titleLabel.frame = titleRect;
            noticeRect.origin.x = titleRect.origin.x +titleRect.size.width;
            self.cellView.titleNoticeLabel.frame = noticeRect;
        }
    }else{
        self.cellView.titleNoticeLabel.hidden = YES;
        titleRect.size.width = titleRect.size.width+noticeRect.size.width;
        self.cellView.titleLabel.frame = titleRect;
    }
    self.cellView.contenLabel.text = [[data.name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] stringByReplacingOccurrencesOfString:@"<br/>" withString:@""];
//    self.cellView.contenLabel.autoResize = YES;
    
}

+ (CGFloat)CellData:(NSArray *)cellData heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!cellData.count) {
        return 0;
    }
    CGFloat height = 20;
    ZFNetServerStoreData *data = [cellData objectAtIndex:indexPath.row];
    CGSize size = CGSizeMake(255,MAXFLOAT);
    CGSize labelsize = [data.shortDesc sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    if (labelsize.height>20) {
        height = labelsize.height;
    }
    [heightArray addObject:[NSString stringWithFormat:@"%f",height+50]];
    return height + 50;
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
