//
//  NetServerDetailViewController.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "AlexBaseViewController.h"
#import "ServerItemView.h"
@interface NetServerDetailViewController : AlexBaseViewController<UIAlertViewDelegate>{
    NSString *hotTell;
    NSString *emergenceTell;
}

@property (strong,nonatomic) ZFNetServerStoreData *dataSource;

@property (strong, nonatomic) IBOutlet UIScrollView *contentView;
@property (strong, nonatomic) IBOutlet UIImageView *banerView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *openTimeTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *serverTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *netUrlTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailTitleLabel;

@property (strong, nonatomic) IBOutlet UILabel *addressValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeValueLabel;
@property (strong, nonatomic) IBOutlet UIImageView *serverView;
@property (strong, nonatomic) IBOutlet UILabel *netUrlValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailValueLabel;
@property (strong, nonatomic) IBOutlet UIView *buttonView;
@property (strong, nonatomic) IBOutlet UIButton *serverBtn;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UITextView *businessTimeText;

@property (strong, nonatomic) IBOutlet UITextView *serviceTimeText;
- (IBAction)nativeNavi:(id)sender;

@end
