//
//  MapViewAnnotation.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "ZFStewardData.h"
@interface MapViewAnnotation : NSObject<MKAnnotation>{
    NSString *_pinTitle;
    NSString *_pinSubTitle;
    CLLocationCoordinate2D _coordinate;
    ZFNetServerStoreData *_storeData;
   
}
@property (nonatomic,strong) ZFNetServerStoreData *storeData;
@property (nonatomic,readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic,copy) NSString *pinTitle;
@property (nonatomic,copy) NSString *pinSubTitle;

- (id) initWithTitle:(NSString *)aTitle andSubTitle:(NSString *)aSubTitle andLatitude:(double)latitude andLongitude:(double)longitude;

@end
