//
//  StewardMainVC.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "StewardMainVC.h"
#import "ReserveFormViewController.h"
#import "ReserveMainViewController.h"
#import "RoadRescueViewController.h"
#import "PeiJIanViewController.h"
#import "JingpingViewController.h"
#import "CarMasterShareViewController.h"
@interface StewardMainVC ()

@end

@implementation StewardMainVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    //auto resize
    CGRect rect = CGRectMake(0, 0,  [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height);
    NSLog(@"%@", NSStringFromCGRect(rect));
    [self.view setFrame: rect];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - navigationDelegate
-(void)navigationBarDidAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}
- (IBAction)reserveAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Steward" bundle:nil];
    ReserveFormViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"TryReserveFormID"];
    controler.reserveType = ReserveTypeTest;
    [self.navigationController pushViewController:controler animated:YES];
}
- (IBAction)serverReserveAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Steward" bundle:nil];
    ReserveMainViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"serverReserveMenuID"];
    controler.reserveType = ReserveTypeService;
    [self.navigationController pushViewController:controler animated:YES];
}
- (IBAction)roadRescueAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Steward" bundle:nil];
    RoadRescueViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"roadRescueID"];
    [self.navigationController pushViewController:controler animated:YES];
    
}
- (IBAction)peijianAction:(id)sender {
    PeiJIanViewController *peijianView = [[PeiJIanViewController alloc] init];
    [self.navigationController pushViewController:peijianView animated:YES];
}
- (IBAction)jinpingAction:(id)sender {
    JingpingViewController *jinpingView = [[JingpingViewController alloc] init];
    [self.navigationController pushViewController:jinpingView animated:YES];
}
- (IBAction)carmasterShareAction:(id)sender {
    CarMasterShareViewController *carmasterView = [[CarMasterShareViewController alloc] init];
    [self.navigationController pushViewController:carmasterView animated:YES];
}

@end
