//
//  customPickerView.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/4/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AlexMacro.h"
#import "ZFStewardData.h"

typedef enum {
    pickerViewTypeCarSeries, //车系
    pickerViewTypeCarType, //车型
    pickerViewTypeCarSeriesReserve,
    pickerViewTypeCarTypeReserve,
    pickerViewTypeCity,//城市
    pickerViewTypeExhibit,//体验店
    pickerViewTypeDate,//日期
    pickerViewTypeServerTime,//时间
    pickerViewTypeTestTime,//预约时间
    pickerViewTypeIndustry,//行业
    pickerViewTypeJob,//职位
    pickerViewTypeIndustryReserve,
    pickerViewTypeJobReserve,
    pickerViewTypeLoan,
}pickerViewType;

@protocol CustomPickerViewDelegate <NSObject>

-(void)pickerViewDidSelectRow:(NSInteger)row withStyle:(pickerViewType)style andData:(id)data;


@end

@interface customPickerView : UIActionSheet<UIPickerViewDelegate, UIPickerViewDataSource>{
    pickerViewType pickerStyle;
    NSArray *_carTypeData;
    int _selectedCarType;
    NSMutableArray *carSeriesTitles;
    NSMutableArray *carTypeTitles;
    ZFNetServerCarTypeData *_carTypeDataReserve;
}
@property (assign,nonatomic) int selectedCarType;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (retain,nonatomic) NSMutableArray *schemeList;
@property (retain, nonatomic) NSString *schemeType;
@property (retain,nonatomic) NSArray *carTypeData;//车系

@property (retain,nonatomic) NSDictionary *exhibitionData;
@property (retain,nonatomic) ZFNetServerCityExhibitionData *cityData;
@property (nonatomic,assign) pickerViewType pickerStyle;
@property (nonatomic,strong) NSMutableArray *dataSource;
@property (nonatomic,unsafe_unretained) id<CustomPickerViewDelegate> pickerDelegate;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBarView;
@property (nonatomic, strong) UIView *optionView;
@property (nonatomic, strong) NSArray *optionArray;
@property (retain,nonatomic) ZFNetServerCarTypeData *carTypeDataReserve;

- (id)initWithTitle:(NSString *)a_title delegate:(id /*<UIActionSheetDelegate>*/)delegate;
@end
