//
//  customPickerView.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/4/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "customPickerView.h"
#import "DBManager.h"
#import "ZFStewardData.h"
#import "ReserveFormViewController.h"
@implementation customPickerView
@dynamic pickerStyle;
@synthesize pickerView, optionArray;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (id)initWithTitle:(NSString *)a_title delegate:(id /*<UIActionSheetDelegate>*/)delegate
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"customPickerView" owner:self options:nil] objectAtIndex:0];
    if (self) {
        self.delegate = delegate;
        self.title = a_title;
        self.pickerView.dataSource = self;
        self.pickerView.delegate = self;
        
    }
    return self;
}
-(pickerViewType)pickerStyle{
    return pickerStyle;
}

//- (void)setHidden:(BOOL)hidden
//{
//
//}

-(void)setPickerStyle:(pickerViewType)currentPickerStyle{
    if (self.dataSource.count) {
        [self.dataSource removeAllObjects];
    }
    pickerStyle = currentPickerStyle;
    switch (pickerStyle) {
        case pickerViewTypeCarSeriesReserve:{
            NSMutableArray *data = [NSMutableArray array];
            NSArray *alldata = [[DBManager sharedManager] getCallCarTypeData];
//           NSArray *benData = [[DBManager sharedManager] getBanzCarData];
//            NSArray *amgData = [[DBManager sharedManager] getAMGCarData];
//            NSArray *smartData = [[DBManager sharedManager] getSmartCarData];
//            [data addObjectsFromArray:benData];
//            [data addObjectsFromArray:amgData];
//            [data addObjectsFromArray:smartData];
            [data addObjectsFromArray:alldata];
            self.dataSource = data;
            
        }
            
            break;
        case pickerViewTypeCarTypeReserve:{
            self.dataSource = self.carTypeDataReserve.productArray;
        }
            break;
        case pickerViewTypeCarSeries:{//车系
            NSMutableArray *data = [NSMutableArray array];
            NSArray *alldata = [[DBManager sharedManager] getCallCarTypeData];
            [data addObjectsFromArray:alldata];
            self.dataSource = data;
            
        }
            
            break;
        case pickerViewTypeCarType:{//车型
            NSMutableArray *data = [NSMutableArray array];
            NSArray *alldata = [[DBManager sharedManager] getCarModelByType: self.selectedCarType];
            [data addObjectsFromArray:alldata];
            self.dataSource = data;

        }
            
            break;
        case pickerViewTypeCity:{
            NSMutableArray *data = [[DBManager sharedManager] getCityData];
            self.dataSource = data;
        }
            
            break;
        case pickerViewTypeDate:{
            NSMutableArray *data = [[DBManager sharedManager] getServiceData];
            self.dataSource = data;
        }
            
            break;
        case pickerViewTypeExhibit:{
            NSDictionary* city = self.cityData;
            int cityID = [[city objectForKey:@"cityid"] integerValue];
            NSMutableArray *data = [[DBManager sharedManager] getExhibitionData:cityID];
            self.dataSource = data;
            //exhibitionData
            //self.dataSource = self.exhibitionData;//self.cityData.exhibitionArray;
        }
            
            break;
        case pickerViewTypeIndustryReserve:
        case pickerViewTypeIndustry:{
            NSMutableArray *data = [[DBManager sharedManager] getIndustryData];
            self.dataSource = data;
        }
            
            break;
        case pickerViewTypeJobReserve:
        case pickerViewTypeJob:{
            NSMutableArray *data = [[DBManager sharedManager] getJobData];
            self.dataSource = data;
        }
            
            break;
        case pickerViewTypeServerTime:{
            NSMutableArray *data = [[DBManager sharedManager] getServiceTimeData];
            self.dataSource = data;
            
        }
            
            break;
        case pickerViewTypeTestTime:{
            NSMutableArray *data = [[DBManager sharedManager] getTestTimeData];
            self.dataSource = data;
            
        }
            
            break;
        case pickerViewTypeLoan:{
            NSMutableArray *data = self.schemeList;
            NSMutableArray *strMuArry = [NSMutableArray array];
            if ([self.schemeType isEqualToString:@"loan"]) {
                for (int i=0; i<data.count; i++) {
                    ZFSchemeData *dataTemp = [data objectAtIndex:i];
                    NSString *strTemp = [NSString stringWithFormat:@"%d月       %.2f%%       %.2f%%", dataTemp.LoanPeriod, dataTemp.LoanRate*100, dataTemp.LoanInterestRate*100];
                    [strMuArry addObject:strTemp];
                }
            } else {
                for (int i=0; i<data.count; i++) {
                    ZFSchemeData *dataTemp = [data objectAtIndex:i];
                    NSString *strTemp = [NSString stringWithFormat:@"%d月       %.2f%%       %.2f%%", dataTemp.LeasingPeriod, dataTemp.LeasingDepositRate*100, dataTemp.LeasingInterestRate*100];
                    [strMuArry addObject:strTemp];
                }
            }
            
            self.dataSource = strMuArry;
            
        }
            break;
            
        default:
            break;
    }
    [self.pickerView reloadAllComponents];
}
- (void)showInView:(UIView *) view
{
    
    CATransition *animation = [CATransition  animation];
    animation.delegate = self;
    animation.duration = kPickerViewDuration;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromTop;
    [self setAlpha:1.0f];
    [self.layer addAnimation:animation forKey:@"pickerView"];
    
    self.frame = CGRectMake(0, view.frame.size.height - self.frame.size.height, self.frame.size.width, self.frame.size.height);
    
    [view addSubview:self];
    
    
    if(self.optionArray!=nil &&[self.optionArray count]!=0)
    {
        if (self.optionView==nil) {
            self.optionView = [[UIView alloc] initWithFrame:CGRectMake(0, self.pickerView.frame.origin.y-42, SCREEN_WIDTH, 44)];
            self.optionView.backgroundColor = [UIColor colorWithRed:144/255.f green:151/255.f blue:167/255.f alpha:1];
            for(int i = 0;i<3;i++)
            {
                UILabel *label = nil;
                label = [[UILabel alloc] initWithFrame:CGRectMake(0+i*106, 0, SCREEN_WIDTH/3, 44)];
                label.backgroundColor = [UIColor clearColor];
                label.numberOfLines = 0;
                label.tag = 10+i;
                label.font = [UIFont systemFontOfSize:13.f];
                label.adjustsFontSizeToFitWidth = YES;
                label.textColor = [UIColor whiteColor];
                label.text = [self.optionArray objectAtIndex:i];
                label.textAlignment = NSTextAlignmentCenter;
                //label.contentMode = UIControlContentVerticalAlignmentTop;
                [self.optionView addSubview:label];
                
                UIView *image = [[UIView alloc] initWithFrame:CGRectMake((i+1)*106,0,1,44)];
                if (i==2) {
                    image.hidden = YES;
                }
                image.backgroundColor = [UIColor grayColor];
                [self.optionView addSubview:image];
            }
            [self addSubview:self.optionView];
        } else {
            ((UILabel *)[self.optionView viewWithTag:10]).text = [self.optionArray objectAtIndex:0];
            ((UILabel *)[self.optionView viewWithTag:11]).text = [self.optionArray objectAtIndex:1];
            ((UILabel *)[self.optionView viewWithTag:12]).text = [self.optionArray objectAtIndex:2];
        }
        
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        if(self.optionArray!=nil &&[self.optionArray count]!=0)
        {
            self.optionView.hidden = NO;
            self.frame = CGRectMake(self.frame.origin.x, [UIScreen mainScreen].bounds.size.height - 304, self.frame.size.width, 304);
            self.pickerView.frame = CGRectMake(0, 88, SCREEN_WIDTH, 304);
            self.optionView.frame = CGRectMake(0, self.pickerView.frame.origin.y-42, SCREEN_WIDTH, 44);
            self.navigationBarView.frame = CGRectMake(0, self.pickerView.frame.origin.y-88, SCREEN_WIDTH, 44);
        }
        else
        {
            self.frame = CGRectMake(self.frame.origin.x, [UIScreen mainScreen].bounds.size.height - 258, self.frame.size.width, 258);
            self.optionView.hidden = YES;
            self.pickerView.frame = CGRectMake(0, 44, SCREEN_WIDTH, 216);
            self.navigationBarView.frame = CGRectMake(0, self.pickerView.frame.origin.y-44, SCREEN_WIDTH, 44);
        }
    }];
    [pickerView selectedRowInComponent:0];
}

#pragma mark - PickerView lifecycle

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView2 numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:{
            int count = [self.dataSource count];
            if (count) {
                if ([self.pickerDelegate respondsToSelector:@selector(pickerViewDidSelectRow:withStyle:andData:)]) {
                    //                    int i = [pickerView2 selectedRowInComponent:component];
                    [self.pickerDelegate pickerViewDidSelectRow:[pickerView selectedRowInComponent:component ] withStyle:pickerStyle andData:[self.dataSource objectAtIndex:0]];
                    if (self.pickerStyle == pickerViewTypeCarSeriesReserve) {
                        self.carTypeDataReserve = [self.dataSource objectAtIndex:[pickerView selectedRowInComponent:component ]];
                    }
                    if (self.pickerStyle == pickerViewTypeCity) {
                        self.cityData = [self.dataSource objectAtIndex:0];
                    }
                }
            }
            return count;
        }
            
            break;
            
        default:
            return 0;
            break;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0:{
            switch (pickerStyle) {
                case pickerViewTypeCarSeriesReserve:{ //维修车系
                    if (self.dataSource.count) {
                        ZFNetServerCarTypeData * data = [self.dataSource objectAtIndex:row];
                        return data.name;
                    }
                }
                    
                    break;
                case pickerViewTypeCarTypeReserve:{
                    if (self.dataSource.count) {
                        ZFNetServerCarProductData * data = [self.dataSource objectAtIndex:row];
                        return data.name;
                    }
                }
                    break;
                case pickerViewTypeCarSeries:{//车系
                    if (self.dataSource.count) {
                        
                        return [[self.dataSource objectAtIndex:row] objectForKey:@"name"];
                    }
                }
                    break;
                case pickerViewTypeCarType:{//车型
                    if (self.dataSource.count) {
                        
                        return [[self.dataSource objectAtIndex:row] objectForKey:@"name"];
                    }
                }
                    break;
                case pickerViewTypeCity:{
                    if (self.dataSource.count) {
                        NSDictionary * data = [self.dataSource objectAtIndex:row];
                        return [data objectForKey:@"name"];
                    }
                }
                    
                    break;
                case pickerViewTypeDate:{
                    if (self.dataSource.count) {
                        NSDate * data = [self.dataSource objectAtIndex:row];
                        
                        //实例化一个NSDateFormatter对象
                        
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        
                        //设定时间格式,这里可以设置成自己需要的格式
                        
                        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                        
                        //用[NSDate date]可以获取系统当前时间
                        
                        NSString *currentDateStr = [dateFormatter stringFromDate:data];
                        
                        //输出格式为：2010-10-27 10:22:13
      
                        return currentDateStr;
                    }
                }
                    
                    break;
                case pickerViewTypeExhibit:{
                    if (self.dataSource.count) {
                        NSDictionary * data = [self.dataSource objectAtIndex:row];
                        return [data objectForKey:@"storename"];
                    }
                }
                    
                    break;
                case pickerViewTypeIndustry:{
                    if (self.dataSource.count) {
                        return [[self.dataSource objectAtIndex:row] valueForKey:@"DisplayText"];
                    }
                }
                    
                    break;
                case pickerViewTypeJob:{
                    if (self.dataSource.count) {
                        return [[self.dataSource objectAtIndex:row] valueForKey:@"DisplayText"];               }
                }
                    
                    break;
                case pickerViewTypeServerTime:{
                    if (self.dataSource.count) {
                        ZFNetServerServiceTimeData * data = [self.dataSource objectAtIndex:row];
                        return data;
                    }
                }
                    break;
                case pickerViewTypeTestTime:{
                    if (self.dataSource.count) {
                        ZFNetServerTestTimeData * data = [self.dataSource objectAtIndex:row];
                        return data;
                    }
                }
                    break;
                case pickerViewTypeLoan:{
                    return [self.dataSource objectAtIndex:row];
                }
                    break;
                case pickerViewTypeIndustryReserve:{
                    if (self.dataSource.count) {
                        ZFNetServerIndustryData * data = [self.dataSource objectAtIndex:row];
                        return data;
                    }
                }
                    
                    break;
                case pickerViewTypeJobReserve:{
                    if (self.dataSource.count) {
                        ZFNetServerJobData * data = [self.dataSource objectAtIndex:row];
                        return data;
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
            
        default:
            return nil;
            break;
    }
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if ([self.pickerDelegate respondsToSelector:@selector(pickerViewDidSelectRow:withStyle:andData:)]) {
        if (self.dataSource.count) {
            [self.pickerDelegate pickerViewDidSelectRow:row withStyle:pickerStyle andData:[self.dataSource objectAtIndex:row]];
            if(pickerStyle == pickerViewTypeCarSeries) {
                self.selectedCarType = [[[self.dataSource objectAtIndex:row] objectForKey:@"cid"] integerValue];
            }
        }
        
    }
    
    
}


#pragma mark - Button lifecycle

- (IBAction)cancel:(id)sender {
    CATransition *animation = [CATransition  animation];
    animation.delegate = self;
    animation.duration = kPickerViewDuration;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromBottom;
    [self setAlpha:0.0f];
    [self.layer addAnimation:animation forKey:@"pickerView"];
    [self performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:kPickerViewDuration];
    if(self.delegate) {
        [self.delegate actionSheet:self clickedButtonAtIndex:0];
    }
}

- (IBAction)done:(id)sender {
    CATransition *animation = [CATransition  animation];
    animation.delegate = self;
    animation.duration = kPickerViewDuration;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromBottom;
    [self setAlpha:0.0f];
    [self.layer addAnimation:animation forKey:@"pickerView"];
    [self performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:kPickerViewDuration];
    if(self.delegate) {
        [self.delegate actionSheet:self clickedButtonAtIndex:1];
    }
    
}

//函数体为空，取消pickview的点击响应
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

@end
