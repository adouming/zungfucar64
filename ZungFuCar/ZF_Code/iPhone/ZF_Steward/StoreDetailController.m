//
//  StoreDetailController.m
//  ZungFuCar
//
//  Created by Ed Lee on 12/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "StoreDetailController.h"
#import "UIImageView+AFNetworking.h"
#import "JSON.h"
#import <BaiduMapAPI/BMapKit.h>
#import <BaiduMapAPI/BMKNavigation.h>
#import "MapLocation.h"

@interface StoreDetailController ()

@end

@implementation StoreDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    NSLog(@"viewWillAppear");
    [self setObjects];
}

-(void)setObjects{
    
    self.titleText.text = [self.dataSource.name stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    self.addressValueLabel.text = self.dataSource.address;
    self.addressValueLabel.textColor = [UIColor whiteColor];
    //self.timeValueLabel.numberOfLines = 0;
    self.serviceHourLabel.text = [self.dataSource.mservice stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    self.timeValueLabel.text = [self.dataSource.sroom stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    self.netUrlValueLabel.text = self.dataSource.url;
    self.emailValueLabel.text = self.dataSource.contactEmail;
    [_callServiceBtn setTitle:self.dataSource.phone forState:UIControlStateNormal];
    
    if ([self.dataSource.emergencyPhone isEqualToString:@""]) {
        _emergencyLabel.hidden = YES;
        _callEmergencyBtn.hidden = YES;
    }
    [_callEmergencyBtn setTitle:self.dataSource.emergencyPhone forState:UIControlStateNormal];
    
    [self.banerView setImageWithURL:[NSURL URLWithString:self.dataSource.bImageUrl] placeholderImage:[UIImage imageNamed:@"loading"]];
    [_serviceImageView setImageWithURL:[NSURL URLWithString:self.dataSource.serviceURL] placeholderImage:[UIImage imageNamed:@"loading"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nativeNavi:(id)sender {
//    //初始化调启导航时的参数管理类
//    NaviPara* para = [[NaviPara alloc]init];
//    //指定导航类型
//    para.naviType = NAVI_TYPE_NATIVE;
//    
//    //初始化终点节点
//    BMKPlanNode* end = [[BMKPlanNode alloc]init];
//    //指定终点经纬度
//    CLLocationCoordinate2D coor2;
//    coor2.latitude = [self.dataSource.latitude doubleValue];
//    coor2.longitude = [self.dataSource.longtitude doubleValue];
//    end.pt = coor2;
//    //指定终点名称
//    end.name = [self.dataSource.name stringByReplacingOccurrencesOfString:@"<br/>" withString:@""];
//    //end.name = @"广州市先烈南路";
//    //指定终点
//    para.endPoint = end;
//    
//    //指定返回自定义scheme，具体定义方法请参考常见问题
//    para.appScheme = @"baidumapsdk://mapsdk.baidu.com";
//    //调启百度地图客户端导航
//    [BMKNavigation openBaiduMapNavigation:para];
    
   // 调启web导航
        //初始化调启导航时的参数管理类
        BMKNaviPara* para = [[BMKNaviPara alloc]init];
        //指定导航类型
        para.naviType = BMK_NAVI_TYPE_WEB;
        //para.naviType = BMK_NAVI_TYPE_NATIVE;
        
        //初始化起点节点
        BMKPlanNode* start = [[BMKPlanNode alloc]init];
        //指定起点经纬度
        CLLocationCoordinate2D coor1;
    coor1.latitude = [MapLocation shareInstance].latitude;
    coor1.longitude = [MapLocation shareInstance].longitude;
        start.pt = coor1;
        //指定起点名称
//        start.name = _webStartName.text;
        //指定起点
        para.startPoint = start;
        
        //初始化终点节点
        BMKPlanNode* end = [[BMKPlanNode alloc]init];
        CLLocationCoordinate2D coor2;
        coor2.latitude = [self.dataSource.latitude doubleValue];
        coor2.longitude = [self.dataSource.longtitude doubleValue];
        end.pt = coor2;
        para.endPoint = end;
        //指定终点名称
//        end.name = _webEndName.text;
        //指定调启导航的app名称
        para.appName = [NSString stringWithFormat:@"%@", @"testAppName"];
        //调启web导航
        [BMKNavigation openBaiduMapNavigation:para];
    

    
}

- (IBAction)callService:(id)sender {
    //    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"确定拨打？" message:hotTell delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    //    alertView.tag = 100;
    //    [alertView show];
}

- (IBAction)callEmergency:(id)sender {
    //    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"确定拨打？" message:emergenceTell delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    //    alertView.tag = 101;
    //    [alertView show];
}

#pragma mark - alertView delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *tell = @"";
    if (alertView.tag==100) {
        tell = hotTell;
    }else if (alertView.tag ==101){
        tell = emergenceTell;
    }
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:{
            NSString *str = [tell stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", str]];
            [[UIApplication sharedApplication] openURL:url];
        }
            break;
        default:
            break;
    }
    
}
@end
