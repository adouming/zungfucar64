//
//  ReserveMainViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/4/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ReserveMainViewController.h"
#import "ZFStewardManager.h"
#import "DBManager.h"
#import "ReserveFormViewController.h"
@interface ReserveMainViewController ()

@end

@implementation ReserveMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}
//定期保养
- (IBAction)regularReserAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Steward" bundle:nil];
    ReserveFormViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"FixReserveFormID"];
    controler.reserveType = ReserveTypeService;
    controler.reserverServerType = ReserveTypeServiceTypeConst;
    [self.navigationController pushViewController:controler animated:YES];
}
//车辆维修

- (IBAction)carMemReserveAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Steward" bundle:nil];
    ReserveFormViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"FixReserveFormID"];
    controler.reserveType = ReserveTypeService;
    controler.reserverServerType = ReserveTypeServiceTypeMend;
    [self.navigationController pushViewController:controler animated:YES];
}



@end
