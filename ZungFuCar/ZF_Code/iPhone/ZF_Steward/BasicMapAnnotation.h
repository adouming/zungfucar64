#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "ZFStewardData.h"

@interface BasicMapAnnotation : NSObject <MKAnnotation> {
	CLLocationDegrees _latitude;
	CLLocationDegrees _longitude;
	NSString *title2;
    ZFNetServerStoreData *_storeData;
}

@property (nonatomic, strong) NSString *title2;
@property (nonatomic,strong) ZFNetServerStoreData *storeData;
- (id)initWithLatitude:(CLLocationDegrees)latitude
		  andLongitude:(CLLocationDegrees)longitude  andTitle:(NSString *)title2;
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;

@end
