//
//  ZFNavigationBarView.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ZFNavigationBarView.h"

@implementation ZFNavigationBarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ZFNavigationBarView" owner:self options:nil];
        UIView *nibView = [nibArray objectAtIndex:0];
        [self addSubview:nibView];
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
}
- (IBAction)itemPressAction:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    if ([self.delegate respondsToSelector:@selector(navigationBarDidPressAtIndex:)]) {
        [self.delegate navigationBarDidPressAtIndex:btn.tag];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


@end
