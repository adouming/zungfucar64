//
//  ReserveFormViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/4/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ReserveFormViewController_ipad.h"
#import "BeMemberViewController.h"

@interface ReserveFormViewController_ipad ()

@end

@implementation ReserveFormViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [[DataDBModel shareInstance] createDataModel];
    
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    if (_showBack) {
        [self.view bringSubviewToFront:self.customNavigaitionBar];
        self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    }else{
        self.customNavigaitionBar.hidden = YES;
        self.statusView.hidden = YES;
        
    }
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"booking_bg"]];
    [self updateCode:self.CodeBtn];
    _userInfo = [[DBManager sharedManager] getUserData];
    if (_userInfo == Nil) {
        _userID = 0;
    }
    if(_userInfo.userID != 0)
    {
        _userID = _userInfo.userID;
        self.nameTextField.text = _userInfo.nickName;
        
        if ([_userInfo.gender isEqualToString:@"0"]) {
            _manBtn.selected = YES;
            _manBtn.userInteractionEnabled = !_manBtn.selected;
            _womanBtn.selected = !_manBtn.selected;
            _womanBtn.userInteractionEnabled = !_manBtn.userInteractionEnabled;
        }
        if ([_userInfo.gender isEqualToString:@"1"]) {
            _womanBtn.selected = YES;
            _womanBtn.userInteractionEnabled = !_womanBtn.selected;
            _manBtn.selected = !_womanBtn.selected;
            _manBtn.userInteractionEnabled = !_womanBtn.userInteractionEnabled;
        }
        //获取登陆用户所属行业
        int industryCount = [[[DBManager sharedManager] getIndustryData] count];
        for (int i=0; i<industryCount; i++) {
            ZFNetServerIndustryData *industryData = [[[DBManager sharedManager] getIndustryData] objectAtIndex:i];
            if ([industryData.value integerValue] == _userInfo.otherJob) {
                [self.industryBtn setTitle:industryData.displayName forState:UIControlStateNormal];
                industryID = industryData.value;
                break;
            }
        }
        
        //获取登陆用户所从事职业
        int jobCount = [[[DBManager sharedManager] getJobData] count];
        for (int i=0; i<jobCount; i++) {
            ZFNetServerJobData *jobData = [[[DBManager sharedManager] getJobData] objectAtIndex:i];
            if ([jobData.value integerValue] == _userInfo.job) {
                [self.jobBtn setTitle:jobData.displayName forState:UIControlStateNormal];
                jobID = jobData.value;
                break;
            }
        }
        
        self.emailTextField.text = ((NSNull *)_userInfo.email == [NSNull null]) ? @"" : _userInfo.email;
        self.telTextField.text = ((NSNull *)_userInfo.homePhone == [NSNull null]) ? @"" : _userInfo.homePhone;
    }
    
    self.nameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.telTextField.delegate = self;
    self.codeTF.delegate = self;
    //    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    //    self.customNavigaitionBar.hidden = YES;
    //    [self scanCalculVCViewFrame:_isExtend2];
}

-(void)scanCalculVCViewFrame:(BOOL)isExtend{
    //    if (isExtend) {
    //        self.reserveContentView.frame = CGRectMake(90, self.reserveContentView.frame.origin.y, self.reserveContentView.frame.size.width, self.reserveContentView.frame.size.height);
    //        self.reserveContentVIewBg.frame = CGRectMake(90, self.reserveContentView.frame.origin.y, self.reserveContentView.frame.size.width, self.reserveContentView.frame.size.height);
    //    }else{
    //        self.reserveContentView.frame = CGRectMake(160, self.reserveContentView.frame.origin.y, self.reserveContentView.frame.size.width, self.reserveContentView.frame.size.height);
    //        self.reserveContentVIewBg.frame = CGRectMake(160, self.reserveContentView.frame.origin.y, self.reserveContentView.frame.size.width, self.reserveContentView.frame.size.height);
    //    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}

- (void)viewDidUnload {
    [self setContentView:nil];
    [self setBannerImageView:nil];
    [self setTitleLabel:nil];
    [self setCarSeriesBtn:nil];
    [self setCarStyleBtn:nil];
    [self setCityBtn:nil];
    [self setExhibitBtn:nil];
    [self setDateBtn:nil];
    [self setNameTextField:nil];
    [self setIndustryBtn:nil];
    [self setJobBtn:nil];
    [self setManBtn:nil];
    [self setWomanBtn:nil];
    [self setEmailTextField:nil];
    [self setTelTextField:nil];
    [self setTimeBtn:nil];
    [super viewDidUnload];
}

#pragma mark - action
- (IBAction)showPickerView:(id)sender {
    [self.nameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.telTextField resignFirstResponder];
    [self.codeTF resignFirstResponder];
    
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.contentView.frame;
    rect.origin.y = -100;
    self.contentView.frame = rect;
    [UIView commitAnimations];
    self.contentView.scrollEnabled = NO;
    
    
    if (pickerView) {
        pickerView.hidden = !pickerView.hidden;
    }
    
    if (pickerView == nil) {
        pickerView = [[customPickerView alloc] initWithTitle:@"" delegate:self];
        pickerView.pickerDelegate = self;
    }
    
    if ([sender isEqual:_carSeriesBtn]) {
        pickerView.pickerStyle = pickerViewTypeCarSeriesReserve;
    }
    if ([sender isEqual:_carStyleBtn]) {
        pickerView.carTypeData = self.carTypeData;
        pickerView.pickerStyle = pickerViewTypeCarTypeReserve;
        
    }
    if ([sender isEqual:_cityBtn]) {
        
        pickerView.pickerStyle = pickerViewTypeCity;
    }
    if ([sender isEqual:_exhibitBtn]) {
        pickerView.cityData = self.cityTypeData;
        pickerView.pickerStyle = pickerViewTypeExhibit;
    }
    if ([sender isEqual:_dateBtn]) {
        CGRect rect = pickerView.frame;
        rect.origin.y = 71;
        rect.size.height = -44;
        if (!datepick ){
            datepick = [[UIDatePicker alloc] initWithFrame:rect];
            datepick.datePickerMode = UIDatePickerModeDate;
            datepick.minuteInterval = 1;
            
            //一年的秒数
            float oneYearTime = 60*60*24*365;
            
            //获得当前时间
            NSDate *currentDate = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
            NSDate *minDate = [currentDate dateByAddingTimeInterval:0];
            NSDate *maxDate = [currentDate dateByAddingTimeInterval:100*oneYearTime];
            //最小可选时间
            datepick.minimumDate = minDate;
            datepick.maximumDate = maxDate;
            [datepick setDate:minDate animated:YES];
            [datepick addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
        }
        
        [pickerView addSubview:datepick];
        pickerView.pickerStyle = pickerViewTypeDate;
    }else if(datepick){
        [datepick removeFromSuperview];
    }
    if ([sender isEqual:_timeBtn]) {
        
        pickerView.pickerStyle = pickerViewTypeServerTime;
        
    }
    if ([sender isEqual:_industryBtn]) {
        pickerView.pickerStyle = pickerViewTypeIndustry;
    }
    if ([sender isEqual:_jobBtn]) {
        pickerView.pickerStyle = pickerViewTypeJob;
    }
    
    [pickerView showInView:sender];
}

-(void)dateChanged:(id)sender{
    UIDatePicker* control = (UIDatePicker*)sender;
    NSDate* _date = control.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSDate *minDate = [dateFormatter dateFromString:@"2013-01-01"];
    control.minimumDate = minDate;
    NSString *destDateString = [dateFormatter stringFromDate:_date];
    [self.dateBtn setTitle:destDateString forState:UIControlStateNormal];
}

- (IBAction)lookAgreeRule:(id)sender {
    
    [self.nameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.telTextField resignFirstResponder];
    [self.codeTF resignFirstResponder];
    
    //协议生成
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Club_iPad" bundle:nil];
    _noteCtl = [stryBoard instantiateViewControllerWithIdentifier:@"noteVCID"];
    _noteCtl.view.frame = self.view.frame;
    [self.view addSubview:_noteCtl.view];
    
    //协议显示
    _noteCtl.view.hidden = NO;
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    _noteCtl.view.alpha = 1;
    [UIView commitAnimations];
    
}


#pragma mark - action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.contentView.frame;
    rect.origin.y = 0;
    self.contentView.frame = rect;
    [UIView commitAnimations];
    self.contentView.scrollEnabled = YES;
    
    if ([actionSheet isKindOfClass:[customPickerView class]]){
        if(buttonIndex == 0) {
            [pickerView removeFromSuperview];
            pickerView = nil;
        }else {
            if (pickerView.pickerStyle == pickerViewTypeDate) {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSString *strDate = [dateFormatter stringFromDate:datepick.date];
                [self.dateBtn setTitle:strDate forState:UIControlStateNormal];
                [datepick  removeFromSuperview];
            }
        }
        [pickerView removeFromSuperview];
        pickerView = nil;
    }
    
}

- (IBAction)appointAction:(id)sender {
    
    [self.nameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.telTextField resignFirstResponder];
    if ([self infoIntegrityCheck]) {
        
        [self sendBookInfo];
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"表单填写有误，请核对后再次发送" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (BOOL)infoIntegrityCheck
{
    
    if ([self.carSeriesBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needSeries.hidden = NO;
    } else {
        _needSeries.hidden = YES;
    }
    
    if ([self.carStyleBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needStyle.hidden = NO;
    } else {
        _needStyle.hidden = YES;
    }
    
    if ([self.cityBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needCity.hidden = NO;
    } else {
        _needCity.hidden = YES;
    }
    
    if ([self.exhibitBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needStore.hidden = NO;
    } else {
        _needStore.hidden = YES;
    }
    
    if ([self.dateBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needDate.hidden = NO;
    } else {
        _needDate.hidden = YES;
    }
    
    if ([self.timeBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needTime.hidden = NO;
    } else {
        _needTime.hidden = YES;
    }
    
    if ([self.nameTextField.text length] == 0) {
        _needNick.hidden = NO;
    } else {
        _needNick.hidden = YES;
    }
    
    if (self.manBtn.isSelected || self.womanBtn.isSelected) {
        _needGender.hidden = YES;
    } else {
        _needGender.hidden = NO;
    }
    
    if ([self isValidateEmail:self.emailTextField.text]) {
        _needEmail.hidden = YES;
    } else {
        if ([self.emailTextField.text length] == 0) {
            _needEmail.text = @"必填";
        } else {
            _needEmail.text = @"有误";
        }
        _needEmail.hidden = NO;
    }
    
    if ([self validateNumeric:self.telTextField.text]) {
        _needPhone.hidden = YES;
    } else {
        if ([self.telTextField.text length] == 0) {
            _needPhone.text = @"必填";
        } else {
            _needPhone.text = @"有误";
        }
        _needPhone.hidden = NO;
    }
    
    if (self.agreeBtn.isSelected) {
        _needAgree.hidden = YES;
    } else {
        _needAgree.hidden = NO;
    }
    
    if ([self.codeTF.text isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"Code"]]) {
        _needCode.hidden = YES;
    } else {
        if ([self.codeTF.text length] == 0) {
            _needCode.text = @"必填";
        } else {
            _needCode.text = @"有误";
        }
        _needCode.hidden = NO;
    }
    
    return (_needSeries.hidden && _needStyle.hidden && _needCity.hidden && _needStore.hidden && _needDate.hidden && _needTime.hidden && _needNick.hidden && _needGender.hidden && _needEmail.hidden && _needPhone.hidden && _needAgree.hidden && _needCode.hidden);
}

- (BOOL)isValidateEmail:(NSString *)email
{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}

- (BOOL)validateNumeric:(NSString *)str2validate
{
    NSString *phoneRegex = @"[0-9]{1,11}";
    
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",phoneRegex];
    
    return [phoneTest evaluateWithObject:str2validate];
}

- (void)sendBookInfo
{
    
    
    [[NSUserDefaults standardUserDefaults] setObject:self.emailTextField.text forKey:@"isBecomeEmail"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys: [NSString stringWithFormat:@"%d", self.reserveType], @"Type",
                         [NSString stringWithFormat:@"%d", self.reserverServerType],                @"Services",
                         @([carTypeID integerValue]),       @"ProductID",
                         @([carModelID integerValue]),      @"ProductDetailID",
                         @([cityID integerValue]),          @"City",
                         @([exhibitID integerValue]),       @"StoreID",
                         self.dateBtn.titleLabel.text,      @"ScheduleDate",
                         self.timeBtn.titleLabel.text,      @"ScheduleTime",
                         @(_userID),                        @"MemberID",
                         self.nameTextField.text,           @"NickName",
                         self.manBtn.selected?@"0":@"1",    @"Gender",
                         @([industryID integerValue]),      @"Industry",
                         @([jobID integerValue]),           @"Job",
                         self.emailTextField.text,          @"Email",
                         self.telTextField.text,            @"CellPhone",
                         @(1),                              @"BecomeMember",
                         nil];
    
    /**************************************/
    ////////////////////////////////
    // 1
    //
    //
    [self showLoadingText];
    
    
    AFHTTPRequestOperationManager *reserveManager = [AFHTTPRequestOperationManager manager];
    [reserveManager.requestSerializer setValue:HTTP_X_U forHTTPHeaderField:@"HTTP-X-U"];
    [reserveManager.requestSerializer setValue:HTTP_X_P forHTTPHeaderField:@"HTTP-X-P"];
    reserveManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [reserveManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [reserveManager POST:[NSString stringWithFormat:@"%@%@/api/Booking/fn/book", ZF_BASE_URL, ZF_BASE_PATH] parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", operation);
        [self hideLoadingText];
        if (_userInfo.userID == 0) {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Steward_iPad" bundle:nil];
            BeMemberViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"BeMemberVCID"];
            [self.navigationController pushViewController:controler animated:YES];
            
        } else {
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"预约成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            successAlert.tag = 88;
            [successAlert show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"operation =====> %@", operation);
        NSLog(@"error with %@", error);
        [self hideLoadingText];
        UIAlertView *failAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"信息发送失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [failAlert show];
    }];
    
    //测试发送方法
    //    [reserveManager POST:[NSString stringWithFormat:@"%@%@/api/Booking/fn/book", ZF_BASE_URL, ZF_BASE_PATH] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    //
    //        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%d", self.reserveType] dataUsingEncoding:NSUTF8StringEncoding] name:@"Type"];
    //        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%d", self.reserverServerType] dataUsingEncoding:NSUTF8StringEncoding] name:@"Services"];
    //        [formData appendPartWithFormData:[carTypeID dataUsingEncoding:NSUTF8StringEncoding] name:@"ProductID"];
    //        [formData appendPartWithFormData:[carModelID dataUsingEncoding:NSUTF8StringEncoding] name:@"ProductDetailID"];
    //        [formData appendPartWithFormData:[cityID dataUsingEncoding:NSUTF8StringEncoding] name:@"City"];
    //        [formData appendPartWithFormData:[exhibitID dataUsingEncoding:NSUTF8StringEncoding] name:@"StoreID"];
    //        [formData appendPartWithFormData:[self.dateBtn.titleLabel.text dataUsingEncoding:NSUTF8StringEncoding] name:@"ScheduleDate"];
    //        [formData appendPartWithFormData:[self.timeBtn.titleLabel.text dataUsingEncoding:NSUTF8StringEncoding] name:@"ScheduleTime"];
    //        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%d", _userID] dataUsingEncoding:NSUTF8StringEncoding] name:@"MemberID"];
    //        [formData appendPartWithFormData:[self.nameTextField.text dataUsingEncoding:NSUTF8StringEncoding] name:@"NickName"];
    //        [formData appendPartWithFormData:[(self.manBtn.selected?@"0":@"1") dataUsingEncoding:NSUTF8StringEncoding] name:@"Gender"];
    //        [formData appendPartWithFormData:[industryID dataUsingEncoding:NSUTF8StringEncoding] name:@"Industry"];
    //        [formData appendPartWithFormData:[jobID dataUsingEncoding:NSUTF8StringEncoding] name:@"Job"];
    //        [formData appendPartWithFormData:[self.emailTextField.text dataUsingEncoding:NSUTF8StringEncoding] name:@"Email"];
    //        [formData appendPartWithFormData:[self.telTextField.text dataUsingEncoding:NSUTF8StringEncoding] name:@"CellPhone"];
    //        [formData appendPartWithFormData:[@"1" dataUsingEncoding:NSUTF8StringEncoding] name:@"BecomeMember"];
    //
    //    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
    //        NSLog(@"%@", operation);
    //        [self hideLoadingText];
    //        if (_userInfo.userID == 0) {
    //
    //            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Steward_iPad" bundle:nil];
    //            BeMemberViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"BeMemberVCID"];
    //            [self.navigationController pushViewController:controler animated:YES];
    //
    //        } else {
    //            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"预约成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    //            successAlert.tag = 88;
    //            [successAlert show];
    //        }
    //
    //    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    //        NSLog(@"operation =====> %@", operation);
    //        NSLog(@"error with %@", error);
    //        [self hideLoadingText];
    //        UIAlertView *failAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"信息发送失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    //        [failAlert show];
    //
    //    }];
}

- (IBAction)genderAction:(UIButton *)sender {
    if ([sender isEqual:_manBtn]) {
        _manBtn.selected = YES;
        _manBtn.userInteractionEnabled = !_manBtn.selected;
        _womanBtn.selected = !_manBtn.selected;
        _womanBtn.userInteractionEnabled = !_manBtn.userInteractionEnabled;
    }
    if ([sender isEqual:_womanBtn]) {
        _womanBtn.selected = YES;
        _womanBtn.userInteractionEnabled = !_womanBtn.selected;
        _manBtn.selected = !_womanBtn.selected;
        _manBtn.userInteractionEnabled = !_womanBtn.userInteractionEnabled;
    }
    
    
}

- (IBAction)agreeIconAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    _isAgreeRule = sender.selected;
}
- (IBAction)agreeRuleAction:(id)sender {
}

- (IBAction)updateCode:(id)sender {
    //获取一个随机整数，范围在[from,to），包括from，不包括to
    NSString *codeNum = [NSString stringWithFormat:@"%d", (int)(1000 + (arc4random() % (9999 - 1000 + 1)))];
    [[NSUserDefaults standardUserDefaults] setObject:codeNum forKey:@"Code"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.CodeLab.text = codeNum;
}

#pragma mark - alert delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 88) {
        //        [self.navigationController popToRootViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GOHOME" object:nil];
    }
}



#pragma mark - textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //    [UIView beginAnimations:@"text" context:nil];
    //    [UIView setAnimationDuration:0.5];
    //    if ([textField isEqual:_nameTextField]) {
    //        self.contentView.contentInset = UIEdgeInsetsMake(-200, 0, 200, 0);
    //    }
    //    if ([textField isEqual:_emailTextField]) {
    //        self.contentView.contentInset = UIEdgeInsetsMake(-350, 0, 350, 0);
    //    }
    //    if ([textField isEqual:_telTextField]) {
    //        self.contentView.contentInset = UIEdgeInsetsMake(-350, 0, 350, 0);
    //    }
    //    [UIView commitAnimations];
    //    self.contentView.scrollEnabled = NO;
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.view.frame;
    if ([textField isEqual:self.codeTF]) {
        rect.origin.y = -330;
    } else {
        rect.origin.y = -300;
    }
    self.view.frame = rect;
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    //    [textField resignFirstResponder];
    //    [UIView beginAnimations:@"text" context:nil];
    //    [UIView setAnimationDuration:0.5];
    //    self.contentView.contentInset = UIEdgeInsetsZero;
    //    [UIView commitAnimations];
    //    self.contentView.scrollEnabled = YES;
    [textField resignFirstResponder];
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.view.frame;
    rect.origin.y = 0;
    self.view.frame = rect;
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)pickerViewDidSelectRow:(NSInteger)row withStyle:(pickerViewType)style andData:(id)data{
    switch (style) {
        case pickerViewTypeCarSeriesReserve:{
//            ZFNetServerCarTypeData *carData = (ZFNetServerCarTypeData *)data;
//            [self.carSeriesBtn setTitle:carData.name forState:UIControlStateNormal];
//            carTypeID = [NSString stringWithFormat:@"%d", carData.carID];
//            self.carTypeData = data;
            //
            int count = [[[DBManager sharedManager] getCarListData:@"Banz_car_list"] count];
            NSString *name;
            if (row<=count) {
                name = [NSString stringWithFormat:@"  奔驰乘用车%@",[data valueForKey:@"Name"]];
            }else{
                name = [NSString stringWithFormat:@"  AMG系列%@",[data valueForKey:@"Name"]];
            }
            [self.carSeriesBtn setTitle:name forState:UIControlStateNormal];
            [self.carStyleBtn setTitle:@"  " forState:UIControlStateNormal];
            carTypeID = [data objectForKey:@"ID"];
            self.carTypeData = (NSDictionary *)data;
        }
            break;
        case pickerViewTypeCarTypeReserve:{
//            ZFNetServerCarProductData *carData = (ZFNetServerCarProductData *)data;
//            [self.carStyleBtn setTitle:carData.name forState:UIControlStateNormal];
//            carModelID = [NSString stringWithFormat:@"%d", carData.carModelID];
            //
            [self.carStyleBtn setTitle:[NSString stringWithFormat:@"  %@",[data valueForKey:@"Name"]] forState:UIControlStateNormal];
            carModelID = [data objectForKey:@"ID"];
        }
            
            break;
        case pickerViewTypeCity:{
            ZFNetServerCityExhibitionData *stData = (ZFNetServerCityExhibitionData *)data;
            [self.cityBtn setTitle:stData.name forState:UIControlStateNormal];
            cityID = stData.cityid;
            self.cityTypeData = stData;
        }
            
            break;
        case pickerViewTypeDate:{
            
        }
            
            break;
        case pickerViewTypeExhibit:{
            ZFNetServerExhibitionData *stData = (ZFNetServerExhibitionData *)data;
            [self.exhibitBtn setTitle:stData.location forState:UIControlStateNormal];
            exhibitID = [NSString stringWithFormat:@"%d", stData.exhibitionID];
            
        }
            
            break;
        case pickerViewTypeIndustry:{
            ZFNetServerIndustryData *stData = (ZFNetServerIndustryData *)data;
            [self.industryBtn setTitle:stData.displayName forState:UIControlStateNormal];
            industryID = stData.value;
        }
            
            break;
        case pickerViewTypeJob:{
            ZFNetServerJobData *stData = (ZFNetServerJobData *)data;
            [self.jobBtn setTitle:stData.displayName forState:UIControlStateNormal];
            jobID = stData.value;
        }
            
            break;
        case pickerViewTypeServerTime:{
            ZFNetServerServiceTimeData *stData = (ZFNetServerServiceTimeData *)data;
            [self.timeBtn setTitle:stData.value forState:UIControlStateNormal];
            
        }
            
            break;
        case pickerViewTypeTestTime:{
            ZFNetServerTestTimeData *stData = (ZFNetServerTestTimeData *)data;
            [self.timeBtn setTitle:stData.value forState:UIControlStateNormal];
            
        }
            
            break;
        default:
            break;
    }
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.nameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.telTextField resignFirstResponder];
    [self.codeTF resignFirstResponder];
}
@end
