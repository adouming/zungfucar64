//
//  CarMasterShareViewController.m
//  ZungFuCar
//
//  Created by adouming on 15/8/13.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "CarMasterShareViewController.h"
#import "AFJSONRequestOperation.h"
#import "UIImageView+AFNetworking.h"
#import "DBManager.h"
#import "MyUILabel.h"
#import "CarDetailVC.h"
#import "SmartCarDetailVC.h"
#import "SelectTableViewCell.h"

@interface CarMasterShareViewController ()

@end

@implementation CarMasterShareViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view bringSubviewToFront:self.customNavigaitionBar];
    }
    return self;
}

- (void) controllerPressed:(id)sender {
    UISegmentedControl *segmentControl =(UISegmentedControl *)sender;
    int selectedSegment = segmentControl.selectedSegmentIndex;
    NSLog(@"Segment %d selected\n", selectedSegment);
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    CGRect bgFrame;
    if (IOS7) {
        bgFrame = CGRectMake(0, 20, SCREEN_WIDTH, SCREEN_HEIGHT);
    }else{
        bgFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    
    UIImageView *bgView = [[UIImageView alloc]initWithFrame:bgFrame];
    bgView.image = [UIImage imageNamed:@"sl_bg.png"];
    [self.view addSubview:bgView];
    
    
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    dataSource = [[NSMutableArray alloc]init];
    
    UILabel *tipLabel = [[UILabel alloc]initWithFrame:CGRectMake(-2, 99, 299, 45)];
    tipLabel.text = @"车主专享";
    dataSource = [[DBManager sharedManager] getCarMasterShareData:@"car_master_share"];
    
    
    tipLabel.backgroundColor = [UIColor clearColor];
    tipLabel.textColor = [UIColor whiteColor];
    tipLabel.font = [UIFont boldSystemFontOfSize:45.0f];
    tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:tipLabel];
    
    hScrollView = [[HorizontalScrollView alloc]initWithFrame:CGRectMake(0, 142, SCREEN_WIDTH, SCREEN_HEIGHT-142)];
    //hScrollView.frame = CGRectMake(0, 142, 270, 250);
    hScrollView.delegate = self;
    if ([dataSource count]>0) {
        if ([dataSource count]>0) {
            
            [hScrollView initScrollView:dataSource type:0];
            
        }
    }
    [self.view addSubview:hScrollView];
    
    
    
    
    
    self.carMap = @{@"奔驰乘用车":
                        @{@"A":@77,
                          @"B":@78,
                          @"C":@79,
                          @"CLA":@80,
                          @"CLS":@81,
                          @"E":@82,
                          @"G":@83,
                          @"GL":@84,
                          @"GLA":@85,
                          @"GLK":@86,
                          @"M":@87,
                          @"R":@88,
                          @"S":@89,
                          @"SL":@90,
                          @"SLK":@121,
                          },
                    @"AMG系列":@{
                            @"A":@123,
                            @"C":@124,
                            @"CLA":@130,
                            @"CLS":@131,
                            @"G":@132,
                            @"GL":@133,
                            @"GLA":@134,
                            @"ML":@135,
                            @"S":@136,
                            @"SL":@137
                            },
                    @"smart":@ {
                        @"A":@126,
                        @"B":@111
                    }
                    };
    
    
    
    _selctTableView = [[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 55, 55, 55, SCREEN_HEIGHT - 70 - 50) style:UITableViewStylePlain];
    [self.view addSubview:_selctTableView];
    
    [_selctTableView setDataSource:self];
    [_selctTableView setDelegate:self];
    [_selctTableView setBackgroundColor:[UIColor clearColor]];
    [_selctTableView setShowsVerticalScrollIndicator:NO];
    
    //[_selctTableView.layer setCornerRadius:10.0f];
    [_selctTableView.layer setMasksToBounds:YES];
    //[_selctTableView.layer setBorderWidth:0.0f];
    //[_selctTableView.layer setBorderColor:[UIColor clearColor].CGColor];
    [_selctTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    NSString* title = @"车主专享";
    
    NSDictionary* subItems = [_carMap objectForKey:title];
    
    //self.carArray = [[subItems allKeys] sortedArrayUsingSelector:@selector(compare:)];
    self.carArray = [subItems allKeys];
    
    
    NSString* itemTitle = [_carArray objectAtIndex:0] ;
    NSInteger* itemKey = [[subItems objectForKey:itemTitle] integerValue];
    
    
    
    [self loadData:24];
    
    
    //[self loadData:77];
    
    
    
}
- (void)loadData:(NSInteger)itemKey {
    
    
    
    
    //https://www.zfchina.com/api/list.php?id=77
    NSString *urlString = nil;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    AFHTTPRequestOperationManager *request = [AFHTTPRequestOperationManager manager];
    [request.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"Http-X-U"];
    [request.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"Http-X-P"];
    request.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    urlString = [NSString stringWithFormat:@"%@%@%@%d", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID, itemKey];
    [request GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"App.net Global Stream: %@", responseObject);
        NSLog(@"operation %@", operation);
        [self setTableViewData:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    
}
#pragma makr - tableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *strId = @"cell";
    SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strId];
    if (!cell) {
        cell = [[SelectTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strId] ;
        
    }
    
    //取消选中颜色
    
    　　UIView *backView = [[UIView alloc] initWithFrame:cell.frame];
    　　cell.selectedBackgroundView = backView;
    　　cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    
    　　//取消边框线
    
    　　[cell setBackgroundView:[[UIView alloc] init]];          //取消边框线
    cell.backgroundColor = [UIColor clearColor];
    
    [cell.carNameLable setText:[_carArray objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_carArray count];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* title = @"车主专享";
    NSDictionary* subItems = [_carMap objectForKey:title];
    NSString* itemTitle = [_carArray objectAtIndex:indexPath.row];
    NSInteger* itemKey = [[subItems objectForKey:itemTitle] integerValue];
    
    [self loadData:itemKey];
    NSLog(@"%d", itemKey);
}
-(void)setTableViewData:(id) data{
    if ([[data valueForKey:@"list"] count] > 0) {
        dataSource = [data valueForKey:@"list"];
        [hScrollView clearAllView];
        if ([dataSource count]>0) {
            
            [hScrollView initScrollView:dataSource type:0];
            
        }
        
        [[DBManager sharedManager] storeCarMasterShareData:dataSource Sucess:nil fail:nil key:@"car_master_share"];
        
    }
}

-(void)pushToDetail:(int )index{
    NSDictionary *dic = [dataSource objectAtIndex:index];
    NSString *type = [dic valueForKey:@"title"];
    
    type = [dic valueForKey:@"title"];
    
    NSString *carId = [dic valueForKey:@"id"];
    NSString *bannerURL = [dic objectForKey:@"timg"];
    NSArray *carInfo = [dic objectForKey:@"content"];
    
    
    //carInfo = [dic objectForKey:@"info"];
    SmartCarDetailVC *smartCarDetailVC = [[SmartCarDetailVC alloc] initWithTitle:@"相关活动" bannerUrl:bannerURL date:@"" descp:carInfo address:@"" shortDesc:@"" bannerName:@""];
    [self.navigationController pushViewController:smartCarDetailVC animated:YES];
    
}

#pragma mark - navigationBarDelegate
-(void)navigationBarDidPressAtIndex:(NSInteger)index{
    switch (index) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
            
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
