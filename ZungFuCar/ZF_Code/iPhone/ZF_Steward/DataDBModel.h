//
//  DataDBModel.h
//  ZungFuCar
//
//  Created by Alex Peng on 11/8/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlexMacro.h"
@interface DataDBModel : NSObject
+ (DataDBModel *)shareInstance;
- (void)createDataModel;
- (void)createReserveInfoModel;
- (void)createCarModelModel:(int)cid;
- (void)createCarModel;
- (void)createExhibitionModel:(int)cityID type:(ReserveType)type;
@end
