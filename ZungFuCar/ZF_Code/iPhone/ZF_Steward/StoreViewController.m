//
//  StoreViewController.m
//  ZungFuCar
//
//  Created by Ed Lee on 12/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "StoreViewController.h"

static NSString * const CellIdentifier = @"StoreListCellIdentifier";

@interface StoreViewController ()

@end

@implementation StoreViewController

#define TABLEW                      320
#define DETAILW                     320
#define METERS_IN_ONE_LAT_DEGREE    110574.61088
#define METERS_IN_ONE_LONG_DEGREE   111302.61697
#define MAP_PRECISION               2000

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.view.backgroundColor = RGB(42, 42, 42);
    
    UIViewController *vc = [[UIViewController alloc]init];
    self.contentNaviCtrl = [[UINavigationController alloc] initWithRootViewController:vc];
    self.contentNaviCtrl.view.frame = CGRectMake(self.view.frame.size.width, IOS7?20:0, 0, self.view.frame.size.height-(IOS7?70:50));
    self.contentNaviCtrl.navigationBarHidden = YES;
    [self.view addSubview:self.contentNaviCtrl.view];
    self.contentNaviCtrl.view.backgroundColor = [UIColor clearColor];
    
    _listView.backgroundColor = [UIColor clearColor];
    _listView.backgroundView = nil;
    
    _listViewContainer.frame = CGRectMake(_listViewContainer.frame.origin.x, IOS7?20:0, _listViewContainer.frame.size.width, self.view.frame.size.height-(IOS7?70:50));
    _mapView.frame = CGRectMake(_mapView.frame.origin.x, IOS7?20:0, _mapView.frame.size.width, self.view.frame.size.height-(IOS7?70:50));
    
    detailIsShown = NO;
    
    [self createModel];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(sortArray) name:@"postLocationUpdate" object:nil];
    
    [self initMapView];
    
    _centerBtn = [[UIButton alloc]initWithFrame:CGRectMake(25, _mapView.frame.size.height-(IOS7?25:45), 30, 30)];
    [_centerBtn setImage:[UIImage imageNamed:@"Current_Position_padicon"] forState:UIControlStateNormal];
    [_centerBtn addTarget:self action:@selector(moveToMapCenter) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_centerBtn];
    
    if (IOS7) {
        UIView *statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 20)];
        statusView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:statusView];
    }
}

-(void)initTableView{
    heightArray = [NSMutableArray array];
    [self sortArray];
}

-(void)sortArray{
    NSMutableArray *sourceArray = [[DBManager sharedManager] getStoreData];
    NSMutableArray *dicArray = [[NSMutableArray alloc]initWithCapacity:[sourceArray count]];
    for (ZFNetServerStoreData *store in sourceArray) {
        NSString *lat = store.latitude;
        NSString *lng = store.longtitude;
        double distance;
        if ([NSStringUtil isBlankString:lat] || [NSStringUtil isBlankString:lat]) {
            distance = MAXFLOAT;
        }else{
            distance = [[MapLocation shareInstance] getDistanceFromLocationLatitude:lat andLongitude:lng];
        }
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:store forKey:@"store"];
        [dic setObject:[NSNumber numberWithDouble:distance] forKey:@"distance"];
        [dicArray addObject:dic];
    }
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sorter count:1];
    NSArray *sortedArray = [dicArray sortedArrayUsingDescriptors:sortDescriptors];
    [sourceArray removeAllObjects];
    for (int i=0; i<[sortedArray count]; i++) {
        NSMutableDictionary *dic = [sortedArray objectAtIndex:i];
        [sourceArray addObject:[dic objectForKey:@"store"]];
    }
    _storeDataArray = sourceArray;
    [_listView reloadData];
    [[DBManager sharedManager] storeStoreData:sourceArray Sucess:^(NSMutableArray *storeData) {
        
    } fail:^(NSError *error) {
        NSLog(@"error=%@",error.description);
    }];
}

#pragma mark - tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = [StoreListCell CellData:self.storeDataArray heightForRowAtIndexPath:indexPath];
    [heightArray addObject:[NSNumber numberWithFloat:height]];
    //height = CELL_HEIGHT;
    return 95;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.storeDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int row = [indexPath row];
    StoreListCell *cell = (StoreListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StoreListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setData:[self.storeDataArray objectAtIndex:row] andHeight:[NSNumber numberWithInt:95] row:row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    StoreDetailController *sdCtrl = [[StoreDetailController alloc]initWithNibName:@"StoreDetailController" bundle:nil];
    sdCtrl.dataSource = [self.storeDataArray objectAtIndex:indexPath.row];
    [_contentNaviCtrl pushViewController:sdCtrl animated:NO];
    
    if (!detailIsShown) {
        CGRect mapViewFrame = CGRectMake(_mapView.frame.origin.x, _mapView.frame.origin.y, 368, _mapView.frame.size.height);
        CGRect listViewFrame = CGRectMake(368, _listViewContainer.frame.origin.y, _listViewContainer.frame.size.width, _listViewContainer.frame.size.height);
        CGRect expandBtnFrame = CGRectMake(368+320, _contentNaviCtrl.view.frame.origin.y, 16, _contentNaviCtrl.view.frame.size.height);
        CGRect naviViewFrame = CGRectMake(368+16+320, _contentNaviCtrl.view.frame.origin.y, DETAILW, _contentNaviCtrl.view.frame.size.height);
        
        
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             _mapView.frame = mapViewFrame;
                             _listViewContainer.frame = listViewFrame;
                             _expandMapBtn.frame = expandBtnFrame;
                             self.contentNaviCtrl.view.frame = naviViewFrame;
                         }
                         completion:nil];
    }
}

- (IBAction)expandMap:(id)sender {
    CGRect mapViewFrame = CGRectMake(_mapView.frame.origin.x, _mapView.frame.origin.y, 704, _mapView.frame.size.height);
    CGRect listViewFrame = CGRectMake(704, _listViewContainer.frame.origin.y, _listViewContainer.frame.size.width, _listViewContainer.frame.size.height);
    CGRect expandBtnFrame = CGRectMake(1024, _contentNaviCtrl.view.frame.origin.y, 16, _contentNaviCtrl.view.frame.size.height);
    CGRect naviViewFrame = CGRectMake(1024+16, _contentNaviCtrl.view.frame.origin.y, DETAILW, _contentNaviCtrl.view.frame.size.height);
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _mapView.frame = mapViewFrame;
                         _listViewContainer.frame = listViewFrame;
                         _expandMapBtn.frame = expandBtnFrame;
                         self.contentNaviCtrl.view.frame = naviViewFrame;
                     }
                     completion:^(BOOL finished){
                         detailIsShown = NO;
                     }];
}

#pragma mark - Map view codes

-(void)initMapView{
    [MapLocation latestStoreID];
    
    CLLocationCoordinate2D theCoordinate;
    theCoordinate.latitude=[[MapLocation shareInstance].nearLatitude doubleValue];
    theCoordinate.longitude=[[MapLocation shareInstance].nearLongitude doubleValue];;
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(theCoordinate.latitude, theCoordinate.longitude);
    float zoomLeavel = 0.1;
    MKCoordinateRegion region = MKCoordinateRegionMake(coord, MKCoordinateSpanMake(zoomLeavel, zoomLeavel));
    
    [_mapView setMapType:MKMapTypeStandard];
    [_mapView setRegion:region];
    _mapView.delegate=self;
    _mapView.showsUserLocation=YES;
    if (IOS7) {
        _mapView.rotateEnabled = NO;
    }
    [self drawAnnotation];
}

-(void)drawAnnotation{
    NSMutableArray *annotations  = [[NSMutableArray alloc] initWithCapacity:[_storeDataArray count]];
    //    MapViewAnnotation *annotation;
    BasicMapAnnotation *annotation;
    for (ZFNetServerStoreData *data in _storeDataArray)
    {
        // loop through each store object and add them as annotations!
        
        double latitude = [data.latitude doubleValue];
        double longitude = [data.longtitude doubleValue];
        NSString *name = data.name;
        NSString *subName = data.location;
        if ([NSStringUtil isBlankString:subName]) {
            subName = name;
        }
        if ([NSStringUtil isBlankString:subName]) {
            subName = @"";
        }
        annotation=[[BasicMapAnnotation alloc] initWithLatitude:latitude andLongitude:longitude andTitle:subName];
        annotation.storeData = data;
        [annotations addObject:annotation];
        [_mapView addAnnotation:annotation];
        
    }
    // draw it out
    [self showAnnotations:annotations];
}
- (void) showAnnotations:(NSArray *)annotations
{
    // first clear all existing pins
    for(MapViewAnnotation *annotation in self.mapView.annotations)
    {
        if(![annotation isKindOfClass:[MKUserLocation class]])
        {
            [self.mapView removeAnnotation:annotation];
        }
    }
    // add
    for(MapViewAnnotation *annotation in annotations)
    {
        //NSLog(@"adding annotation %@", annotation);
        [self.mapView addAnnotation:annotation];
    }
}

- (void) closeSelectedAnnotation
{
    // close call out bubble
    for(MapViewAnnotation *annotation in self.mapView.selectedAnnotations)
    {
        [self.mapView deselectAnnotation:annotation animated:NO];
    }
}

- (void) redrawSelectedAnnotation
{
    // when you move the map, the callout bubble may go out of screen
    // to fix this, this function deselect and reselect the selected annotation
    // so that the call out bubble will be nicely presented in the map
    for(MapViewAnnotation *annotation in self.mapView.selectedAnnotations)
    {
        //NSLog(@"reselect %@", annotation.title);
        [self.mapView deselectAnnotation:annotation animated:NO];
        [self.mapView selectAnnotation:annotation animated:YES];
    }
}
- (void) centraliseOnLat:(double)latitude andLong:(double)longitude andOnTop:(BOOL)isOnTop andRadiusFromCenter:(double)radiusInMeter
{
    
	
#if FIX_MAP_CRASH_WHEN_NETWORK_DOWN
	
	if (latitude <= -90 || latitude >= 90 ||
		longitude <= -180 || longitude >= 180) {
        
        return;
    }
	
#endif
    
    double existingLongitudeDeltaInMeters = self.mapView.region.span.longitudeDelta * METERS_IN_ONE_LONG_DEGREE;
    double existingLatitudeDeltaInMeters = self.mapView.region.span.latitudeDelta * METERS_IN_ONE_LAT_DEGREE;
    double longitudeDeltaInMeters = radiusInMeter == 0 ? existingLongitudeDeltaInMeters : 2 * radiusInMeter;
    double latitudeDeltaInMeters = radiusInMeter == 0 ? existingLatitudeDeltaInMeters : 2 * radiusInMeter;
    double latitudeDeltaInDegrees = latitudeDeltaInMeters / METERS_IN_ONE_LAT_DEGREE;
    latitude = isOnTop ? latitude - 0.5 * latitudeDeltaInDegrees : latitude;
    MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(latitude, longitude), latitudeDeltaInMeters, longitudeDeltaInMeters);
    [self.mapView setRegion:newRegion animated:YES];
    
}
#pragma mark Mapview Delegate Functions

- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[CalloutMapAnnotation class]]) {
        
        CallOutAnnotationVifew *annotationView = (CallOutAnnotationVifew *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CalloutView"];
        if (!annotationView) {
            annotationView = [[CallOutAnnotationVifew alloc] initWithAnnotation:annotation reuseIdentifier:@"CalloutView"];
            [annotationView.contentView addTarget:self action:@selector(gotoDetail) forControlEvents:UIControlEventTouchUpInside];
        }
        [annotationView.contentView setTitle:((CalloutMapAnnotation *)annotation).title2 forState:UIControlStateNormal];
        return annotationView;
	} else if ([annotation isKindOfClass:[BasicMapAnnotation class]]) {
        
        MKAnnotationView *annotationView =[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomAnnotation"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:@"CustomAnnotation"];
            annotationView.canShowCallout = NO;
            annotationView.opaque = NO;
            annotationView.leftCalloutAccessoryView = nil;
            annotationView.userInteractionEnabled = YES;
            UIImage *image = [UIImage imageNamed:@"pos_icon.png"];
            annotationView.image = image;
        }
		
		return annotationView;
    }
	return nil;
    
}

- (void)showCallout {
    [self.mapView selectAnnotation:myAnnotation.annotation animated:YES];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view.annotation isKindOfClass:[BasicMapAnnotation class]]) {
        if (_calloutAnnotation.coordinate.latitude == view.annotation.coordinate.latitude&&
            _calloutAnnotation.coordinate.longitude == view.annotation.coordinate.longitude) {
            return;
        }
        if (_calloutAnnotation) {
            [mapView removeAnnotation:_calloutAnnotation];
            _calloutAnnotation = nil;
        }
        BasicMapAnnotation *baseAnnotation = (BasicMapAnnotation *)view.annotation;
        _calloutAnnotation = [[CalloutMapAnnotation alloc]
                              initWithLatitude:view.annotation.coordinate.latitude
                              andLongitude:view.annotation.coordinate.longitude andTitle:baseAnnotation.title2];
        nowData = baseAnnotation.storeData;
        [mapView addAnnotation:_calloutAnnotation];
        [mapView setCenterCoordinate:_calloutAnnotation.coordinate animated:YES];
	}else{
        
    }
    
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if (_calloutAnnotation&& ![view isKindOfClass:[CallOutAnnotationVifew class]]) {
        if (_calloutAnnotation.coordinate.latitude == view.annotation.coordinate.latitude&&
            _calloutAnnotation.coordinate.longitude == view.annotation.coordinate.longitude) {
            [mapView removeAnnotation:_calloutAnnotation];
            _calloutAnnotation = nil;
        }
    }
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    // 整个方法用 gotoDetail 代替了
//    MapViewAnnotation *annotation = (MapViewAnnotation *)view.annotation;
//    StoreDetailController *sdCtrl = [[StoreDetailController alloc]initWithNibName:@"StoreDetailController" bundle:nil];
//    sdCtrl.dataSource = annotation.storeData;
//    [_contentNaviCtrl pushViewController:sdCtrl animated:NO];
//    
//    if (!detailIsShown) {
//        CGRect mapViewFrame = CGRectMake(_mapView.frame.origin.x, _mapView.frame.origin.y, 368, _mapView.frame.size.height);
//        CGRect listViewFrame = CGRectMake(368, _listViewContainer.frame.origin.y, _listViewContainer.frame.size.width, _listViewContainer.frame.size.height);
//        CGRect expandBtnFrame = CGRectMake(368+DETAILW, _contentNaviCtrl.view.frame.origin.y, 16, _contentNaviCtrl.view.frame.size.height);
//        CGRect naviViewFrame = CGRectMake(368+16+DETAILW, _contentNaviCtrl.view.frame.origin.y, DETAILW, _contentNaviCtrl.view.frame.size.height);
//        
//        
//        [UIView animateWithDuration:0.3
//                              delay:0
//                            options:UIViewAnimationOptionCurveEaseInOut
//                         animations:^{
//                             _mapView.frame = mapViewFrame;
//                             _listViewContainer.frame = listViewFrame;
//                             _expandMapBtn.frame = expandBtnFrame;
//                             self.contentNaviCtrl.view.frame = naviViewFrame;
//                         }
//                         completion:nil
//         ];
//    }
}

-(void)gotoDetail {
    StoreDetailController *sdCtrl = [[StoreDetailController alloc]initWithNibName:@"StoreDetailController" bundle:nil];
    sdCtrl.dataSource = nowData;
    [_contentNaviCtrl pushViewController:sdCtrl animated:NO];
    
    if (!detailIsShown) {
        CGRect mapViewFrame = CGRectMake(_mapView.frame.origin.x, _mapView.frame.origin.y, 368, _mapView.frame.size.height);
        CGRect listViewFrame = CGRectMake(368, _listViewContainer.frame.origin.y, _listViewContainer.frame.size.width, _listViewContainer.frame.size.height);
        CGRect expandBtnFrame = CGRectMake(368+DETAILW, _contentNaviCtrl.view.frame.origin.y, 16, _contentNaviCtrl.view.frame.size.height);
        CGRect naviViewFrame = CGRectMake(368+16+DETAILW, _contentNaviCtrl.view.frame.origin.y, DETAILW, _contentNaviCtrl.view.frame.size.height);
        
        
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             _mapView.frame = mapViewFrame;
                             _listViewContainer.frame = listViewFrame;
                             _expandMapBtn.frame = expandBtnFrame;
                             self.contentNaviCtrl.view.frame = naviViewFrame;
                         }
                         completion:nil
         ];
    }
}

- (void) mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated
{
    //NSLog(@"region changed");
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	
    NSString *lat=[[NSString alloc] initWithFormat:@"%f",userLocation.coordinate.latitude];
    NSString *lng=[[NSString alloc] initWithFormat:@"%f",userLocation.coordinate.longitude];
    [MapLocation shareInstance].latitude = userLocation.coordinate.latitude;
    [MapLocation shareInstance].longitude = userLocation.coordinate.longitude;
    _userLatitude=lat;
    _userlongitude=lng;
    NSLog(@" lat    %f",userLocation.coordinate.latitude);
    NSLog(@" long   %f",userLocation.coordinate.longitude);
    
#define FIX_MAP_CRASH_WHEN_NETWORK_DOWN 1
#if FIX_MAP_CRASH_WHEN_NETWORK_DOWN
	if (userLocation.coordinate.latitude >= 90 || userLocation.coordinate.latitude <= -90 ||
		userLocation.coordinate.longitude >= 180 || userLocation.coordinate.longitude <= -180 ||
        userLocation.coordinate.latitude == 0 || userLocation.coordinate.longitude == 0)
    {
        return;
	}
	
#endif
    
    float zoomLeavel = 0.1;
    MKCoordinateRegion region = MKCoordinateRegionMake([userLocation coordinate], MKCoordinateSpanMake(zoomLeavel, zoomLeavel));
    [_mapView setRegion:region];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postLocationUpdate" object:nil];
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    //NSLog(@"map view did finish load!");
}
- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error{
    
}

-(void)moveToMapCenter{
    CLLocationCoordinate2D userLocation = CLLocationCoordinate2DMake([MapLocation shareInstance].latitude, [MapLocation shareInstance].longitude);
    float zoomLeavel = 0.1;
    MKCoordinateRegion region = MKCoordinateRegionMake(userLocation, MKCoordinateSpanMake(zoomLeavel, zoomLeavel));
    [_mapView setRegion:region];
}

#pragma mark - data
-(void)createModel{
    [[ZFStewardManager sharedManager] getStoreWithPage:@"" andPageSize:@"" Success:^(NSMutableArray *data) {
        [[DBManager sharedManager] storeStoreData:data Sucess:^(NSMutableArray *storeData) {
            [self sortArray];
            _storeDataArray = storeData;
            if (!self.mapView.hidden) {
                [self drawAnnotation];
                [self sortArray];
            }
        } fail:^(NSError *error) {
            NSLog(@"error=%@",error.description);
        }];
    } failure:^(NSError *error) {
        NSString *message = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"error" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        NSLog(@"error");
    }];
    
}

-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
    [self.view bringSubviewToFront:self.customNavigaitionBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
