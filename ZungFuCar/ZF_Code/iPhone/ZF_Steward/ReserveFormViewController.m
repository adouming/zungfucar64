//
//  ReserveFormViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/4/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "ReserveFormViewController.h"
#import "BeMemberViewController.h"
#import "noteViewController.h"

@interface ReserveFormViewController ()

@end

@implementation ReserveFormViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    
    [[DataDBModel shareInstance] createDataModel];
    
}
- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    //auto resize
    if (IOS7) {
        self.view.frame = CGRectMake(0, 20, SCREEN_WIDTH, SCREEN_HEIGHT);
    }else{
        self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    [self getIndustryJobData];
    [self updateCode:self.CodeBtn];
    _userInfo = [[DBManager sharedManager] getUserData];
    if (_userInfo == Nil) {
        _userID = 0;
    }
    if(_userInfo.userID != 0)
    {
        _userID = _userInfo.userID;
        self.nameTextField.text = _userInfo.nickName;
        
        if ([_userInfo.gender isEqualToString:@"0"]) {
            _manBtn.selected = YES;
            _manBtn.userInteractionEnabled = !_manBtn.selected;
            _womanBtn.selected = !_manBtn.selected;
            _womanBtn.userInteractionEnabled = !_manBtn.userInteractionEnabled;
        }
        if ([_userInfo.gender isEqualToString:@"1"]) {
            _womanBtn.selected = YES;
            _womanBtn.userInteractionEnabled = !_womanBtn.selected;
            _manBtn.selected = !_womanBtn.selected;
            _manBtn.userInteractionEnabled = !_womanBtn.userInteractionEnabled;
        }
        //获取登陆用户所属行业
        int industryCount = [[[DBManager sharedManager] getIndustryData] count];
        for (int i=0; i<industryCount; i++) {
            ZFNetServerIndustryData *industryData = [[[DBManager sharedManager] getIndustryData] objectAtIndex:i];
            if ([industryData.value integerValue] == _userInfo.otherJob) {
                [self.industryBtn setTitle:industryData.displayName forState:UIControlStateNormal];
                industryName = industryData.value;
                break;
            }
        }
        
        //获取登陆用户所从事职业
        int jobCount = [[[DBManager sharedManager] getJobData] count];
        for (int i=0; i<jobCount; i++) {
            ZFNetServerJobData *jobData = [[[DBManager sharedManager] getJobData] objectAtIndex:i];
            if ([jobData.value integerValue] == _userInfo.job) {
                [self.jobBtn setTitle:jobData.displayName forState:UIControlStateNormal];
                jobName = jobData.value;
                break;
            }
        }
        
        //城市、展厅初始化
        //ZFStewardClient::getCityExhibitionClientSucess
        
        self.emailTextField.text = ((NSNull *)_userInfo.email == [NSNull null]) ? @"" : _userInfo.email;
        self.telTextField.text = ((NSNull *)_userInfo.homePhone == [NSNull null]) ? @"" : _userInfo.homePhone;
    }
    
    self.nameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.telTextField.delegate = self;
    self.codeTF.delegate = self;
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    [self.contentView setContentSize:CGSizeMake(320, 890)];
    // Do any additional setup after loading the view.
}

#pragma mark - 获取行业、职业数据
-(void)getIndustryJobData{
    //    @"http://54.250.184.46/zf-web-ios/index.php/api/Booking/fn/getInfo"
    
    NSMutableArray *dataSourceIndustry = [@[
                                        @"地产/建筑/装潢",
                                        @"金融/银行/投资/保险",
                                        @"生产/工艺/制造",
                                        @"会计",
                                        @"法律/法务",
                                        @"商业/服务业/个体",
                                        @"进出口/贸易",
                                        @"旅游",
                                        @"酒店/餐饮/娱乐",
                                        @"快递/物流",
                                        @"文化/广告/传媒",
                                        @"公务员/事业单位",
                                        @"计算机/网络/通讯",
                                        @"矿产/能源",
                                        @"教育/培训",
                                        @"娱乐/表演/艺术",
                                        @"汽车销售/汽配",
                                        @"体育/健身/运动",
                                        @"医疗/护理/制药",
                                        @"其他"
                                        ] mutableCopy];
    NSMutableArray* dataSourceJob = [@[
                                        @"私营企业主",
                                        @"工商企业主",
                                        @"企业高管",
                                        @"自由职业者",
                                        @"白领",
                                        @"其他"
                                        ] mutableCopy];

            if ([dataSourceIndustry count]>0) {
                [[DBManager sharedManager] storeIndustryData:dataSourceIndustry Sucess:nil fail:nil];
            }
    
            if ([dataSourceJob count]>0) {
                [[DBManager sharedManager]storeJobData:dataSourceJob Sucess:nil fail:nil];
            }
       

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)navigationBarWillAppear{
    self.customNavigaitionBar.rightBtn.hidden = YES;
}

- (void)viewDidUnload {
    [self setContentView:nil];
    [self setBannerImageView:nil];
    [self setTitleLabel:nil];
    [self setCarSeriesBtn:nil];
    [self setCarStyleBtn:nil];
    [self setCityBtn:nil];
    [self setExhibitBtn:nil];
    [self setDateBtn:nil];
    [self setNameTextField:nil];
    [self setIndustryBtn:nil];
    [self setJobBtn:nil];
    [self setManBtn:nil];
    [self setWomanBtn:nil];
    [self setEmailTextField:nil];
    [self setTelTextField:nil];
    [self setTimeBtn:nil];
    [self setOtherCarStyle:nil];
    [super viewDidUnload];
}
#pragma mark - pickerView
-(void)pickerViewDidSelectRow:(NSInteger)row withStyle:(pickerViewType)style andData:(id)data{
    switch (style) {
        case pickerViewTypeCarSeries:{
            int count = [[[DBManager sharedManager] getCarListData: ZF_ALL_car_type_list] count];
            NSString *name = [data valueForKey:@"name"];
            //            if (row<=count) {
            //                name = [NSString stringWithFormat:@"  奔驰乘用车%@",[data valueForKey:@"Name"]];
            //            }else{
            //                name = [NSString stringWithFormat:@"  AMG系列%@",[data valueForKey:@"Name"]];
            //            }
            [self.carSeriesBtn setTitle:name forState:UIControlStateNormal];
            [self.carStyleBtn setTitle:@"  " forState:UIControlStateNormal];
            carTypeID = [data objectForKey:@"cid"];
            self.carTypeData = [[DBManager sharedManager] getCarListData: ZF_ALL_car_model_list];//(NSDictionary *)data;
            
            
            //选择了车系，获取车型
            selectedTypeId = [carTypeID integerValue];
            [[DataDBModel shareInstance] createCarModelModel:selectedTypeId];
            
            break;
        }
            
        case pickerViewTypeCarType:{
            [self.carStyleBtn setTitle:[NSString stringWithFormat:@"  %@",[data valueForKey:@"name"]] forState:UIControlStateNormal];
            carModelID = [data objectForKey:@"carid"];
             break;
        }
           
        case pickerViewTypeCity:{
            NSDictionary *stData = (ZFNetServerCityExhibitionData *)data;
            [self.cityBtn setTitle:[stData objectForKey:@"name"] forState:UIControlStateNormal];
            cityID = [stData objectForKey:@"cityid"];
            self.cityTypeData = stData;
            
            //选择了城市,获取展厅
            int cid = [cityID integerValue];
            [[DataDBModel shareInstance] createExhibitionModel:cid type:self.reserveType];
           
            break;
        }
            
            
            
        case pickerViewTypeExhibit:{
            NSDictionary *stData = (ZFNetServerExhibitionData *)data;
            NSString* title = [stData objectForKey:@"storename"];
            storeID = [stData objectForKey:@"storeid"];
            [self.exhibitBtn setTitle:title forState:UIControlStateNormal];
            ExhibitName = title;
              break;
        }
            
          
        case pickerViewTypeIndustryReserve:{
            ZFNetServerIndustryData *stData = (ZFNetServerIndustryData *)data;
            [self.industryBtn setTitle:stData forState:UIControlStateNormal];
            industryName = stData;
            break;
        }
            
            
        case pickerViewTypeJobReserve:{
            ZFNetServerJobData *stData = (ZFNetServerJobData *)data;
            [self.jobBtn setTitle:stData forState:UIControlStateNormal];
            jobName = stData;
            break;
        }
            
            
        case pickerViewTypeDate: {
            
            //NSDate * data = [self.dataSource objectAtIndex:row];
            
            //实例化一个NSDateFormatter对象
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            
            //设定时间格式,这里可以设置成自己需要的格式
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            
            //用[NSDate date]可以获取系统当前时间
            
            NSString *currentDateStr = [dateFormatter stringFromDate:data];
            
            //输出格式为：2010-10-27 10:22:13
           

            
            [self.dateBtn setTitle:currentDateStr forState:UIControlStateNormal];
            break;
        }
        case pickerViewTypeServerTime:{
            ZFNetServerServiceTimeData *stData = (ZFNetServerServiceTimeData *)data;
            [self.timeBtn setTitle:stData forState:UIControlStateNormal];
            break;
        }

        case pickerViewTypeTestTime:{
            ZFNetServerTestTimeData *stData = (ZFNetServerTestTimeData *)data;
            [self.timeBtn setTitle:stData forState:UIControlStateNormal];
            
            break;
        }

        default:
            break;
    }
    
}
#pragma mark - action
- (IBAction)showPickerView:(id)sender {
    
    [self.nameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.telTextField resignFirstResponder];
    [self.codeTF resignFirstResponder];
    
//    pickerView.hidden = NO;
    
    //CGPoint position = CGPointMake(0, ((UIButton *)sender).frame.origin.y-100);
    //[self.contentView setContentOffset:position animated:YES];
    
    //self.contentView.scrollEnabled = NO;
    
    
    if (pickerView) {
        pickerView.hidden = !pickerView.hidden;
    }
    
    if (pickerView == nil) {
        pickerView = [[customPickerView alloc] initWithTitle:@"" delegate:self];
        pickerView.pickerDelegate = self;
    }
    
    if ([sender isEqual:_carSeriesBtn]) {
        pickerView.pickerStyle = pickerViewTypeCarSeries;
    }
    if ([sender isEqual:_carStyleBtn]) {
        pickerView.carTypeData = self.carTypeData;
        pickerView.selectedCarType = selectedTypeId;
        pickerView.pickerStyle = pickerViewTypeCarType;
        
    }
    if ([sender isEqual:_cityBtn]) {
        
        pickerView.pickerStyle = pickerViewTypeCity;
    }
    if ([sender isEqual:_exhibitBtn]) {
        pickerView.cityData = self.cityTypeData;
        pickerView.pickerStyle = pickerViewTypeExhibit;
    }
    if ([sender isEqual:_dateBtn]) {
//        CGRect rect = pickerView.frame;
//        //rect.origin.y = 44;
//        //rect.size.height = -44;
//        if (!datepick ){
//            datepick = [[UIDatePicker alloc] initWithFrame:rect];
//            datepick.datePickerMode = UIDatePickerModeDate;
//            datepick.minuteInterval = 1;
//            
//            //一年的秒数
//            float oneYearTime = 60*60*24*365;
//            [super viewDidLoad];
//            // Do any additional setup after loading the view, typically from a nib.
//            self.view.backgroundColor = [UIColor whiteColor];
//            //获得当前时间
//            NSDate *currentDate = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
//            NSDate *minDate = [currentDate dateByAddingTimeInterval:0];
//            NSDate *maxDate = [currentDate dateByAddingTimeInterval:100*oneYearTime];
//            //最小可选时间
//            datepick.minimumDate = minDate;
//            datepick.maximumDate = maxDate;
//            [datepick setDate:minDate animated:YES];
//            [datepick addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
//            
//        }
//        
//        datepick.frame = rect;
//        datepick.hidden = false;
//        
//        [pickerView addSubview:datepick];
        
        pickerView.pickerStyle = pickerViewTypeDate;
    } else {
//        if (datepick) {
//            [datepick removeFromSuperview];
//        }
    }
    if ([sender isEqual:_timeBtn]) {
        if (_reserveType == ReserveTypeTest) {
            pickerView.pickerStyle = pickerViewTypeServerTime;
        }else if (_reserveType == ReserveTypeService){
            pickerView.pickerStyle = pickerViewTypeTestTime;
        }
        
        
    }
    if ([sender isEqual:_industryBtn]) {
        pickerView.pickerStyle = pickerViewTypeIndustryReserve;
    }
    if ([sender isEqual:_jobBtn]) {
        pickerView.pickerStyle = pickerViewTypeJobReserve;
    }
    
    [pickerView showInView:self.view];
}

-(void)dateChanged:(id)sender{
    UIDatePicker* control = (UIDatePicker*)sender;
    NSDate* _date = control.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSDate *minDate = [dateFormatter dateFromString:@"2013-01-01"];
    control.minimumDate = minDate;
    NSString *destDateString = [dateFormatter stringFromDate:_date];
    [self.dateBtn setTitle:destDateString forState:UIControlStateNormal];
}

#pragma mark - action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    CGRect rect = self.contentView.frame;
    rect.origin.y = 0;
    self.contentView.frame = rect;
    [UIView commitAnimations];
    //self.contentView.scrollEnabled = YES;
    
    if ([actionSheet isKindOfClass:[customPickerView class]]){
        
        if(buttonIndex == 0) {
            [pickerView removeFromSuperview];
            pickerView = nil;
            
        }
//            else {
//            if (pickerView.pickerStyle == pickerViewTypeDate) {
//                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//                NSString *strDate = [dateFormatter stringFromDate:datepick.date];
//                [self.dateBtn setTitle:strDate forState:UIControlStateNormal];
//                [datepick  removeFromSuperview];
//            }
//            
//        }
//        [pickerView removeFromSuperview];
//        pickerView = nil;
    }
    
}

- (IBAction)appointAction:(id)sender {
    
    [self.nameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.telTextField resignFirstResponder];
    if ([self infoIntegrityCheck]) {
        
        [self sendBookInfo];
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"表单填写有误，请核对后再次发送" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (BOOL)infoIntegrityCheck
{
    
    if ([self.carSeriesBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needSeries.hidden = NO;
    } else {
        _needSeries.hidden = YES;
    }
    
    if ([self.carStyleBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needStyle.hidden = NO;
    } else {
        _needStyle.hidden = YES;
    }
    
    if ([self.cityBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needCity.hidden = NO;
    } else {
        _needCity.hidden = YES;
    }
    
    if ([self.exhibitBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needStore.hidden = NO;
    } else {
        _needStore.hidden = YES;
    }
    
    if ([self.dateBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needDate.hidden = NO;
    } else {
        _needDate.hidden = YES;
    }
    
    if ([self.timeBtn.titleLabel.text isEqualToString:@"选择"]) {
        _needTime.hidden = NO;
    } else {
        _needTime.hidden = YES;
    }
    
    if ([self.nameTextField.text length] == 0) {
        _needNick.hidden = NO;
    } else {
        _needNick.hidden = YES;
    }
    
    if (self.manBtn.isSelected || self.womanBtn.isSelected) {
        _needGender.hidden = YES;
    } else {
        _needGender.hidden = NO;
    }
    
    if ([self isValidateEmail:self.emailTextField.text]) {
        _needEmail.hidden = YES;
    } else {
        if ([self.emailTextField.text length] == 0) {
            _needEmail.text = @"必填";
        } else {
            _needEmail.text = @"有误";
        }
        _needEmail.hidden = NO;
    }
    
    if ([self validateNumeric:self.telTextField.text]) {
        _needPhone.hidden = YES;
    } else {
        if ([self.telTextField.text length] == 0) {
            _needPhone.text = @"必填";
        } else {
            _needPhone.text = @"有误";
        }
        _needPhone.hidden = NO;
    }
    
    if (self.agreeBtn.isSelected) {
        _needAgree.hidden = YES;
    } else {
        _needAgree.hidden = NO;
    }
    
    if ([self.codeTF.text isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"Code"]]) {
        _needCode.hidden = YES;
    } else {
        if ([self.codeTF.text length] == 0) {
            _needCode.text = @"必填";
        } else {
            _needCode.text = @"有误";
        }
        _needCode.hidden = NO;
    }
    
    return (_needSeries.hidden   && _needCity.hidden && _needStore.hidden && _needDate.hidden && _needTime.hidden && _needNick.hidden && _needGender.hidden && _needEmail.hidden && _needPhone.hidden && _needAgree.hidden && _needCode.hidden);
}

- (BOOL)isValidateEmail:(NSString *)email
{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}

- (BOOL)validateNumeric:(NSString *)str2validate
{
    NSString *phoneRegex = @"[0-9]{1,11}";
    
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",phoneRegex];
    
    return [phoneTest evaluateWithObject:str2validate];
}

- (void)sendBookInfo
{
    
    [[NSUserDefaults standardUserDefaults] setObject:self.emailTextField.text forKey:@"isBecomeEmail"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSString* services = nil;
    if(self.reserverServerType == ReserveTypeServiceTypeConst) //定期保养
    {
        services = @1;
    }
    else if(self.reserverServerType == ReserveTypeServiceTypeMend)
    {
        services = @2;
    }
    else {
        services = @"";
    }
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:
                         @([carTypeID integerValue]),       @"cid", //车系
                         @([carModelID integerValue]),      @"carid",//车型
                         @([cityID integerValue]),          @"cityid",
                         storeID,                           @"storeid",
                         self.dateBtn.titleLabel.text,      @"datetime",
                         self.timeBtn.titleLabel.text,      @"scheduletime",
                         services,                          @"services[]",//定期保养值为：1，车辆维修值为：2
                         self.nameTextField.text,           @"nikename",
                         self.manBtn.selected?@"0":@"1",    @"gender",
                         @([industryName integerValue]),    @"industry",
                         jobName,                           @"job",
                         self.emailTextField.text,          @"email",
                         self.telTextField.text,            @"tel",
                         @(1),                              @"BecomeMember",
                         nil];
    
    /**************************************/
    ////////////////////////////////
    // 1
    //
    //
    [self showLoadingText];
    
    //    @"http://54.250.184.46/zf-web-ios/index.php/api/Booking/fn/book"
    AFHTTPRequestOperationManager *reserveManager = [AFHTTPRequestOperationManager manager];
    [reserveManager.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"HTTP-X-U"];
    [reserveManager.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"HTTP-X-P"];
    reserveManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSString* url = [NSString stringWithFormat:@"%@/%@/%@", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_SAVE_BOOKING_WX];
    
    if(self.reserveType == ReserveTypeTest)  {
        url = [NSString stringWithFormat:@"%@/%@/%@", ZF_BASE_URL,ZF_BASE_PATH, ZF_PATH_SAVE_BOOKING_SC];
    }

    [reserveManager POST:url parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self hideLoadingText];
//        if (_userInfo.userID == 0) {
//            
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Steward" bundle:nil];
//            BeMemberViewController *controler = [storyboard instantiateViewControllerWithIdentifier:@"BeMemberID"];
//            [self.navigationController pushViewController:controler animated:YES];
//            
//        } else {
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"预约成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            successAlert.tag = 88;
            [successAlert show];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error with %@", error);
        NSLog(@"%@", [operation responseString] );
        [self hideLoadingText];
        UIAlertView *failAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"信息发送失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [failAlert show];
    }];
    
}

- (IBAction)genderAction:(UIButton *)sender {
    if ([sender isEqual:_manBtn]) {
        _manBtn.selected = YES;
        _manBtn.userInteractionEnabled = !_manBtn.selected;
        _womanBtn.selected = !_manBtn.selected;
        _womanBtn.userInteractionEnabled = !_manBtn.userInteractionEnabled;
    }
    if ([sender isEqual:_womanBtn]) {
        _womanBtn.selected = YES;
        _womanBtn.userInteractionEnabled = !_womanBtn.selected;
        _manBtn.selected = !_womanBtn.selected;
        _manBtn.userInteractionEnabled = !_womanBtn.userInteractionEnabled;
    }
    
    
}

- (IBAction)agreeIconAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    _isAgreeRule = sender.selected;
}
- (IBAction)agreeRuleAction:(id)sender {
    UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Club" bundle:nil];
    noteViewController *noteCtl = [stryBoard instantiateViewControllerWithIdentifier:@"noteVCID"];
    [self.navigationController pushViewController:noteCtl animated:YES];
}

- (IBAction)updateCode:(id)sender {
    //获取一个随机整数，范围在[from,to），包括from，不包括to
    NSString *codeNum = [NSString stringWithFormat:@"%d", (int)(1000 + (arc4random() % (9999 - 1000 + 1)))];
    [[NSUserDefaults standardUserDefaults] setObject:codeNum forKey:@"Code"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.CodeLab.text = codeNum;
}

#pragma mark - alert delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 88) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}



#pragma mark - textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    pickerView.hidden = YES;
    //CGPoint position = CGPointMake(0, textField.frame.origin.y-150);
    //[self.contentView setContentOffset:position animated:YES];
    //self.contentView.scrollEnabled = NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    [UIView beginAnimations:@"text" context:nil];
    [UIView setAnimationDuration:0.5];
    self.contentView.contentInset = UIEdgeInsetsZero;
    [UIView commitAnimations];
    //self.contentView.scrollEnabled = YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.otherCarStyle resignFirstResponder];
    [self.nameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.telTextField resignFirstResponder];
    [self.codeTF resignFirstResponder];
    pickerView.hidden = YES;
    //self.contentView.scrollEnabled = YES;
}
@end
