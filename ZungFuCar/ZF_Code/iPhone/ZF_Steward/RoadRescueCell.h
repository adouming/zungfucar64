//
//  RoadRescueCell.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/5/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlexMacro.h"

@interface RoadRescueCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *telLabel;
-(void)setData:(NSString *)title subtitle:(NSString *)subtitle;
@end
