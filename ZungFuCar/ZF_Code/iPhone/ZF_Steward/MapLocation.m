//
//  MapLocation.m
//  ZungFuCar
//
//  Created by Alex Peng on 11/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "MapLocation.h"
#import "DBManager.h"
#import "ZFStewardData.h"
#define PI                      3.141592613
#define EARTH_RADIUS            6378.137




@implementation MapLocation
static MapLocation *share_Location = nil;
+ (MapLocation *)shareInstance{
    static dispatch_once_t pred;
	dispatch_once(&pred, ^{
        share_Location = [[self alloc] init];
    });
	return share_Location;
    
}
-(id)init{
    self = [super init];
    if (self) {
        _latitude = 0.0;
        _longitude = 0.0;
    }
    return self;
}
-(double) radian:(double)d{
    return d*PI / 180.0;
}
-(double)getDistanceFromLocationLatitude:(NSString *)latitude andLongitude:(NSString *)longitude{
    if (latitude == nil || longitude == nil || !latitude.length || !longitude.length) {
        return -1;
    }
    double localLatitude = [self radian:self.latitude];
    double localLongitude = [self radian:self.longitude];
    double desLatitude = [self radian:[latitude doubleValue]];
    double desLogitude = [self radian:[longitude doubleValue]];
    double a = localLatitude - desLatitude;
    double b = localLongitude - desLogitude;
    double dst = 2 * asin((sqrt(pow(sin(a / 2), 2) + cos(localLatitude) * cos(desLatitude) * pow(sin(b / 2), 2) )));
    dst = dst * EARTH_RADIUS;
    dst= round(dst * 10000) / 10000;
    return ABS(dst);
}
+ (NSString *)latestStoreID{
    NSString *latestID = @"";
    NSString *latitude = @"0";
    NSString *longtitude = @"0";
    double minDistance = MAXFLOAT;
    NSMutableArray *dataArray = [[DBManager sharedManager] getStoreData];
    NSMutableArray *temArray = [NSMutableArray array];
    for (ZFNetServerStoreData *store in dataArray) {
        double distance = [[MapLocation shareInstance] getDistanceFromLocationLatitude:store.latitude andLongitude:store.longtitude];
       NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",distance],
                            @"distance",[NSString stringWithFormat:@"%d",store.storeID],@"storeID",store.latitude,@"latitude",store.longtitude,@"longtitude",nil];
        [temArray addObject:dic];
    }
    for (NSDictionary *dic in temArray) {
        double distance = [[dic objectForKey:@"distance"] doubleValue];
        if (minDistance>distance) {
            minDistance = distance;
            continue;
        }
        latestID = [dic objectForKey:@"storeID"];
        latitude = [dic objectForKey:@"latitude"];
        longtitude = [dic objectForKey:@"longtitude"];
    }
    [MapLocation shareInstance].nearLatitude = latitude;
    [MapLocation shareInstance].nearLongitude = longtitude;
    return latestID;
}
@end
