//
//  NetServerListCell.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFStewardData.h"
#import "NetServerListCellView.h"
#import "AlexMacro.h"
@interface NetServerListCell : UITableViewCell{
    NSInteger nearestStoreID;
    
}

@property (nonatomic,strong) NetServerListCellView *cellView;
-(void)setData:(ZFNetServerStoreData *)data andHeight:(NSNumber *)height row:(int)row;
+ (CGFloat)CellData:(NSArray *)cellData heightForRowAtIndexPath:(NSIndexPath *)indexPath;
@end
