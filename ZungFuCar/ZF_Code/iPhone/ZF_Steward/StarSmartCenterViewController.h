//
//  StarSmartCenterViewController.h
//  ZungFuCar
//
//  Created by adouming on 15/8/17.
//  Copyright (c) 2015年 Alex. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
#import "HorizontalScrollView.h"
#import "SelectTableViewCell.h"

@interface StarSmartCenterViewController : MenuAbstractViewController<HorizontalScrollViewDelegate,UITableViewDataSource, UITableViewDelegate,UITableViewDataSource>
{
    UIImageView *_bgImageView;
    NSMutableArray *dataSource;
    UITableView *_tableView;
    HorizontalScrollView *hScrollView;
    
}
-(id)initWithType:(int)type;
@property int carType;
@property NSArray* carArray;
@property NSDictionary* carMap;
@property NSString* storekey;
//@property (nonatomic, retain) UITableView *selctTableView;
@end