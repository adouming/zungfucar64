//
//  MapViewAnnotation.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "MapViewAnnotation.h"

@implementation MapViewAnnotation


- (id) initWithTitle:(NSString *)aTitle andSubTitle:(NSString *)aSubTitle andLatitude:(double)latitude andLongitude:(double)longitude
{
    
    self = [super init];
    if (self) {
        CLLocationCoordinate2D location;
        location.latitude = latitude;
        location.longitude = longitude;
        self.pinTitle = aTitle;
        self.pinSubTitle = aSubTitle;
        _coordinate = location;
    }

    return self;
}
- (NSString *)title
{
    return self.pinTitle;
}

// optional
- (NSString *)subtitle
{
    return self.pinSubTitle;
}

@end
