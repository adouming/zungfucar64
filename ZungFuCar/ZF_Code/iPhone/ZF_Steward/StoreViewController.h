//
//  StoreViewController.h
//  ZungFuCar
//
//  Created by Ed Lee on 12/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

// Subview
#import "MenuAbstractViewController.h"
#import "StoreListCell.h"
#import "BasicMapAnnotation.h"
#import "CallOutAnnotationVifew.h"
#import "CalloutMapAnnotation.h"

// Category:
#import "NSStringUtil.h"
#import "MapViewAnnotation.h"
#import "MapLocation.h"
#import "NSStringUtil.h"
#import "DBManager.h"

// VC
#import "StoreDetailController.h"

@interface StoreViewController : MenuAbstractViewController <MKMapViewDelegate, CLLocationManagerDelegate>
{
    NSString *_userlongitude;
    NSString *_userLatitude;
    MKAnnotationView *myAnnotation;
    NSMutableArray *heightArray;
    BOOL detailIsShown;
    
    CalloutMapAnnotation *_calloutAnnotation;
	CalloutMapAnnotation *_previousdAnnotation;
    ZFNetServerStoreData *nowData;
}

@property (strong, nonatomic) IBOutlet UITableView *listView;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UIView *listViewContainer;
@property (strong, nonatomic) UINavigationController *contentNaviCtrl;
@property (nonatomic,copy) NSArray *storeDataArray;
@property (strong, nonatomic) IBOutlet UIButton *expandMapBtn;
@property (strong, nonatomic) UIButton *centerBtn;
- (IBAction)expandMap:(id)sender;
@end
