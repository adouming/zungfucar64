//
//  StewardMainVC.h
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuAbstractViewController.h"
@interface StewardMainVC : MenuAbstractViewController
- (IBAction)peijianAction:(id)sender;
- (IBAction)jinpingAction:(id)sender;
- (IBAction)carmasterShareAction:(id)sender;
@end
