//
//  NetServerMapView.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NetServerMapView.h"
#import "MapViewAnnotation.h"
#import "MapLocation.h"
#import "NetServerDetailViewController.h"
#import "NSStringUtil.h"
#import "NetServerViewController.h"

#define METERS_IN_ONE_LAT_DEGREE 110574.61088
#define METERS_IN_ONE_LONG_DEGREE 111302.61697
#define MAP_PRECISION               2000

@implementation NetServerMapView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSString *nibName;
        if (iPhone) {
            nibName = @"NetServerMapView";
        }else if (iPad){
            nibName = @"NetServerMapView_iPad";
        }
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        UIView *nibView = [nibArray objectAtIndex:0];
        [self addSubview:nibView];
        [self initMapView];
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib{
    
    [super awakeFromNib];
    
}
-(void)initMapView{
    [MapLocation latestStoreID];
    CGRect mapViewRect = self.mapView.frame;
    if (iPhone5) {
        mapViewRect.size.height += iPhone5Step;
    }
    self.mapView.frame = mapViewRect;
    
    CLLocationCoordinate2D theCoordinate;
    theCoordinate.latitude=[[MapLocation shareInstance].nearLatitude doubleValue];
    theCoordinate.longitude=[[MapLocation shareInstance].nearLongitude doubleValue];;
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(theCoordinate.latitude, theCoordinate.longitude);
    float zoomLeavel = 0.1;
    MKCoordinateRegion region = MKCoordinateRegionMake(coord, MKCoordinateSpanMake(zoomLeavel, zoomLeavel));
    
    [_mapView setMapType:MKMapTypeStandard];
    [_mapView setRegion:region];
    _mapView.delegate=self;
    _mapView.showsUserLocation=YES;
    if (IOS7) {
        _mapView.rotateEnabled = NO;
    }
    [self drawAnnotation];
    
    _mapView.userInteractionEnabled = YES;
    
    UIButton *centerButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-40, SCREEN_HEIGHT-80, 25, 25)];
    [centerButton setImage:[UIImage imageNamed:@"Current_Position_icon"] forState:UIControlStateNormal];
    [centerButton addTarget:self action:@selector(moveToMapCenter) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:centerButton];
}

-(void)drawAnnotation{
    
    NSMutableArray *annotations  = [[NSMutableArray alloc] initWithCapacity:[_storeDataArray count]];
//    MapViewAnnotation *annotation;
    BasicMapAnnotation *annotation;
    for (ZFNetServerStoreData *data in _storeDataArray)
    {
        // loop through each store object and add them as annotations!
        
        double latitude = [data.latitude doubleValue];
        double longitude = [data.longtitude doubleValue];
        NSString *name = data.name;
        NSString *subName = data.location;
        if ([NSStringUtil isBlankString:subName]) {
            subName = name;
        }
        if ([NSStringUtil isBlankString:subName]) {
            subName = @"";
        }
//        annotation = [[MapViewAnnotation alloc] initWithTitle:subName andSubTitle:@""  andLatitude:latitude    andLongitude:longitude];
//        annotation.storeData = data;
//        [annotations addObject:annotation];
        annotation=[[BasicMapAnnotation alloc] initWithLatitude:latitude andLongitude:longitude andTitle:subName];
        annotation.storeData = data;
        [annotations addObject:annotation];
        [_mapView addAnnotation:annotation];
        
    }
    // draw it out
    [self showAnnotations:annotations];
}
- (void) showAnnotations:(NSArray *)annotations
{
    // first clear all existing pins
    for(MapViewAnnotation *annotation in self.mapView.annotations)
    {
        if(![annotation isKindOfClass:[MKUserLocation class]])
        {
            [self.mapView removeAnnotation:annotation];
        }
    }
    // add
    for(MapViewAnnotation *annotation in annotations)
    {
        //NSLog(@"adding annotation %@", annotation);
        [self.mapView addAnnotation:annotation];
    }
}

- (void) closeSelectedAnnotation
{
    // close call out bubble
    for(MapViewAnnotation *annotation in self.mapView.selectedAnnotations)
    {
        [self.mapView deselectAnnotation:annotation animated:NO];
    }
}

- (void) redrawSelectedAnnotation
{
    // when you move the map, the callout bubble may go out of screen
    // to fix this, this function deselect and reselect the selected annotation
    // so that the call out bubble will be nicely presented in the map
    for(MapViewAnnotation *annotation in self.mapView.selectedAnnotations)
    {
        //NSLog(@"reselect %@", annotation.title);
        [self.mapView deselectAnnotation:annotation animated:NO];
        [self.mapView selectAnnotation:annotation animated:YES];
    }
}
- (void) centraliseOnLat:(double)latitude andLong:(double)longitude andOnTop:(BOOL)isOnTop andRadiusFromCenter:(double)radiusInMeter
{
    
	
#if FIX_MAP_CRASH_WHEN_NETWORK_DOWN
	
	if (latitude <= -90 || latitude >= 90 ||
		longitude <= -180 || longitude >= 180) {
        
        return;
    }
	
#endif
    
    double existingLongitudeDeltaInMeters = self.mapView.region.span.longitudeDelta * METERS_IN_ONE_LONG_DEGREE;
    double existingLatitudeDeltaInMeters = self.mapView.region.span.latitudeDelta * METERS_IN_ONE_LAT_DEGREE;
    double longitudeDeltaInMeters = radiusInMeter == 0 ? existingLongitudeDeltaInMeters : 2 * radiusInMeter;
    double latitudeDeltaInMeters = radiusInMeter == 0 ? existingLatitudeDeltaInMeters : 2 * radiusInMeter;
    double latitudeDeltaInDegrees = latitudeDeltaInMeters / METERS_IN_ONE_LAT_DEGREE;
    latitude = isOnTop ? latitude - 0.5 * latitudeDeltaInDegrees : latitude;
    MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(latitude, longitude), latitudeDeltaInMeters, longitudeDeltaInMeters);
    [self.mapView setRegion:newRegion animated:YES];
    
}
#pragma mark - Delegate Functions

//- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id<MKAnnotation>)annotation
//{
//    
//    // if it's the user location, just return nil.
//    if ([annotation isKindOfClass:[MKUserLocation class]])
//    {
//        return nil;
//    }
//    
//    static NSString* SFAnnotationIdentifier = @"CustomPin";
//    MKAnnotationView* pinView =
//    (MKAnnotationView *)[theMapView dequeueReusableAnnotationViewWithIdentifier:SFAnnotationIdentifier];
//    if (!pinView)
//    {
//        
//        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
//                                                                        reuseIdentifier:SFAnnotationIdentifier];
//        annotationView.canShowCallout = YES;
//        UIImage *flagImage = [UIImage imageNamed:@"pos_icon"];
//        annotationView.image = flagImage;
//        annotationView.opaque = NO;
//        annotationView.leftCalloutAccessoryView = nil;
//        annotationView.userInteractionEnabled = YES;
//        annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//        
//        myAnnotation = annotationView;
//        
//        return annotationView;
//    }
//    else
//    {
//        pinView.annotation = annotation;
//    }
//    myAnnotation = pinView;
////    [self performSelector:@selector(showCallout) withObject:self afterDelay:0.1];
//    
//    return pinView;
//    
//}

- (void)showCallout {
    [self.mapView selectAnnotation:myAnnotation.annotation animated:YES];
}

//- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
//{
//    //    MapViewAnnotation *annotation = (MapViewAnnotation *)view.annotation;
//}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    MapViewAnnotation *annotation = (MapViewAnnotation *)view.annotation;
    NetServerDetailViewController *detailVC = [[NetServerDetailViewController alloc] initWithNibName:@"NetServerDetailViewController" bundle:nil];
    detailVC.dataSource = annotation.storeData;
    [self.controller.navigationController pushViewController:detailVC animated:YES];
    // a callout is tapped
    
}

- (void) mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated
{
    //NSLog(@"region changed");
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	
    NSString *lat=[[NSString alloc] initWithFormat:@"%f",userLocation.coordinate.latitude];
    NSString *lng=[[NSString alloc] initWithFormat:@"%f",userLocation.coordinate.longitude];
    [MapLocation shareInstance].latitude = userLocation.coordinate.latitude;
    [MapLocation shareInstance].longitude = userLocation.coordinate.longitude;
    _userLatitude=lat;
    _userlongitude=lng;
    NSLog(@" lat    %f",userLocation.coordinate.latitude);
    NSLog(@" long   %f",userLocation.coordinate.longitude);
    
#define FIX_MAP_CRASH_WHEN_NETWORK_DOWN 1
#if FIX_MAP_CRASH_WHEN_NETWORK_DOWN
	if (userLocation.coordinate.latitude >= 90 || userLocation.coordinate.latitude <= -90 ||
		userLocation.coordinate.longitude >= 180 || userLocation.coordinate.longitude <= -180 ||
        userLocation.coordinate.latitude == 0 || userLocation.coordinate.longitude == 0)
    {
        return;
	}
	
#endif
    
    float zoomLeavel = 0.1;
    MKCoordinateRegion region = MKCoordinateRegionMake([userLocation coordinate], MKCoordinateSpanMake(zoomLeavel, zoomLeavel));
    [_mapView setRegion:region];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postLocationUpdate" object:nil];
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    //NSLog(@"map view did finish load!");
}
- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error{
    
}

-(void)moveToMapCenter{
    CLLocationCoordinate2D userLocation = CLLocationCoordinate2DMake([MapLocation shareInstance].latitude, [MapLocation shareInstance].longitude);
    float zoomLeavel = 0.1;
    MKCoordinateRegion region = MKCoordinateRegionMake(userLocation, MKCoordinateSpanMake(zoomLeavel, zoomLeavel));
    [_mapView setRegion:region];
}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
	if ([view.annotation isKindOfClass:[BasicMapAnnotation class]]) {
        if (_calloutAnnotation.coordinate.latitude == view.annotation.coordinate.latitude&&
            _calloutAnnotation.coordinate.longitude == view.annotation.coordinate.longitude) {
            return;
        }
        if (_calloutAnnotation) {
            [mapView removeAnnotation:_calloutAnnotation];
            _calloutAnnotation = nil;
        }
        BasicMapAnnotation *baseAnnotation = (BasicMapAnnotation *)view.annotation;
        _calloutAnnotation = [[CalloutMapAnnotation alloc]
                               initWithLatitude:view.annotation.coordinate.latitude
                               andLongitude:view.annotation.coordinate.longitude andTitle:baseAnnotation.title2];
        nowData = baseAnnotation.storeData;
        [mapView addAnnotation:_calloutAnnotation];
        [mapView setCenterCoordinate:_calloutAnnotation.coordinate animated:YES];
	}else{
        
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if (_calloutAnnotation&& ![view isKindOfClass:[CallOutAnnotationVifew class]]) {
        if (_calloutAnnotation.coordinate.latitude == view.annotation.coordinate.latitude&&
            _calloutAnnotation.coordinate.longitude == view.annotation.coordinate.longitude) {
            [mapView removeAnnotation:_calloutAnnotation];
            _calloutAnnotation = nil;
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
	if ([annotation isKindOfClass:[CalloutMapAnnotation class]]) {
        
        CallOutAnnotationVifew *annotationView = (CallOutAnnotationVifew *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CalloutView"];
        if (!annotationView) {
            annotationView = [[CallOutAnnotationVifew alloc] initWithAnnotation:annotation reuseIdentifier:@"CalloutView"];
            [annotationView.contentView addTarget:self action:@selector(gotoDetail) forControlEvents:UIControlEventTouchUpInside];
        }
        [annotationView.contentView setTitle:((CalloutMapAnnotation *)annotation).title2 forState:UIControlStateNormal];
        return annotationView;
	} else if ([annotation isKindOfClass:[BasicMapAnnotation class]]) {
        
        MKAnnotationView *annotationView =[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomAnnotation"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                           reuseIdentifier:@"CustomAnnotation"];
            annotationView.canShowCallout = NO;
            annotationView.opaque = NO;
            annotationView.leftCalloutAccessoryView = nil;
            annotationView.userInteractionEnabled = YES;
            UIImage *image = [UIImage imageNamed:@"pos_icon.png"];
            annotationView.image = image;
        }
		
		return annotationView;
    }
	return nil;
}
-(void)gotoDetail{
    NetServerDetailViewController *detailVC = [[NetServerDetailViewController alloc] initWithNibName:@"NetServerDetailViewController" bundle:nil];
    detailVC.dataSource = nowData;
    [self.controller.navigationController pushViewController:detailVC animated:YES];
}
@end




