//
//  NetServerListCellView.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/2/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NetServerListCellView.h"

@implementation NetServerListCellView

//-(id)initWithCoder:(NSCoder *)aDecoder{
//    self = [super initWithCoder:aDecoder];
//    if (self) {
//        NSString *nibName;
//        if (iPhone) {
//            nibName = @"NetServerListCellView";
//        }else if (iPad){
//            nibName = @"NetServerListCellView_iPad";
//        }
//        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
//        UIView *nibView = [nibArray objectAtIndex:0];
//        [self addSubview:nibView];
//    }
//    return self;
//}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSString *nibName;
        if (iPhone) {
            nibName = @"NetServerListCellView";
        }else if (iPad){
            nibName = @"NetServerListCellView_iPad";
        }
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        UIView *nibView = [nibArray objectAtIndex:0];
        [self addSubview:nibView];
        
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    [self setObjects];
}
-(void)reSetFrame:(CGRect)frame{
    self.frame = frame;
    CGRect bgframe = self.bgImageView.frame;
    bgframe.origin.y = frame.size.height - bgframe.size.height;
    self.bgImageView.frame = bgframe;
}
-(void)setObjects{
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
