//
//  StoreListCell.m
//  ZungFuCar
//
//  Created by Ed Lee on 12/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "StoreListCell.h"
#import "UIImageView+AFNetworking.h"
#import "MapLocation.h"
#import "UILabel+autoResize.h"

static NSMutableArray *heightArray=nil;

@implementation StoreListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setData:(ZFNetServerStoreData *)data andHeight:(NSNumber *)height row:(int)row{
    //    CGRect frame = self.bounds;
    //    frame.size.height = [height floatValue];
    //    [self.cellView reSetFrame:frame];
    //    [self.cellView.iconImageView setImageWithURL:[NSURL URLWithString:data.sImageUrl] placeholderImage:[UIImage imageNamed:@"radiobtn1.png"]];
    
    //CGRect titleRect = CGRectMake(35, 15, 170, 20);
    //CGRect noticeRect = CGRectMake(210, 18, 95, 18);
    _titleLabel.text = data.location;
    //    if (data.storeID == nearestStoreID) {
    if (row == 0) {
        _titleNoticeLabel.hidden = NO;
        _titleNoticeLabel.text = @"最近您的经销商";
        /*
         CGSize size = CGSizeMake(MAXFLOAT, 20);
         CGSize labelsize = [_titleLabel.text sizeWithFont:_titleLabel.font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
         if (labelsize.width>titleRect.size.width) {
         _titleLabel.frame = titleRect;
         }else{
         titleRect.size.width = labelsize.width;
         _titleLabel.frame = titleRect;
         noticeRect.origin.x = titleRect.origin.x +titleRect.size.width;
         _titleNoticeLabel.frame = noticeRect;
         }
         */
    }else{
        _titleNoticeLabel.hidden = YES;
        //titleRect.size.width = titleRect.size.width+noticeRect.size.width;
        //_titleLabel.frame = titleRect;
    }
    _contenLabel.text = [[data.name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    //    self.cellView.contenLabel.autoResize = YES;
    
}

+ (CGFloat)CellData:(NSArray *)cellData heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!cellData.count) {
        return 0;
    }
    CGFloat height = 20;
    ZFNetServerStoreData *data = [cellData objectAtIndex:indexPath.row];
    CGSize size = CGSizeMake(255,MAXFLOAT);
    CGSize labelsize = [data.shortDesc sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    if (labelsize.height>20) {
        height = labelsize.height;
    }
    [heightArray addObject:[NSString stringWithFormat:@"%f",height+50]];
    return height + 50;
    
    
}

@end
