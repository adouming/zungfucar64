//
//  NetServerDetailViewController.m
//  ZungFuCar
//
//  Created by Alex Peng on 10/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import "NetServerDetailViewController.h"
#import "JSON.h"
#import <BaiduMapAPI/BMapKit.h>
#import <BaiduMapAPI/BMKNavigation.h>
#import "MapLocation.h"

#define OBJECT_STEP 2

@interface NetServerDetailViewController ()

@end

@implementation NetServerDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view bringSubviewToFront:self.customNavigaitionBar];
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    CGRect frame = self.contentView.frame;
    frame.size.height = self.view.bounds.size.height;
    self.contentView.frame = frame;
    CGRect contentRect = self.contentView.frame;
    if (iPhone5) {
        contentRect.size.height += iPhone5Step;
    }
    self.contentView.contentSize = CGSizeMake(320, 600);
    self.contentView.delaysContentTouches = NO;
}
-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [self setObjects];
}
-(void)awakeFromNib{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)navigationBarWillAppear{
    
    self.customNavigaitionBar.rightBtn.hidden = YES;
}

-(void)setObjects{
    
    self.titleLabel.text = self.dataSource.name;
    self.addressValueLabel.text = self.dataSource.address;
    self.timeValueLabel.numberOfLines = 0;
    NSString *serviceString = [self.dataSource.mservice stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    self.serviceTimeText.text = serviceString;
    NSString *businessString = [self.dataSource.sroom stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    self.businessTimeText.text = businessString;
    self.netUrlValueLabel.text = self.dataSource.url;
    self.emailValueLabel.text = self.dataSource.contactEmail;
    hotTell = self.dataSource.phone;
    emergenceTell = self.dataSource.emergencyPhone;
    [self.banerView setImageWithURL:[NSURL URLWithString:self.dataSource.bImageUrl] placeholderImage:[UIImage imageNamed:@"loading"]]
    ;
    [self.serverView setImageWithURL:[NSURL URLWithString:self.dataSource.serviceURL] placeholderImage:[UIImage imageNamed:@"loading"]]
    ;
}

#pragma mark - Action
- (IBAction)serverHotLineAction:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"确定拨打？" message:hotTell delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = 100;
    [alertView show];
}

- (IBAction)hoursRescueAction:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"确定拨打？" message:emergenceTell delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = 101;
    [alertView show];
}

#pragma mark - alertView delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *tell = @"";
    if (alertView.tag==100) {
        tell = hotTell;
    }else if (alertView.tag ==101){
        tell = emergenceTell;
    }
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:{
            NSString *str = [tell stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", str]];
            [[UIApplication sharedApplication] openURL:url];
        }
            break;
        default:
            break;
    }
    
}
- (void)viewDidUnload {
    [self setContentView:nil];
    [self setBanerView:nil];
    [self setTitleLabel:nil];
    [self setAddressTitleLabel:nil];
    [self setOpenTimeTitleLabel:nil];
    [self setServerTitleLabel:nil];
    [self setNetUrlTitleLabel:nil];
    [self setEmailTitleLabel:nil];
    [self setAddressValueLabel:nil];
    [self setTimeValueLabel:nil];
    [self setServerView:nil];
    [self setNetUrlValueLabel:nil];
    [self setEmailValueLabel:nil];
    [self setButtonView:nil];
    [super viewDidUnload];
}

// Dynamic textview height:
- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
}

////调启百度地图客户端导航
//- (IBAction)nativeNavi:(id)sender
//{
//    //初始化调启导航时的参数管理类
//    NaviPara* para = [[NaviPara alloc]init];
//    //指定导航类型
//    para.naviType = NAVI_TYPE_NATIVE;
//
//    //初始化终点节点
//    BMKPlanNode* end = [[BMKPlanNode alloc]init];
//    //指定终点经纬度
//    CLLocationCoordinate2D coor2;
//    coor2.latitude = [self.dataSource.latitude doubleValue];
//    coor2.longitude = [self.dataSource.longtitude doubleValue];
//    end.pt = coor2;
//    //指定终点名称
//    end.name = self.dataSource.name;
//    //指定终点
//    para.endPoint = end;
//
//    //指定返回自定义scheme，具体定义方法请参考常见问题
//    para.appScheme = @"baidumapsdk://mapsdk.baidu.com";
//    //调启百度地图客户端导航
//    [BMKNavigation openBaiduMapNavigation:para];
//}
//调启web导航
- (IBAction)nativeNavi:(id)sender
{
    // 调启web导航
    //初始化调启导航时的参数管理类
    BMKNaviPara* para = [[BMKNaviPara alloc]init];
    //指定导航类型
    para.naviType = BMK_NAVI_TYPE_WEB;
    
    //初始化起点节点
    BMKPlanNode* start = [[BMKPlanNode alloc]init];
    //指定起点经纬度
    CLLocationCoordinate2D coor1;
    coor1.latitude = [MapLocation shareInstance].latitude;
    coor1.longitude = [MapLocation shareInstance].longitude;
    start.pt = coor1;
    //指定起点名称
    //        start.name = _webStartName.text;
    //指定起点
    para.startPoint = start;
    
    //初始化终点节点
    BMKPlanNode* end = [[BMKPlanNode alloc]init];
    CLLocationCoordinate2D coor2;
    coor2.latitude = [self.dataSource.latitude doubleValue];
    coor2.longitude = [self.dataSource.longtitude doubleValue];
    end.pt = coor2;
    para.endPoint = end;
    //指定终点名称
    //        end.name = _webEndName.text;
    //指定调启导航的app名称
    para.appName = [NSString stringWithFormat:@"%@", @"testAppName"];
    //调启web导航
    [BMKNavigation openBaiduMapNavigation:para];
}

@end




