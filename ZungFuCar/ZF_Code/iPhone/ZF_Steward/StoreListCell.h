//
//  StoreListCell.h
//  ZungFuCar
//
//  Created by Ed Lee on 12/3/13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFStewardData.h"

@interface StoreListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *contenLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleNoticeLabel;
-(void)setData:(ZFNetServerStoreData *)data andHeight:(NSNumber *)height row:(int)row;

+ (CGFloat)CellData:(NSArray *)cellData heightForRowAtIndexPath:(NSIndexPath *)indexPath;

@end
