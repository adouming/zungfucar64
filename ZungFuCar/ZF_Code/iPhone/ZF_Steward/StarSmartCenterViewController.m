//
//  StarSmartCenterViewController.m
//  ZungFuCar
//
//  Created by adouming on 15/8/13.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "StarSmartCenterViewController.h"
#import "AFJSONRequestOperation.h"
#import "UIImageView+AFNetworking.h"
#import "DBManager.h"
#import "MyUILabel.h"
#import "CarDetailVC.h"
#import "SmartCarDetailVC.h"
#import "SelectTableViewCell.h"
#define SSCTITLE @"星睿中心";

@interface StarSmartCenterViewController ()

@end


@implementation StarSmartCenterViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view bringSubviewToFront:self.customNavigaitionBar];
    }
    return self;
}

- (void) controllerPressed:(id)sender {
    UISegmentedControl *segmentControl =(UISegmentedControl *)sender;
    int selectedSegment = segmentControl.selectedSegmentIndex;
    NSLog(@"Segment %d selected\n", selectedSegment);
}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad");
    [super viewDidLoad];
    _storekey = @"StarSmartCenter";
    CGRect bgFrame;
    if (IOS7) {
        bgFrame = CGRectMake(0, 20, 320, SCREEN_HEIGHT);
    }else{
        bgFrame = CGRectMake(0, 0, 320, SCREEN_HEIGHT);
    }
    
    UIImageView *bgView = [[UIImageView alloc]initWithFrame:bgFrame];
    bgView.image = [UIImage imageNamed:@"sl_bg.png"];
    [self.view addSubview:bgView];
    
    
    self.customNavigaitionBar.itemType = NavigationItemTypeBack;
    
    dataSource = [[NSMutableArray alloc]init];
    
    UILabel *tipLabel = [[UILabel alloc]initWithFrame:CGRectMake(-2, 99, 299, 45)];
    tipLabel.text = SSCTITLE;
    dataSource = [[DBManager sharedManager] getCarMasterShareData: _storekey];
    
    
    tipLabel.backgroundColor = [UIColor clearColor];
    tipLabel.textColor = [UIColor whiteColor];
    tipLabel.font = [UIFont boldSystemFontOfSize:45.0f];
    tipLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:tipLabel];
    _tableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 162, SCREEN_WIDTH, SCREEN_HEIGHT-142)];
    //hScrollView = [[HorizontalScrollView alloc]initWithFrame:CGRectMake(0, 142, SCREEN_WIDTH, SCREEN_HEIGHT-142)];
    //hScrollView.frame = CGRectMake(0, 142, 270, 250);
    //hScrollView.delegate = self;
    _tableView.delegate = self;
    _tableView.dataSource=self;
    
//    if ([dataSource count]>0) {
//        if ([dataSource count]>0) {
//            
//            [hScrollView initScrollView:dataSource type:0];
//            
//        }
//    }
    [self.view addSubview:_tableView];
    
    
    
    
    
    self.carMap = @{@"奔驰乘用车":
                        @{@"A":@77,
                          @"B":@78,
                          @"C":@79,
                          @"CLA":@80,
                          @"CLS":@81,
                          @"E":@82,
                          @"G":@83,
                          @"GL":@84,
                          @"GLA":@85,
                          @"GLK":@86,
                          @"M":@87,
                          @"R":@88,
                          @"S":@89,
                          @"SL":@90,
                          @"SLK":@121,
                          },
                    @"AMG系列":@{
                            @"A":@123,
                            @"C":@124,
                            @"CLA":@130,
                            @"CLS":@131,
                            @"G":@132,
                            @"GL":@133,
                            @"GLA":@134,
                            @"ML":@135,
                            @"S":@136,
                            @"SL":@137
                            },
                    @"smart":@ {
                        @"A":@126,
                        @"B":@111
                    }
                    };
    
    
    
//    _selctTableView = [[UITableView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 55, 55, 55, SCREEN_HEIGHT - 70 - 50) style:UITableViewStylePlain];
//    [self.view addSubview:_selctTableView];
//    
//    [_selctTableView setDataSource:self];
//    [_selctTableView setDelegate:self];
//    [_selctTableView setBackgroundColor:[UIColor clearColor]];
//    [_selctTableView setShowsVerticalScrollIndicator:NO];
//    
//    //[_selctTableView.layer setCornerRadius:10.0f];
//    [_selctTableView.layer setMasksToBounds:YES];
//    //[_selctTableView.layer setBorderWidth:0.0f];
//    //[_selctTableView.layer setBorderColor:[UIColor clearColor].CGColor];
//    [_selctTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    
    
    NSString* title = SSCTITLE;
  
    
    [self loadData:36];
    
}
#pragma mark - UITableViewDataSource
// 返回多少组
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
// 返回每一组有多少行
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}
// 返回哪一组的哪一行显示什么内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 1.创建CELL
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    // 2.设置数据
    // 2.1取出对应行的模型
    NSDictionary *dic = dataSource[indexPath.row];
    // 2.2赋值对应的数据
    cell.textLabel.text = [dic objectForKey:@"title"];
    //cell.detailTextLabel.text = hero.intro;
    cell.imageView.image = [UIImage imageNamed:@"noticenum"];
    // 3.返回cell
    return cell;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //NSDictionary *dic = dataSource[indexPath.row];
    [self pushToDetail: indexPath.row];
}
/*
 // 当每一行的cell的高度不一致的时候就使用代理方法设置cell的高度
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (1 == indexPath.row) {
 return 180;
 }
 return 44;
 }
 */

- (void)loadData:(NSInteger)itemKey {
    NSString *urlString = nil;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    AFHTTPRequestOperationManager *request = [AFHTTPRequestOperationManager manager];
    [request.requestSerializer setValue:DEFAULTHttp_X_U forHTTPHeaderField:@"Http-X-U"];
    [request.requestSerializer setValue:DEFAULTHttp_X_P forHTTPHeaderField:@"Http-X-P"];
    request.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    urlString = [NSString stringWithFormat:@"%@%@%@%d", ZF_BASE_URL, ZF_BASE_PATH, ZF_PATH_VIEWID, itemKey];
    [request GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"App.net Global Stream: %@", responseObject);
        NSLog(@"operation %@", operation);
        [self setTableViewData:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    
}

-(void)setTableViewData:(id) data{
    if ([[data valueForKey:@"list"] count] > 0) {
        dataSource = [data valueForKey:@"list"];
        [hScrollView clearAllView];
        if ([dataSource count]>0) {
            
            [hScrollView initScrollView:dataSource type:0];
            
        }
        
        [[DBManager sharedManager] storeCarMasterShareData:dataSource Sucess:nil fail:nil key: _storekey];
        
    }
}

-(void)pushToDetail:(int )index{
    NSDictionary *dic = [dataSource objectAtIndex:index];
    NSString *type = [NSString stringWithFormat:@"%@-Class", [dic valueForKey:@"title"]];
    
    type = [dic valueForKey:@"title"];
    
    NSString *carId = [dic valueForKey:@"id"];
    NSString *bannerURL = [dic objectForKey:@"timg"];
    NSArray *carInfo = [dic objectForKey:@"content"];
    
    
    //carInfo = [dic objectForKey:@"info"];
    SmartCarDetailVC *smartCarDetailVC = [[SmartCarDetailVC alloc] initWithTitle:@"详情" bannerUrl:bannerURL date:@"" descp:carInfo address:@"" shortDesc:@"" bannerName:@""];
//    SmartCarDetailVC *smartCarDetailVC = [[SmartCarDetailVC alloc] initWithType:type rightTitle:@"相关活动" productid:carId carType:carInfo bannerURL:bannerURL carType:0];
    [self.navigationController pushViewController:smartCarDetailVC animated:YES];
    
}

#pragma mark - navigationBarDelegate
-(void)navigationBarDidPressAtIndex:(NSInteger)index{
    switch (index) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
            
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
