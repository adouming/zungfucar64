//
//  ShareView.m
//  ZungFuCar
//
//  Created by 刘 勇斌 on 13-12-7.
//  Copyright (c) 2013年 Alex. All rights reserved.
//

#import "ShareView.h"

@implementation ShareView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(id)initWithImage:(UIImage *)image1 imageUrl:(NSString *)imageUrl1 text:(NSString *)text1 title:(NSString *)title bannerName:(NSString *)bannerName1{
    self = [super init];
    image = image1;
    imageUrl = imageUrl1;
    text = text1;
    newsTitle = title;
    bannerName = bannerName1;
    [self initSinaWeiboEngine];
    return self;
}

-(void)showInView:(UIView *)view{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"分享到新浪微博", @"Email分享", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showFromRect:view.frame inView:view.superview animated: YES];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self shareToSina];
    }else if (buttonIndex == 1) {
//        [self shareToWx:WXSceneSession];
        [self shareWithEmail];
    }
//    else if(buttonIndex == 2) {
//        [self shareToWx:WXSceneTimeline];
//    }
}

- (void)shareWithEmail
{
    NSString *content = [NSString stringWithFormat:@"<p>\
                         </p>\
                         <p style=\"text-align: center\">\
                         <img alt=\"\" src=\"%@\" style=\"width: 194px; height: 129px;\" /> </p>\
                         <p>\
                         %@ </p>\
                         <p>\
                         <br />\
                         &nbsp;</p>", imageUrl, text];
    NSString *mailStr = [NSString stringWithFormat:@"mailto:?to=%@&subject=%@&body=%@",
                         [@"" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                         [newsTitle stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                         [content stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mailStr]];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendEmail" object:nil];
}

-(void)shareToSina{
    if (_shareEngine.accessToken) {
        [self showLoadingText];
        if (text.length>140) {
            text = [text substringToIndex:139];
        }
        /**********************弹出发布内容展示窗口**************************/
        NSArray *appWindows = [UIApplication sharedApplication].windows;
        UIView *bgView = [[UIView alloc] init];
        [bgView setTransform:CGAffineTransformIdentity];
        bgView.tag = 10;
        [bgView setFrame:CGRectMake(0, 0, 1024, 768)];
        [bgView setCenter:CGPointMake(768/2, 1024/2)];
        [[appWindows objectAtIndex:0] addSubview:bgView];
        [bgView setTransform:[self transformForOrientation:[self currentOrientation]]];
        
        UIView *sendView = [[UIView alloc] init];
        sendView.frame = CGRectMake(200, 100, SCREEN_HEIGHT-400, SCREEN_WIDTH-200);
        sendView.layer.cornerRadius = 10;
        sendView.layer.borderWidth = 8;
        sendView.layer.borderColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.8] CGColor];
        sendView.backgroundColor = [UIColor whiteColor];
        [bgView addSubview:sendView];
        
        
        UIImageView *shareImgView = [[UIImageView alloc] initWithImage:image];
        shareImgView.frame = CGRectMake((sendView.frame.size.width-image.size.width/2)/2,
                                        15,
                                        image.size.width/2,
                                        image.size.height/2);
        [sendView addSubview:shareImgView];
        
        UITextView *shareText = [[UITextView alloc] init];
        shareText.frame = CGRectMake(20,
                                     shareImgView.frame.origin.y+shareImgView.frame.size.height+20,
                                     sendView.frame.size.width-40,
                                     sendView.frame.size.height-shareImgView.frame.size.height-100);
        shareText.editable = NO;
        shareText.font = [UIFont systemFontOfSize:17.0f];
        shareText.text = text;
        [sendView addSubview:shareText];
        
        UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [sendBtn setTitle:@"分享" forState:UIControlStateNormal];
        sendBtn.frame = CGRectMake((sendView.frame.size.width-150)/2,
                                   sendView.frame.size.height-50,
                                   60,
                                   30);
        [sendBtn addTarget:self action:@selector(shareToSinaAction) forControlEvents:UIControlEventTouchUpInside];
        [sendView addSubview:sendBtn];
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        cancelBtn.frame = CGRectMake(sendBtn.frame.origin.x+sendBtn.frame.size.width+30,
                                     sendView.frame.size.height-50,
                                     60,
                                     30);
        [cancelBtn addTarget:self action:@selector(hideShare) forControlEvents:UIControlEventTouchUpInside];
        [sendView addSubview:cancelBtn];
        /************************************************/
//        [_shareEngine sendWeiBoWithText:text image:image];
    }else{
        [_shareEngine logIn];
    }
}

- (void)hideShare
{
    [(UIView *)[[[UIApplication sharedApplication].windows objectAtIndex:0] viewWithTag:10] removeFromSuperview];
}

- (void)shareToSinaAction
{
    [self showLoadingText:@"加载中..." onView:[[UIApplication sharedApplication].windows objectAtIndex:0]];
    if (text.length>140) {
        text = [text substringToIndex:139];
    }
    [_shareEngine sendWeiBoWithText:text image:image];
}

- (UIInterfaceOrientation)currentOrientation
{
    return [UIApplication sharedApplication].statusBarOrientation;
}

- (CGAffineTransform)transformForOrientation:(UIInterfaceOrientation)orientation
{
	if (orientation == UIInterfaceOrientationLandscapeLeft)
    {
		return CGAffineTransformMakeRotation(-M_PI / 2);
	}
    else if (orientation == UIInterfaceOrientationLandscapeRight)
    {
		return CGAffineTransformMakeRotation(M_PI / 2);
	}
    else if (orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
		return CGAffineTransformMakeRotation(-M_PI);
	}
    else
    {
		return CGAffineTransformIdentity;
	}
}

- (void)shareToWx:(int)_scene{
    WXMediaMessage *message = [WXMediaMessage message];
    message.description = text;
    
    CGSize size = {120, 120};
    UIGraphicsBeginImageContext(size);
    CGRect rect = {{0,0}, size};
    [image drawInRect:rect];
    UIImage *compressedImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    [message setThumbImage:compressedImg];
    
    WXWebpageObject *ext = [WXWebpageObject object];
    
    ext.webpageUrl = @"https://www.zfchina.com//";
    
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = _scene;
    
    [WXApi sendReq:req];
}

/**
 * 初始化新浪微博引擎
 */
- (void)initSinaWeiboEngine
{
    _shareEngine = [[WBEngine alloc] initWithAppKey:kWBSDKDemoAppKey appSecret:kWBSDKDemoAppSecret];
    UINavigationController *nav = [[UINavigationController alloc]init];
    [_shareEngine setRootViewController: nav];
    [_shareEngine setDelegate:self];
    [_shareEngine setRedirectURI:@"https://www.zfchina.com//"];
    [_shareEngine setIsUserExclusive:NO];
}

#pragma mark - WBEngineDelegate Methods
#pragma mark Authorize
- (void)engineAlreadyLoggedIn:(WBEngine *)engine
{
    if ([engine isUserExclusive])
    {
        UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
                                                           message:@"请先登出！"
                                                          delegate:nil
                                                 cancelButtonTitle:@"确定"
                                                 otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)engineDidLogIn:(WBEngine *)engine
{
    [self showLoadingText];
    if (text.length>140) {
        text = [text substringToIndex:139];
    }
    [_shareEngine sendWeiBoWithText:[NSString stringWithFormat:@"%@%@", text, bannerName] image:image];
}

- (void)engine:(WBEngine *)engine didFailToLogInWithError:(NSError *)error
{
    NSLog(@"didFailToLogInWithError: %@", error);
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"登录失败！"
													  delegate:nil
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
	[alertView show];
}

- (void)engineAuthorizeExpired:(WBEngine *)engine
{
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"请重新登录！"
													  delegate:nil
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
	[alertView show];
}

- (void)engine:(WBEngine *)engine requestDidSucceedWithResult:(id)result
{
    [[[UIApplication sharedApplication].windows objectAtIndex:0] hideLoadingText];
    [self hideShare];
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"分享成功！"
													  delegate:nil
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
	[alertView show];
}

- (void)engine:(WBEngine *)engine requestDidFailWithError:(NSError *)error
{
    [[[UIApplication sharedApplication].windows objectAtIndex:0] hideLoadingText];
    UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:nil
													   message:@"分享失败！"
													  delegate:nil
											 cancelButtonTitle:@"确定"
											 otherButtonTitles:nil];
	[alertView show];
}


@end





