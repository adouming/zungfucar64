//
//  ViewController.m
//  HMScrollViewDemo
//
//  Created by Wang Eric on 12-5-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "HorizontalScrollView.h"
#import "NSStringUtil.h"
#import "UIImageView+AFNetworking.h"
#import "MyUILabel.h"
#import "NewsMainVC.h"

@interface HorizontalScrollView ()

@end

@implementation HorizontalScrollView

@synthesize imageViews;
@synthesize scrollView;
@synthesize pageControl;
@synthesize spotNameLabel;
@synthesize scoreLabel;
@synthesize currentPage;
@synthesize pageControlUsed;
@synthesize titles;
@synthesize subtitles;
@synthesize delegate;

#define TYPE_COMPONENT  0
#define TYPE_BENCHI     1
#define TYPE_NEWS       2
#define TYPE_SECOND     4
#define TYPE_DISCOUNT   5
#define TYPE_PEIJIAN    6
#define TYPE_SERVICENETWORK    7


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(void)clearAllView{
    if (scrollView) {
        [scrollView removeFromSuperview];
    }
}

- (void)initScrollView:(NSArray *)dataSource type:(int)type{
    imageUrls = [[NSMutableArray alloc]init];
    self.titles = [[NSMutableArray alloc]init];
    subTitles = [[NSMutableArray alloc]init];
    for (int i=0; i<[dataSource count]; i++) {
        NSDictionary *dic = [dataSource objectAtIndex:i];
        NSString *title = @"";
//        if (type==1) {
//            title = [NSString stringWithFormat:@"%@-Class", [dic valueForKey:@"title"]];
//        } else {
            title = [dic valueForKey:@"title"];
//        }
        [self.titles addObject:title];
        
        NSString *imageUrl;
        if( type == TYPE_COMPONENT) {
            imageUrl = [dic valueForKey:@"timg"];
        }
        else if(type == TYPE_DISCOUNT) {
            imageUrl = [dic valueForKey:@"img"];
        }
        else if (type==TYPE_NEWS) {
            imageUrl = [dic valueForKey:@"timg"];
        }
        else if (type==TYPE_BENCHI) {
            imageUrl = [dic valueForKey:@"img"];
        }
        else if( type == TYPE_SERVICENETWORK) {
            imageUrl = [dic valueForKey:@"dimg"];
        }
        else{
            imageUrl = [dic valueForKey:@"timg"];
        }
        
        if (!imageUrl) {
            imageUrl = @"";
        }
        
        NSString* fullUrl = [NSString stringWithFormat:@"%@/%@", ZF_BASE_URL, imageUrl];
        [imageUrls addObject:fullUrl];
        
        if (type == TYPE_NEWS || type == TYPE_DISCOUNT) {
            NSString *time = [NewsMainVC getTimeToShowWithTimestamp: [dic objectForKey:@"updatetime"]];
            [subTitles addObject:time];
        }else if (type == TYPE_BENCHI){
            NSString *descp = [dic objectForKey:@"info"];
            [subTitles addObject:descp];
        }
        
    }
    mTtype = type;
    int kNumberOfPages = [dataSource count];
    
    // in the meantime, load the array with placeholders which will be replaced on demand
    NSMutableArray *views = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++) {
        [views addObject:[NSNull null]];
    }
    self.imageViews = views;
    
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    [self addSubview:scrollView];
    
    goImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-16, scrollView.frame.size.height-85, 10, 16)];
    goImageView.image = [UIImage imageNamed:@"slivermenugo"];
    
    backImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, scrollView.frame.size.height-85, 10, 16)];
    backImageView.image = [UIImage imageNamed:@"slivermenuback"];
    
    [self addSubview:backImageView];
    [self addSubview:goImageView];
    
    pageControl = [[UIPageControl alloc] init];
    
    // a page is the width of the scroll view
    scrollView.pagingEnabled = YES;
    scrollView.clipsToBounds = NO;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height);
    scrollView.showsHorizontalScrollIndicator = YES;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = NO;
    scrollView.delegate = self;
    scrollView.userInteractionEnabled = YES;
    currentPage = 0;
    
    pageControl.numberOfPages = kNumberOfPages;
    pageControl.currentPage = 0;
    pageControl.backgroundColor = [UIColor whiteColor];
    
    [self createAllEmptyPagesForScrollView: kNumberOfPages type:type];
    
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    currentPage = 0;
    [scrollView setContentOffset:CGPointMake(0, 0)];
}

- (void)createAllEmptyPagesForScrollView:(int)pages type:(int)type{
    if (pages < 0) {
        return;
    }
    
    for (int page = 0; page < pages; page++) {
        CGRect frame = scrollView.frame;
        
        UIButton *backgroundButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width * page, 0, frame.size.width, frame.size.height-70)];
        backgroundButton.backgroundColor = [UIColor clearColor];
        backgroundButton.tag = page+100;
        [backgroundButton addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:backgroundButton];
        
        UIImageView *bgView = [[UIImageView alloc]initWithFrame:CGRectMake(frame.size.width * page , frame.size.height-160, frame.size.width, 160)];
        bgView.image = [UIImage imageNamed:@"latestnews_textbg.png"];
        bgView.tag = page+500;
        bgView.hidden = NO;
        [scrollView addSubview:bgView];
        
        MyUILabel *titleLabel = [[MyUILabel alloc]init];
        if (type == TYPE_DISCOUNT || type==TYPE_NEWS || type == TYPE_COMPONENT || type == TYPE_SECOND) {
            titleLabel.frame = CGRectMake(frame.size.width * page , frame.size.height-85, frame.size.width-65, 45);
            titleLabel.numberOfLines = 2;
        }else if(type==TYPE_BENCHI){
            titleLabel.frame = CGRectMake(frame.size.width * page , frame.size.height-85, frame.size.width-65, 20);
        }
        else {
            titleLabel.frame = CGRectMake(frame.size.width * page , frame.size.height-85, frame.size.width-65, 20);
        }
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentRight;
        titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
        titleLabel.text = [[self.titles objectAtIndex:page] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        titleLabel.tag = 202;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping|NSLineBreakByTruncatingTail;
        titleLabel.verticalAlignment = VerticalAlignmentTop;
        [scrollView addSubview:titleLabel];
        
        MyUILabel *detailLabel = [[MyUILabel alloc]init];
        NSString *detailString;
        if (type == TYPE_DISCOUNT || type==TYPE_NEWS) {
            detailLabel.frame = CGRectMake(frame.size.width * page , frame.size.height-40, frame.size.width-15, 13);
            detailLabel.numberOfLines = 1;
            detailString = [subTitles objectAtIndex:page];
            if (detailString.length>11) {
                detailString = [detailString substringToIndex:10];
            }
        }else if(type==TYPE_BENCHI){
            detailLabel.frame = CGRectMake(frame.size.width * page , frame.size.height-40, frame.size.width-15, 40);
            detailLabel.numberOfLines = 2;
            detailString = [subTitles objectAtIndex:page];
            detailLabel.hidden = YES;
        }
        detailLabel.lineBreakMode = NSLineBreakByWordWrapping|NSLineBreakByTruncatingTail;
        detailLabel.text = [detailString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        detailLabel.verticalAlignment = VerticalAlignmentTop;
        detailLabel.backgroundColor = [UIColor clearColor];
        detailLabel.textColor = HEXRGB(0x444444);
        detailLabel.textAlignment = NSTextAlignmentRight;
        detailLabel.font = [UIFont systemFontOfSize:13.0f];
        [scrollView addSubview:detailLabel];
    }
}

- (void)loadScrollViewWithPage:(int)page {
	int kNumberOfPages = [imageUrls count];
    if(page <=1) {
        backImageView.hidden = true;
    }
    else {
        backImageView.hidden = false;
    }
    if (page >= kNumberOfPages) {
        goImageView.hidden = true;
    }
    else {
        goImageView.hidden = false;
    }
    
    if (page < 0) {
        
        return;
    }
    if (page >= kNumberOfPages) {
        return;
    }
    
//    if (currentPage==page) {
//        ((UIImageView *)[scrollView viewWithTag:page+500]).hidden = NO;
//    }else{
//        ((UIImageView *)[scrollView viewWithTag:page+500]).hidden = YES;
//    }
    
    UIButton *button = (UIButton *)[scrollView viewWithTag:page+100];
    UIImageView *buttonBgView = [[UIImageView alloc]initWithFrame:button.bounds];
    buttonBgView.contentMode = UIViewContentModeScaleAspectFit;
    buttonBgView.clipsToBounds = YES;
    if (TYPE_SECOND==mTtype) {
        [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"xr_%d.jpg", page+1]] forState:UIControlStateNormal];
    }else{
        [buttonBgView setImageWithURL:[NSURL URLWithString:[imageUrls objectAtIndex:page]] placeholderImage:[UIImage imageNamed:@"loading.png"]];
    }
    [button addSubview:buttonBgView];
    [button sendSubviewToBack:buttonBgView];
    

}


- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page < 0 || page >= 5) {
        return;
    }
    pageControl.currentPage = page;
    if (pageControlUsed) {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
//    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    currentPage = page;
    pageControlUsed = NO;
}

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view 
{ 
    return YES; 
} 
- (BOOL)touchesShouldCancelInContentView:(UIView *)view 
{ 
    
    return NO; 
} 

//for TapDetectingImageViewDelegate
- (void)doTabImageViewAction {
//    DetailedView *detailView = [[DetailedView alloc]initWithNibName:@"DetailedView" bundle:nil];
//    detailView.hidesBottomBarWhenPushed = YES;
//    detailView.page = self.currentPage + 1;
//    [[self navigationController] pushViewController:detailView animated:YES];
    //[self.parentView pushViewAction:objectDetailView];
}

-(void)clickButton:(UIButton *)sender{
    int tag = sender.tag-100;
    if (delegate) {
        [delegate pushToDetail:tag];
    }
}

@end
